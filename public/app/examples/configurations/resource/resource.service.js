(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .service('ResourceService', ResourceService);

    //  PrincipalService.$inject = ['$http', '$q'];

    /* @ngInject */
    function ResourceService($http,API_CONFIG) {
        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;

        this.getResource = getResource;

        function getResource() {
            return $http.get(vm.baseUrl + 'resource');
        }

        this.postResources= postResources;

        function postResources(principals) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'resource',
                data: $.param({detail: principals})
            });

        }
        this.editResources = editResources;
        function editResources(id, editDetail) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'resource/' + id,
                data: $.param({detail: editDetail})
            });
        }

        this.deActivate = deActivate;
        function deActivate(id) {
            return $http.get(vm.baseUrl + 'resource/' + id + '/deactivate');
        }

        this.activate = activate;
        function activate(id) {
            return $http.get(vm.baseUrl + 'resource/' + id + '/activate');

        }

    }
})();
