(function() {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('ResourceFabController', ResourceFabController);

    /* @ngInject */
    function ResourceFabController($rootScope) {
        var vm = this;
        vm.addTodo = addTodo;

        ////////////////

        function addTodo($event) {
            $rootScope.$broadcast('addTodo', $event);
        }
    }
})();
