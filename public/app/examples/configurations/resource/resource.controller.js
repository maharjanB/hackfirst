(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('ResourceController', ResourceController);

    /* @ngInject */
    function ResourceController($scope, $state, $mdDialog, ResourceService, $mdToast, $filter) {
        var vm = this;
        vm.resources = [];
        vm.resources = getResource();
        vm.postResources = postResources;
        vm.editResources = editResources;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1
        };
        vm.selected = [];
        vm.filter = {
            options: {
                debounce: 500
            }
        };
       // // vm.resources = getresources();
        function getResource() {
            ResourceService.getResource().then(function (resource) {
                vm.resources = resource.data.data;
            });
        }


        function postResources(principal) {
            ResourceService.postResources(principal).then(function (resources) {
                if (resources.status == 200 && resources.data) {
                    if (resources.data.error) {
                        var last = Object.keys(resources.data.error.message).length;
                        var key = Object.keys(resources.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Principal Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.resources.push(resources.data.data);
                    }
                } else {

                }
            });
        }


        function editResources(id, principal) {
            ResourceService.editResources(id, principal).then(function (resources) {
                if (resources.status == 200 && resources.data) {
                    if (resources.data.error) {
                        var last = Object.keys(resources.data.error.message).length;
                        var key = Object.keys(resources.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Resource Updated'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }

        vm.updatePrincipal = updatePrincipal;

        function updatePrincipal(index, event, $event) {
            $mdDialog.show({
                templateUrl: 'app/examples/configurations/resource/add-todo-dialog.tmpl.html',
                targetEvent: $event,
                controller: 'ResourceDialogController',
                controllerAs: 'vm',
                locals: {
                    dialogData: {
                        title: 'Edit Resource',
                        confirmButtonText: 'OK'
                    },
                    event: vm.resources[index],
                }
            })
                .then(function (answer) {
                    vm.resources[index].title = answer.title;
                    var id_principal = vm.resources[index].id_principal;
                    vm.editPrincipal(id_principal, answer);
                });
        }

        function addResource(event, $event) {
            $mdDialog.show({
                templateUrl: 'app/examples/configurations/resource/add-todo-dialog.tmpl.html',
                targetEvent: $event,
                controller: 'ResourceDialogController',
                controllerAs: 'vm',
                locals: {
                    dialogData: {
                        title: 'Create New Resource',
                        confirmButtonText: 'OK'
                    },
                    event: {
                        title: '',
                        status: false
                    },
                }
            })
                .then(function (answer) {
                    vm.postResources(answer);
                });
        }


        $scope.$on('addTodo', addResource);

    }
})();
