(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider, $httpProvider) {

        $stateProvider
            .state('triangular.admin-default.configuration-story', {
                url: '/configuration/story',
                views: {
                    '': {
                        templateUrl: 'app/examples/configurations/user/story.tmpl.html',
                        // controller: 'UserController',
                        // controllerAs: 'vm'
                    },

                }
            })

            .state('triangular.admin-default.configuration-user', {
                url: '/configuration/user',
                views: {
                    '': {
                        templateUrl: 'app/examples/configurations/user/user.tmpl.html',
                        controller: 'UserController',
                        controllerAs: 'vm'
                    },
                    'belowContent': {
                        templateUrl: 'app/examples/configurations/user/fab-button.tmpl.html',
                        controller: 'UserFabController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('triangular.admin-default.user-detail', {
                url: '/configuration/user',
                templateUrl: 'app/examples/configurations/user/detail.tmpl.html'
            })
            .state('triangular.admin-default.configuration-resource', {
                url: '/configuration/resource',
                views: {
                    '': {
                        templateUrl: 'app/examples/configurations/resource/resource.tmpl.html',
                        controller: 'ResourceController',
                        controllerAs: 'vm'
                    },
                    'belowContent': {
                        templateUrl: 'app/examples/configurations/resource/fab-button.tmpl.html',
                        controller: 'ResourceFabController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('triangular.admin-default.configuration-principal', {
                url: '/configuration/principal',
                views: {
                    '': {
                        templateUrl: 'app/examples/configurations/principal/principal.tmpl.html',
                        controller: 'PrincipalController',
                        controllerAs: 'vm'
                    },
                    'belowContent': {
                        templateUrl: 'app/examples/configurations/principal/fab-button.tmpl.html',
                        controller: 'PrincipalFabController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('triangular.admin-default.configuration-project', {
                url: '/configuration/project',
                views: {
                    '': {
                        templateUrl: 'app/examples/configurations/project/project.tmpl.html',
                        controller: 'ProjectController',
                        controllerAs: 'vm'
                    },
                    'belowContent': {
                        templateUrl: 'app/examples/configurations/project/fab-button.tmpl.html',
                        controller: 'ProjectFabController',
                        controllerAs: 'vm'
                    }
                }
            })
        ;
        triMenuProvider.addMenu({
            name: 'Configure',
            icon: 'zmdi zmdi-settings',
            type: 'dropdown',
            priority: 2.7,
            children: [{
                name: 'Universe',
                icon: 'zmdi zmdi-account',
                type: 'dropdown',
                children: [

                    {
                        name: 'Principal',
                        icon: 'zmdi zmdi-account',
                        type: 'link',
                        state: 'triangular.admin-default.configuration-principal'
                    },
                    {
                        name: 'Project',
                        icon: 'zmdi zmdi-account',
                        type: 'link',
                        state: 'triangular.admin-default.configuration-project'
                    },
                    {
                        name: 'User',
                        icon: 'zmdi zmdi-account',
                        type: 'link',
                        state: 'triangular.admin-default.configuration-user'
                    },
                    {
                        name: 'Resource',
                        icon: 'zmdi zmdi-account',
                        type: 'link',
                        state: 'triangular.admin-default.configuration-resource'
                    },

                    {
                        name: 'Success Stories',
                        icon: 'zmdi zmdi-account',
                        type: 'link',
                        state: 'triangular.admin-default.configuration-story'
                    },

                ]
            },
            ]
        });

        $httpProvider.interceptors.push('APIInterceptor');

    }
})();
