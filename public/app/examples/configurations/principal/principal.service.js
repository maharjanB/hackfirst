(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .service('PrincipalService', PrincipalService);

  //  PrincipalService.$inject = ['$http', '$q'];

    /* @ngInject */
    function PrincipalService($http,API_CONFIG) {
        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;

        this.getPrincipals = getPrincipals;

        function getPrincipals() {
            return $http.get(vm.baseUrl + 'principal');
        }

        this.postPrincipals = postPrincipals;

        function postPrincipals(principals) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'principal',
                data: $.param({detail: principals})
            });

        }
        this.editPrincipal = editPrincipal;
        function editPrincipal(id, editDetail) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'principal/' + id,
                data: $.param({detail: editDetail})
            });
        }

        this.deActivate = deActivate;
        function deActivate(id) {
            return $http.get(vm.baseUrl + 'principal/' + id + '/deactivate');
        }

        this.activate = activate;
        function activate(id) {
            return $http.get(vm.baseUrl + 'principal/' + id + '/activate');

        }

    }
})();
