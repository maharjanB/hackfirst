(function() {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('PrincipalFabController', PrincipalFabController);

    /* @ngInject */
    function PrincipalFabController($rootScope) {
        var vm = this;
        vm.addTodo = addTodo;

        ////////////////

        function addTodo($event) {
            $rootScope.$broadcast('addTodo', $event);
        }
    }
})();
