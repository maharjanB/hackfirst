(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('PrincipalController', PrincipalController);

    /* @ngInject */
    function PrincipalController($scope, $state, $mdDialog, PrincipalService, $mdToast, $filter) {
        var vm = this;
        vm.principals = [];
        vm.principals = getPrincipals();
        vm.postPrincipals = postPrincipals;
        vm.editPrincipal = editPrincipal;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1
        };
        vm.selected = [];
        vm.filter = {
            options: {
                debounce: 500
            }
        };
        function getPrincipals() {
            PrincipalService.getPrincipals().then(function (principal) {
                vm.principals = principal.data.data;
            });
        }


        function postPrincipals(principal) {
            PrincipalService.postPrincipals(principal).then(function (principals) {
                if (principals.status == 200 && principals.data) {
                    if (principals.data.error) {
                        var last = Object.keys(principals.data.error.message).length;
                        var key = Object.keys(principals.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Principal Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.principals.push(principals.data.data);
                    }
                } else {

                }
            });
        }


        function editPrincipal(id, principal) {
            PrincipalService.editPrincipal(id, principal).then(function (principals) {
                if (principals.status == 200 && principals.data) {
                    if (principals.data.error) {
                        var last = Object.keys(principals.data.error.message).length;
                        var key = Object.keys(principals.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Principal Updated'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }

        vm.updatePrincipal = updatePrincipal;

        function updatePrincipal(index, event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/configurations/principal/add-todo-dialog.tmpl.html',
                    targetEvent: $event,
                    controller: 'PrincipalDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Edit Principal',
                            confirmButtonText: 'OK'
                        },
                        event: vm.principals[index],
                    }
                })
                .then(function (answer) {
                    vm.principals[index].title = answer.title;
                    var id_principal = vm.principals[index].id_principal;
                    vm.editPrincipal(id_principal, answer);
                });
        }

        function addPrincipal(event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/configurations/principal/add-todo-dialog.tmpl.html',
                    targetEvent: $event,
                    controller: 'PrincipalDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Create New Principal',
                            confirmButtonText: 'OK'
                        },
                        event: {
                            title: '',
                            status: false
                        },
                    }
                })
                .then(function (answer) {
                    vm.postPrincipals(answer);
                });
        }

        vm.activateDeactivate = activateDeactivate;
        function activateDeactivate($event,principal) {
            var id = principal.id_principal;
            principal.status = !principal.status;

           var status = principal.status;
            var confirm = $mdDialog.confirm($event)
                .title('Are you sure you want to change the status?')
                .ariaLabel('Change Option')
                .targetEvent($event)
                .ok('Yes')
                .cancel('No');
            $mdDialog.show(confirm).then(function(){
                ////compliment value on confirmation
                if (status)
                PrincipalService.deActivate(id);
                else
                PrincipalService.activate(id);
                principal.status = !principal.status;
            }, function(){

                return false;
            });


        }

        $scope.$on('addTodo', addPrincipal);

    }
})();
