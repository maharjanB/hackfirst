(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('DialogController', DialogController);


    /* @ngInject */
    function DialogController($state, $mdDialog, event, dialogData) {
        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.dialogData = dialogData;
        vm.item = {
            title: '',
        };

        if (event) {
            vm.item.title = event.title;

        }


        /////////////////////////

        function hide() {

            $mdDialog.hide(vm.item);
        }

        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
