(function () {
    'use strict';
    angular
        .module('app.examples.configurations')
        .controller('UserController', UserController);

    /* @ngInject */
    function UserController($scope, $state, $mdDialog, ResourceService, userService, $mdToast, $filter) {

        var vm = this;

        vm.businessUnits = [];

        vm.businessUnits = getBusinessUnit();

        function getBusinessUnit() {
            userService.getBusinessUnit().then(function (businessUnits) {
                vm.businessUnits = businessUnits.data.data;
            });
        }

        vm.postBusinessUnit = postBusinessUnit;

        vm.detailView = detailView;
        function detailView(registration) {

            $state.go('triangular.admin-default.user-detail', {
                userID: registration.id_user
            });
        }

        function postBusinessUnit(businessUnit) {
            userService.postBusinessUnit(businessUnit).then(function (businessUnits) {
                if (businessUnits.status == 200 && businessUnits.data) {
                    if (businessUnits.data.error) {
                        var last = Object.keys(businessUnits.data.error.message).length;
                        var key = Object.keys(businessUnits.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('User Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.businessUnits.push(businessUnits.data.data);
                    }
                } else {

                }
            });
        }

        vm.resource = [];

        vm.resource = getResource();
        function getResource() {
            ResourceService.getResource().then(function (resource) {
                vm.resources = resource.data.data;
            });
        }

        vm.filterUser = function () {
            var skill_desc = $filter('filter')(vm.businessUnits, {id_resource: vm.item.resource_id}, true)[0];
            vm.businessUnits = skill_desc;
        }

        function addBU(event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/configurations/user/create-form.tmpl.html',
                    targetEvent: $event,
                    controller: 'DialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Create New User',
                            confirmButtonText: 'OK'
                        },
                        event: {
                            title: '',
                            status: false
                        },
                        edit: false

                    }
                })
                .then(function (answer) {
                    vm.postBusinessUnit(answer);
                });
        }



        vm.activateDeactivate = activateDeactivate;

        function activateDeactivate($event,businessUnit) {

            var id = businessUnit.id_business_unit;

            /**
             * Set the trigger to the original position
             * counter action to the switch of state on click
             * of NGSWITCH.
             *
             * @type {boolean}
             */
            businessUnit.status= !businessUnit.status;
            var status = businessUnit.status;
            var confirm = $mdDialog.confirm($event)
                .title('Are you sure you want to change the status?')
                .ariaLabel('Change Option')
                .targetEvent($event)
                .ok('Yes')
                .cancel('No');
            $mdDialog.show(confirm).then(function(){
                ////compliment value on confirmation
                if (status == true)
                    userService.deActivate(id);
                else
                    userService.activate(id);
                businessUnit.status = !businessUnit.status;
            }, function(){

                return false;
            });

        }

        //watch
        $scope.$on('addTodo', addBU);
    }
})();





