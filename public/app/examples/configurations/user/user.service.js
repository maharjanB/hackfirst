(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .service('userService', userService);

    //Service.$inject = ['$http', '$q'];

    /* @ngInject */
    function userService($http,API_CONFIG) {

        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;

       // vm.baseUrl = "http://rosiav2.local/api/v2/";
        /*  this.pushBusinessUnits = function (data) {
         this.businessUnits.push(data);
         };*/

        this.getBusinessUnit = getBusinessUnit;
        this.postBusinessUnit = postBusinessUnit;
        function getBusinessUnit() {
            return $http.get(vm.baseUrl+'user').
            success(function(data) {
                return data;
            });
            //return $http.get('http://rosiav2.local/api/v2/business-unit');
        }


        function postBusinessUnit(postDetail) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl+'user',
                data: $.param({detail: postDetail})
            });
        }




        this.deActivate = deActivate;
        function deActivate(id) {
            return $http.get(vm.baseUrl+'user/'+ id + '/deactivate');
        }

        this.activate = activate;
        function activate(id) {
            return $http.get(vm.baseUrl+'user/' + id + '/activate');

        }
    }
})();
