(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('UserFabController', UserFabController);

    /* @ngInject */
    function UserFabController($rootScope) {
        var vm = this;
        vm.todos = [{title: '', status: true},
        ];

        vm.addTodo = addTodo;
        function addTodo($event) {
         $rootScope.$broadcast('addTodo', $event);
         }


    }

})();
