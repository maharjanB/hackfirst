(function() {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('ProjectFabController', ProjectFabController);

    /* @ngInject */
    function ProjectFabController($rootScope) {
        var vm = this;
        vm.addTodo = addTodo;

        ////////////////

        function addTodo($event) {
            $rootScope.$broadcast('addTodo', $event);
        }
    }
})();
