(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('ProjectController', ProjectController);

    /* @ngInject */
    function ProjectController($scope, $state, $mdDialog, ProjectService, $mdToast, $filter,$timeout,ResourceService) {
        var vm = this;
        vm.project = [];
        vm.project = getProject();
        vm.postProject = postProject;
        vm.editProject = editProject;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1
        };
        vm.selected = [];
        vm.filter = {
            options: {
                debounce: 500
            }
        };

        $scope.user = null;
        $scope.users = null;

        vm.resource = [];

        vm.resource = getResource();
        function getResource() {
            ResourceService.getResource().then(function (resource) {
                vm.resources = resource.data.data;
            });
        }



        vm.project = getProject();
        function getProject() {
            ProjectService.getProject().then(function (project) {
                vm.project = project.data.data;
            });
        }


        function postProject(project) {
            console.log(project);
            ProjectService.postProject(project).then(function (project) {
                if (project.status == 200 && project.data) {
                    if (project.data.error) {
                        var last = Object.keys(project.data.error.message).length;
                        var key = Object.keys(project.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Project Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.project.push(principals.data.data);
                    }
                } else {

                }
            });
        }


        function editProject(id, project) {
            PrincipalService.editPrincipal(id, project).then(function (project) {
                if (project.status == 200 && project.data) {
                    if (project.data.error) {
                        var last = Object.keys(project.data.error.message).length;
                        var key = Object.keys(project.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('project Updated'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }

        // vm.updateproject = updateproject;
        //
        // function updateProject(index, event, $event) {
        //     $mdDialog.show({
        //             templateUrl: 'app/examples/configurations/project/add-todo-dialog.tmpl.html',
        //             targetEvent: $event,
        //             controller: 'ProjectController',
        //             controllerAs: 'vm',
        //             locals: {
        //                 dialogData: {
        //                     title: 'Edit Project',
        //                     confirmButtonText: 'OK'
        //                 },
        //                 event: vm.project[index],
        //             }
        //         })
        //         .then(function (answer) {
        //             vm.project[index].title = answer.title;
        //             var id_project = vm.project[index].id_project;
        //             vm.editPrincipal(id_project, answer);
        //         });
        // }

        function addProject(event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/configurations/project/add-todo-dialog.tmpl.html',
                    targetEvent: $event,
                    controller: 'ProjectDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Create New Project',
                            confirmButtonText: 'OK'
                        },
                        event: {
                            title: '',
                            user_id : '',
                            description: '',
                            resource_id: '',
                            product_id: '',
                            status: false
                        },
                    }
                })
                .then(function (answer) {
                    vm.postProject(answer);
                });
        }

        vm.activateDeactivate = activateDeactivate;
        function activateDeactivate($event,project) {
            var id = principal.id_project;
            project.status = !project.status;

           var status = project.status;
            var confirm = $mdDialog.confirm($event)
                .title('Are you sure you want to change the status?')
                .ariaLabel('Change Option')
                .targetEvent($event)
                .ok('Yes')
                .cancel('No');
            $mdDialog.show(confirm).then(function(){
                ////compliment value on confirmation
                if (status)
                ProjectService.deActivate(id);
                else
                ProjectService.activate(id);
                project.status = !project.status;
            }, function(){

                return false;
            });


        }

        $scope.$on('addTodo', addProject);

    }
})();
