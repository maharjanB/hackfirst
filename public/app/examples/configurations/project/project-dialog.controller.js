(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('ProjectDialogController', ProjectDialogController);

    /* @ngInject */
    function ProjectDialogController($state, $mdDialog, event,  dialogData,ResourceService) {

        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.dialogData = dialogData;
        vm.item = {
            title: '',
            description: '',
            resource_id: '',
            product_id: '',

        };

        vm.resource = [];

        vm.resource = getResource();
        function getResource() {
            ResourceService.getResource().then(function (resource) {
                vm.resources = resource.data.data;
            });
        }
        console.log(event);
        if (event) {
            vm.item.title = event.title;
            vm.item.description = event.description;
            vm.item.resource_id = event.id_resource;
            vm.item.product_id = event.product_id;
        }
        /////////////////////////

        function hide() {
            $mdDialog.hide(vm.item);
        }

        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
