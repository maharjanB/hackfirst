(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .service('ProjectService', ProjectService);

  //  PrincipalService.$inject = ['$http', '$q'];

    /* @ngInject */
    function ProjectService($http,API_CONFIG) {
        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;

        this.getProject = getProject;

        function getProject() {
            return $http.get(vm.baseUrl + 'project');
        }

        this.postProject = postProject;

        function postProject(project) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'project',
                data: $.param({detail: project})
            });

        }
        this.editProject = editProject;
        function editProject(id, editDetail) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'project/' + id,
                data: $.param({detail: editDetail})
            });
        }

        this.deActivate = deActivate;
        function deActivate(id) {
            return $http.get(vm.baseUrl + 'project/' + id + '/deactivate');
        }

        this.activate = activate;
        function activate(id) {
            return $http.get(vm.baseUrl + 'project/' + id + '/activate');

        }

    }
})();
