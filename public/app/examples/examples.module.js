(function () {
    'use strict';
    angular
        .module('app.examples',[
            'app.examples.authentication',
            'app.examples.dashboards',
            'app.examples.configurations',
            'app.examples.auth'
        ]);

})();
