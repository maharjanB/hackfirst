(function() {
    'use strict';

    angular
        .module('app.examples.auth')
        .service('AuthService',AuthService);

    /* @ngInject */

    function AuthService($http,$q,API_CONFIG, $window , AuthDataShareService) {

        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;

        var deferred = $q.defer();

        var userInfo;

        function login(userName, password) {

            $http({
                method:"POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl+'login',
                data: $.param({
                    grant_type: 'password',
                    username: userName,
                    password: password,
                    client_id: 'client',
                    client_secret: 'secret'})
            }).then(function (result) {

               // console.log(result.data.status);

                if(result.data.status === 'success') {

                    userInfo = {
                        accessToken: result.data.data.access_token,
                        user: result.data.data.userInfo,
                    };
                }else{
                    alert(result.data.data.error_description);
                }
                AuthDataShareService.setUserInfo(userInfo);
                deferred.resolve(userInfo);

            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;

        }

        return {
            login: login
        };



    }


})();