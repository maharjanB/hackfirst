(function() {
    'use strict';

    angular
        .module('app.examples.authentication')
        .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($state, triSettings, AuthService,$rootScope) {
        var vm = this;
        vm.loginClick = loginClick;
        vm.triSettings = triSettings;
        // create blank user variable for login form
        vm.user = {
            email: '',
            password: ''
        };

        vm.userInfo;

        ////////////////

        function loginClick() {

            AuthService.login(vm.user.email, vm.user.password).then(function(response){

                console.log(response);

                if($rootScope.returnToState)
                {
                   $state.go($rootScope.returnToState);
                }
                $state.go('triangular.admin-default.dashboard-sales');
/*

                /!*
                                $state.go('triangular.admin-default.dashboard-sales');
                *!/
                console.log(response.accessToken);
                console.log($rootScope.returnToState);*/

            });
/*
                ,function(data){
                console.log( data)
            });*/
           // console.log(vm.userInfo.$state)
           // $state.go('404');
        }
    }
})();