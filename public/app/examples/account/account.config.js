(function () {
    'use strict';

    angular
        .module('app.examples.account')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.admin-default.account-list', {
                url: '/accounts',
                templateUrl: 'app/examples/account/composer.tmpl.html',
                controller: 'AccountController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.account-receipt', {
                url : '/accounts/receipts',
                templateUrl: 'app/examples/account/receipts/receipt.tml.html',
                controller: 'AccountController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.account-ledgers', {
                url : '/accounts/ledgers',
                templateUrl: 'app/examples/account/ledgers/ledgers.tml.html',
                controller: 'AccountController',
                controllerAs: 'vm'
            });


        /*

         $stateProvider
         .state('triangular.admin-default.layouts-composer', {
         url: '/layouts/composer',
         templateUrl: 'app/examples/layouts/composer.tmpl.html',
         controller: 'LayoutsComposerController',
         controllerAs: 'vm'
         })
         .state('triangular.admin-default.layouts-example-full-width', {
         data: {
         layout: {
         sideMenuSize: 'hidden'
         }
         },
         url: '/layouts/full-width',
         templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
         })
         .state('triangular.admin-default.layouts-example-tall-toolbar', {
         data: {
         layout: {
         toolbarSize: 'md-tall',
         toolbarClass: 'full-image-background mb-bg-fb-14'
         }
         },
         url: '/layouts/tall-toolbar',
         templateUrl: 'app/examples/dashboards/server/dashboard-server.tmpl.html',
         controller: 'DashboardServerController',
         controllerAs: 'vm'
         })
         .state('triangular.admin-default.layouts-example-icon-menu', {
         data: {
         layout: {
         sideMenuSize: 'icon'
         }
         },
         url: '/layouts/icon-menu',
         templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
         });
         */

        triMenuProvider.addMenu({
            name: 'Account Receivables',
            icon: 'zmdi zmdi-cutlery',
            type: 'dropdown',
            priority: 2.5,
            children: [{
                name: 'Receipt',
                type: 'link',
                state: 'triangular.admin-default.account-receipt'
            }, {
                name: 'Ledgers',
                type: 'link',
                state: 'triangular.admin-default.account-ledgers'
            }]
        });
    }
})();