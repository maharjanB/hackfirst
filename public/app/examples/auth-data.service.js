(function () {
    'use strict';

    angular
        .module('app.examples.auth')
        .service('AuthDataShareService', AuthDataShareService);

    /* @ngInject */
    function AuthDataShareService($window) {
        var vm = this;



        vm.userInfo;
        vm.getUserInfo = getUserInfo;
        vm.setUserInfo = setUserInfo;
        function getUserInfo() {
            return vm.userInfo;
        }

        function setUserInfo(userInfo) {
            vm.userInfo = userInfo;
            $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
        }

        vm.getSession = getSession();
        function getSession() {
           // alert("Session Check");
            if ($window.sessionStorage["userInfo"]) {
                vm.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            }
        }

    }
})();
