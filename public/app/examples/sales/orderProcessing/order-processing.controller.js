(function() {
    'use strict';

    angular
        .module('app.examples.sales')
        .controller('OrderProcessingController', OrderProcessingController);

    /* @ngInject */
    function OrderProcessingController($scope,$state, $mdToast,$mdDialog,$window, $filter,OrderProcessingService,$stateParams) {
            var vm = this;
            vm.sum = 0;
            vm.pageSetup = pageSetup();
            vm.retailOutlets = [];
            vm.received = [];
            vm.dispatched = [];
            vm.invoiced = [];
            vm.data = [];
            vm.dateRange = {
            start: moment().startOf('month'),
            end: moment().endOf('month')
        };
        vm.selected = [];
        vm.checked = false;


        function pageSetup() {
            $("#admin-panel-content-view")
                .css('background-image', "url('http://localhost/rosiav2-design-static/assets/images/calendar/2.jpg')");

        }
        vm.orderStatus = getOrderStatus();

        function getOrderStatus() {
            OrderProcessingService.getOrderStatus().then(function(statusDetail){
                vm.orderStatus = statusDetail.data.data;
                vm.received = getSalesOrderDetails(1);

              //  vm.dispatched = getSalesOrderDetails(3);

               // vm.invoiced = getSalesOrderDetails(2);

            });
        }
       //  vm.retailOutlets = getSalesOrderDetails;
        function getSalesOrderDetails(id){
            OrderProcessingService.getSalesOrderDetails(id).then(function(orderDetail){
                vm.retailOutlets = orderDetail.data.data;
                $scope.players = vm.retailOutlets;
            });
        }

        vm.status = status;

        function status(index){
            var id = vm.orderStatus[index].id_order_status;
            OrderProcessingService.generateSales(vm.dateRange,id).then(function(orderDetail){
                vm.retailOutlets = orderDetail.data.data;
                $scope.players = vm.retailOutlets;
            });
        }

        vm.modifyRateQuantity = modifyRateQuantity;
        function modifyRateQuantity(id, data) {
            OrderProcessingService.modifyRateQuantity(id, data).then(function (datas) {
                console.log(datas);
                if (datas.status == 200 && datas.data) {
                    if (datas.data.error) {
                        var last = Object.keys(datas.data.error.message).length;
                        var key = Object.keys(datas.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Sales Order Modified'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }

        vm.modifySalesOrder = modifySalesOrder;
        function modifySalesOrder(index, event, $event){
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/orderProcessing/add-todo-dialog.tmpl.html',
                    targetEvent: $event,
                    controller: 'OrderProcessingDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Modify Sales Order',
                            confirmButtonText: 'SAVE'
                        },
                        event: vm.retailOutlets[index],
                    }
                })
                .then(function (answer) {
                    vm.retailOutlets[index].quantity = answer.quantity;
                    vm.retailOutlets[index].price = answer.price;
                    vm.retailOutlets[index].reason = answer.reason;
                    var id = vm.retailOutlets[index].id_sales_order;
                    vm.modifyRateQuantity(id,answer);
                });
        }


        vm.filterSalesOrder = filterSalesOrder;
        function filterSalesOrder(index,event, $event){
            var id = vm.orderStatus[index].id_order_status;
            $mdDialog.show({
                    controller: 'DateFilterDialogController',
                    controllerAs: 'vm',
                    templateUrl: 'app/examples/sales/orderProcessing/filter.tmpl.html',
                    locals: {
                        range: vm.dateRange
                    },
                    targetEvent: $event
                })
                .then(function() {
                    OrderProcessingService.generateSales(vm.dateRange,id).then(function(orderDetail){
                        vm.retailOutlets = orderDetail.data.data;
                        $scope.players = vm.retailOutlets;
                    });
                });
        }


        vm.cancelRequest = cancelRequest;
        function cancelRequest(id, data) {
            OrderProcessingService.cancelRequest(id, data).then(function (requests) {
                if (requests.status == 200 && requests.data) {
                    if (requests.data.error) {
                        var last = Object.keys(requests.data.error.message).length;
                        var key = Object.keys(requests.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {
                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Sales Order Cancelled'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }
        vm.cancelSalesOrder = cancelSalesOrder;
        function cancelSalesOrder(index, event, $event){
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/orderProcessing/cancelReason.tmpl.html',
                    targetEvent: $event,
                    controller: 'OrderProcessingDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Cancel Sales Order',
                            confirmButtonText: 'SAVE'
                        },
                        event: vm.retailOutlets[index],
                    }
                })
                .then(function (answer) {
                    answer.order_status_id=4;
                    vm.retailOutlets[index].order_status_id = 4;
                    vm.retailOutlets[index].reason = answer.reason;
                    var id = vm.retailOutlets[index].id_sales_order;
                    vm.retailOutlets.splice(index, 1);
                    vm.cancelRequest(id,answer);
                });
        }


        $scope.nameStatus = function(order_ID) {
            if(order_ID == 1){
                var newStatus = 'MARK AS INVOICED';
            }
            if(order_ID == 2){
                var  newStatus = 'MARK AS  DISPATCHED';
            }
            if(order_ID == 3){
                var  newStatus = '';
            }
            return newStatus;
        }

        var indexedTeams = [];

        $scope.playersToFilter = function() {
            indexedTeams = [];
           return $scope.players;
        }

        $scope.filterTeams = function(player) {
            var teamIsNew = indexedTeams.indexOf(player.GID) == -1;
            if (teamIsNew) {
                indexedTeams.push(player.GID);
            }
            return teamIsNew;
        }
        $scope.sum = function(list) {
            var total = 0;
            total = parseFloat(total);
            angular.forEach(list, function(item) {
                    total += parseFloat(item.price);
            });
            return total;
        }
        vm.toggle = toggle;
        function toggle(item, list,allData) {
            var idx = list.indexOf(item);
            if (idx > -1) {
                list.splice(idx, 1);
            }
            else {
                list.push(item);
                if(vm.selected.length == allData.length){
                    vm.checked = true;
                }
                else{
                    vm.checked = false;
                }
            }

             vm.data = list;
        };

        vm.exists = exists;

        function exists(item, list) {
            return list.indexOf(item) > -1;
        };

        //$scope.selected = {};
       vm.selectAll = selectAll;
        //$scope.selectAll= function(allData){
        function selectAll(allData){
            if(vm.selected.length == allData.length && vm.checked == true){
                vm.selected = [];
            }
            else{
                for (var i = 0; i < allData.length; i++) {
                    vm.selected = allData;
                    vm.data=  vm.selected;
                }
            }

        }
        // Change the status of the 'Check All' checkbox
        // According to the other checkboxes

        //http://jsfiddle.net/63e03sgm/2/

        //$scope.checkAll = function () {
        //    angular.forEach($scope.checkbox, function (obj) {
        //        obj.selected = $scope.select;
        //    });
        //};

       vm.markAs = markAs;
        function markAs(order_ID){
            vm.newData = [];
            vm.itemData = {};
            vm.itemStatus = {};
            var Invoiced = $filter('filter')(vm.orderStatus, { title: "Invoiced"  }, true)[0];
            var Dispatched = $filter('filter')(vm.orderStatus, { title: "Dispatched"  }, true)[0];
            var Received = $filter('filter')(vm.orderStatus, { title: "Received"  }, true)[0];

            if(order_ID == 1){
                var newStatus = Invoiced.id_order_status;
            }
            if(order_ID == 2){
                var  newStatus = Dispatched.id_order_status;
            }
            angular.forEach(vm.data, function(value, key){
                vm.newData.push(value.id_sales_order);
            });
            vm.itemData['id_sales_order']  = vm.newData;
            vm.itemStatus['order_status_id'] = newStatus;
            console.log(vm.itemData);
            if(vm.itemData['id_sales_order'].length == 0){
                alert("Please Select the OrderID");
                return false;
            }
            else {
                OrderProcessingService.changeOrderStatus(vm.itemData, vm.itemStatus).then(function (response) {
                    if (response) {
                        angular.forEach(vm.data, function (value, key) {
                            if (vm.retailOutlets.indexOf(value) > -1) {
                                vm.retailOutlets.splice(vm.retailOutlets.indexOf(value), 1);
                            }

                        });

                    }
                });
            }
        }

        vm.toggleInnerTable = toggleInnerTable;

        function toggleInnerTable($event,domElement)
        {
            $($event.currentTarget).toggleClass('close');
            $('#'+domElement).toggleClass('hide');
        }

    }
})();