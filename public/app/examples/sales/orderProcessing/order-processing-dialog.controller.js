(function() {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('OrderProcessingDialogController', OrderProcessingDialogController);

    /* @ngInject */
    function OrderProcessingDialogController($state, $mdDialog, event , dialogData ) {
        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.dialogData = dialogData;
        vm.item = {
            quantity: '',
            price: '',
            reason : '',
        };

        if (event) {
            console.log(event);

            vm.item.quantity = event.quantity;
            vm.item.price = event.price;
            vm.item.reason = event.reason;
        }


        /////////////////////////

        function hide() {
            $mdDialog.hide(vm.item);
        }

        function cancel() {
            $mdDialog.cancel();
        }
    }
})();
