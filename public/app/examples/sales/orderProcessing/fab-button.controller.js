(function() {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('DistributorFabController', DistributorFabController);

    /* @ngInject */
    function DistributorFabController($rootScope) {
        var vm = this;
        vm.addTodo = addTodo;

        ////////////////

        function addTodo($event) {
            $rootScope.$broadcast('addTodo', $event);
        }
    }
})();
