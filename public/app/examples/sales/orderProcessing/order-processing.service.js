(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .service('OrderProcessingService', OrderProcessingService);

    /* @ngInject */
    function OrderProcessingService($http,$q,API_CONFIG,$filter) {
        var vm = this;
        vm.baseUrl = API_CONFIG.baseUrl;


        this.getOrderStatus = getOrderStatus;
        function getOrderStatus(){
            return $http.get(vm.baseUrl + 'orderStatus');
        }

        this.getSalesOrderDetails = getSalesOrderDetails;
        function getSalesOrderDetails(orderStatusID){
            return $http.get(vm.baseUrl + 'sales-order/detail/'+orderStatusID);

        }

        this.modifyRateQuantity = modifyRateQuantity;
        function modifyRateQuantity(id, datas) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl+'sales-order/' + id,
                data: $.param({detail: datas})
            });
        }
this.cancelRequest = cancelRequest;
        function cancelRequest(id, datas) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl+'sales-order/' + id,
                data: $.param({detail: datas})
            });
        }
        this.changeOrderStatus = changeOrderStatus;
        function changeOrderStatus(item,itemStatus) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'sales-order/changeStatus',
                data: $.param({detail: item, order_status_id: itemStatus})
            });
        }

        this.generateSales = generateSales;
        function generateSales(dateRange,id) {
            var $url = '';
            var startTime = dateRange.start.toDate();
            var endTime = dateRange.end.toDate();
            var startTime = $filter('date')(startTime,'yyyy-MM-dd');
            var endTime = $filter('date')(endTime,'yyyy-MM-dd');
            if(id)
                $url =vm.baseUrl + 'sales-order/orderByDate/'+ id;
             else
                $url =vm.baseUrl + 'sales-order/orderByDate';
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: $url,
                data: $.param({start: startTime, end: endTime})
            });
        }
        //this.getSalesOrderDispached = getSalesOrderDispached;
        //function getSalesOrderDispached(orderStatusID){
        //    return $http.get(vm.baseUrl + 'sales-order/detail/'+2);
        //
        //}
        //this.getSalesOrderInvoiced = getSalesOrderInvoiced;
        //function getSalesOrderInvoiced(orderStatusID){
        //    return $http.get(vm.baseUrl + 'sales-order/detail/'+3);
        //
        //}

    }
})();
