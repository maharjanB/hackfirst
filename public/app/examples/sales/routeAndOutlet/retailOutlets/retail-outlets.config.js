(function() {
    'use strict';

    angular
        .module('app.examples.retail-outlets')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider, uiGmapGoogleMapApiProvider) {
        //$translatePartialLoaderProvider.addPart('app/examples/layouts');

        uiGmapGoogleMapApiProvider.configure({
            v: '3.17',
            libraries: 'weather,geometry,visualization'
        });

        $stateProvider
        .state('triangular.admin-default.retail-outlets-list', {
            url: '/retail-outlets/list',
            templateUrl: 'app/examples/retailOutlets/retail-outlets.tmpl.html',
            controller: 'RetailOutletController',
            controllerAs: 'vm'
        })
        .state('triangular.admin-default.retail-outlet-detail', {
/*            data: {
                layout: {
                    sideMenuSize: 'hidden'
                }
            },*/
            url: '/retail-outlet/detail',
            templateUrl: 'app/examples/sales/routAndOutlet/retailOutlet/detail.tmpl.html'
        })
        .state('triangular.admin-default.layouts-example-tall-toolbar', {
            data: {
                layout: {
                    toolbarSize: 'md-tall',
                    toolbarClass: 'full-image-background mb-bg-fb-14'
                }
            },
            url: '/layouts/tall-toolbar',
            templateUrl: 'app/examples/dashboards/server/dashboard-server.tmpl.html',
            controller: 'DashboardServerController',
            controllerAs: 'vm'
        })
        .state('triangular.admin-default.layouts-example-icon-menu', {
            data: {
                layout: {
                    sideMenuSize: 'icon'
                }
            },
            url: '/layouts/icon-menu',
            templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
        });
        triMenuProvider.addMenu({
            name: 'Retail Outlets',
            icon: 'zmdi zmdi-store',
            type: 'link',
            priority: 2.9,
            state: 'triangular.admin-default.retail-outlets-list'
        });
    }
})();