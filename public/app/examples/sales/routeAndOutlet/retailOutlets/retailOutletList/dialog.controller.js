(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('RetailOutletDialogController', RetailOutletDialogController);

    /* @ngInject */
    function RetailOutletDialogController($state, $mdDialog, event, dialogData, RetailOutletService) {
        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.dialogData = dialogData;
        vm.item = {
            title: '',
            contact_number1: '',
            contact_number2: '',
            channel_id: '',
            category_id: '',
            town_id: '',
            PAN_number: ''
        };

        if (event) {
            vm.item.title = event.title;
            vm.item.contact_number1 = event.contact_number1;
            vm.item.contact_number2 = event.contact_number2;
            vm.item.channel_id = event.channel_id;
            vm.item.category_id = event.category_id;
            vm.item.town_id = event.town_id;
            vm.item.PAN_number = event.PAN_number;
            vm.item.status = event.status;
        }

        /////////////////////////

        function hide() {
            $mdDialog.hide(vm.item);
        }

        function cancel() {
            $mdDialog.cancel();
        }


        var init = function () {
            RetailOutletService.getChannel().then(function (data) {
                vm.channels = data.data.data;
            });

            RetailOutletService.getCategory().then(function (data) {
                vm.categories = data.data.data;
            });

            RetailOutletService.getTown().then(function (data) {
                vm.towns = data.data.data;
            });
        }

        init();
    }
})();
