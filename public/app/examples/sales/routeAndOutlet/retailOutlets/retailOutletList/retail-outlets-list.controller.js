(function () {
    'use strict';
    angular
        .module('app.examples.sales')
        .controller('TablesAdvancedController', TablesAdvancedController);

    /* @ngInject */
    function TablesAdvancedController($scope, $timeout, $q, RetailOutletService, $state, $mdDialog, $filter, $mdToast) {
        var vm = this;

        vm.getRetailOutlets = getRetailOutlets();

        function getRetailOutlets() {
            RetailOutletService.getRetailOutlets().then(function (retailoutlets) {
                vm.retailoutlets = retailoutlets.data.data;
            });
        }

        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1
        };
        vm.selected = [];
        vm.filter = {
            options: {
                debounce: 500
            }
        };

        vm.detailView = detailView;

        // opens an email
        /*        function openMail(email) {
         $state.go(vm.baseState.name + '.email', {
         emailID: email.id
         });
         email.unread = false;
         vm.selectedMail = email.id;
         }*/

        function detailView(retailoutlet) {
            $state.go('triangular.admin-default.sales-route-outlet-detail', {
                retailOutletID: retailoutlet.id_retail_outlet
            });
        }

        vm.editRetailOutlets = editRetailOutlets;
        function editRetailOutlets(id, retailoutlet) {
            RetailOutletService.editRetailOutlets(id, retailoutlet).then(function (retailoutlets) {
                if (retailoutlets.status == 200 && retailoutlets.data) {
                    if (retailoutlets.data.error) {
                        var last = Object.keys(retailoutlets.data.error.message).length;
                        var key = Object.keys(retailoutlets.data.error.message);
                        alert(key + "Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Retail-Outlet Updated'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }

        vm.UpdateRetailOutlet = UpdateRetailOutlet;
        function UpdateRetailOutlet(id, event, $event) {
            var index = id;
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/routeAndOutlet/retailOutlets/retailOutletList/add-todo-dialog.tmpl.html',
                    targetEvent: $event,
                    controller: 'RetailOutletDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Edit RetailOutlet',
                            confirmButtonText: 'SAVE'
                        },
                        event: vm.retailoutlets[index],
                    }
                })
                .then(function (answer) {
                    vm.retailoutlets[index].title = answer.title;
                    vm.retailoutlets[index].contact_number1 = answer.contact_number1;
                    vm.retailoutlets[index].contact_number2 = answer.contact_number2;
                    vm.retailoutlets[index].channel_id = answer.channel_id;
                    vm.retailoutlets[index].category_id = answer.category_id;
                    vm.retailoutlets[index].town_id = answer.town_id;
                    vm.retailoutlets[index].PAN_number = answer.PAN_number;
                    var id = vm.retailoutlets[index].id_retail_outlet;
                    vm.editRetailOutlets(id, answer);
                });
        }

        // listeners

        /*
         function addTodo($event) {
         $rootScope.$broadcast('addTodo', $event);
         }*/


        /* vm.goHome = function() {
         $state.go('triangular.admin-default.retail-outlet-detail');
         };

         vm.viewMap = viewMap;

         function viewMap(){
         $('#retail-outlet-map-view').toggleClass('hide');
         }

         vm.query = {
         filter: '',
         limit: '10',
         order: '-id',
         page: 1
         };
         vm.selected = [];
         vm.filter = {
         options: {
         debounce: 500
         }
         };
         vm.getUsers = getUsers;
         vm.removeFilter = removeFilter;

         activate();

         ////////////////

         function activate() {
         var bookmark;
         $scope.$watch('vm.query.filter', function (newValue, oldValue) {
         if(!oldValue) {
         bookmark = vm.query.page;
         }

         if(newValue !== oldValue) {
         vm.query.page = 1;
         }

         if(!newValue) {
         vm.query.page = bookmark;
         }

         vm.getUsers();
         });
         }



         function removeFilter() {
         vm.filter.show = false;
         vm.query.filter = '';

         if(vm.filter.form.$dirty) {
         vm.filter.form.$setPristine();
         }
         }*/
    }
})();
