(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .service('RetailOutletService', Service);

    //Service.$inject = ['$http', '$q'];

    /* @ngInject */
    function Service($http,$q,API_CONFIG) {


        this.getRetailOutlets = getRetailOutlets;
        this.getRetailOutlet = getRetailOutlet;
        var vm = this;

        //base Url configuration
        vm.baseUrl = API_CONFIG.baseUrl;


        /**
         *
         * @returns {*}
         */
        function getRetailOutlets() {
            //var order = query.order === 'id' ? 'desc':'asc';
            return $http.get(vm.baseUrl+'retail-outlet').success(function (data) {
                return data;
            });
        }

this.getDetailRetailOutlets = getDetailRetailOutlets;
        function getDetailRetailOutlets(id) {
            console.log(id);
            return $http.get(vm.baseUrl+'sales-order/detail/'+ id).success(function (data) {
                return data;
            });
        }

        this.editRetailOutlets = editRetailOutlets;
        function editRetailOutlets(id, retailOutlet) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl+'retail-outlet/' + id,
                data: $.param({detail: retailOutlet})
            });
        }

        function getRetailOutlet(retailOutletID) {
            //var order = query.order === 'id' ? 'desc':'asc';
            return $http.get(vm.baseUrl+'retail-outlet/' + retailOutletID).success(function (data) {
                return data;
            });
        }

        this.getChannel = getChannel;
        function getChannel() {
            return $http.get(vm.baseUrl+'channel');
        }

        this.getTown = getTown;
        function getTown() {
            return $http.get(vm.baseUrl+'town');
        }

        this.getCategory = getCategory;
        function getCategory() {
            return $http.get(vm.baseUrl+'category');
        }

    }
})();
