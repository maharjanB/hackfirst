(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .controller('RetailOutletController', RetailOutletController);

    /* @ngInject */
    function RetailOutletController($scope, uiGmapGoogleMapApi, $state, $mdDialog,$mdSidenav) {
        var vm = this;
        vm.openSidebar = openSidebar;

        //alert($stateParams.retailOutletID);
       // console.log($stateParams.retailOutletID);

        function openSidebar(id){
            $mdSidenav(id).toggle();
        }
/*            this.openSidebar = function (id) {
                $mdSidenav(id).toggle();
            };*/

        vm.columns = [
            {
                title: 'Name',
                field: 'name',
                sortable: true,
            },
            {
                title: 'Channel',
                field: 'channel',
                sortable: true,
            },
            {
                title: 'Category',
                field: 'category',
                sortable: true,
            }, {
                title: 'Town',
                field: 'town',
                sortable: true,
            }, {
                title: 'Street',
                field: 'street',
                sortable: true
            }, {
                title: 'Registered On',
                field: 'registered_on',
                sortable: true
            }, {
                title: 'Verification',
                field: 'verified_on',
                sortable: true
            }, {
                title: 'Image',
                field: 'thumb',
                sortable: false,
                filter: 'tableImage'
            },
            {
                title: 'Hotspot',
                field: 'thumb',
                sortable: false,
                filter: 'tableImage'
            }];

        vm.contents = [{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        },{
            channel: 'HFS',
            category: 'Small',
            town: 'Patan',
            street: 'Twagal',
            thumb: 'assets/images/avatars/avatar-1.png',
            name: 'Chris Doe',
            registered_on: 'Jan 27, 1984',
            verified_on:'Jan 27, 1984'
        }];


        vm.todos = [
            {description: 'Material Design', priority: 'high', selected: true},
            {description: 'Install espresso machine', priority: 'high', selected: false},
            {description: 'Deploy to Server', priority: 'medium', selected: true},
            {description: 'Cloud Sync', priority: 'medium', selected: false},
            {description: 'Test Configurations', priority: 'low', selected: false},
            {description: 'Validate markup', priority: 'low', selected: false},
            {description: 'Debug javascript', priority: 'low', selected: true},
            {description: 'Arrange meeting', priority: 'low', selected: true}
        ];
        vm.orderTodos = orderTodos;
        vm.removeTodo = removeTodo;


        ///////////////////////////
        uiGmapGoogleMapApi.then(function (maps) {
            vm.map = {
                center: {
                    latitude: 35.027469,
                    longitude: -111.022753
                },
                zoom: 4,
                marker: {
                    id: 0,
                    coords: {
                        latitude: 35.027469,
                        longitude: -111.022753
                    },
                    options: {
                        icon: {
                            anchor: new maps.Point(36, 36),
                            origin: new maps.Point(0, 0),
                            url: 'assets/images/maps/blue_marker.png'
                        }
                    }
                }
            };
        });


        //////////////////////////

        function orderTodos(task) {
            switch (task.priority) {
                case 'high':
                    return 1;
                case 'medium':
                    return 2;
                case 'low':
                    return 3;
                default: // no priority set
                    return 4;
            }
        }

        function removeTodo(todo) {
            for (var i = vm.todos.length - 1; i >= 0; i--) {
                if (vm.todos[i] === todo) {
                    vm.todos.splice(i, 1);
                }
            }
        }



        // watches

        $scope.$on('addTodo', function (ev) {
            $mdDialog.show({
                    templateUrl: 'app/examples/todo/add-todo-dialog.tmpl.html',
                    targetEvent: ev,
                    controller: 'DialogController',
                    controllerAs: 'vm'
                })
                .then(function (answer) {
                    vm.todos.push(answer);
                });
        });
    }
})();