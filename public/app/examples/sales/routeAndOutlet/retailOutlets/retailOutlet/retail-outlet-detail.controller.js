(function() {
    'use strict';
    angular
        .module('app.examples.sales')
        .controller('RetailOutletDetailController', RetailOutletDetailController);

    /* @ngInject */
    function RetailOutletDetailController($scope, $timeout, $q, RetailOutletService, $state,$stateParams) {
        var vm = this;

        vm.retailOutletID = $stateParams.retailOutletID;
        vm.getRetailOutlet = getRetailOutlet(vm.retailOutletID);
        vm.salesOrders = getDetailRetailOutlets(vm.retailOutletID);
        vm.retailOutlet = [];

        function getRetailOutlet(retailOutletID) {
            RetailOutletService.getRetailOutlet(retailOutletID).then(function(retailOutlet){
                vm.retailOutlet = retailOutlet.data.data;
            });
        }
        function getDetailRetailOutlets(retailOutletID) {
            RetailOutletService.getDetailRetailOutlets(retailOutletID).then(function(retailOutlet){
                vm.salesOrders = retailOutlet.data.data;
            });
        }

    }
})();