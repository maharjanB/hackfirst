(function () {
    'use strict';

    angular
        .module('app.examples.configurations')
        .controller('RouteDialogController', RouteDialogController);

    /* @ngInject */
    function RouteDialogController($scope, $state, $mdDialog, event, dialogData, RouteService, $rootScope) {
        var vm = this;
        vm.cancel = cancel;
        vm.hide = hide;
        vm.route = [];
        vm.routeRetails = [];
        vm.assignRoute = [];
        vm.routeAssign = [];
        vm.listOutlets = event;
        vm.listOutlets.id_route = event.id_route;

        vm.dialogData = dialogData;
        vm.item = {
            title: '',
            route_visit_category_id: '',
            route_visit_type_id: '',
            route_delivery_type_id: '',
            route_visit_frequency_id: '',
        }
        if (event) {
            vm.item.title = event.title;
            vm.item.route_visit_category_id = event.route_visit_category_id;
            vm.item.route_visit_type_id = event.route_visit_category_id;
            vm.item.route_delivery_type_id = event.route_delivery_type_id;
            vm.item.route_visit_frequency_id = event.route_visit_frequency_id;
            //  vm.item.retail_outlet_counts = event.retail_outlet_counts;
            // vm.item.retail_outlets = event.retail_outlets;
        }

        /////////////////////////

        function hide() {
            $mdDialog.hide(vm.item);
        }

        function cancel() {
            $mdDialog.cancel();
        }

        vm.deleteOutlets = deleteOutlets;
        function deleteOutlets(id, event, $event) {
            console.log(vm.listOutlets.retail_outlets[id].id_retail_outlet);
            var index = vm.listOutlets.retail_outlets[id];
            vm.listOutlets.retail_outlets[id].id_route = vm.listOutlets.id_route;
            RouteService.deleteOutlets(index).then(function (data) {
                vm.routeRetails.push(data.data.data);
                vm.listOutlets.retail_outlets.splice(index, 1);

            });

        }

        //  $scope.removeRow = function (name) {
        function removeRow(name) {
            var index = name;
            var comArr = eval(vm.routeRetails);
            if (index === -1) {
                alert("Something gone wrong");
            }
            vm.routeRetails.splice(index, 1);
        }

        vm.addRetailOutlet = addRetailOutlet;
        function addRetailOutlet(id, event, $event) {
            var index = id;
            vm.route.id_route = vm.listOutlets.id_route;
            vm.route.retail_outlet_id = vm.routeRetails[id].id_retail_outlet;
            vm.routeRetails[id].id_route = vm.listOutlets.id_route;
            vm.route = vm.routeRetails[id];
            RouteService.addRetailOutlet(vm.route).then(function (data) {
                vm.routeAssign = data;
                vm.removeRow = removeRow(id);
                var keys = Object.keys(vm.listOutlets.retail_outlets);
                var len = keys.length + 1;
                vm.listOutlets.retail_outlets.push(data.data.data);
            });
        }

        var init = function () {
            RouteService.getUnassignedOutlets().then(function (data) {
                vm.routeRetails = data.data;

            });
        }
        init();

        //   vm.deleteOutlets = deleteOutlets;
        //   function deleteOutlets(id, event, $event) {
        //     console.log(id);
        //   var index = id;
        //console.log(vm.listOutlets.id_route);
        // vm.assignRoute.id_route = vm.listOutlets.id_route;

        //  vm.assignRoute = vm.listOutlets.retail_outlets[id];
        // vm.assignRoute.push(vm.listOutlets.id_route);

        // console.log(assignRoute);

        //vm.assignRoute = vm.listOutlets.retail_outlets[id];
        /*var confirm = $mdDialog.confirm()
         .title('Would you like to delete your debt?')
         .ok('OK!')
         .cancel('Cancel');
         $mdDialog.show(confirm).then(function (event) {*/
        // RouteService.deleteOutlets(vm.assignRoute).then(function (data) {
        // console.log(vm.assignRoute);
        // vm.listOutlets.retail_outlets.splice(index, 1);

        //});
        //  return true;
        //   });

        /* vm.showConfirm = function (ev) {
         // function showConfirm() {
         var confirm = $mdDialog.confirm()
         .title('Would you like to delete your debt?')
         .ok('OK!')
         .cancel('Cancel');
         $mdDialog.show(confirm).then(function (event) {
         RouteService.deleteOutlets(id).then(function () {
         });
         return true;
         }, function () {
         ev.stopPropagation();
         ev.preventDefault();
         alert('hhh');
         });
         };*/
    }
})
();
