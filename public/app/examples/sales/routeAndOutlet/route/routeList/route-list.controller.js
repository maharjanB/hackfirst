(function () {
    'use strict';
    angular
        .module('app.examples.sales')
        .controller('RouteListController', RouteListController);

    /* @ngInject */
    function RouteListController($scope, RouteDataShareService, $timeout, $q, RouteService, $state, $mdSidenav, uiGmapGoogleMapApi, $mdDialog, $mdToast, $filter) {

        var vm = this;

/*
        Toggle Map View
*/
        vm.showMap = RouteDataShareService.getShowMap();


        vm.setShowMap = setShowMap;
        function setShowMap(){
            RouteDataShareService.setShowMap(true);
            vm.showMap = RouteDataShareService.getShowMap();
        };

        vm.getRoutes = getRoutes();
        vm.openSidebar = openSidebar;
        vm.query = {
            filter: '',
            limit: '10',
            order: '-id',
            page: 1
        };

        vm.selected = [];
        vm.filter = {
            options: {
                debounce: 500
            }
        };

        function getRoutes() {
            RouteService.getRoutes().then(function (routes) {
                vm.routes = routes.data.data;
            });
        }


        function openSidebar(id) {
            $mdSidenav(id).toggle();
        }

        vm.postRoute = postRoute;

        function postRoute(route) {
            RouteService.postRoute(route).then(function (routes) {
                if (routes.status == 200 && routes.data) {
                    if (routes.data.error) {
                        var last = Object.keys(routes.data.error.message).length;
                        var key = Object.keys(routes.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Route Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.routes.push(routes.data.data);
                    }
                } else {

                }
            });
        }

        function addRoute(event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/routeAndOutlet/route/routeList/create-route-form.tmpl.html',
                    targetEvent: $event,
                    controller: 'RouteDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Create New Route',
                            confirmButtonText: 'OK'
                        },
                        event: {
                            title: '',
                            description: '',
                            status: false
                        },
                        edit: false
                    }
                })
                .then(function (answer) {
                    vm.postRoute(answer);

                });
        }

        $scope.$on('addTodo', addRoute);


        vm.toggleInnerTableOutlet = toggleInnerTableOutlet;

        function toggleInnerTableOutlet(domElement, $event) {

            console.log(domElement);
            // $($event.currentTarget).toggleClass('close');
            $('#' + domElement).toggleClass('hide');
        }

        //
        //
        //uiGmapGoogleMapApi.then(function (maps) {
        //    vm.terrainMap = {
        //        center: {
        //            latitude: 51.219053,
        //            longitude: 4.404418
        //        },
        //        zoom: 10,
        //        marker: {
        //            id: 0,
        //            coords: {
        //                latitude: 51.219053,
        //                longitude: 4.404418
        //            },
        //            options: {
        //                icon: {
        //                    anchor: new maps.Point(36, 36),
        //                    origin: new maps.Point(0, 0),
        //                    url: 'assets/images/maps/blue_marker.png'
        //                }
        //            }
        //        },
        //        options: {
        //            scrollwheel: false,
        //            mapTypeId: maps.MapTypeId.TERRAIN
        //        }
        //    };
        //});


        /* vm.goHome = function() {
         $state.go('triangular.admin-default.retail-outlet-detail');
         };

         vm.viewMap = viewMap;

         function viewMap(){
         $('#retail-outlet-map-view').toggleClass('hide');
         }



         vm.removeFilter = removeFilter;

         activate();

         ////////////////

         function activate() {
         var bookmark;
         $scope.$watch('vm.query.filter', function (newValue, oldValue) {
         if(!oldValue) {
         bookmark = vm.query.page;
         }

         if(newValue !== oldValue) {
         vm.query.page = 1;
         }

         if(!newValue) {
         vm.query.page = bookmark;
         }

         vm.getUsers();
         });
         }



         function removeFilter() {
         vm.filter.show = false;
         vm.query.filter = '';

         if(vm.filter.form.$dirty) {
         vm.filter.form.$setPristine();
         }
         }*/


        vm.editRoute = editRoute;
        function editRoute(id, route) {
            RouteService.editRoute(id, route).then(function (routes) {
                if (routes.status == 200 && routes.data) {
                    if (routes.data.error) {
                        var last = Object.keys(routes.data.error.message).length;
                        var key = Object.keys(routes.data.error.message);
                        alert(key + "Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Route Updated'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                    }
                } else {

                }
            });
        }


        vm.UpdateRoute = UpdateRoute;
        function UpdateRoute(id, event, $event) {
            var index = id;
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/routeAndOutlet/route/routeList/edit-route-form.tmpl.html',
                    targetEvent: $event,
                    controller: 'RouteDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Edit Route',
                            confirmButtonText: 'SAVE'
                        },
                        event: vm.routes[index],
                    }
                })
                .then(function (answer) {
                    vm.routes[index].title = answer.title;
                    vm.routes[index].route_visit_category_id = answer.route_visit_category_id;
                    vm.routes[index].route_visit_type_id = answer.route_visit_category_id;
                    vm.routes[index].route_delivery_type_id = answer.route_delivery_type_id;
                    vm.routes[index].route_visit_frequency_id = answer.route_visit_frequency_id;
                    var id = vm.routes[index].id_route;
                    vm.editRoute(id, answer);
                });
        }


    }
})();
