(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .service('RouteService', RouteService);

    // RouteService.$inject = ['$http', '$q'];

    /* @ngInject */
    function RouteService($http,$q,API_CONFIG) {
        var vm = this;

       // console.log(API_CONFIG.url);

        vm.baseUrl = API_CONFIG.baseUrl;

        this.getRoutes = getRoutes;


        function getRoutes() {
            return $http.get(vm.baseUrl + 'route').success(function (data) {
                return data;
            });

        }

        this.getRetailOutlets = getRetailOutlets;
        function getRetailOutlets() {
            return $http.get(vm.baseUrl + 'retail-outlet');
        }

        this.getUnassignedOutlets = getUnassignedOutlets;
        function getUnassignedOutlets() {
            return $http.get('retail-outlet/nullOutlets');
        }

        this.deleteOutlets = deleteOutlets;
        function deleteOutlets(route) {
            console.log(route);
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: 'retail-outlet/removeOutlets',
                data: $.param({detail: route})
            });
        }

        this.editRoute = editRoute;
        function editRoute(id, route) {
            return $http({
                method: "PUT",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'route/' + id,
                data: $.param({detail: route})
            });
        }

        this.postRoute = postRoute;
        function postRoute(route) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'route',
                data: $.param({detail: route})
            });
        }

        this.addRetailOutlet = addRetailOutlet;
        function addRetailOutlet(route) {
            return $http({
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: vm.baseUrl + 'route/assignRetailOutlet',
                data: $.param({detail: route})
            });
        }
    }
})();
