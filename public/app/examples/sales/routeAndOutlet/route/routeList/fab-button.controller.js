(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .controller('RouteFabController', RouteFabController);

    /* @ngInject */
    function RouteFabController($rootScope) {
        var vm = this;
        vm.addTodo = addTodo;

        ////////////////

        function addTodo($event) {
            $rootScope.$broadcast('addTodo', $event);
        }
    }
})();
