(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .controller('RouteController', RouteController);

    /* @ngInject */
    function RouteController($scope,RouteDataShareService, $state, $mdDialog, $mdSidenav, RouteService, $mdToast, $filter) {

        var vm = this;
        vm.openSidebar = openSidebar;
        vm.routes = [];
        vm.getRoutes = getRoutes();

        vm.showMap = RouteDataShareService.getShowMap();


        function getRoutes() {
            RouteService.getRoutes().then(function (routes) {
                vm.routes = routes.data.data;
            });
        }

        function openSidebar(id) {
            $mdSidenav(id).toggle();
        }

        vm.postRoute = postRoute;

        function postRoute(route) {
            RouteService.postRoute(route).then(function (routes) {
                if (routes.status == 200 && routes.data) {
                    if (routes.data.error) {
                        var last = Object.keys(routes.data.error.message).length;
                        var key = Object.keys(routes.data.error.message);
                        alert(key + " Validation Fails");
                        return false;
                    }
                    else {

                        $mdToast.show(
                            $mdToast.simple()
                                .content($filter('translate')('Route Created'))
                                .position('bottom right')
                                .hideDelay(2000)
                        );
                        vm.routes.push(routes.data.data);
                    }
                } else {

                }
            });
        }

        function addRoute(event, $event) {
            $mdDialog.show({
                    templateUrl: 'app/examples/sales/routeAndOutlet/route/routeList/create-route-form.tmpl.html',
                    targetEvent: $event,
                    controller: 'RouteDialogController',
                    controllerAs: 'vm',
                    locals: {
                        dialogData: {
                            title: 'Create New Route',
                            confirmButtonText: 'OK'
                        },
                        event: {
                            title: '',
                            description: '',
                            status: false
                        },
                        edit: false
                    }
                })
                .then(function (answer) {
                    vm.postRoute(answer);

                });
        }

        $scope.$on('addTodo', addRoute);


        // watches

        /*    $scope.$on('addTodo', function (ev) {
         $mdDialog.show({
         templateUrl: 'app/examples/todo/add-todo-dialog.tmpl.html',
         targetEvent: ev,
         controller: 'DialogController',
         controllerAs: 'vm'
         })
         .then(function (answer) {
         vm.todos.push(answer);
         });
         });*/
    }
})();
