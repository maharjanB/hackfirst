(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .controller('RouteMapController', RouteMapController);

    /* @ngInject */
    function RouteMapController(uiGmapGoogleMapApi, $mdSidenav, RetailOutletService, RouteService, TownService) {
        var vm = this;
        vm.openSidebar = openSidebar;

        function openSidebar(id) {
            $mdSidenav(id).toggle();
        }

        vm.selectedRoute = [];

        vm.selectRoute = selectRoute;
        function selectRoute(route)
        {
            vm.selectedRoute = route;
            vm.showOutletList = true;
            vm.showList = false;
             $scope.$apply();

           // console.log(vm.selectedRoute);
        }


        vm.selected = [];

        vm.toggle = toggle;

        function toggle(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) list.splice(idx, 1);
            else list.push(item);
        };

        vm.exists = exists;

        function exists(item, list) {
            return list.indexOf(item) > -1;
        };


        vm.filterTown = [];


        vm.towns = [];
        vm.getTowns = getTowns();

        function getTowns() {
            TownService.getTown().then(function (towns) {
                vm.towns = towns.data.data;
            });
        }


        /*
         get the retailOutlets to the Map
         */

        vm.randomMarkers = [];

        vm.retailOutlets = [];

        var createMarkers = function (i, idKey) {


            if (idKey == null) {
                idKey = "id";
            }

            var latitude = 51.219053 + (Math.random() );
            var longitude = 51.219053 + (Math.random() );
            var ret = {
                latitude: latitude,
                longitude: longitude,
                title: 'm' + i,
                icon: 'images/maps/blue_marker.png'
            };
            ret[idKey] = i;
            return ret;
        };


        vm.populateOutlets = populateOutlets;

        function populateOutlets(){

            var markers = [];

            RetailOutletService.getRetailOutlets().then(function (retailoutlets) {
                vm.retailOutlets = retailoutlets.data.data;

                for (var i = 0; i < vm.retailOutlets.length; i++) {

                    var ret = {
                        latitude: vm.retailOutlets[i].geolocation_latitude,
                        longitude: vm.retailOutlets[i].geolocation_longitude,
                        title: vm.retailOutlets[i].retailoutlet_title,
                        channel: vm.retailOutlets[i].channel_title,
                        category: vm.retailOutlets[i].category_title,
                        icon: 'images/maps/blue_marker.png',
                        idRoute: vm.retailOutlets[i].id_retail_outlet,
                    };
                    ret['id'] = vm.retailOutlets[i].id_retail_outlet;

                    markers.push(ret)
                }

                vm.randomMarkers = markers;
            });
            vm.getRoutes();
            vm.showList = true;
            vm.showOutletList = false;

        }


        vm.options = {
            pixelOffset: {width:-1,height:-20}
        };


        vm.getRoutes = getRoutes;
        function getRoutes() {
            RouteService.getRoutes().then(function (routes) {
                vm.routes = routes.data.data;
            });
        }


/*
        Assign Route to Retail Outlet
*/


        vm.addOutletToRoute = addOutletToRoute;

        function addOutletToRoute(id)
        {
            if(vm.selectedRoute){
                alert(id+""+ vm.seletectedRoute.id_route);
                //addOutletToRoute(id,vm.seletectedRoute.id_route);
            }
        }



        vm.selectedMarker = [];

        vm.showMarker = false;

        vm.markersEvents = {
            click: function(marker, eventName, model, args) {
                vm.selectedMarker = model;
                vm.showMarker = true;
/*
                $scope.map.window.model = model;
                $scope.map.window.show = true;*/

            }
        }


/*        vm.onClick = function (marker, eventName, model) {

            vm.selectedMarker = model;
            model.show;
        };*/


        // show the list over the map .
        // triggered when town selected on search.
        vm.showList = false;
        vm.showOutletList = false;

        uiGmapGoogleMapApi.then(function (maps) {
            vm.terrainMap = {
                center: {
                    latitude: 51.219053,
                    longitude: 4.404418
                },
                zoom: 10,
                marker: {
                    id: 0,
                    coords: {
                        latitude: 51.219053,
                        longitude: 4.404418
                    },
                    options: {
                        icon: {
                            anchor: new maps.Point(36, 36),
                            origin: new maps.Point(0, 0),
                            url: 'images/maps/blue_marker.png'
                        }
                    }
                },
                options: {
                    scrollwheel: false,
                    mapTypeId: maps.MapTypeId.TERRAIN
                }
            };
        });


        vm.generatePath = generatePath;

        function generatePath(){

            var path = [
                {
                    latitude: 45,
                    longitude: -74
                },
                {
                    latitude: 30,
                    longitude: -89
                },
                {
                    latitude: 37,
                    longitude: -122
                },
                {
                    latitude: 60,
                    longitude: -95
                }
            ];

            return path;

        };


        // section for the path of //

        vm.polylineJoin = [];
/*        vm.polylineJoins = function(){

            for(var i = vm.routes.length - 1; i >= 0; i--) {

                var path = [];
                 for(var j = vm.routes[i].retail_outlets.length - 1;i>=0;i--)
                 {

                 }


                vm.polylineJoin[i].path =
            }

            return vm.routes;

        };*/

        function removeTodo(todo){

        }



        vm.polylines = [
            {
                id: 1,
                path: [
                    {
                        latitude: 45,
                        longitude: -74
                    },
                    {
                        latitude: 30,
                        longitude: -89
                    },
                    {
                        latitude: 37,
                        longitude: -122
                    },
                    {
                        latitude: 60,
                        longitude: -95
                    }
                ],
                stroke: {
                    color: '#6060FB',
                    weight: 3
                },
                editable: true,
                draggable: true,
                geodesic: true,
                visible: true,
                icons: [{
                    icon: {
                        path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW
                    },
                    offset: '25px',
                    repeat: '50px'
                }]
            },
            {
                id: 2,
                path: [
                    {
                        latitude: 47,
                        longitude: -74
                    },
                    {
                        latitude: 32,
                        longitude: -89
                    },
                    {
                        latitude: 39,
                        longitude: -122
                    },
                    {
                        latitude: 62,
                        longitude: -95
                    }
                ],
                stroke: {
                    color: '#6060FB',
                    weight: 3
                },
                editable: true,
                draggable: true,
                geodesic: true,
                visible: true,
                icons: [{
                    icon: {
                        path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW
                    },
                    offset: '25px',
                    repeat: '50px'
                }]
            }
        ];



    }
})();