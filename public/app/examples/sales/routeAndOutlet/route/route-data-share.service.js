(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .service('RouteDataShareService', RouteDataShareService);

    /* @ngInject */
    function RouteDataShareService() {
        var vm = this;

        vm.showMap = false;

        vm.getShowMap =function(){
             return vm.showMap;
        };

        vm.setShowMap = function(data){
            vm.showMap = data;
        }

    }
})();
