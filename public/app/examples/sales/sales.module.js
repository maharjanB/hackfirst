(function() {
    'use strict';

    angular
        .module('app.examples.sales', ['app.examples.configurations']);
})();