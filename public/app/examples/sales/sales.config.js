(function () {
    'use strict';

    angular
        .module('app.examples.sales')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        triMenuProvider.addMenu({
            name: 'Sales',
            icon: 'icon-sales',
            type: 'dropdown',
            priority: 2.1,
            children: [{
                name: 'Order Fulfilment',
                type: 'link',
                state: 'triangular.admin-default.order-processing'
            }/*,{
             name: 'Account Receivables',
             type: 'link',
             state: 'triangular.admin-default.layouts-example-icon-menu'
             }*/, {
                name: 'Route and Outlets',
                type: 'dropdown',
                children: [{
                    name: 'Retail Outlets',
                    type: 'link',
                    state: 'triangular.admin-default.sales-retail-outlet'
                }, {
                    name: 'Routes',
                    type: 'link',
                    state: 'triangular.admin-default.sales-route'
                }]
            }, {
                name: 'Sales Force',
                type: 'link',
                state: 'triangular.admin-default.layouts-composer'
            }/*,{
             name: 'Promotions',
             type: 'link',
             state: 'triangular.admin-default.layouts-example-tall-toolbar'
             }*//*,{
             name: 'Targets',
             type: 'link',
             state: 'triangular.admin-default.layouts-example-tall-toolbar'
             }*/]
        });


        $stateProvider
            .state('triangular.admin-default.order-processing', {
                url: '/sales/order-processing',
                templateUrl: 'app/examples/sales/orderProcessing/order-processing-tmp.tmpl.html',
                controller: 'OrderProcessingController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.sales-retail-outlet', {
                url: '/sales/retail-outlet',
                templateUrl: 'app/examples/sales/routeAndOutlet/retailOutlets/retail-outlets.tmpl.html',
                controller: 'RetailOutletController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.sales-route', {
                url: '/sales/route',
                views: {
                    '': {
                        templateUrl: 'app/examples/sales/routeAndOutlet/route/route.tmpl.html',
                        controller: 'RouteListController',
                        controllerAs: 'vm'
                    },
                    'belowContent': {
                        templateUrl: 'app/examples/sales/routeAndOutlet/route/routeList/fab-button.tmpl.html',
                        controller: 'RouteFabController',
                        controllerAs: 'vm'
                    }
                }
            })
            .state('triangular.admin-default.sales-route-outlet-detail', {
                url: '/sales/retail-outlet/detail/:retailOutletID',
                templateUrl: 'app/examples/sales/routeAndOutlet/retailOutlets/retailOutlet/detail.tmpl.html',
                controller: 'RetailOutletDetailController',
                controllerAs: 'vm',
            });


        /*        $stateProvider
         .state(route.state + '.email', {
         url: '/mail/:emailID',
         templateUrl: 'app/examples/email/email.tmpl.html',
         controller: 'EmailController',
         controllerAs: 'vm',
         resolve: {
         email: function($stateParams, emails) {
         emails = emails.data;
         var foundEmail = false;
         for(var i = 0; i < emails.length; i++) {
         if(emails[i].id === $stateParams.emailID) {
         foundEmail = emails[i];
         break;
         }
         }
         return foundEmail;
         }
         },
         onEnter: function($state, email){
         if (false === email) {
         $state.go(route.state);
         }
         }
         });
         */


        /*

         $stateProvider
         .state('triangular.admin-default.layouts-composer', {
         url: '/layouts/composer',
         templateUrl: 'app/examples/layouts/composer.tmpl.html',
         controller: 'LayoutsComposerController',
         controllerAs: 'vm'
         })
         .state('triangular.admin-default.layouts-example-full-width', {
         data: {
         layout: {
         sideMenuSize: 'hidden'
         }
         },
         url: '/layouts/full-width',
         templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
         })
         .state('triangular.admin-default.layouts-example-tall-toolbar', {
         data: {
         layout: {
         toolbarSize: 'md-tall',
         toolbarClass: 'full-image-background mb-bg-fb-14'
         }
         },
         url: '/layouts/tall-toolbar',
         templateUrl: 'app/examples/dashboards/server/dashboard-server.tmpl.html',
         controller: 'DashboardServerController',
         controllerAs: 'vm'
         })
         .state('triangular.admin-default.layouts-example-icon-menu', {
         data: {
         layout: {
         sideMenuSize: 'icon'
         }
         },
         url: '/layouts/icon-menu',
         templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
         });
         */


    }
})();
