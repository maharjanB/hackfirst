(function() {
    'use strict';

    angular
        .module('app.examples.security')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig( $stateProvider, triMenuProvider) {
        $stateProvider
            .state('triangular.admin-default.security-audit-log', {
                url: '/security/audit',
                templateUrl: 'app/examples/security/auditLog/audit-log.tml.html',
                controller: 'SecurityController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.security-users', {
                url : '/security/users',
                templateUrl: 'app/examples/security/users/users.tml.html',
                controller: 'SecurityController',
                controllerAs: 'vm'
            })
            .state('triangular.admin-default.security-roles', {
                url : '/security/roles',
                templateUrl: 'app/examples/security/roles/roles.tml.html',
                controller: 'SecurityController',
                controllerAs: 'vm'
            });

/*

        $stateProvider
        .state('triangular.admin-default.layouts-composer', {
            url: '/layouts/composer',
            templateUrl: 'app/examples/layouts/composer.tmpl.html',
            controller: 'LayoutsComposerController',
            controllerAs: 'vm'
        })
        .state('triangular.admin-default.layouts-example-full-width', {
            data: {
                layout: {
                    sideMenuSize: 'hidden'
                }
            },
            url: '/layouts/full-width',
            templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
        })
        .state('triangular.admin-default.layouts-example-tall-toolbar', {
            data: {
                layout: {
                    toolbarSize: 'md-tall',
                    toolbarClass: 'full-image-background mb-bg-fb-14'
                }
            },
            url: '/layouts/tall-toolbar',
            templateUrl: 'app/examples/dashboards/server/dashboard-server.tmpl.html',
            controller: 'DashboardServerController',
            controllerAs: 'vm'
        })
        .state('triangular.admin-default.layouts-example-icon-menu', {
            data: {
                layout: {
                    sideMenuSize: 'icon'
                }
            },
            url: '/layouts/icon-menu',
            templateUrl: 'app/examples/dashboards/general/dashboard-general.tmpl.html'
        });
*/

        triMenuProvider.addMenu({
            name: 'Security',
            icon: 'zmdi zmdi-cutlery',
            type: 'dropdown',
            priority: 2.7,
            children: [{
                name: 'Audit Log',
                type: 'link',
                state: 'triangular.admin-default.security-audit-log'
            },{
                name: 'Users',
                type: 'link',
                state: 'triangular.admin-default.security-users'
            },{
                name: 'Roles',
                type: 'link',
                state: 'triangular.admin-default.security-roles'
            }]
        });
    }
})();