<!doctype html>
<html class="no-js" ng-app="app">
<head>
    <meta charset="utf-8">
    <title>Resource Mapping - universe of nepal </title>
    <meta name="description" content="">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Lato:100,100italic,300,300italic,400,500,700,900,400italic">
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,500,700,900,400italic">
    <!-- Place favicon.png in the root directory -->
    <link rel="icon" type="image/png" href="favicon.png"/>

    <!-- build:css({.tmp/serve,src}) styles/vendor.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="{{ asset('bower_components/angular-chart.js/dist/angular-chart.css')}}"/>
    <link rel="stylesheet" href="{{ asset('bower_components/angular-dragula/dist/dragula.min.css')}}"/>
    <link rel="stylesheet"
          href="{{ asset('bower_components/angular-material-data-table/dist/md-data-table.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.css')}}"/>
    <link rel="stylesheet"
          href="{{ asset('bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('bower_components/textAngular/src/textAngular.css')}}"/>
    <link rel="stylesheet" href="{{ asset('bower_components/weather-icons/css/weather-icons.css')}}"/>
    <!-- endbower -->
    <!-- endbuild -->

    <!-- build:css({.tmp/serve,src}) styles/app.css -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('styles/custom-fonts.css')}}">
    <link rel="stylesheet" href="{{ asset('styles/app.css')}}">
    <link rel="stylesheet" href="{{ asset('styles/vendor.css')}}">


    <!-- endinject -->
    <link rel="stylesheet" href="{{ asset('bower_components/highlightjs/styles/xcode.css')}}">
    <!-- endbuild -->
</head>
<body translate-cloak ng-class="bodyClasses">
<!--[if lt IE 10]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="full-height" ui-view></div>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
{{--
<script>
    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = '//www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');
</script>
--}}

<!-- build:js(src) scripts/vendor.js -->
<!-- bower:js -->
<script src="{{ asset('bower_components/jquery/dist/jquery.js')}}"></script>
<script src="{{ asset('bower_components/angular/angular.js')}}"></script>
<script src="{{ asset('bower_components/angular-animate/angular-animate.js')}}"></script>
<script src="{{ asset('bower_components/Chart.js/Chart.js')}}"></script>
<script src="{{ asset('bower_components/angular-chart.js/dist/angular-chart.js')}}"></script>
<script src="{{ asset('bower_components/angular-cookies/angular-cookies.js')}}"></script>
<script src="{{ asset('bower_components/angular-digest-hud/digest-hud.js')}}"></script>
<script src="{{ asset('bower_components/angular-dragula/dist/angular-dragula.js')}}"></script>
<script src="{{ asset('bower_components/angular-google-chart/ng-google-chart.js')}}"></script>
<script src="{{ asset('bower_components/lodash/lodash.js')}}"></script>
<script src="{{ asset('bower_components/angular-google-maps/dist/angular-google-maps.js')}}"></script>
<script src="{{ asset('bower_components/highlightjs/highlight.pack.js')}}"></script>
<script src="{{ asset('bower_components/angular-highlightjs/build/angular-highlightjs.js')}}"></script>
<script src="{{ asset('bower_components/angular-linkify/angular-linkify.js')}}"></script>
<script src="{{ asset('bower_components/angular-local-storage/dist/angular-local-storage.js')}}"></script>
<script src="{{ asset('bower_components/angular-aria/angular-aria.js')}}"></script>
<script src="{{ asset('bower_components/angular-material/angular-material.js')}}"></script>
<script src="{{ asset('bower_components/angular-material-data-table/dist/md-data-table.min.js')}}"></script>
<script src="{{ asset('bower_components/angular-messages/angular-messages.js')}}"></script>
<script src="{{ asset('bower_components/moment/moment.js')}}"></script>
<script src="{{ asset('bower_components/angular-moment/angular-moment.js')}}"></script>
<script src="{{ asset('bower_components/angular-resource/angular-resource.js')}}"></script>
<script src="{{ asset('bower_components/angular-touch/angular-touch.js')}}"></script>
<script src="{{ asset('bower_components/angular-translate/angular-translate.js')}}"></script>
<script src="{{ asset('bower_components/angular-translate-loader-partial/angular-translate-loader-partial.js')}}"></script>
<script src="{{ asset('bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.js')}}"></script>
<script src="{{ asset('bower_components/angular-translate-storage-local/angular-translate-storage-local.js')}}"></script>
<script src="{{ asset('bower_components/angular-ui-calendar/src/calendar.js')}}"></script>
<script src="{{ asset('bower_components/fullcalendar/dist/fullcalendar.js')}}"></script>
<script src="{{ asset('bower_components/angular-ui-router/release/angular-ui-router.js')}}"></script>
<script src="{{ asset('bower_components/countUp.js/countUp.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-core.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-classapplier.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-highlighter.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-selectionsaverestore.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-serializer.js')}}"></script>
<script src="{{ asset('bower_components/rangy/rangy-textrange.js')}}"></script>
<script src="{{ asset('bower_components/textAngular/src/textAngular.js')}}"></script>
<script src="{{ asset('bower_components/textAngular/src/textAngular-sanitize.js')}}"></script>
<script src="{{ asset('bower_components/textAngular/src/textAngularSetup.js')}}"></script>
<script src="{{ asset('bower_components/ng-file-upload/ng-file-upload-shim.js')}}"></script>
<script src="{{ asset('bower_components/ng-file-upload/ng-file-upload.js')}}"></script>
<!-- endbower -->

<!-- enqueue all translations for calendar (remove if you dont need multilanguage) -->
<!-- <script src="{{ asset('bower_components/fullcalendar/dist/lang-all.js')}}"></script> -->
<!-- endbuild -->

<!-- build:js({.tmp/serve,.tmp/partials,src}) scripts/app.js -->
<!-- inject:js -->

<script src="{{ asset('app/examples/auth.module.js')}}"></script>
<script src="{{ asset('app/examples/route-scope.run.js')}}"></script>
<script src="{{ asset('app/examples/http-interceptor.service.js')}}"></script>
<script src="{{ asset('app/examples/auth-data.service.js')}}"></script>
<script src="{{ asset('app/examples/auth.service.js')}}"></script>


<script src="{{ asset('app/triangular/layouts/layouts.module.js')}}"></script>
<script src="{{ asset('app/triangular/layouts/default/default-layout.controller.js')}}"></script>
<script src="{{ asset('app/triangular/layouts/default/default-content.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/components.module.js')}}"></script>
<script src="{{ asset('app/triangular/components/wizard/wizard.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/wizard/wizard-form.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/widget/widget.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/toolbars/toolbar.controller.js')}}"></script>
<script src="{{ asset('app/triangular/components/table/table.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/table/table-start-from.filter.js')}}"></script>
<script src="{{ asset('app/triangular/components/table/table-cell-image.filter.js')}}"></script>
<script src="{{ asset('app/triangular/components/notifications-panel/notifications-panel.controller.js')}}"></script>
<script src="{{ asset('app/triangular/components/menu/menu.provider.js')}}"></script>
<script src="{{ asset('app/triangular/components/menu/menu.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/menu/menu.controller.js')}}"></script>
<script src="{{ asset('app/triangular/components/menu/menu-item.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/loader/loader.directive.js')}}"></script>
<script src="{{ asset('app/triangular/components/loader/loader-service.js')}}"></script>
<script src="{{ asset('app/triangular/components/footer/footer.controller.js')}}"></script>
<script src="{{ asset('app/triangular/components/breadcrumbs/breadcrumbs.service.js')}}"></script>
<script src="{{ asset('app/examples/elements/elements.module.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/upload-animate.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/upload-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/toast-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/table-advanced.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/table-advanced-service.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/table-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/grids-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/fab-speed-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/dialog-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/examples/chips.controller.js')}}"></script>
<script src="{{ asset('app/examples/menu/menu.module.js')}}"></script>
<script src="{{ asset('app/examples/menu/examples/dynamic-menu.controller.js')}}"></script>
<script src="{{ asset('app/examples/maps/maps.module.js')}}"></script>
<script src="{{ asset('app/examples/maps/examples/map-terrain-demo.controller.js')}}"></script>
<script src="{{ asset('app/examples/maps/examples/map-label-demo.controller.js')}}"></script>
<script src="{{ asset('app/examples/forms/forms.module.js')}}"></script>


<script src="{{ asset('app/examples/forms/examples/binding-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/forms/examples/autocomplete-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/dashboards.module.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-weather.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-twitter.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-todo.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-server.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-load-data.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-google-geochart.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-contacts.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chat.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-ticker.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-pie.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-line.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-calendar.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/social/dashboard-social.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/server/dashboard-server.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/sales.service.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/order-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/date-change-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/dashboard-sales.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/analytics/dashboard-analytics.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/charts.module.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-scatter.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-line.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-bar.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-ticker.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-pie.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-line.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-bar.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/authentication.module.js')}}"></script>
<script src="{{ asset('app/examples/authentication/signup/signup.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/profile/profile.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/login/login.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/lock/lock.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/forgot/forgot.controller.js')}}"></script>
<script src="{{ asset('app/triangular/themes/themes.module.js')}}"></script>
<script src="{{ asset('app/triangular/themes/theming.provider.js')}}"></script>
<script src="{{ asset('app/triangular/themes/skins.provider.js')}}"></script>
<script src="{{ asset('app/triangular/profiler/profiler.module.js')}}"></script>
<script src="{{ asset('app/triangular/profiler/profiler.config.js')}}"></script>
<script src="{{ asset('app/triangular/triangular.module.js')}}"></script>
<script src="{{ asset('app/triangular/layouts/layouts.provider.js')}}"></script>
<script src="{{ asset('app/triangular/directives/directives.module.js')}}"></script>
<script src="{{ asset('app/triangular/directives/theme-background.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/same-password.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/palette-background.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/countupto.directive.js')}}"></script>
<script src="{{ asset('app/examples/elements/progress.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/elements.config.js')}}"></script>
<script src="{{ asset('app/examples/elements/buttons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/webfont-loader-module.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.module.js')}}"></script>
<script src="{{ asset('app/examples/ui/weather-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.run.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.config.js')}}"></script>
<script src="{{ asset('app/examples/ui/typography.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/typography-switcher.service.js')}}"></script>
<script src="{{ asset('app/examples/ui/skins.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/material-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/fa-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/colors.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/color-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.module.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.config.js')}}"></script>
<script src="{{ asset('app/examples/todo/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/menu/menu.config.js')}}"></script>
<script src="{{ asset('app/examples/menu/level.controller.js')}}"></script>
<script src="{{ asset('app/examples/menu/dynamicMenu.service.js')}}"></script>
<script src="{{ asset('app/examples/maps/maps.config.js')}}"></script>
<script src="{{ asset('app/examples/maps/map.controller.js')}}"></script>
<script src="{{ asset('app/examples/layouts/layouts.module.js')}}"></script>
<script src="{{ asset('app/examples/layouts/layouts.config.js')}}"></script>
<script src="{{ asset('app/examples/layouts/composer.controller.js')}}"></script>
<script src="{{ asset('app/examples/github/github.module.js')}}"></script>
<script src="{{ asset('app/examples/github/github.controller.js')}}"></script>
<script src="{{ asset('app/examples/github/github.config.js')}}"></script>
<script src="{{ asset('app/examples/forms/forms.config.js')}}"></script>
<script src="{{ asset('app/examples/forms/form-wizard.controller.js')}}"></script>

<script src="{{ asset('app/examples/forms/examples/binding-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/forms/examples/autocomplete-1.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/dashboards.module.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-weather.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-twitter.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-todo.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-server.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-load-data.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-google-geochart.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-contacts.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chat.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-ticker.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-pie.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-chartjs-line.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/widgets/widget-calendar.directive.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/social/dashboard-social.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/server/dashboard-server.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/sales.service.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/order-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/date-change-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/sales/dashboard-sales.controller.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/analytics/dashboard-analytics.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/charts.module.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-scatter.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-line.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/google-bar.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-ticker.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-pie.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-line.controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/examples/chartjs-bar.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/authentication.module.js')}}"></script>
<script src="{{ asset('app/examples/authentication/signup/signup.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/profile/profile.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/login/login.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/lock/lock.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/forgot/forgot.controller.js')}}"></script>
<script src="{{ asset('app/triangular/themes/themes.module.js')}}"></script>
<script src="{{ asset('app/triangular/themes/theming.provider.js')}}"></script>
<script src="{{ asset('app/triangular/themes/skins.provider.js')}}"></script>
<script src="{{ asset('app/triangular/profiler/profiler.module.js')}}"></script>
<script src="{{ asset('app/triangular/profiler/profiler.config.js')}}"></script>
<script src="{{ asset('app/triangular/triangular.module.js')}}"></script>
<script src="{{ asset('app/triangular/layouts/layouts.provider.js')}}"></script>
<script src="{{ asset('app/triangular/directives/directives.module.js')}}"></script>
<script src="{{ asset('app/triangular/directives/theme-background.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/same-password.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/palette-background.directive.js')}}"></script>
<script src="{{ asset('app/triangular/directives/countupto.directive.js')}}"></script>
<script src="{{ asset('app/examples/elements/progress.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/elements/elements.config.js')}}"></script>
<script src="{{ asset('app/examples/elements/buttons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/webfont-loader-module.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.module.js')}}"></script>
<script src="{{ asset('app/examples/ui/weather-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.run.js')}}"></script>
<script src="{{ asset('app/examples/ui/ui.config.js')}}"></script>
<script src="{{ asset('app/examples/ui/typography.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/typography-switcher.service.js')}}"></script>
<script src="{{ asset('app/examples/ui/skins.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/material-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/fa-icons.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/colors.controller.js')}}"></script>
<script src="{{ asset('app/examples/ui/color-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.module.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/todo.config.js')}}"></script>
<script src="{{ asset('app/examples/todo/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/todo/dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/menu/menu.config.js')}}"></script>
<script src="{{ asset('app/examples/menu/level.controller.js')}}"></script>
<script src="{{ asset('app/examples/menu/dynamicMenu.service.js')}}"></script>
<script src="{{ asset('app/examples/maps/maps.config.js')}}"></script>
<script src="{{ asset('app/examples/maps/map.controller.js')}}"></script>
<script src="{{ asset('app/examples/layouts/layouts.module.js')}}"></script>
<script src="{{ asset('app/examples/layouts/layouts.config.js')}}"></script>
<script src="{{ asset('app/examples/layouts/composer.controller.js')}}"></script>
<script src="{{ asset('app/examples/github/github.module.js')}}"></script>
<script src="{{ asset('app/examples/github/github.controller.js')}}"></script>
<script src="{{ asset('app/examples/github/github.config.js')}}"></script>
<script src="{{ asset('app/examples/forms/forms.config.js')}}"></script>
<script src="{{ asset('app/examples/forms/form-wizard.controller.js')}}"></script>

<script src="{{ asset('app/examples/extras/extras.module.js')}}"></script>
<script src="{{ asset('app/examples/extras/timeline.controller.js')}}"></script>

<script src="{{ asset('app/examples/extras/replace-with.directive.js')}}"></script>
<script src="{{ asset('app/examples/extras/gallery.controller.js')}}"></script>
<script src="{{ asset('app/examples/extras/gallery-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/extras/extras.config.js')}}"></script>
<script src="{{ asset('app/examples/extras/avatars.controller.js')}}"></script>
<script src="{{ asset('app/examples/extras/animate-element.directive.js')}}"></script>
<script src="{{ asset('app/examples/email/email.module.js')}}"></script>
<script src="{{ asset('app/examples/email/toolbar.controller.js')}}"></script>
<script src="{{ asset('app/examples/email/inbox.controller.js')}}"></script>
<script src="{{ asset('app/examples/email/emailgroup.filter.js')}}"></script>
<script src="{{ asset('app/examples/email/email.controller.js')}}"></script>
<script src="{{ asset('app/examples/email/email.config.textangular.js')}}"></script>
<script src="{{ asset('app/examples/email/email.config.js')}}"></script>
<script src="{{ asset('app/examples/email/email-search.filter.js')}}"></script>
<script src="{{ asset('app/examples/email/email-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/email/cut.filter.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/dashboards.config.js')}}"></script>
<script src="{{ asset('app/examples/dashboards/dashboard-draggable-controller.js')}}"></script>
<script src="{{ asset('app/examples/charts/charts.config.js')}}"></script>
<script src="{{ asset('app/examples/calendar/calendar.module.js')}}"></script>
<script src="{{ asset('app/examples/calendar/toolbar.controller.js')}}"></script>
<script src="{{ asset('app/examples/calendar/padding.filter.js')}}"></script>
<script src="{{ asset('app/examples/calendar/event-dialog.controller.js')}}"></script>
<script src="{{ asset('app/examples/calendar/calendar.controller.js')}}"></script>
<script src="{{ asset('app/examples/calendar/calendar.config.js')}}"></script>
<script src="{{ asset('app/examples/calendar/calendar-fabs.controller.js')}}"></script>
<script src="{{ asset('app/examples/authentication/authentication.config.js')}}"></script>
<script src="{{ asset('app/triangular/triangular.run.js')}}"></script>
<script src="{{ asset('app/triangular/settings.provider.js')}}"></script>
<script src="{{ asset('app/triangular/config.route.js')}}"></script>
<script src="{{ asset('app/seed-module/seed.module.js')}}"></script>
<script src="{{ asset('app/seed-module/seed.config.js')}}"></script>
<script src="{{ asset('app/seed-module/seed-page.controller.js')}}"></script>
<script src="{{ asset('app/examples/examples.module.js')}}"></script>
<script src="{{ asset('app/app.module.js')}}"></script>
<script src="{{ asset('app/value.googlechart.js')}}"></script>
<script src="{{ asset('app/config.triangular.themes.js')}}"></script>
<script src="{{ asset('app/config.triangular.settings.js')}}"></script>
<script src="{{ asset('app/config.triangular.layout.js')}}"></script>
<script src="{{ asset('app/config.translate.js')}}"></script>
<script src="{{ asset('app/config.route.js')}}"></script>
<script src="{{ asset('app/config.chartjs.js')}}"></script>

<script src="{{ asset('app/examples/tools/tools.module.js')}}"></script>
<script src="{{ asset('app/examples/tools/tools.config.js')}}"></script>


<script src="{{ asset('app/examples/configurations/configurations.module.js')}}"></script>
<script src="{{ asset('app/examples/configurations/configurations.config.js')}}"></script>
<script src="{{ asset('app/examples/configurations/configurations.controller.js')}}"></script>

{{--<script src="{{ asset('app/examples/configurations/weightsAndVolume/weightsAndVolume.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/weightsAndVolume/weightsAndVolume.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/weightsAndVolume/weightsAndVolume-dialog.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/weightsAndVolume/fab-button.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/configurations/distributors/distributors.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/distributors/distributor-dialog.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/tools/tools.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/tools/tools.config.js')}}"></script>--}}

<script src="{{ asset('app/examples/configurations/user/user.service.js')}}"></script>
<script src="{{ asset('app/examples/configurations/user/user.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/user/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/user/dialog.controller.js')}}"></script>


<script src="{{ asset('app/examples/configurations/resource/resource.service.js')}}"></script>
<script src="{{ asset('app/examples/configurations/resource/resource.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/resource/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/resource/resource-dialog.controller.js')}}"></script>


<script src="{{ asset('app/examples/configurations/principal/principal.service.js')}}"></script>
<script src="{{ asset('app/examples/configurations/principal/principal.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/principal/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/principal/principal-dialog.controller.js')}}"></script>



<script src="{{ asset('app/examples/configurations/project/project.service.js')}}"></script>
<script src="{{ asset('app/examples/configurations/project/project.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/project/fab-button.controller.js')}}"></script>
<script src="{{ asset('app/examples/configurations/project/project-dialog.controller.js')}}"></script>


{{--<script src="{{ asset('app/examples/configurations/town/town.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/town/town.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/town/fab-button.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/town/town-dialog.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/configurations/geographicLocation/geographicLocation.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/geographicLocation/geographicLocation.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/geographicLocation/fab-button.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/geographicLocation/geoLocation-dialog.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/configurations/channelAndCategory/channel/channel.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/channelAndCategory/channel/channel.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/channelAndCategory/channel/fab-button.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/channelAndCategory/channel/channel-dialog.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/account/account.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/sales/sales.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/account/account.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/sales/sales.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/account/account.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/account/account.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/sales/sales.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/sales.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/inventory/inventory.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/inventory/inventory.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/inventory/inventory.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/reports/reports.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/reports/reports.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/reports/reports.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/salesForce/sales-force.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/salesForce/sales-force.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/salesForce/sales-force.controller.js')}}"></script>--}}

<!-- endinject -->

{{--<script src="{{ asset('app/examples/security/security.module.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/security/security.config.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/security/security.controller.js')}}"></script>--}}


{{--<script src="{{ asset('app/examples/configurations/distributors/distributors.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/configurations/distributors/fab-button.controller.js')}}"></script>--}}

{{--<script src="{{ asset('app/examples/sales/orderProcessing/order-processing.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/orderProcessing/order-processing.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/orderProcessing/order-processing-dialog.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/orderProcessing/date-change-dialog.controller.js')}}"></script>--}}




{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/retail-outlets.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/maps/map-terrain-demo.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/retailOutletList/retail-outlets-list.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/retailOutletList/retail-outlet-list-service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/retailOutletList/dialog.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/retailOutlets/retailOutlet/retail-outlet-detail.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/route.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/routeList/route-list.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/routeList/route-list.service.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/map/route-map.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/routeList/fab-button.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/routeList/route-dialog.controller.js')}}"></script>--}}
{{--<script src="{{ asset('app/examples/sales/routeAndOutlet/route/route-data-share.service.js')}}"></script>--}}



<!-- endinject -->

<!-- inject:partials -->
<!-- angular templates will be automatically converted in js and inserted here -->
<!-- endinject -->
<!-- endbuild -->
</body>
</html>
