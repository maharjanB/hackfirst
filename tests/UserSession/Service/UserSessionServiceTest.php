<?php

namespace tests\User\Service;

use Modules\User\Repositories\UserSessionRepositoryInterface;
use Modules\User\Services\UserSessionService;
use TestCase;
use Mockery;
use Faker;

class UserSessionServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    /**
     * UserSessionServiceTest constructor.
     */
    public function __construct()
    {
        $this->Repository = $this->createApplication()[UserSessionRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(UserSessionRepositoryInterface::class);
        $this->Service = new UserSessionService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    protected $fillable = ['user_id', 'token', 'latitude', 'longitude', 'created_on', 'expired_on', 'status'];

    public function setUp()
    {
        parent::setUp();

        $createArray = array(
            'user_id' => '1',
            'token' => $this->fakerData->token,
            'latitude' => '1',
            'longitude' => '1',
            'created_on' => '2016-01-01',
            'expired_on' => '2016-01-02',
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_user;
    }

    public function testCreateUserSession()
    {
        //echo __NAMESPACE__;exit();
        $createArray = array(
            'user_id' => '1',
            'token' => $this->fakerData->token,
            'latitude' => '1',
            'longitude' => '1',
            'created_on' => '2016-01-01',
            'expired_on' => '2016-01-02',
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateUserSession()
    {
        $updateArray = array(
            'user_id' => '1',
            'token' => $this->fakerData->token,
            'latitude' => '1',
            'longitude' => '1',
            'created_on' => '2016-01-01',
            'expired_on' => '2016-01-02',
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivateUserSession()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeleteUserSession()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectUserSession()
    {

        //Set up fake values for select parameter
        $field_title = 'id_user_session';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
