<?php

namespace test\CatalogDetail\Service;

use Modules\Configure\Repositories\CatalogDetailRepositoryInterface;
use Modules\Configure\Services\Universe\Catalog\CatalogDetailService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\CatalogDetailTableSeeder;

class CatalogDetailServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $CatalogDetailRepository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->CatalogDetailRepository = $this->createApplication()[CatalogDetailRepositoryInterface::class];
        $this->CatalogDetailRepositoryMock = Mockery::mock(CatalogDetailRepositoryInterface::class);
        $this->Service = new CatalogDetailService($this->CatalogDetailRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        //CatalogDetailTableSeeder::setUpCatalogDetailSeed();

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'catalog_id' => '1',
            'status' => 0,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->CatalogDetailRepository->create($Array);
    }

    /**
     * @covers BuService::createCatalogDetail
     *
     * @todo   Implement testCreateCatalogDetail().
     */
    public function testCreateCatalogDetail()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'catalog_id' => '1',
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->CatalogDetailRepository->create($Array);
        $this->CatalogDetailRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers CatalogDetailService::edit
     *
     * @todo   Implement testUpdateCatalogDetail().
     */
    public function testUpdateCatalogDetail()
    {
        //Set up a fake database row
        $Array = array(
            'title' => 'EDITE Detail',
            'catalog_id' => '2',
            'status' => 0,
        );
        $id = $this->testRowInsert->id_catalog_detail;

        //call real createBu() method of Repository class
        $result = $this->CatalogDetailRepository->update($id, $Array);

        $this->CatalogDetailRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deleteRetailOutlet
     *
     * @todo   Implement testDeleteCatalogDetail().
     */
    public function testDeleteCatalogDetail()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'catalog_id' => '1',
            'status' => 0,
        );

        $createResult = $this->CatalogDetailRepository->create($Array);

        $result = $this->CatalogDetailRepository->delete($createResult->id_catalog_detail);

        $this->CatalogDetailRepositoryMock->shouldReceive('delete')->with($createResult->id_catalog_detail)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_catalog_detail);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateCatalogDetail()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'catalog_id' => '1',
            'status' => 1,
        );

        //call real create() method of Repository class
        $createResult = $this->CatalogDetailRepository->create($Array);
        $result = $this->CatalogDetailRepository->deActivate($createResult->id_catalog_detail);

        $this->CatalogDetailRepositoryMock->shouldReceive('deActivate')->with($createResult->id_catalog_detail)->andReturn($result);

        $resultMock = $this->Service->deactivate($createResult->id_catalog_detail);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectCatalogDetail()
    {
        //Set up fake values for select parameter
        $field_title = 'id_catalog_detail';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->CatalogDetailRepository->select($field_title, $value_title);

        $this->CatalogDetailRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
//        CatalogDetailTableSeeder::cleanDatabase();
    }
}
