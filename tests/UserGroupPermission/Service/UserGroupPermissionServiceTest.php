<?php

namespace tests\User\Service;

use Modules\User\Repositories\UserGroupPermissionRepositoryInterface;
use Modules\User\Services\UserGroupPermissionService;
use TestCase;
use Mockery;
use Faker;

class UserGroupPermissionServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    /**
     * UserGroupPermissionServiceTest constructor.
     */
    public function __construct()
    {
        $this->Repository = $this->createApplication()[UserGroupPermissionRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(UserGroupPermissionRepositoryInterface::class);
        $this->Service = new UserGroupPermissionService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();

        $createArray = array(
            'user_group_id' => $this->fakerData->user_group_id,
            'permission_id' => '1',
            'module_id' => '1',
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_user_group_permission;
    }

    public function testCreateUserGroupPermission()
    {
        //echo __NAMESPACE__;exit();
        $createArray = array(
            'user_group_id' => $this->fakerData->user_group_id,
            'permission_id' => '1',
            'module_id' => '1',
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateUserGroupPermission()
    {
        $updateArray = array(
            'user_group_id' => $this->fakerData->user_group_id,
            'permission_id' => '1',
            'module_id' => '1',
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivateUserGroupPermission()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeleteUserGroupPermission()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectUserGroupPermission()
    {

        //Set up fake values for select parameter
        $field_title = 'id_user_group_permission';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
