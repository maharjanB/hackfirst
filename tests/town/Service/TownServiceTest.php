<?php

namespace tests\town\Service;

use Modules\Configure\Services\Universe\Geography\TownService;
use TestCase;
use Mockery;
use Modules\Configure\Repositories\TownRepositoryInterface;
use Faker;
use Modules\Zseeders\TownTableSeeder;

class TownServiceTest extends TestCase
{
    /**
     * @var TownRepository
     */
    protected $townRepository;
    protected $townRepositoryMock;
    protected $townService;
    protected $fakerData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->townRepository = $this->createApplication()[TownRepositoryInterface::class];
        $this->townRepositoryMock = Mockery::mock(TownRepositoryInterface::class);
        $this->townService = new TownService($this->townRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakerData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        TownTableSeeder::seedTown();
        //Set up a fake database row
        $boundary = array('lat' => 87.66, 'lon' => 88.90);
        $townArray = array(
            'title' => $this->fakerData->name,
            'geographic_location_id' => 1,
            'boundary' => $boundary,
            'status' => '1',
        );

        $this->testRowInsert = $this->townRepository->create($townArray);
    }

    /**
     * @covers TownService::createTown
     *
     * @todo   Implement testCreateTown().
     */
    public function testCreateTown()
    {
        //Set up a fake database row
        $boundary = array('lat' => 87.66, 'lon' => 88.90);
        $townArray = array(
            'title' => $this->fakerData->name,
            'geographic_location_id' => 1,
            'boundary' => $boundary,
            'status' => '1',
        );

        $result = $this->townRepository->create($townArray);
        $this->townRepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->townService->create($townArray);
        //Confirm message of the returned Town is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers TownService::editTown
     *
     * @todo   Implement testEditTown().
     */
    public function testEditTown()
    {
        //Set up a fake array of boundary
        $boundary = array('lat' => 87.66, 'lon' => 88.99);

        //Set update fake data
        $townArray = array(
            'title' => 'Kathmandu'.time(),
            'geographic_location_id' => 1,
            'boundary' => $boundary,
            'status' => 1,
        );
        $id = $this->testRowInsert->id_town;

        $result = $this->townRepository->update($id, $townArray);
        $this->townRepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->townService->edit($id, $townArray);
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers TownService::deleteTown
     *
     * @todo   Implement testDeleteTown().
     */
    public function testDeleteTown()
    {
        $boundary = array('lat' => 87.66, 'lon' => 88.99);
        $CreateArray = array(
            'title' => $this->fakerData->name,
            'geographic_location_id' => 1,
            'boundary' => $boundary,
            'status' => 1,
        );
        $new_data = $this->townRepository->create($CreateArray);
        $result = $this->townRepository->delete($new_data->id_town);
        $this->townRepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->townService->delete($new_data->id_town);
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers TownService::deactivateTown
     *
     * @todo   Implement testDeactivateTown().
     */
    public function testDeactivateTown()
    {
        $boundary = array('lat' => 87.66, 'lon' => 88.99);
        $CreateArray = array(
            'title' => $this->fakerData->name,
            'geographic_location_id' => 1,
            'boundary' => $boundary,
            'status' => 1,
        );
        $new_data = $this->townRepository->create($CreateArray);
        $result = $this->townRepository->deActivate($new_data->id_town);
        $this->townRepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->townService->deactivate($new_data->id_town);
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers TownService::selectTown
     *
     * @todo   Implement testSelectTown().
     */
    public function testSelectTown()
    {
        //Set up fake values for select parameter
        $field_id = 'id_town';
        $value_id = 7;
        $result = $this->townRepository->select($field_id, $value_id);
        $this->townRepositoryMock->shouldReceive('select')->with($field_id, $value_id)->andReturn($result);
        $resultMock = $this->townService->select($field_id, $value_id);
        $this->assertEquals($result, $resultMock);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
 //       TownTableSeeder::cleanDatabase();
    }
}
