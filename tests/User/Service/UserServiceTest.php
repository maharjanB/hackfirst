<?php

namespace tests\User\Service;

use Modules\User\Repositories\UserRepositoryInterface;
use Modules\User\Services\UserService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\UserTableSeeder;

class UserServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    /**
     * UserServiceTest constructor.
     */
    public function __construct()
    {
        $this->Repository = $this->createApplication()[UserRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(UserRepositoryInterface::class);
        $this->Service = new UserService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();
        UserTableSeeder::seedUser();
        $createArray = array(
            'user_group_id' => '1',
            'email' => $this->fakerData->email,
            'password' => $this->fakerData->password(6, 10),
            'first_name' => $this->fakerData->firstName,
            'last_name ' => $this->fakerData->lastName,
            'IMEI_number' => $this->fakerData->phoneNumber,
            'mobile_number' => $this->fakerData->phoneNumber,
            'MAC_id' => $this->fakerData->randomDigitNotNull,
            'auth_type' => $this->fakerData->amPm,
            'password_reset_hash' => $this->fakerData->password(6, 10),
            'password_reset_time' => $this->fakerData->time('H:i:s'),
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_user;
    }

    public function testCreateUser()
    {
        //echo __NAMESPACE__;exit();
        $createArray = array(
            'user_group_id' => '1',
            'email' => $this->fakerData->email,
            'password' => $this->fakerData->password(6, 10),
            'first_name' => $this->fakerData->firstName,
            'last_name ' => $this->fakerData->lastName,
            'IMEI_number' => $this->fakerData->phoneNumber,
            'mobile_number' => $this->fakerData->phoneNumber,
            'MAC_id' => $this->fakerData->randomAscii,
            'auth_type' => $this->fakerData->amPm,
            'password_reset_hash' => $this->fakerData->password(6, 10),
            'password_reset_time' => $this->fakerData->time('H:i:s'),
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateUser()
    {
        $updateArray = array(
            'user_group_id' => '1',
            'email' => $this->fakerData->email,
            'password' => $this->fakerData->password(6, 10),
            'first_name' => $this->fakerData->firstName,
            'last_name ' => $this->fakerData->lastName,
            'IMEI_number' => $this->fakerData->phoneNumber,
            'mobile_number' => $this->fakerData->phoneNumber,
            'MAC_id' => $this->fakerData->randomAscii,
            'auth_type' => $this->fakerData->amPm,
            'password_reset_hash' => $this->fakerData->password(6, 10),
            'password_reset_time' => $this->fakerData->time('H:i:s'),
            'status' => '1',

        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivateUser()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeleteUser()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectUser()
    {

        //Set up fake values for select parameter
        $field_title = 'id_user';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub

        UserTableSeeder::cleanSeeding();
    }
}
