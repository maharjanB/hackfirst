<?php

namespace test\Sbd\Service;

use Modules\Configure\Repositories\SbdRepositoryInterface;
use Modules\Configure\Services\Universe\Sbd\SbdService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\SbdTableSeeder;

class SbdServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $sbdRepository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->sbdRepository = $this->createApplication()[SbdRepositoryInterface::class];
        $this->sbdRepositoryMock = Mockery::mock(SbdRepositoryInterface::class);
        $this->Service = new SbdService($this->sbdRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        SbdTableSeeder::setUpSbdSeed();

        //Set up a fake database row
        $Array = array(
            'catalog_sku_id' => 1,
            'category_id' => 2,
            'compliance' => '1',
            'visibility' => '3',
            'status' => 0,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->sbdRepository->create($Array);
    }

    /**
     * @covers BuService::createSbd
     *
     * @todo   Implement testCreateSbd().
     */
    public function testCreateSbd()
    {
        //Set up a fake database row
        $Array = array(
            'catalog_sku_id' => 1,
            'category_id' => 2,
            'compliance' => '1',
            'visibility' => '3',
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->sbdRepository->create($Array);
        $this->sbdRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers SbdService::edit
     *
     * @todo   Implement testUpdateSbd().
     */
    public function testUpdateSbd()
    {
        //Set up a fake database row
        $Array = array(
            'catalog_sku_id' => 11,
            'category_id' => 21,
            'compliance' => '11',
            'visibility' => '31',
            'status' => 0,
        );
        $id = $this->testRowInsert->id_sbd;

        //call real createBu() method of Repository class
        $result = $this->sbdRepository->update($id, $Array);

        $this->sbdRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deleteRetailOutlet
     *
     * @todo   Implement testDeleteSbd().
     */
    public function testDeleteSbd()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'catalog_sku_id' => 1,
            'category_id' => 2,
            'compliance' => '1',
            'visibility' => '3',
            'status' => 0,
        );

        $createResult = $this->sbdRepository->create($Array);

        $result = $this->sbdRepository->delete($createResult->id_sbd);

        $this->sbdRepositoryMock->shouldReceive('delete')->with($createResult->id_sbd)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_sbd);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateSbd()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'catalog_sku_id' => 1,
            'category_id' => 2,
            'compliance' => '1',
            'visibility' => '3',
            'status' => 1,
        );

        //call real create() method of Repository class
        $createResult = $this->sbdRepository->create($Array);
        $result = $this->sbdRepository->deActivate($createResult->id_sbd);

        $this->sbdRepositoryMock->shouldReceive('deActivate')->with($createResult->id_sbd)->andReturn($result);

        $resultMock = $this->Service->deactivate($createResult->id_sbd);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectSbd()
    {
        //Set up fake values for select parameter
        $field_title = 'id_sbd';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->sbdRepository->select($field_title, $value_title);

        $this->sbdRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
//        SbdTableSeeder::cleanDatabase();
    }
}
