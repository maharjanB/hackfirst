<?php

namespace tests\Unit\Service;

use Modules\Configure\Repositories\UnitRepositoryInterface;
use Modules\Configure\Services\Universe\Unit\UnitService;
use \TestCase;
use Mockery;
use Faker;

class UnitServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    public function __construct()
    {
        $this->Repository = $this->createApplication()[UnitRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(UnitRepositoryInterface::class);
        $this->Service = new UnitService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();$createArray = array(
            'title' => $this->fakerData->name,
        );
        $res = $this->Repository->create($createArray);
        $this->lastInsertData = $res->id_unit;
    }

    public function testCreateUnit()
    {
        $createArray = array(
            'title' => $this->fakerData->name,
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateUnit()
    {
        $updateArray = array(
            'title' => $this->fakerData->name,
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }


    public function testDeleteUnit()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectUnit()
    {

        //Set up fake values for select parameter
        $field_title = 'id_unit';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
