<?php

namespace test\BU\Service;

use Modules\Configure\Repositories\BusinessUnitRepositoryInterface;
use Modules\Configure\Services\Universe\Organization\BusinessUnitService;
use TestCase;
use Mockery;
use Faker;

class BuServiceTest extends TestCase
{
    /**
     * @var BuRepository
     */
    protected $buRepository;
    protected $buRepositoryMock;
    protected $buService;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->buRepository = $this->createApplication()[BusinessUnitRepositoryInterface::class];
        $this->buRepositoryMock = Mockery::mock(BusinessUnitRepositoryInterface::class);
        $this->buService = new  BusinessUnitService($this->buRepositoryMock);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {

        /*
         * Create the row for data checkout in edit, delete and deactivate.
         */

        //Set up a fake database row
        $buArray = array(
            'title' => $this->fakeData->title,
            'status' => 0,
        );

        //call real createBu() method of BuRepository class
        $this->testRowInsert = $this->buRepository->create($buArray);
    }

    /**
     * @covers BuService::createPBu
     *
     * @todo   Implement testCreateBu().
     */
    public function testCreateBu()
    {
        //Set up a fake database row
        $buArray = array(
            'title' => $this->fakeData->title,
            'status' => 0,
        );

        //call real createBu() method of BuRepository class
        $result = $this->buRepository->create($buArray);

        $this->buRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->buService->create($buArray);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers BuService::editBu
     *
     * @todo   Implement testEditBu().
     */
    public function testEditBu()
    {

        //Set up a fake database row
        $buArray = array(
            'title' => $this->fakeData->title,
            'status' => 1,
        );
        $id = $this->testRowInsert->id_business_unit;

        //call real createBu() method of BuRepository class
        $result = $this->buRepository->update($id, $buArray);

        $this->buRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->buService->edit($id, $buArray);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deleteBu
     *
     * @todo   Implement testDeleteBu().
     */
    public function testDeleteBu()
    {
        $result = $this->buRepository->delete($this->testRowInsert->id_business_unit);

        $this->buRepositoryMock->shouldReceive('delete')->with($this->testRowInsert->id_business_unit)->andReturn($result);

        $resultMock = $this->buService->delete($this->testRowInsert->id_business_unit);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deactivateBu
     *
     * @todo   Implement testDeactivateBu().
     */
    public function testDeactivateBu()
    {
        $result = $this->buRepository->deActivate($this->testRowInsert->id_business_unit);

        $this->buRepositoryMock->shouldReceive('deActivate')->with($this->testRowInsert->id_business_unit)->andReturn($result);

        $resultMock = $this->buService->deActivate($this->testRowInsert->id_business_unit);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::selectBu
     *
     * @todo   Implement testselectBu().
     */
    public function testselectBu()
    {
        //Set up fake values for select parameter
        $field_title = 'id_business_unit';
        $value_title = 3;
        /*
                $field_title ="title";
                $value_title = "Test4";

                $field_status ="status";
                $value_status = 0;*/
        //call real createBu() method of BuRepository class
        $result = $this->buRepository->select($field_title, $value_title);

        $this->buRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->buService->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
