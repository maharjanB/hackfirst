<?php

namespace test\RetailOutlet\Service;

use Modules\Sales\Repositories\RetailOutletRepositoryInterface;
use Modules\Sales\Services\RouteAndOutlets\RetailOutlets\RetailOutletService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\RetailOutletTableSeeder;

class RetailOutServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        RetailOutletTableSeeder::setUpRetailOutlet();
        //RetailOutletTableSeeder::setUpRetailOutlet();

        $this->Repository = $this->createApplication()[RetailOutletRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(RetailOutletRepositoryInterface::class);

        $this->Service = new RetailOutletService($this->RepositoryMock);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * @covers BuService::createRetailOutlet
     *
     * @todo   Implement testCreateRetailOut().
     */
    public function testCreateRetailOut()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'contact_number1' => $this->fakeData->phoneNumber,
            'contact_number2' => $this->fakeData->phoneNumber,
            'contact_name' => $this->fakeData->phoneNumber,
            'geolocation_longitude' => '90.09',
            'geolocation_latitude' => '099.99',
            'channel_id' => 1,
            'category_id' => 1,
            'town_id' => 1,
            'PAN_number' => $this->fakeData->phoneNumber,
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->Repository->create($Array);

        $this->RepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers BuService::edit
     *
     * @todo   Implement testEditRetailOutlet().
     */
    public function testEditRetailOut()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'contact_number1' => $this->fakeData->phoneNumber,
            'contact_number2' => $this->fakeData->phoneNumber,
            'contact_name' => $this->fakeData->phoneNumber,
            'geolocation_longitude' => '90.89',
            'geolocation_latitude' => '79.90',
            'channel_id' => 1,
            'category_id' => 1,
            'town_id' => 1,
            'PAN_number' => $this->fakeData->phoneNumber,
            'status' => 1,
        );
        $id = 1;

        //call real createBu() method of BuRepository class
        $result = $this->Repository->update($id, $Array);

        $this->RepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deleteRetailOutlet
     *
     * @todo   Implement testDeleteRetailOutlet().
     */
    public function testDeleteRetailOut()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'contact_number1' => $this->fakeData->phoneNumber,
            'contact_number2' => $this->fakeData->phoneNumber,
            'contact_name' => $this->fakeData->phoneNumber,
            'geolocation_longitude' => '56.34',
            'geolocation_latitude' => '12.90',
            'channel_id' => '1',
            'category_id' => '1',
            'town_id' => '1',
            'PAN_number' => $this->fakeData->phoneNumber,
            'status' => '1',
        );

        $createResult = $this->Repository->create($Array);

        $result = $this->Repository->delete($createResult->id_retail_outlet);

        $this->RepositoryMock->shouldReceive('delete')->with($createResult->id_retail_outlet)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_retail_outlet);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateRetailOut()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'contact_number1' => $this->fakeData->phoneNumber,
            'contact_number2' => $this->fakeData->phoneNumber,
            'contact_name' => $this->fakeData->phoneNumber,
            'geolocation_longitude' => $this->fakeData->randomNumber(5),
            'geolocation_latitude' => $this->fakeData->randomNumber(5),
            'channel_id' => 1,
            'category_id' => 1,
            'town_id' => 1,
            'PAN_number' => $this->fakeData->randomNumber(5),
            'status' => 1,
        );

        //call real createBu() method of Repository class
        $createResult = $this->Repository->create($Array);

        $result = $this->Repository->deActivate($createResult->id_retail_outlet);

        $this->RepositoryMock->shouldReceive('deActivate')->with($createResult->id_retail_outlet)->andReturn($result);

        $resultMock = $this->Service->deActivate($createResult->id_retail_outlet);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectRetailOutlet()
    {
        //Set up fake values for select parameter
        $field_title = 'id_retail_outlet';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        // parent::tearDown(); // TODO: Change the autogenerated stub
       // RetailOutletTableSeeder::cleanDatabase();
    }
}
