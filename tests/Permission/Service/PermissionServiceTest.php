<?php

namespace tests\User\Service;

use Modules\User\Repositories\PermissionRepositoryInterface;
use Modules\User\Services\PermissionService;
use TestCase;
use Mockery;
use Faker;

class PermissionServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    /**
     * PermissionServiceTest constructor.
     */
    public function __construct()
    {
        $this->Repository = $this->createApplication()[PermissionRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(PermissionRepositoryInterface::class);
        $this->Service = new PermissionService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();

        $createArray = array(
            'description' => $this->fakerData->description,
            'role_name' => '1',
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_permission;
    }

    public function testCreatePermission()
    {
        //echo __NAMESPACE__;exit();
        $createArray = array(
            'description' => $this->fakerData->description,
            'role_name' => '1',
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdatePermission()
    {
        $updateArray = array(
            'description' => $this->fakerData->description,
            'role_name' => '1',
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivatePermission()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeletePermission()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectPermission()
    {

        //Set up fake values for select parameter
        $field_title = 'id_permission';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
