<?php

namespace test\RouteVisitFrequency\Service;

use Modules\Configure\Repositories\RouteVisitFrequencyRepository;
use Modules\Configure\Services\Universe\Routes\RouteVisitFrequencyService;
use TestCase;
use Mockery;
use Faker;

class RouteVisitFrequencyServiceTest extends TestCase
{
    /**
     * @var BuRepository
     */
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->Repository = $this->createApplication()[RouteVisitFrequencyRepository::class];
        $this->RepositoryMock = Mockery::mock(RouteVisitFrequencyRepository::class);

        $this->Service = new RouteVisitFrequencyService($this->RepositoryMock);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->title,
            'frequency' => '1',
            'status' => 1,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->Repository->create($Array);
    }

    /**
     * @covers
     *
     * @todo   Implement testCreate().
     */
    public function testCreateRouteVisitFrequency()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->title,
            'frequency' => '1',
            'status' => 1,
        );

        //call real create() method of Repository class
        $result = $this->Repository->create($Array);

        $this->RepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers BuService::editB
     *
     * @todo   Implement testEdit().
     */
    public function testEditRouteVisitFrequency()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'frequency' => '1',
            'status' => 1,
        );
        $id = $this->testRowInsert->id_route_visit_frequency;

        //call real createBu() method of BuRepository class
        $result = $this->Repository->update($id, $Array);

        $this->RepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testDelete().
     */
    public function testDeleteRouteVisitFrequency()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'frequency' => '1',
            'status' => 0,
        );

        $createResult = $this->Repository->create($Array);

        $result = $this->Repository->delete($createResult->id_route_visit_frequency);

        $this->RepositoryMock->shouldReceive('delete')->with($createResult->id_route_visit_frequency)
            ->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_route_visit_frequency);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testDeactivate().
     */
    public function testDeactivateRouteVisitFrequency()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'frequency' => '1',
            'status' => 0,
        );
        $createResult = $this->Repository->create($Array);

        $result = $this->Repository->deActivate($createResult->id_route_visit_frequency);

        $this->RepositoryMock->shouldReceive('deActivate')->with($createResult->id_route_visit_frequency)
            ->andReturn($result);

        $resultMock = $this->Service->deActivate($createResult->id_route_visit_frequency);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testselect().
     */
    public function testSelectRouteVisitFrequency()
    {
        //Set up fake values for select parameter
        $field_title = 'id_route_visit_frequency';
        $value_title = 1;

        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
