<?php

namespace test\CatalogSku\Service;

use Modules\Configure\Repositories\CatalogSkuRepositoryInterface;
use Modules\Configure\Services\Universe\Catalog\CatalogSkuService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\CatalogSkuTableSeeder;

class CatalogSkuServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $catalogSkuRepository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->catalogSkuRepository = $this->createApplication()[CatalogSkuRepositoryInterface::class];
        $this->catalogSkuRepositoryMock = Mockery::mock(CatalogSkuRepositoryInterface::class);
        $this->Service = new CatalogSkuService($this->catalogSkuRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
//        CatalogSkuTableSeeder::setUpCatalogSkuSeed();

        //Set up a fake database row
        $Array = array(
            //'title' => $this->fakeData->name,
            'catalog_detail_id' => '1',
            'sku_id' => 1,
            'status' => 0,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->catalogSkuRepository->create($Array);
    }

    /**
     * @covers BuService::createCatalogSku
     *
     * @todo   Implement testCreateCatalogSku().
     */
    public function testCreateCatalogSku()
    {
        //Set up a fake database row
        $Array = array(
            // 'title' => $this->fakeData->name,
            'catalog_detail_id' => '1',
            'sku_id' => 1,
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->catalogSkuRepository->create($Array);
        $this->catalogSkuRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers CatalogSkuService::edit
     *
     * @todo   Implement testUpdateCatalogSku().
     */
    public function testUpdateCatalogSku()
    {
        //Set up a fake database row
        $Array = array(
            'catalog_detail_id' => '2',
            'sku_id' => 2,
            'status' => 0,
        );
        $id = $this->testRowInsert->id_catalog_sku;

        //call real createBu() method of Repository class
        $result = $this->catalogSkuRepository->update($id, $Array);

        $this->catalogSkuRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deleteRetailOutlet
     *
     * @todo   Implement testDeleteCatalogSku().
     */
    public function testDeleteCatalogSku()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'catalog_detail_id' => '1',
            'sku_id' => 1,
            'status' => 0,
        );

        $createResult = $this->catalogSkuRepository->create($Array);

        $result = $this->catalogSkuRepository->delete($createResult->id_catalog_sku);

        $this->catalogSkuRepositoryMock->shouldReceive('delete')->with($createResult->id_catalog_sku)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_catalog_sku);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateCatalogSku()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'catalog_detail_id' => '1',
            'sku_id' => 1,
            'status' => 1,
        );

        //call real create() method of Repository class
        $createResult = $this->catalogSkuRepository->create($Array);
        $result = $this->catalogSkuRepository->deActivate($createResult->id_catalog_sku);

        $this->catalogSkuRepositoryMock->shouldReceive('deActivate')->with($createResult->id_catalog_sku)->andReturn($result);

        $resultMock = $this->Service->deactivate($createResult->id_catalog_sku);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectCatalogSku()
    {
        //Set up fake values for select parameter
        $field_title = 'id_catalog_sku';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->catalogSkuRepository->select($field_title, $value_title);

        $this->catalogSkuRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
//        CatalogSkuTableSeeder::cleanDatabase();
    }
}
