<?php

namespace test\PRINCIPAL\Service;

use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Modules\Configure\Services\Universe\Organization\PrincipalService;
use TestCase;
use Mockery;
use Faker;

class PrincipalServiceTest extends TestCase
{
    /**
     * @var BuRepository
     */
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    public $testRowInsert;

    public function __construct()
    {
        $this->principalRepository = $this->createApplication()[PrincipalRepositoryInterface::class];
        $this->principalRepositoryMock = Mockery::mock(PrincipalRepositoryInterface::class);

        $this->Service = new PrincipalService($this->principalRepositoryMock);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        //Set up a fake database row
        $Array = [
            'title' => $this->fakeData->title,
            'status' => 0,
        ];

        //call real create() method of Repository class
        $this->testRowInsert = $this->principalRepository->create($Array);
    }

    /**
     * @covers BuService::createPBu
     *
     * @todo   Implement testCreateBu().
     */
    public function testCreateP()
    {
        //Set up a fake database row
        $Array = [
            'title' => $this->fakeData->title,
            'status' => 0,
        ];

        //call real create() method of Repository class
        $result = $this->principalRepository->create($Array);

        $this->principalRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers BuService::editBu
     *
     * @todo   Implement testEdit().
     */
    public function testEditP()
    {
        //Set up a fake database row
        $Array = [
            'title' => $this->fakeData->title,
            'status' => 1,
        ];
        $id = $this->testRowInsert->id_principal;

        //call real createBu() method of BuRepository class
        $result = $this->principalRepository->update($id, $Array);

        $this->principalRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deleteBu
     *
     * @todo   Implement testDelete().
     */
    public function testDeleteP()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = [
            'title' => $this->fakeData->title,
            'status' => 0,
        ];

        $createResult = $this->principalRepository->create($Array);

        $result = $this->principalRepository->delete($createResult->id_principal);

        $this->principalRepositoryMock->shouldReceive('delete')->with($createResult->id_principal)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_principal);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deactivateBu
     *
     * @todo   Implement testDeactivate().
     */
    public function testDeactivateP()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = [
            'title' => $this->fakeData->title,
            'status' => 0,
        ];

        //call real createBu() method of Repository class
        $createResult = $this->principalRepository->create($Array);

        $result = $this->principalRepository->deActivate($createResult->id_principal);

        $this->principalRepositoryMock->shouldReceive('deActivate')->with($createResult->id_principal)->andReturn($result);

        $resultMock = $this->Service->deActivate($createResult->id_principal);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testselect().
     */
    public function testselect()
    {
        //Set up fake values for select parameter
        $field_title = 'id_principal';
        $value_title = 1;
        /*
                $field_title ="title";
                $value_title = "Test4";

                $field_status ="status";
                $value_status = 0;*/
        //call real createBu() method of BuRepository class
        $result = $this->principalRepository->select($field_title, $value_title);

        $this->principalRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
