<?php

namespace test\Logging\Service;

use Modules\Configure\Repositories\LoggingRepositoryInterface;
use Modules\Configure\Services\Universe\Outlets\LoggingService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\LoggingTableSeeder;

class LoggingServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $loggingRepository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->loggingRepository = $this->createApplication()[LoggingRepositoryInterface::class];
        $this->loggingRepositoryMock = Mockery::mock(LoggingRepositoryInterface::class);
        $this->Service = new LoggingService($this->loggingRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }
    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        LoggingTableSeeder::setUpLoggingSeed();

        //Set up a fake database row
        $Array = array(
            'module_name' => $this->fakeData->name,
            'label' => 1,
            'value' => '1',
            'status' => 0,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->loggingRepository->create($Array);
    }

    /**
     * @covers BuService::createLogging
     *
     * @todo   Implement testCreateLogging().
     */
    public function testCreateLogging()
    {
        //Set up a fake database row
        $Array = array(
            'module_name' => $this->fakeData->name,
            'label' => 1,
            'value' => '1',
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->loggingRepository->create($Array);
        $this->loggingRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers LoggingService::edit
     *
     * @todo   Implement testUpdateLogging().
     */
    public function testUpdateLogging()
    {
        //Set up a fake database row
        $Array = array(
            'module_name' => 'EDITED',
            'label' => 1,
            'value' => '1',
            'status' => 0,
        );
        $id = $this->testRowInsert->id_logging;

        //call real createBu() method of Repository class
        $result = $this->loggingRepository->update($id, $Array);

        $this->loggingRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deleteRetailOutlet
     *
     * @todo   Implement testDeleteLogging().
     */
    public function testDeleteLogging()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'module_name' => $this->fakeData->name,
            'label' => 1,
            'value' => '1',
            'status' => 0,
        );

        $createResult = $this->loggingRepository->create($Array);

        $result = $this->loggingRepository->delete($createResult->id_logging);

        $this->loggingRepositoryMock->shouldReceive('delete')->with($createResult->id_logging)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_logging);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateLogging()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'module_name' => $this->fakeData->name,
            'label' => 1,
            'value' => '1',
            'status' => 1,
        );

        //call real create() method of Repository class
        $createResult = $this->loggingRepository->create($Array);
        $result = $this->loggingRepository->deActivate($createResult->id_logging);

        $this->loggingRepositoryMock->shouldReceive('deActivate')->with($createResult->id_logging)->andReturn($result);

        $resultMock = $this->Service->deactivate($createResult->id_logging);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectLogging()
    {
        //Set up fake values for select parameter
        $field_title = 'id_logging';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->loggingRepository->select($field_title, $value_title);

        $this->loggingRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
//        LoggingTableSeeder::cleanDatabase();
    }
}
