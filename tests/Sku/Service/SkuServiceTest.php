<?php

namespace test\Sku\Service;

use Modules\Configure\Repositories\SkuRepositoryInterface;
use Modules\Configure\Services\Universe\Catalog\SkuService;
use Modules\Zseeders\SkustockTableSeeder;
use TestCase;
use Mockery;
use Faker;

class SkuServiceTest extends TestCase
{
    /**
     * @var skuRepository
     */
    protected $skuRepository;
    protected $skuRepositoryMock;
    protected $skuService;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->skuRepository = $this->createApplication()[SkuRepositoryInterface::class];
        $this->skuRepositoryMock = Mockery::mock(SkuRepositoryInterface::class);
        $this->skuService = new SkuService($this->skuRepositoryMock);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();

        SkustockTableSeeder::seedSkustock();
        /*
         * Create the row for data checkout in edit, delete and deactivate.
         */

        //Set up a fake database row
        $skuArray = [
            'title' => $this->fakeData->title,
            'GTIN' => $this->fakeData->randomNumber(4),
            'EAN' => $this->fakeData->randomNumber(4),
            'tag' => $this->fakeData->title,
            'PSKU' => 1,
            'SU' => 1,
            'business_unit_id' => 1,
            'status' => 1,
        ];

        //call real createsku() method of skuRepository class
        $this->testRowInsert = $this->skuRepository->create($skuArray);
    }

    /**
     * @covers skuService::createPsku
     *
     * @todo   Implement testCreatesku().
     */
    public function testCreateSku()
    {
        //Set up a fake database row
        $skuArray = [
            'title' => $this->fakeData->title,
            'GTIN' => $this->fakeData->randomNumber(4),
            'EAN' => $this->fakeData->randomNumber(4),
            'tag' => $this->fakeData->title,
            'PSKU' => 1,
            'SU' => 1,
            'business_unit_id' => 1,
        ];

        //call real createsku() method of skuRepository class
        $result = $this->skuRepository->create($skuArray);

        $this->skuRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->skuService->create($skuArray);

        //Confirm message of the returned sku is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers skuService::editsku
     *
     * @todo   Implement testEditsku().
     */
    public function testEditSku()
    {

        //Set up a fake database row
        $skuArray = [
            'title' => $this->fakeData->title,
            'business_unit_id' => 1,
        ];
        $id = $this->testRowInsert->id_sku;

        //call real createsku() method of skuRepository class
        $result = $this->skuRepository->update($id, $skuArray);

        $this->skuRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->skuService->edit($id, $skuArray);

        //Confirm message of the returned sku is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers skuService::deletesku
     *
     * @todo   Implement testDeletesku().
     */
    public function testDeleteSku()
    {
        $result = $this->skuRepository->delete($this->testRowInsert->id_sku);

        $this->skuRepositoryMock->shouldReceive('delete')->with($this->testRowInsert->id_sku)->andReturn($result);

        $resultMock = $this->skuService->delete($this->testRowInsert->id_sku);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers skuService::deactivatesku
     *
     * @todo   Implement testDeactivatesku().
     */
    public function testDeactivateSku()
    {
        $result = $this->skuRepository->deActivate($this->testRowInsert->id_sku);

        $this->skuRepositoryMock->shouldReceive('deActivate')->with($this->testRowInsert->id_sku)->andReturn($result);

        $resultMock = $this->skuService->deActivate($this->testRowInsert->id_sku);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers skuService::selectsku
     *
     * @todo   Implement testselectsku().
     */
    public function testSelectSku()
    {
        //Set up fake values for select parameter
        $field_title = 'id_sku';
        $value_title = 3;
        /*
                $field_title ="title";
                $value_title = "Test4";

                $field_status ="status";
                $value_status = 0;*/
        //call real createsku() method of skuRepository class
        $result = $this->skuRepository->select($field_title, $value_title);

        $this->skuRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->skuService->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
