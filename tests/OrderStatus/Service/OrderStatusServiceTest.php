<?php
/**
 * Created by PhpStorm.
 * User: binita
 * Date: 2/18/16
 * Time: 12:15 PM
 */

namespace tests\OrderStatus\Service;


use Modules\Sales\Entities\OrderStatusInterface;
use Modules\Sales\Repositories\OrderStatusRepositoryInterface;
use Modules\Sales\Services\OrderProcessing\Orders\OrderStatusService;
use \TestCase;
use Mockery;
use Faker;

class OrderStatusServiceTest extends TestCase
{

    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    public function __construct()
    {
        $this->Repository = $this->createApplication()[OrderStatusRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(OrderStatusRepositoryInterface::class);
        $this->Service = new OrderStatusService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();
        $createArray = array(
            'title' => $this->fakerData->name,
            'status' => '1',
            );
        $res = $this->Repository->create($createArray);
        $this->lastInsertData = $res->id_order_status;
    }

    /**
     *
     */
    public function testCreateOrderStatus()
    {
        $createArray = array(
            'title' => $this->fakerData->name,
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateOrderStatus()
    {
        $updateArray = array(
            'title' => $this->fakerData->name,
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivateOrderStatus()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeleteOrderStatus()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectOrderStatus()
    {

        //Set up fake values for select parameter
        $field_title = 'id_order_status';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

}