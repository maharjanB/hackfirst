<?php

namespace test\Route\Service;

use Modules\Sales\Repositories\RouteRepositoryInterface;
use Modules\Sales\Services\RouteAndOutlets\Routes\RouteService;
use TestCase;
use Mockery;
use Faker;
use Modules\Zseeders\RouteTableSeeder;

class RouteServiceTest extends TestCase
{
    /**
     * @var Repository
     */
    protected $routeRepository;
    protected $RepositoryMock;
    protected $Service;
    public $fakeData;
    protected $testRowInsert;

    public function __construct()
    {
        $this->routeRepository = $this->createApplication()[RouteRepositoryInterface::class];
        $this->routeRepositoryMock = Mockery::mock(RouteRepositoryInterface::class);
        $this->Service = new RouteService($this->routeRepositoryMock);
        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }
    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        RouteTableSeeder::setUpRouteSeed();

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'route_visit_category_id' => 1,
            'route_visit_frequency_id' => '1',
            'route_visit_type_id' => 1,
            'route_delivery_type_id' => 1,
            'status' => 0,
        );

        //call real create() method of Repository class
        $this->testRowInsert = $this->routeRepository->create($Array);
    }

    /**
     * @covers BuService::createRoute
     *
     * @todo   Implement testCreateRoute().
     */
    public function testCreateRoute()
    {
        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'route_visit_category_id' => 1,
            'route_visit_frequency_id' => '1',
            'route_visit_type_id' => 1,
            'route_delivery_type_id' => 1,
            'status' => 0,
        );

        //call real create() method of Repository class
        $result = $this->routeRepository->create($Array);
        $this->routeRepositoryMock->shouldReceive('create')->andReturn($result);

        $resultMock = $this->Service->create($Array);

        //Confirm message of the returned is correct
        $this->assertEquals($resultMock, $result);
    }

    /**
     * @covers RouteService::edit
     *
     * @todo   Implement testUpdateRoute().
     */
    public function testUpdateRoute()
    {
        //Set up a fake database row
        $Array = array(
            'title' => 'EDITE_ROUTE',
            'route_visit_category_id' => 1,
            'route_visit_type_id' => 1,
            'route_delivery_type_id' => 1,
            'status' => 0,
        );
        $id = $this->testRowInsert->id_route;

        //call real createBu() method of Repository class
        $result = $this->routeRepository->update($id, $Array);

        $this->routeRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->Service->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deleteRetailOutlet
     *
     * @todo   Implement testDeleteRoute().
     */
    public function testDeleteRoute()
    {

        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'route_visit_category_id' => 1,
            'route_visit_frequency_id' => '1',
            'route_visit_type_id' => 1,
            'route_delivery_type_id' => 1,
            'status' => 0,
        );

        $createResult = $this->routeRepository->create($Array);

        $result = $this->routeRepository->delete($createResult->id_route);

        $this->routeRepositoryMock->shouldReceive('delete')->with($createResult->id_route)->andReturn($result);

        $resultMock = $this->Service->delete($createResult->id_route);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers Service::deactivateBuf
     *
     * @todo   Implement testDeactivateRetailOutlet().
     */
    public function testDeactivateRoute()
    {
        //create a row in database to delete

        //Set up a fake database row
        $Array = array(
            'title' => $this->fakeData->name,
            'route_visit_category_id' => 1,
            'route_visit_frequency_id' => '1',
            'route_visit_type_id' => 1,
            'route_delivery_type_id' => 1,
            'status' => 1,
        );

        //call real create() method of Repository class
        $createResult = $this->routeRepository->create($Array);
        $result = $this->routeRepository->deActivate($createResult->id_route);

        $this->routeRepositoryMock->shouldReceive('deActivate')->with($createResult->id_route)->andReturn($result);

        $resultMock = $this->Service->deactivate($createResult->id_route);

        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers
     *
     * @todo   Implement testSelectRetailOutlet().
     */
    public function testSelectRoute()
    {
        //Set up fake values for select parameter
        $field_title = 'id_route';
        $value_title = 1;
        //call real create() method of Repository class
        $result = $this->routeRepository->select($field_title, $value_title);

        $this->routeRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
//        RouteTableSeeder::cleanDatabase();
    }
}
