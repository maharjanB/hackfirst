<?php

namespace tests\User\Service;

use Modules\User\Repositories\UserGroupRepositoryInterface;
use Modules\User\Services\UserGroupService;
use TestCase;
use Mockery;
use Faker;

class UserGroupServiceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    /**
     * UserGroupServiceTest constructor.
     */
    public function __construct()
    {
        $this->Repository = $this->createApplication()[UserGroupRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(UserGroupRepositoryInterface::class);
        $this->Service = new UserGroupService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();

        $createArray = array(
            'parent_group_id' => '1',
            'group_name' => $this->fakerData->group_name,
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_user_group;
    }

    public function testCreateUserGroup()
    {
        //echo __NAMESPACE__;exit();
        $createArray = array(
            'parent_group_id' => '1',
            'group_name' => $this->fakerData->group_name,
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testUpdateUserGroup()
    {
        $updateArray = array(
            'parent_group_id' => '1',
            'group_name' => $this->fakerData->group_name,
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeactivateUserGroup()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDeleteUserGroup()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testSelectUserGroup()
    {

        //Set up fake values for select parameter
        $field_title = 'id_user_group';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
