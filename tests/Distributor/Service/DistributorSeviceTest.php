<?php

namespace tests\Distributor\Service;

use Modules\Configure\Repositories\ResourceRepositoryInterface;
use Modules\Configure\Services\Universe\Resource\ResourceService;
use TestCase;
use Mockery;
use Faker;

class DistributorSeviceTest extends TestCase
{
    protected $Repository;
    protected $RepositoryMock;
    protected $Service;
    protected $fakerData;
    private $lastInsertData;

    public function __construct()
    {
        $this->Repository = $this->createApplication()[ResourceRepositoryInterface::class];
        $this->RepositoryMock = Mockery::mock(ResourceRepositoryInterface::class);
        $this->Service = new ResourceService($this->RepositoryMock);
        $faker = new Faker\Factory();
        $this->fakerData = $faker->create();
    }

    public function setUp()
    {
        parent::setUp();
        $createArray = array(
            'title' => '',
            'description' => $this->fakerData->paragraph(10),
            'status' => '1',
        );
        $res = $this->Repository->create($createArray);

        $this->lastInsertData = $res->id_distributor;
    }

    public function testDiCreate()
    {
        $createArray = array(
            'title' => $this->fakerData->name,
            'description' => $this->fakerData->paragraph(10),
            'status' => '1',
        );
        $result = $this->Repository->create($createArray);
        $this->RepositoryMock->shouldReceive('create')->andReturn($result);
        $resultMock = $this->Service->create($createArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDistributorUpdate()
    {
        $updateArray = array(
            'title' => $this->fakerData->name,
            'description' => $this->fakerData->paragraph(10),
            'status' => '1',
        );
        $id = $this->lastInsertData;
        $result = $this->Repository->update($id, $updateArray);
        $this->RepositoryMock->shouldReceive('update')->andReturn($result);
        $resultMock = $this->Service->edit($id, $updateArray);
        $this->assertEquals($resultMock, $result);
    }

    public function testDistributorDeactivate()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->deActivate($id);
        $this->RepositoryMock->shouldReceive('deActivate')->andReturn($result);
        $resultMock = $this->Service->deactivate($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDistributorDelete()
    {
        $id = $this->lastInsertData;
        $result = $this->Repository->delete($id);
        $this->RepositoryMock->shouldReceive('delete')->andReturn($result);
        $resultMock = $this->Service->delete($id);
        $this->assertEquals($resultMock, $result);
    }

    public function testDistributorSelect()
    {

        //Set up fake values for select parameter
        $field_title = 'id_distributor';
        $value_title = 4;
        //call real create() method of Repository class
        $result = $this->Repository->select($field_title, $value_title);

        $this->RepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->Service->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }
}
