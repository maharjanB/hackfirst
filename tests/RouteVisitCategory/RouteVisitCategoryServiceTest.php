<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 2/11/2016
 * Time: 9:49 PM
 */

namespace tests\RouteVisitCategory;

use Modules\Configure\Services\Universe\Routes\RouteVisitCategoryService;
use Modules\Configure\Repositories\RouteVisitCategoryRepositoryInterface;
use TestCase;
use Mockery;
use Faker;

class RouteVisitCategoryServiceTest extends TestCase
{

    /**
     * @var mixed|object
     *
     */
    protected $visitRepository;
    protected $visitRepositoryMock;
    protected $visitService;
    public $fakeData;
    public $testRowInsert;
    protected $testRowInsertId;

    public function __construct()
    {
        $this->visitRepository = $this->createApplication()[RouteVisitCategoryRepositoryInterface::class];
        $this->visitRepositoryMock = Mockery::mock(RouteVisitCategoryRepositoryInterface::class);
        $this->visitService = new RouteVisitCategoryService($this->visitRepositoryMock);
     //   dd($this->visitService);

        $faker = new Faker\Factory(); //::create();
        $this->fakeData = $faker->create();
    }

    /**
     * Sets up the Repository.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();
        $Array = [
            'title' => $this->fakeData->name,
        ];

        //call real create() method of Repository class
        $result = $this->visitService->create($Array);
        $this->testRowInsert = $result->id_route_visit_category;


    }

    /**
     * @covers Service::createRouteSchedule
     * @todo   Implement testCreateRouteVisitCategory
     */
    public function testCreateRouteVisitCategory()
    {
        //Set up a fake database row

        $Array = [
            'title' => $this->fakeData->name,
        ];

        //call real create() method of Repository class
        $result = $this->visitRepository->create($Array);

       // $this->visitRepositoryMock->shouldReceive('create')->andReturn($result);

       // $resultMock = $this->visitService->create($Array);

        //Confirm message of the returned is correct
       // $this->assertEquals($resultMock, $result);

    }

    /**
     * @covers Service::editRouteVisitCategory
     * @todo   Implement testEditRouteVisitCategory().
     */
    public function testEditRouteVisitCategory()
    {
        $Array = [
            'route_id' => 1,
            'retail_outlet_id' => '1',
            'schedule' => $this->fakeData->date('Y-m-d'),
        ];
        $id = $this->testRowInsert;

        //call real create() method of Repository class
        $result = $this->visitRepository->update($id, $Array);

        $this->visitRepositoryMock->shouldReceive('update')->andReturn($result);

        $resultMock = $this->visitService->edit($id, $Array);

        //Confirm message of the returned Bu is correct
        $this->assertEquals($result, $resultMock);
    }

    /**
     * @covers BuService::deleteRouteVisitCategory
     * @todo   Implement testDeleteRouteVisitCategory().
     */
    public function testDeleteRouteVisitCategory()
    {
        $id = $this->testRowInsert;

        $result = $this->visitRepository->delete($id);

        $this->visitRepositoryMock->shouldReceive('delete')->with($id)->andReturn($result);

        $resultMock = $this->visitService->delete($id);

        $this->assertEquals($result, $resultMock);

    }


    /**
     * @covers
     * @todo   Implement testSelectRouteSchedule().
     */
    public function testSelectRouteVisitCategory()
    {
        //Set up fake values for select parameter
        $field_title = "id_route_visit_category";
        $value_title = 1;

        //call real create() method of  Repository class
        $result = $this->visitRepository->select($field_title, $value_title);

        $this->visitRepositoryMock->shouldReceive('select')->with($field_title, $value_title)->andReturn($result);

        $resultMock = $this->visitService->select($field_title, $value_title);

        $this->assertEquals($resultMock, $result);
    }

    public function tearDown()
    {
        parent::tearDown(); // TODO: Change the autogenerated stub
    }
}
