<?php

namespace Modules;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $user = ['id' => 1];
            $model->created_by = $user['id'];
            $model->updated_by = $user['id'];
        });
        static::updating(function ($model) {
            $user = ['id' => 1];
            $model->updated_by = $user['id'];
        });
    }

    public function creator()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'created_by', 'id');
    }

    public function updater()
    {
        return $this->belongsTo('Modules\User\Entities\User', 'updated_by', 'id');
    }

    public function findById($id)
    {
        return $this->find($id);
    }
}
