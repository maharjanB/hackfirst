<?php

namespace Modules\Inventory\Repositories;

use Modules\AbstractRepository;
use Modules\Inventory\Entities\SkuStockInterface;

class SkuStockRepository extends AbstractRepository implements SkuStockRepositoryInterface
{
    /**
     * The Model instance.
     *
     * @var SkustockInterface
     */
    protected $model;

    /**
     * Create a new TownRepository instance.
     *
     * @param SkustockInterface $model
     */
    public function __construct(SkuStockInterface $model)
    {
        $this->model = $model;
    }
}
