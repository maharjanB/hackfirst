<?php

namespace Modules\Inventory\Repositories;

use Modules\BaseRepositoryInterface;

interface SkuStockRepositoryInterface extends BaseRepositoryInterface
{
}
