<?php

namespace Modules\Inventory\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class SkuStockValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'sku_id' => 'required',
        'invoice_number' => 'required',
        'batch_number' => 'required',
        'expiry_date' => 'required',
        'manufacture_date' => 'required',
        'price' => 'required',
        'distributor_id' => 'required',
        'status' => 'required',

    );
}
