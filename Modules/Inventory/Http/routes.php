<?php

Route::group(['prefix' => 'inventory', 'namespace' => 'Modules\Inventory\Http\Controllers'], function () {
    Route::get('/', 'InventoryController@index');
});
