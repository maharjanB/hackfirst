<?php

namespace Modules\Inventory\Services\StockLedger;

use Modules\AbstractService;
use Modules\Inventory\Repositories\SkuStockRepositoryInterface;
use Modules\Inventory\Validators\SkuStockValidator;

class SkuStockService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(SkuStockRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new SkuStockValidator();
    }
}
