<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateSkuStockTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('sku_stock', function (CustomBluePrint $table) {
            $table->increments('id_sku_stock');
            $table->integer('sku_id')->unsigned();
            $table->string('invoice_number');
            $table->string('batch_number');
            $table->date('expiry_date');
            $table->date('manufacture_date');
            $table->float('price');
            $table->integer('distributor_id')->unsigned();
            $table->authors();
            $table->timestamps();

            $table->foreign('sku_id')->references('id_sku')->on('sku');
            $table->foreign('distributor_id')->references('id_distributor')->on('distributor');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('sku_stock');
    }
}
