<?php

namespace Modules\Inventory\Entities;

use Modules\BaseModel;

class SkuStock extends BaseModel implements SkuStockInterface
{
    /**
     * Database Table reference.
     */

    /**
     * table name.
     */
    protected $table = 'sku_stock';

    protected $fillable = ['sku_id', 'invoice_number	', 'batch_number', 'expiry_date', 'manufacture_date', 'price', 'distributor_id', 'status'];

    protected $primaryKey = 'id_sku_stock';

    public function sku()
    {
        return $this->belongsTo('Modules\Configure\Entities\SKU');
    }

    public function distributor()
    {
        return $this->belongsTo('Modules\Configure\Entities\Distributor');
    }
}
