<?php

namespace Modules\Inventory\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Inventory\Entities\SkuStockInterface;
use Modules\Inventory\Entities\SkuStock;

class SkuStockProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return Skustock
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(SkuStockInterface::class, function () {
            return new SkuStock();
        });
    }
}
