<?php

namespace Modules\Inventory\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Inventory\Entities\SkuStockInterface;
use Modules\Inventory\Repositories\SkuStockRepository;
use Modules\Inventory\Repositories\SkuStockRepositoryInterface;

class SkuStockRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return SkustockRepository
     */
    public function register()
    {
        // Register SkustockRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(SkustockRepositoryInterface::class, function () {
            return new SkuStockRepository($this->app[SkuStockInterface::class]);
        });
    }
}
