<?php

namespace Modules\Configure\Services\Universe\User;

use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Modules\AbstractService;
use Modules\Configure\Repositories\UserRepositoryInterface;
use Modules\Configure\Validators\PrincipalValidator;
use Modules\Configure\Validators\UserValidator;

class PrincipalService extends AbstractService
{
    /**
     * @param PrincipalRepositoryInterface|CategoryRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;

        $this->validator = new UserValidator();
    }
}
