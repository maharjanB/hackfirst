<?php

namespace Modules\Configure\Services\Universe\Organization;

use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Modules\AbstractService;
use Modules\Configure\Validators\PrincipalValidator;

class PrincipalService extends AbstractService
{
    /**
     * @param PrincipalRepositoryInterface|CategoryRepositoryInterface $repository
     */
    public function __construct(PrincipalRepositoryInterface $repository)
    {
        $this->repository = $repository;

        $this->validator = new PrincipalValidator();
    }
}
