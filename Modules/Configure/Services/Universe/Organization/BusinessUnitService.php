<?php

namespace Modules\Configure\Services\Universe\Organization;

use Modules\AbstractService;
use Modules\Configure\Repositories\BusinessUnitRepositoryInterface;
use Modules\Configure\Validators\BusinessUnitValidator;

/**
 * Business Logic Resides Here.
 *
 * Class BusinessUnitService
 */
class BusinessUnitService extends AbstractService
{
    public function __construct(BusinessUnitRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new BusinessUnitValidator();
    }
}
