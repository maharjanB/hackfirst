<?php

namespace Modules\Configure\Services\Universe\Resource;

use Modules\Configure\Repositories\ResourceRepositoryInterface;
use Modules\AbstractService;
use Modules\Configure\Validators\ResourceValidator;

class ResourceService extends AbstractService
{
	protected $repository;
	protected $validator;

	public function __construct(ResourceRepositoryInterface $repository)
	{
		$this->repository = $repository;
		$this->validator = new ResourceValidator();
	}
}
