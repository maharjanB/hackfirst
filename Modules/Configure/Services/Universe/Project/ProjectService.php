<?php

namespace Modules\Configure\Services\Universe\Project;

use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Modules\AbstractService;
use Modules\Configure\Repositories\ProjectRepositoryInterface;
use Modules\Configure\Validators\ProjectValidator;

class ProjectService extends AbstractService
{
    /**
     * @param PrincipalRepositoryInterface|CategoryRepositoryInterface $repository
     */
    public function __construct(ProjectRepositoryInterface $repository)
    {
        $this->repository = $repository;

        $this->validator = new ProjectValidator();
    }
}
