<?php

namespace Modules\Configure\Repositories;

use Modules\BaseRepositoryInterface;

interface ProjectRepositoryInterface extends BaseRepositoryInterface
{
}
