<?php

namespace Modules\Configure\Repositories;

use Modules\BaseRepositoryInterface;

interface ResourceRepositoryInterface extends BaseRepositoryInterface
{
}
