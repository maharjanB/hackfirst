<?php

namespace Modules\Configure\Repositories;

use Modules\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
}
