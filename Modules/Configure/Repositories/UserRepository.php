<?php

/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Repositories;

use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\AbstractRepository;
use Modules\Configure\Entities\ProjectModelInterface;
use Modules\Configure\Entities\UserModelInterface;

class UserRepository  extends  AbstractRepository implements UserRepositoryInterface
{
    /**
     * The Model instance.
     *
     * @var PrincipalModelInterface
     */
    protected $model;

    /**
     * Create a new PrincipalRepository instance.
     *
     * @param PrincipalModelInterface $model
     */
    public function __construct(UserModelInterface $model)
    {
        $this->model = $model;
    }

    public function index()
    {
        $user_all =  DB::select('select u.*, r.id_resource as resource_id from user u 
                      Left JOIN project p ON u.id_user = p.user_id 
                      LEFT JOIN resource r on p.resource_id = r.id_resource')->toArray();
        dump('hello');
        dd($user_all);
        return $user_all;
    }
}
