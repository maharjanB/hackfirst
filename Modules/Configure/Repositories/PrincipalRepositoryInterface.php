<?php

namespace Modules\Configure\Repositories;

use Modules\BaseRepositoryInterface;

interface PrincipalRepositoryInterface extends BaseRepositoryInterface
{
}
