<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/25/2016
 * Time: 11:13 AM.
 */

namespace Modules\Configure\Repositories;

use Modules\Configure\Entities\ResourceInterface;
use Modules\AbstractRepository;

class ResourceRepository extends AbstractRepository implements ResourceRepositoryInterface
{
    protected $model;

    public function __construct(ResourceInterface $model)
    {
        $this->model = $model;
    }
}
