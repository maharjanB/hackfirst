<?php

/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Repositories;

use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\AbstractRepository;
use Modules\Configure\Entities\ProjectModelInterface;

class ProjectRepository  extends  AbstractRepository implements ProjectRepositoryInterface
{
    /**
     * The Model instance.
     *
     * @var PrincipalModelInterface
     */
    protected $model;

    /**
     * Create a new PrincipalRepository instance.
     *
     * @param PrincipalModelInterface $model
     */
    public function __construct(ProjectModelInterface $model)
    {
        $this->model = $model;
    }
}
