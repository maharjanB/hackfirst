<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Town extends BaseModel implements TownInterface
{
    /*
      id	int(10)
      title	varchar(255)
      geographic_location_id	int(10)
      boundary	text
      status	tinyint(4)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $table = 'town';

    protected $primaryKey = 'id_town';

    protected $fillable = ['title', 'geographic_location_id', 'bounday', 'status'];

    public function geographic_location()
    {
        return $this->belongsTo('Modules\Configure\Entities\GeographicLocation', 'geographic_location_id');
    }
}
