<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Channel extends BaseModel implements ChannelInterface
{
    /**
     * Database Structure.
     *
     * id_channel       int(10)
     * title            varchar(255)
     * status           tinyint(4)
     * created_by    	int(10)
     * updated_by    	int(10)
     * created_at    	timestamp
     * updated_at    	timestamp
     */

    /**
     * @var string Table Name
     */
    protected $table = 'channel';

    /**
     * @var string Primary Key
     */
    protected $primaryKey = 'id_channel';

    /**
     * @var array fillable for mass assignments.
     */
    protected $fillable = ['title', 'status'];

    /**
     * Channel has many Category. Relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->hasMany('Modules\Configure\Entities\Category');
    }
}
