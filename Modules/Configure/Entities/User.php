<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class User extends BaseModel implements UserModelInterface
{
    /*
      id_principal	int(10)
      title             varchar(255)
      status            tinyint(4)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $fillable = ['id_user','email', 'password','first_name','last_name','mobile_number','updated_by','created_at','updated_at'];

    protected $table = 'user';

    protected $primaryKey = 'id_user';

    /*    public function resour()
        {
            return $this->belongsToMany('BusinessUnit')->withTimestamps();
        }
    }*/
}
