<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Catalog extends BaseModel implements CatalogInterface
{
    /*
     *id_category       int(10)
      title             varchar(255)
      status            tinyint(4)
      channel_id	int(10)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $table = 'catalog';
    protected $primaryKey = 'id_catalog';
    protected $fillable = ['description', 'parent_id', 'status'];
}
