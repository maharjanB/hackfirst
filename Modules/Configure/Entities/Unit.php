<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Unit extends BaseModel implements UnitInterface
{
    /**
     * Database structure of  th model.
     *
     * id_unit
     */
    /**
     *  table of the model.
     */
    protected $table = 'unit';

    /**
     *  primary table of the table.
     */
    protected $primaryKey = 'id_unit';

    protected $fillable = ['id_unit', 'title'];

    protected $guarded = [];

    public function packagingUnit()
    {
        return $this->hasMany('Modules\Configure\Entities\Packaging');
    }
}
