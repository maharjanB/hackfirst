<?php

namespace Modules\Configure\Entities;

use Modules\ModelInterface;

interface ResourceInterface extends ModelInterface
{
}
