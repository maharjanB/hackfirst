<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

/**
 * Business Unit Model to persistance Layer.
 */
class BusinessUnit extends BaseModel implements BusinessUnitInterface
{
    /**
     *  Database Structure for Reference.
     */

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'business_unit';

    /**
     * The primary key of table bu.
     *
     * @var string
     */
    protected $primaryKey = 'id_business_unit';

    /**
     * unguarded property of the table.
     *
     * @var array
     */
    protected $fillable = ['id_business_unit','title', 'status'];

    /** RELATIONSHIP */

    /**
     * Business Unit belongs to many principals.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function principals()
    {
        return $this->belongsToMany('Principal')->withTimestamps();
    }

    /**
     * Business Unit belongs to many Distributors.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function distributors()
    {
        return $this->belongsToMany('Distributor');
    }
}
