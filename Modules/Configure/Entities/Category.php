<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Category extends BaseModel implements CategoryInterface
{
    /*
     *id_category       int(10)
      title             varchar(255)
      status            tinyint(4)
      channel_id	int(10)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $table = 'category';
    protected $primaryKey = 'id_category';
    protected $fillable = ['title', 'status', 'channel_id'];

    /**
     * Category Belongs to a channel.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('Modules\Configure\Entities\Channel','channel_id');
    }

    public function Sbd()
    {
        return $this->hasMany('Sbd');
    }
}
