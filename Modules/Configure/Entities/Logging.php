<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Logging extends BaseModel implements LoggingInterface
{
    /*
     *id_logging       int(10)
      module_name             varchar(255)
     label             varchar(255)
     value             int(10)
      status            tinyint(4)
      channel_id	int(10)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $table = 'logging';
    protected $primaryKey = 'id_logging';
    protected $fillable = ['module_name', 'status', 'label', 'value'];

}
