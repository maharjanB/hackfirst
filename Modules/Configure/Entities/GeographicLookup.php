<?php

namespace Modules\Configure\Entities;

class GeographicLookup extends \Modules\BaseModel
{
    /*
      id_geographical_lookup            int(10)
      title                             varchar(255)
      parent_geographic_lookup_id	int(10)
      status                            varchar(255)
      created_by                        int(10)
      updated_by                        int(10)
      created_at                        timestamp
      updated_at                        timestamp
     */

    protected $table = 'geographic_lookup';
    protected $fillable = ['title', 'status'];

    protected $primaryKey = 'id_geographic_lookup';

    public function children()
    {
        return $this->hasMany('GeographicLookup', 'parent_geographic_lookup_id', 'id');
    }

    public function parent_geographic_lookup()
    {
        return $this->belongsTo('Modules\Configure\Entities\GeographicLookup', 'parent_geographic_lookup_id');
    }

    public function geographic_location()
    {
        return $this->hasMany('Modules\Configure\Entities\GeographicLocation');
    }
}
