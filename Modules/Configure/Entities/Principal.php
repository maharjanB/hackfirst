<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Principal extends BaseModel implements PrincipalModelInterface
{
    /*
      id_principal	int(10)
      title             varchar(255)
      status            tinyint(4)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $fillable = ['title', 'status'];

    protected $table = 'principal';

    protected $primaryKey = 'id_principal';

    public function business_units()
    {
        return $this->belongsToMany('BusinessUnit')->withTimestamps();
    }
}
