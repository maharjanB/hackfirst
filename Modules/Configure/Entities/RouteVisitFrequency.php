<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class RouteVisitFrequency extends BaseModel implements RouteVisitFrequencyInterface
{
    /*
      id_routevisitfrequency	int(10)
      title	                    varchar(255)
      frequency	                int(11)
      startdate	                date
      status	                tinyint(4)
      created_by	            int(10)
      updated_by	            int(10)
      created_at	            timestamp
      updated_at	            timestamp
     */

    protected $fillable = ['title', 'status'];

    protected $table = 'route_visit_frequency';

    protected $primaryKey = 'id_route_visit_frequency';

    public function route()
    {
        return $this->hasMany('\Modules\Configure\Entities\Route','id_route');
    }
}
