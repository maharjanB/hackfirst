<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Project extends BaseModel implements ProjectModelInterface
{
    /*
      id_principal	int(10)
      title             varchar(255)
      status            tinyint(4)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $fillable = ['title', 'description','resource_id','product_id','created_by','updated_by','created_at','updated_at'];

    protected $table = 'project';

    protected $primaryKey = 'id_project';

    /*    public function resour()
        {
            return $this->belongsToMany('BusinessUnit')->withTimestamps();
        }
    }*/
}
