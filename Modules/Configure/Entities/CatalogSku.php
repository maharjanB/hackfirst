<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class CatalogSku extends BaseModel implements CatalogSkuInterface
{
    /***
     *  Database Structure of CatalogSku
     *
     * /*
     * id    int(11)
     * created_by    int(11)
     * updated_by    int(11)
     * created_on    datetime
     * updated_on    datetime
     */

    protected $fillable = ['catalog_detail_id', 'sku_id', 'status'];

    /***
     * Table name of This Model
     */

    protected $table = 'catalog_sku';

    /**
     * @primary key of table distributor
     */
    protected $primaryKey = 'id_catalog_sku';
}
