<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class CatalogDetail extends BaseModel implements CatalogDetailInterface
{
    /***
     *  Database Structure of CatalogDetail
     *
     * id_catalog_detail    int(10)
     * catalog_id            int(10)
     * title       varchar(255)
     * created_by    int(11)
     * updated_by    int(11)
     * created_on    datetime
     * updated_on    datetime
     */

    protected $fillable = ['catalog_id', 'title', 'status'];

    /***
     * Table name of This Model
     */

    protected $table = 'catalog_detail';

    /**
     * @primary key of table distributor
     */
    protected $primaryKey = 'id_catalog_detail';
}
