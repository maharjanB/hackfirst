<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class RouteDeliveryType extends  BaseModel implements RouteDeliveryTypeInterface
{
    /**
     * Database table Structure.
     */
    protected $table = 'route_delivery_type';
    protected $primaryKey = 'id_route_delivery_type';
    protected $fillable = ['title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function route()
    {
        return $this->hasMany('\Modules\Configure\Entities\Route','id_route');
    }
}
