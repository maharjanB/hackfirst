<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Sku extends BaseModel implements SkuInterface
{
    /**
     *  Database Structure for Reference.
     *
     * /*
     * /*
     * id_sku    int(10)
     * title    varchar(255)
     * GTIN    varchar(255)
     * EAN    varchar(255)
     * tag    varchar(255)
     * PSKU    tinyint(4)
     * SU    double(8,2)
     * status tinyint(1)
     * business_unit_id    int(10)
     * created_by    int(10)
     * updated_by    int(10)
     * created_at    timestamp
     * updated_at    timestamp
     */

    /* The database table used by the model.
    *
    * @var string
    */
    protected $table = 'sku';

    /**
     * The primary key of table bu.
     *
     * @var string
     */
    protected $primaryKey = 'id_sku';

    protected $guarded = [];

    /**
     * Sku belongs to one Catalog Detail.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function catalog_detail()
    {
        return $this->belongsToMany('CatalogDetail');
    }

    public function packaging()
    {
        return $this->hasMany('Modules\Configure\Entities\Packaging');
    }

    public function salesOrder()
    {
        return $this->hasMany('Modules\Sales\Entities\SalesOrder');
    }
}
