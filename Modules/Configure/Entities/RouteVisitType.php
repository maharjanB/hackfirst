<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;


class RouteVisitType extends BaseModel implements RouteVisitTypeInterface
{
	/*
	 * id	int(10)
	  title	varchar(255)
	  created_at	timestamp
	  updated_at	timestamp
	 */

	protected $fillable = ["title", "status"];

	protected $table = "route_visit_type";

	protected $primaryKey = "id_route_visit_type";

	public function route()
	{
		return $this->hasMany('\Modules\Configure\Entities\Route');
	}

}
