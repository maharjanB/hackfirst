<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class RouteVisitCategory extends BaseModel implements RouteVisitCategoryInterface
{
    /*
      id	int(10)
      title	varchar(255)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $fillable = ["title", "status"];

    protected $table = "route_visit_category";

    protected $primaryKey = 'id_route_visit_category';


    public function route()
    {
        return $this->hasMany('\Modules\Configure\Entities\Route','id_route');
    }

}
