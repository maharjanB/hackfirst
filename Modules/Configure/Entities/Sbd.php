<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Sbd extends BaseModel implements SbdInterface
{
    /*
    id_sbd  		int(10)
   catelog_sku_id      	int(10)
   category_id            	varchar(255)
   productivity		int(10)
   status			int(10)
   created_by         	int(10)
   updated_by         	int(10)
   created_at         	timestamp
   updated_at         	timestamp

    */
    /* The database table used by the model.
       *
       * @var string
       */
    protected $table = 'sbd';

    /**
     * The primary key of table sbd.
     *
     * @var string
     */
    protected $primaryKey = 'id_sbd';
    protected $fillable = ['catalog_sku_id', 'category_id', 'compliance', 'visibility', 'status'];

    public function category()
    {
        $this->belongsTo('Category');
    }
}
