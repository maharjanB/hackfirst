<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 2/1/2016
 * Time: 11:32 AM
 */

namespace Modules\Configure\Entities;

use Modules\BaseModel;


class Packaging extends BaseModel implements PackagingInterface
{
    /***
     *  Database Structure of the model
     * /*
     *id_packing
     * sku_id
     * unit_id
     * quantity
     * created on
     * updated on
     * created by
     * updated by
     */

    /**
     * @var array
     * Fillable fields of the table
     */
    protected $fillable = ['id_packing', 'sku_id', 'unit_id', 'quantity'];

    /**
     *  table of the model
     */
    protected $table = "packaging";

    /**
     *  primary key of the table;
     */
    protected $primaryKey = "id_packing";

    protected $guarded = [];

    public function sku()
    {
        return $this->belongsTo('Modules\Configure\Entities\Sku', 'sku_id');
    }

    public function unit()
    {
        return $this->belongsTo('Modules\Configure\Entities\Unit', 'unit_id');
    }

}

