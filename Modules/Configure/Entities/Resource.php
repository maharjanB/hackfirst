<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class Resource extends BaseModel implements ResourceInterface
{
    /***
     *  Database Structure of Distributor
     *
     * /*
     * id    int(11)
     * created_by    int(11)
     * updated_by    int(11)
     * name    varchar(64)
     * location    varchar(64)
     * description    longtext
     * created_on    datetime
     * updated_on    datetime
     * town_id    int(11)
     */

    protected $fillable = ['title', 'description'];

    /***
     * Table name of This Model
     */

    protected $table = 'resource';

    /**
     * @primary key of table distributor
     */
    protected $primaryKey = 'id_resource';


}
