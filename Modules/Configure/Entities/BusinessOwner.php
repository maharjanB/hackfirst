<?php

namespace Modules\Configure\Entities;

class BusinessOwner extends \Modules\BaseModel
{
    /*
      id	int(10)
      title	varchar(255)
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $fillable = ['title'];
}
