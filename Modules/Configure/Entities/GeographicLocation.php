<?php

namespace Modules\Configure\Entities;

use Modules\BaseModel;

class GeographicLocation extends BaseModel implements GeographicLocationInterface
{
    /**
     *  Database Structure of table.
     *
     * /*
     * id_geographic_location    int(10)
     * geographic_lookup_id    int(10)
     * title                     varchar(255)
     * parent_id                 int(10)
     * status                    tinyint(4)
     * created_by                int(10)
     * updated_by                int(10)
     * created_at                timestamp
     * updated_at                timestamp
     */
    protected $fillable = ['geographic_lookup_id', 'title', 'parent_id', 'status'];

    /**
     * table use by this model.
     */
    protected $table = 'geographic_location';

    /**
     * primary key of table geographic_location.
     */
    protected $primaryKey = 'id_geographic_location';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('Modules\Configure\Entities\GeographicLocation', 'parent_id', 'id_geographic_location');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('Modules\Configure\Entities\GeographicLocation', 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function geographic_lookup()
    {
        return $this->belongsTo('Modules\Configure\Entities\GeographicLookup', 'geographic_lookup_id');
    }

    public function towns()
    {
        return $this->hasMany('Modules\Configure\Entities\Town');
    }
}
