<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;

class ConfigureServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     */
    public function boot()
    {
        $this->registerConfig();
        $this->registerTranslations();
        $this->registerViews();
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //
    }

    /**
     * Register config.
     */
    protected function registerConfig()
    {
        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('configure.php'),
        ]);
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'configure'
        );
    }

    /**
     * Register views.
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/configure');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath,
        ]);

        $this->loadViewsFrom([$viewPath, $sourcePath], 'configure');
    }

    /**
     * Register translations.
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/configure');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'configure');
        } else {
            $this->loadTranslationsFrom(__DIR__.'/../Resources/lang', 'configure');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }
}
