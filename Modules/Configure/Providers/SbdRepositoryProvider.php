<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\SbdRepository;
use Modules\Configure\Repositories\SbdRepositoryInterface;
use Modules\Configure\Entities\SbdInterface;
use Illuminate\Support\ServiceProvider;

class SbdRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(SbdRepositoryInterface::class, function () {
            return new SbdRepository($this->container[SbdInterface::class]);
        });
    }
}
