<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\GeographicLocation;
use Modules\Configure\Entities\GeographicLocationInterface;
use Illuminate\Support\ServiceProvider;

class GeographicLocationProvider extends ServiceProvider
{
    public function register()
    {
        /*
         * Register Distributor Model
         */
        $this->app->bind(GeographicLocationInterface::class, function () {
            return new GeographicLocation();
        });
    }
}
