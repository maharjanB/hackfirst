<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\TownInterface;
use Modules\Configure\Repositories\TownRepository;
use Modules\Configure\Repositories\TownRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class TownRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return TownRepository
     */
    public function register()
    {
        // Register TownRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(TownRepositoryInterface::class, function () {
            return new TownRepository($this->app[TownInterface::class]);
        });
    }
}
