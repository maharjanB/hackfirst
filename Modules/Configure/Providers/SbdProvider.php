<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\Sbd;
use Modules\Configure\Entities\SbdInterface;
use Illuminate\Support\ServiceProvider;

class SbdProvider extends ServiceProvider
{
    public function register()
    {
        /*
         * Register Sbd Model
         */
        $this->app->bind(SbdInterface::class, function () {
            return new Sbd();
        });
    }
}
