<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:05 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\RouteVisitTypeInterface;
use Modules\Configure\Repositories\RouteVisitTypeRepositoryInterface;
use Modules\Configure\Repositories\RouteVisitTypeRepository;

class RouteVisitTypeRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;

        $this->container->bind(RouteVisitTypeRepositoryInterface::class, function () {
            return new RouteVisitTypeRepository($this->app[RouteVisitTypeInterface::class]);
        });
    }
}
