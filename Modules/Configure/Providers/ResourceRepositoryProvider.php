<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\ResourceRepository;
use Modules\Configure\Repositories\ResourceRepositoryInterface;
use Modules\Configure\Entities\ResourceInterface;
use Illuminate\Support\ServiceProvider;

class ResourceRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(ResourceRepositoryInterface::class, function () {
            return new ResourceRepository($this->container[ResourceInterface::class]);
        });
    }
}
