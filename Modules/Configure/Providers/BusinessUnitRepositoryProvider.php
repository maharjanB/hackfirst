<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\BusinessUnitInterface;
use Illuminate\Support\ServiceProvider;
use Modules\Configure\Repositories\BusinessUnitRepository;
use Modules\Configure\Repositories\BusinessUnitRepositoryInterface;

class BusinessUnitRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BuRepository
     */
    public function register()
    {
        // Register BuRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(BusinessUnitRepositoryInterface::class, function () {
            return new BusinessUnitRepository($this->app[BusinessUnitInterface::class]);
        });
    }
}
