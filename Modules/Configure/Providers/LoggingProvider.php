<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\Logging;
use Modules\Configure\Entities\LoggingInterface;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class LoggingProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(LoggingInterface::class, function () {
            return new Logging();
        });
    }
}
