<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\Principal;
use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\Configure\Entities\Project;
use Modules\Configure\Entities\ProjectModelInterface;
use Modules\Configure\Entities\User;
use Modules\Configure\Entities\UserModelInterface;

class UserModelProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PrincipalModel
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserModelInterface::class, function () {
            return new User();
        });
    }
}
