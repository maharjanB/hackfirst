<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\BusinessUnit;
use Modules\Configure\Entities\BusinessUnitInterface;

class BusinessUnitProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BuModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(BusinessUnitInterface::class, function () {
            return new BusinessUnit();
        });
    }
}
