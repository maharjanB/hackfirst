<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\Resource;
use Modules\Configure\Entities\ResourceInterface;
use Illuminate\Support\ServiceProvider;

class ResourceProvider extends ServiceProvider
{
    public function register()
    {
        /*
         * Register Distributor Model
         */
        $this->app->bind(ResourceInterface::class, function () {
            return new Resource();
        });
    }
}
