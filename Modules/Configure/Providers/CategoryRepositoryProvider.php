<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\CategoryInterface;
use Modules\Configure\Repositories\CategoryRepository;
use Modules\Configure\Repositories\CategoryRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class CategoryRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        // Register CategoryRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(CategoryRepositoryInterface::class, function () {
            return new CategoryRepository($this->app[CategoryInterface::class]);
        });
    }
}
