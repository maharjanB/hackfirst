<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\LoggingInterface;
use Modules\Configure\Repositories\LoggingRepository;
use Modules\Configure\Repositories\LoggingRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class LoggingRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        // Register LoggingRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(LoggingRepositoryInterface::class, function () {
            return new LoggingRepository($this->app[LoggingInterface::class]);
        });
    }
}
