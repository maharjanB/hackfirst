<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\SkuInterface;
use Modules\Configure\Repositories\SkuRepository;
use Modules\Configure\Repositories\SkuRepositoryInterface;

class SkuRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BuRepository
     */
    public function register()
    {
        // Register BuRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(SkuRepositoryInterface::class, function () {
            return new SkuRepository($this->app[SkuInterface::class]);
        });
    }
}
