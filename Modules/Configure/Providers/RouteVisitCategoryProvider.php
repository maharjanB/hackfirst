<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:05 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\RouteVisitCategoryInterface;
use Modules\Configure\Entities\RouteVisitCategory;
use Illuminate\Support\ServiceProvider;

class RouteVisitCategoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RouteVisitCategoryInterface::class, function () {
            return new RouteVisitCategory();
        });
    }
}
