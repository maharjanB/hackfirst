<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Repositories\UnitRepositoryInterface;
use Modules\Configure\Entities\UnitInterface;
use Modules\Configure\Repositories\UnitRepository;

class UnitRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register TownRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(UnitRepositoryInterface::class, function () {
            return new UnitRepository($this->app[UnitInterface::class]);
        });
    }
}
