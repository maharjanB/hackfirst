<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\Sku;
use Modules\Configure\Entities\SkuInterface;

class SkuProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return BuModel
     */
    public function register()
    {
        //Register BuModelInterface
        $this->container = $this->app;
        $this->container->bind(SkuInterface::class, function () {
            return new Sku();
        });
    }
}
