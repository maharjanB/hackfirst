<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\Catalog;
use Modules\Configure\Entities\CatalogInterface;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class CatalogProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(CatalogInterface::class, function () {
            return new Catalog();
        });
    }
}
