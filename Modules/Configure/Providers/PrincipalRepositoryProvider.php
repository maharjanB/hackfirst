<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\Configure\Repositories\PrincipalRepository;
use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class PrincipalRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PrincipalRepository
     */
    public function register()
    {
        // Register PrincipalREpositoryInterface
        $this->container = $this->app;
        $this->container->bind(PrincipalRepositoryInterface::class, function () {
            return new PrincipalRepository($this->app[PrincipalModelInterface::class]);
        });
    }
}
