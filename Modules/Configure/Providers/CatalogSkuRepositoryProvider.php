<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\CatalogSkuRepository;
use Modules\Configure\Repositories\CatalogSkuRepositoryInterface;
use Modules\Configure\Entities\CatalogSkuInterface;
use Illuminate\Support\ServiceProvider;

class CatalogSkuRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(CatalogSkuRepositoryInterface::class, function () {
            return new CatalogSkuRepository($this->container[CatalogSkuInterface::class]);
        });
    }
}
