<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:08 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\Unit;
use Modules\Configure\Entities\UnitInterface;

class UnitProvider extends  ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UnitInterface::class, function () {
            return new Unit();
        });
    }
}
