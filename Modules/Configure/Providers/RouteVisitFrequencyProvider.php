<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\RouteVisitFrequency;
use Modules\Configure\Entities\RouteVisitFrequencyInterface;

class RouteVisitFrequencyProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(RouteVisitFrequencyInterface::class, function () {
            return new RouteVisitFrequency();
        });
    }
}
