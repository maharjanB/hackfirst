<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:05 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\RouteVisitTypeInterface;
use Modules\Configure\Entities\RouteVisitType;

class RouteVisitTypeProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RouteVisitTypeInterface::class, function () {
            return new RouteVisitType();
        });
    }
}
