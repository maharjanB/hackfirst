<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\Configure\Entities\ProjectModelInterface;
use Modules\Configure\Entities\UserModelInterface;
use Modules\Configure\Repositories\PrincipalRepository;
use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use Modules\Configure\Repositories\ProjectRepository;
use Modules\Configure\Repositories\ProjectRepositoryInterface;
use Modules\Configure\Repositories\UserRepository;
use Modules\Configure\Repositories\UserRepositoryInterface;

class UserRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PrincipalRepository
     */
    public function register()
    {
        // Register PrincipalREpositoryInterface
        $this->container = $this->app;
        $this->container->bind(UserRepositoryInterface::class, function () {
            return new UserRepository($this->app[UserModelInterface::class]);
        });
    }
}
