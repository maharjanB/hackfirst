<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:05 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\RouteVisitCategoryRepositoryInterface;
use Modules\Configure\Repositories\RouteVisitCategoryRepository;
use Modules\Configure\Entities\RouteVisitCategoryInterface;
use Illuminate\Support\ServiceProvider;

class  RouteVisitCategoryRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register RouteRepositoryInterface
        $this->container = $this->app;

        $this->container->bind(RouteVisitCategoryRepositoryInterface::class, function () {
            return new RouteVisitCategoryRepository($this->app[RouteVisitCategoryInterface::class]);
        });
    }
}
