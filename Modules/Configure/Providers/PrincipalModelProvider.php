<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\Principal;
use Modules\Configure\Entities\PrincipalModelInterface;

class PrincipalModelProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PrincipalModel
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PrincipalModelInterface::class, function () {
            return new Principal();
        });
    }
}
