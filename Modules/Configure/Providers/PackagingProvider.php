<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 2/1/2016
 * Time: 11:35 AM
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\PackagingInterface;
use Modules\Configure\Entities\Packaging;

class PackagingProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PackagingInterface::class, function () {
            return new Packaging();
        });
    }
}
