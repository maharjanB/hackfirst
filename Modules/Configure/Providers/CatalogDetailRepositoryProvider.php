<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\CatalogDetailRepository;
use Modules\Configure\Repositories\CatalogDetailRepositoryInterface;
use Modules\Configure\Entities\CatalogDetailInterface;
use Illuminate\Support\ServiceProvider;

class CatalogDetailRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(CatalogDetailRepositoryInterface::class, function () {
            return new CatalogDetailRepository($this->container[CatalogDetailInterface::class]);
        });
    }
}
