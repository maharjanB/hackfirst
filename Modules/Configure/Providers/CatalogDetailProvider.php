<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\CatalogDetail;
use Modules\Configure\Entities\CatalogDetailInterface;
use Illuminate\Support\ServiceProvider;

class CatalogDetailProvider extends ServiceProvider
{
    public function register()
    {
        /*
         * Register CatalogDetail Model
         */
        $this->app->bind(CatalogDetailInterface::class, function () {
            return new CatalogDetail();
        });
    }
}
