<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\RouteVisitFrequencyInterface;
use Modules\Configure\Repositories\RouteVisitFrequencyRepository;
use Modules\Configure\Repositories\RouteVisitFrequencyRepositoryInterface;

class RouteVisitFrequencyRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        // Register RouteVisitFrequencyRepositoryInterface
        $this->app->bind(RouteVisitFrequencyRepositoryInterface::class, function () {
            return new RouteVisitFrequencyRepository($this->app[RouteVisitFrequencyInterface::class]);
        });
    }
}

?>

