<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\PrincipalModelInterface;
use Modules\Configure\Entities\ProjectModelInterface;
use Modules\Configure\Repositories\PrincipalRepository;
use Modules\Configure\Repositories\PrincipalRepositoryInterface;
use Illuminate\Support\ServiceProvider;
use Modules\Configure\Repositories\ProjectRepository;
use Modules\Configure\Repositories\ProjectRepositoryInterface;

class ProjectRepositoryProvider extends  ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return PrincipalRepository
     */
    public function register()
    {
        // Register PrincipalREpositoryInterface
        $this->container = $this->app;
        $this->container->bind(ProjectRepositoryInterface::class, function () {
            return new ProjectRepository($this->app[ProjectModelInterface::class]);
        });
    }
}
