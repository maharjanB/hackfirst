<?php
/**
 * Created by Suresh
 * Date: 12/19/2015
 * Time: 6:16 PM.
 */

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\Town;
use Modules\Configure\Entities\TownInterface;

class TownModelProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return Town
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(TownInterface::class, function () {
            return new Town();
        });
    }
}
