<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/15/2016
 * Time: 12:01 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\RouteDeliveryTypeInterface;
use Modules\Configure\Entities\RouteDeliveryType;
use Illuminate\Support\ServiceProvider;

class RouteDeliveryTypeProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RouteDeliveryTypeInterface::class, function () {
            return new RouteDeliveryType();
        });
    }
}
