<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/15/2016
 * Time: 12:01 PM.
 */

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\RouteDeliveryTypeRepositoryInterface;
use Modules\Configure\Repositories\RouteDeliveryTypeRepository;
use Modules\Configure\Repositories\RouteDeliveryTypeInterface;
use Illuminate\Support\ServiceProvider;

class RouteDeliveryTypeRepositoryProvider extends ServiceProvider
{
    public function register()
    {

        // Register RouteVisitFrequencyRepositoryInterface
        $this->app->bind(RouteDeliveryTypeRepositoryInterface::class, function () {
            return new RouteDeliveryTypeRepository($this->app[RouteDeliveryTypeInterface::class]);
        });
    }
}
