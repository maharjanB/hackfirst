<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 2/1/2016
 * Time: 11:36 AM
 */

namespace modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;

use Modules\Configure\Repositories\PackagingRepositoryInterface;
use Modules\Configure\Repositories\PackagingRepository;
use Modules\Configure\Entities\PackagingInterface;

class PackagingRepositoryProvider extends ServiceProvider
{

    public function register()
    {
        // Register RetailOutletRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(PackagingRepositoryInterface::class, function () {
            return new PackagingRepository($this->app[PackagingInterface::class]);
        });
    }

}
