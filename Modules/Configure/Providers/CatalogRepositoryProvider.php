<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\CatalogInterface;
use Modules\Configure\Repositories\CatalogRepository;
use Modules\Configure\Repositories\CatalogRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class CatalogRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        // Register CatalogRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(CatalogRepositoryInterface::class, function () {
            return new CatalogRepository($this->app[CatalogInterface::class]);
        });
    }
}
