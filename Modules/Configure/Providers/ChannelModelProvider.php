<?php

namespace Modules\Configure\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Configure\Entities\ChannelInterface;
use Modules\Configure\Entities\Channel;

class ChannelModelProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(ChannelInterface::class, function () {
            return new Channel();
        });
    }
}
