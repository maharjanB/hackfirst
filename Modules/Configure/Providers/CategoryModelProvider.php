<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\Category;
use Modules\Configure\Entities\CategoryInterface;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class CategoryModelProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(CategoryInterface::class, function () {
            return new Category();
        });
    }
}
