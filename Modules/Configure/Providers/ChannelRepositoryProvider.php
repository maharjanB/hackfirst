<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\ChannelInterface;
use Modules\Configure\Repositories\ChannelRepository;
use Modules\Configure\Repositories\ChannelRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class ChannelRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // TODO: Implement register() method.
        $this->container = $this->app;
        $this->container->bind(ChannelRepositoryInterface::class, function () {
            return new ChannelRepository($this->container[ChannelInterface::class]);
        });
    }
}
