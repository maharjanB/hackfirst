<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Entities\CatalogSku;
use Modules\Configure\Entities\CatalogSkuInterface;
use Illuminate\Support\ServiceProvider;

class CatalogSkuProvider extends ServiceProvider
{
    public function register()
    {
        /*
         * Register CatalogSku Model
         */
        $this->app->bind(CatalogSkuInterface::class, function () {
            return new CatalogSku();
        });
    }
}
