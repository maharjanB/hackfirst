<?php

namespace Modules\Configure\Providers;

use Modules\Configure\Repositories\GeographicLocationRepository;
use Modules\Configure\Repositories\GeographicLocationRepositoryInterface;
use Modules\Configure\Entities\GeographicLocationInterface;
use Illuminate\Support\ServiceProvider;

class GeographicLocationRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(GeographicLocationRepositoryInterface::class, function () {
            return new GeographicLocationRepository($this->container[GeographicLocationInterface::class]);
        });
    }
}
