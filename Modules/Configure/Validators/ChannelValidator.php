<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/23/2016
 * Time: 7:48 PM.
 */

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class ChannelValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:3',
        'status' => 'required',
    );
}
