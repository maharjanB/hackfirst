<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class SbdValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'catalog_sku_id' => 'required',
        'category_id' => 'required',
        'status' => 'required|integer',
    );
}
