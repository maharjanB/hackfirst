<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class CatalogValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'description' => 'required|min:2',
        'parent_id' => 'required|integer',
        'status' => 'required|integer',
    );
}
