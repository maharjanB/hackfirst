<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/23/2016
 * Time: 10:32 PM.
 */

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class TownValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:3',
        'geographic_location_id' => 'required',
        'boundary' => 'required',
        'status' => 'required',
    );
}
