<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class CatalogSkuValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'catalog_detail_id' => 'required',
        'sku_id' => 'required',
        'status' => 'required|integer',
    );
}
