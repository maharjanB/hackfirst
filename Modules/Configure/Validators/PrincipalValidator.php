<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class PrincipalValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:2',
    );
}
