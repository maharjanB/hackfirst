<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class GeographicLocationValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'geographic_lookup_id' => 'required',
        'title' => 'required',
    );
}
