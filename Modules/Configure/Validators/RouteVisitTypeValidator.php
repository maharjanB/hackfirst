<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 1:57 PM.
 */

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RouteVisitTypeValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:3',
    );
}
