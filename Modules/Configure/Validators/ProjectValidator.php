<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class ProjectValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:2',
        'description' => 'required|min:2',
        'resource_id' => 'required',
        'product_id' => 'required'
    );
}
