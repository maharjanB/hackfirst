<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class LoggingValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'module_name'=>'required',
        'label'=>'required',
        'value'=>'required',
        'status' => 'required|integer',

    );
}
