<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RouteVisitFrequencyValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required|min:2',
        'frequency' => 'required|integer',
        //'startdate' => 'required',
        'status' => 'required|integer',
    );
}
