<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:22 PM.
 */

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class UnitValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required',
    );
}
