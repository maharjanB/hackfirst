<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RouteDeliveryTypeValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required',
        'status' => 'required',
    );
}
