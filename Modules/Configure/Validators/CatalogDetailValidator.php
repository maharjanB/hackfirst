<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class CatalogDetailValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'catalog_id' => 'required',
        'title' => 'required',
        'status' => 'required|integer',
    );
}
