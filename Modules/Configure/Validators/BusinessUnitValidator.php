<?php

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class BusinessUnitValidator extends LaravelValidator implements ValidationInterface
{
    /**
         * Validation for creating a new User.
         *
         * @var array
         */
        protected $rules = array(
            'title' => 'required|min:2',
        );
}
