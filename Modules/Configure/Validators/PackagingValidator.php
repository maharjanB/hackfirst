<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/31/2016
 * Time: 12:13 AM.
 */

namespace Modules\Configure\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class PackagingValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating new SalesOrder.
     */
    protected $rules = array(
        'sku_id' => 'required',
        'unit_id' => 'required',
        'quantity' => 'required',
    );
}
