<?php

Route::group(['prefix' => 'configure', 'namespace' => 'Modules\Configure\Http\Controllers'], function () {
    Route::get('/', 'ConfigureController@index');
});
