<?php

namespace Modules\Configure\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ConfigureController extends Controller
{
    public function index()
    {
        return view('configure::index');
    }
}
