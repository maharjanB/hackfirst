<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateRouteVisitFrequencyTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('route_visit_frequency', function (CustomBluePrint $table) {
            $table->increments('id_route_visit_frequency');
            $table->string('title');
            $table->integer('frequency');
            $table->date('startdate');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route_visit_frequency');
    }
}
