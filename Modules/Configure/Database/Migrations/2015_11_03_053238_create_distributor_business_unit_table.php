<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateDistributorBusinessUnitTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('distributor_business_unit', function (CustomBluePrint $table) {
            $table->increments('id_distributor_business_unit');
            $table->integer('distributor_id')->unsigned();
            $table->integer('business_unit_id')->unsigned();
            $table->timestamps();
            $table->authors();

            $table->foreign('distributor_id')->references('id_distributor')->on('distributor');
            $table->foreign('business_unit_id')->references('id_business_unit')->on('business_unit');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('distributor_business_unit');
    }
}
