<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * static consits single_day / multi_day route as default.
 */
class CreateRouteVisitCategoryTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('route_visit_category', function (Blueprint $table) {
            $table->increments('id_route_visit_category');
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route_visit_category');
    }
}
