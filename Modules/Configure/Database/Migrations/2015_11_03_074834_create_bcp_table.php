<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateBcpTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('bcp', function (CustomBluePrint $table) {
            $table->increments('id_bcp');
            $table->string('call_title');
            $table->integer('call_order');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('bcp');
    }
}
