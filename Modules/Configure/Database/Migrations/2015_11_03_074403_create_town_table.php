<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateTownTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('town', function (CustomBluePrint $table) {
            $table->increments('id_town');
            $table->string('title');
            $table->integer('geographic_location_id')->unsigned();
            $table->text('bounday');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

            $table->foreign('geographic_location_id')->references('id_geographic_location')->on('geographic_location');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('town');
    }
}
