<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * static consits open / schedule route as default.
 */
class CreateRouteVisitTypeTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('route_visit_type', function (Blueprint $table) {
            $table->increments('id_route_visit_type');
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route_visit_type');
    }
}
