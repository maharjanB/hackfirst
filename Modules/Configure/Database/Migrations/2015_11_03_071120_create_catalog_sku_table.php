<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateCatalogSkuTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('catalog_sku', function (CustomBluePrint $table) {
            $table->increments('id_catalog_sku');
            $table->integer('catalog_detail_id')->unsigned();
            $table->integer('sku_id')->unsigned();
            $table->authors();
            $table->timestamps();

            $table->foreign('sku_id')->references('id_sku')->on('sku');
            $table->foreign('catalog_detail_id')->references('id_catalog_detail')->on('catalog_detail');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('catalog_sku');
    }
}
