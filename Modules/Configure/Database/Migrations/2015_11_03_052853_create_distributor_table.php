<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateDistributorTable extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('distributor', function (CustomBluePrint $table) {
            $table->increments('id_distributor');
            $table->string('title');
            $table->text('description');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('distributor');
    }
}
