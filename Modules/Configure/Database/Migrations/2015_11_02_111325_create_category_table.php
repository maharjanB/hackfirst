<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateCategoryTable extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('category', function (CustomBluePrint $table) {
            $table->increments('id_category');
            $table->string('title');
            $table->tinyInteger('status');
            $table->integer('channel_id')->unsigned();
            $table->authors();
            $table->timestamps();
        });

        $this->schema->table('category', function (\Modules\CustomBluePrint $table) {
        $table->foreign('channel_id')->references('id_channel')->on('channel');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('category');
    }
}
