<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateGeographicLookup extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('geographic_lookup', function (CustomBluePrint $table) {
            $table->increments('id_geographic_lookup');
            $table->string('title');
            $table->integer('parent_geographic_lookup_id')->unsigned();
            $table->string('status');
            $table->authors();
            $table->timestamps();
            $table->foreign('parent_geographic_lookup_id')->references('id_geographic_lookup')->on('geographic_lookup');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('');
    }
}
