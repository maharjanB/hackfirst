<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateCatalogTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('catalog', function (CustomBluePrint $table) {
            $table->increments('id_catalog');
            $table->string('description');
            $table->integer('parent_id')->unsigned();
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

            $table->foreign('parent_id')->references('id_catalog')->on('catalog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('catalog');
    }
}
