<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateCatalogDetailTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('catalog_detail', function (CustomBluePrint $table) {
            $table->increments('id_catalog_detail');
            $table->integer('catalog_id')->unsigned();
            $table->string('title');
            $table->authors();
            $table->timestamps();

            $table->foreign('catalog_id')->references('id_catalog')->on('catalog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('catalog_detail');
    }
}
