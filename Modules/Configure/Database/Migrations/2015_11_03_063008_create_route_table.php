<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateRouteTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('route', function (CustomBluePrint $table) {
            $table->increments('id_route');
            $table->string('title');
            $table->integer('route_visit_category_id')->unsigned();
            $table->integer('route_visit_type_id')->unsigned();
            $table->integer('route_delivery_type_id')->unsigned();
            $table->integer('route_visit_frequency_id')->unsigned();
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

            $table->foreign('route_visit_category_id')->references('id_route_visit_category')->on('route_visit_category');
            $table->foreign('route_visit_type_id')->references('id_route_visit_type')->on('route_visit_type');
            $table->foreign('route_delivery_type_id')->references('id_route_delivery_type')->on('route_delivery_type');
            $table->foreign('route_visit_frequency_id')->references('id_route_visit_frequency')->on('route_visit_frequency');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route');
    }
}
