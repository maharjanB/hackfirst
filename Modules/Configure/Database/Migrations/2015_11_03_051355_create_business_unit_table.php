<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateBusinessUnitTable extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('business_unit', function (CustomBluePrint $table) {
            $table->increments('id_business_unit');
            $table->string('title');
            $table->tinyInteger('string');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('business_unit');
    }
}
