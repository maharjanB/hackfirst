<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateGeographicLocation extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('geographic_location', function (CustomBluePrint $table) {
            $table->increments('id_geographic_location');
            $table->integer('geographic_lookup_id')->unsigned();
            $table->string('title');
            $table->integer('parent_id')->unsigned();
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });

        $this->schema->table('geographic_location', function (CustomBluePrint $table) {
            $table->foreign('geographic_lookup_id')->references('id_geographic_lookup')->on('geographic_lookup');
            $table->foreign('parent_id')->references('id_geographic_location')->on('geographic_location');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('');
    }
}
