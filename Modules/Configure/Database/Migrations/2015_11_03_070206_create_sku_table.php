<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateSkuTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('sku', function (CustomBluePrint $table) {
            $table->increments('id_sku');
            $table->string('title');
            $table->string('GTIN');
            $table->string('EAN');
            $table->string('tag');
            $table->tinyInteger('PSKU');
            $table->float('SU'); // conversion factor
            $table->integer('business_unit_id')->unsigned();
            $table->authors();
            $table->timestamps();
            $table->foreign('business_unit_id')->references('id_business_unit')->on('business_unit');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('sku');
    }
}
