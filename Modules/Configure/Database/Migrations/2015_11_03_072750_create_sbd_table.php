<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateSbdTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('sbd', function (CustomBluePrint $table) {
            $table->increments('id_sbd');
            $table->integer('catalog_sku_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('compliance')->unsigned();
            $table->integer('visibility')->unsigned();
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

            $table->foreign('catalog_sku_id')->references('id_catalog')->on('catalog');
            $table->foreign('category_id')->references('id_category')->on('category');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('sbd');
    }
}
