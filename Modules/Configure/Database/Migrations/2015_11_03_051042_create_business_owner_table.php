<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateBusinessOwnerTable extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('business_owner', function (CustomBluePrint $table) {
            $table->increments('id_business_owner');
            $table->string('title');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('business_owner');
    }
}
