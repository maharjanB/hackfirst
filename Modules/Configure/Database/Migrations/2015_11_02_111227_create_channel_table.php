<?php

use Modules\CustomBluePrint;
use Modules\Migrations;

class CreateChannelTable extends \Modules\CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('channel', function (CustomBluePrint $table) {
            $table->increments('id_channel');
            $table->string('title');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('channel');
    }
}
