<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreatePrincipalBusinessUnitTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('principal_business_unit', function (CustomBluePrint $table) {
            $table->increments('id_principal_business_unit');
            $table->integer('principal_id')->unsigned();
            $table->integer('business_unit_id')->unsigned();
            $table->timestamps();
            $table->authors();

            $table->foreign('principal_id')->references('id_principal')->on('principal');
            $table->foreign('business_unit_id')->references('id_business_unit')->on('business_unit');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('principal_business_unit');
    }
}
