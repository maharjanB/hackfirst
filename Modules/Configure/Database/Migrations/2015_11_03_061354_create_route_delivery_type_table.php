<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * static consits ready_stock / delivery_later route as default.
 */
class CreateRouteDeliveryTypeTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('route_delivery_type', function (Blueprint $table) {
            $table->increments('id_route_delivery_type');
            $table->string('title');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route_delivery_type');
    }
}
