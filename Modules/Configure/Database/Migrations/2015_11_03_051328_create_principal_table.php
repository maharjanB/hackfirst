<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreatePrincipalTable extends CustomMigration
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('principal', function (CustomBluePrint $table) {
            $table->increments('id_principal');
            $table->string('title');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('principal');
    }
}
