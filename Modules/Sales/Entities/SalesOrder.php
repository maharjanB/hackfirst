<?php

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class SalesOrder extends BaseModel implements SalesOrderInterface
{
    /**
     * Database Structure.
     *
     *
     * id    int(10)
     * retail_outlet_id    int(10)
     * GID    varchar(255)
     * sku_id    int(10)
     * quantity    int(11)
     * price    double(8,2)
     * promotion_id    double(8,2)
     * order_status_id    int(10)
     * geolocation_latitude    varchar(255)
     * geolocation_longitude    varchar(255)
     * created_by    int(10)
     * updated_by    int(10)
     * created_at    timestamp
     * updated_at    timestamp
     * reason  text
     */

    /**
     * @var SalesOrder Database Table
     */
    protected $table = 'sales_order';
    /**
     * @var SalesOrder Database Table Primary key
     */
    protected $primaryKey = 'id_sales_order';
    /*
    * @var array()
    */
    protected $fillable = ['retail_outlet_id', 'GID', 'sku_id', 'quantity', 'price', 'promotion_id', 'order_status_id',
        'geolocation_latitude', 'geolocation_longitude', 'created_by', 'updated_by','reason' ];

    public function retail_outlet()
    {
        return $this->belongsTo('Modules\Sales\Entities\RetailOutlet');
    }

    public function sku()
    {
        return $this->belongsTo('Modules\Configure\Entities\Sku');
    }

    public function order_status()
    {
        return $this->belongsTo('Modules\Sales\Entities\OrderStatus');
    }
}
