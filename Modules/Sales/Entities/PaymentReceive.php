<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/31/2016
 * Time: 12:02 PM
 */

namespace Modules\Sales\Entities;

use Modules\BaseModel;


class PaymentReceive extends BaseModel implements PaymentReceiveInterface
{
    /**
     * Database Structure of the model
     *
     * id_payemnt_receive
     * sales_order_id
     * payment_date
     * payment_amount
     * payment_mode_id
     * acceptance
     * created on
     * updated on
     * created by
     * updated by
     *
     */


    protected $fillable = ['sales_order_id', 'payment_date', 'payment_amount', 'payment_mode_id', 'acceptance'];

    /**
     *  table name of the model
     *
     */
    protected $table = "payment_receive";

    /**
     *
     * primary key of the table
     */
    protected $primaryKey = "id_payemnt_receive";


}
