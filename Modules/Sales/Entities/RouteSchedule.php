<?php

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class RouteSchedule extends BaseModel implements RouteScheduleInterface
{
    /**
     *  Database Structure of This model.
     *
     * id    int(10)
     * route_id    int(10)
     * retail_outlet_id    int(10)
     * schedule    date
     * created_by    int(10)
     * updated_by    int(10)
     * created_at    timestamp
     * updated_at    timestamp
     */
    protected $fillable = ['route_id','retail_outlet_id','schedule'];

    /**
     * table used by the model.
     */
    protected $table = 'route_schedule';

    /**
     *  primary key of the table.
     */
    protected $primaryKey = 'id_route_schedule';

    /**
     * Route Schedule belongs to route.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route()
    {
        return $this->belongsTo('Modules\Configure\Entities\Route');
    }

    /**
     * Route Schedule belongs To Retail_outlet.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function retail_outlet()
    {
        return $this->belongsTo('RetailOutlet');
    }
}
