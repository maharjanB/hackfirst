<?php

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class RetailOutlet extends BaseModel implements RetailOutletInterface
{
    /**
     * Database Table Structure.
     *
     * id    int(10)
     * title    varchar(255)
     * contact_number1    varchar(255)
     * contact_number2    varchar(255)
     * contact_name    varchar(255)
     * geolocation_longitude    varchar(255)
     * geolocation_latitude    varchar(255)
     * channel_id    int(10)
     * category_id    int(10)
     * town_id    int(10)
     * PAN_number    varchar(255)
     * status    tinyint(4)
     * created_by    int(10)
     * updated_by    int(10)
     * created_at    timestamp
     * updated_at    timestamp
     */
    protected $guarded = [];

    protected $table = 'retail_outlet';

    protected $primaryKey = 'id_retail_outlet';

    public function channel()
    {
        return $this->belongsTo('Modules\Configure\Entities\Channel');
    }

    public function category()
    {
        return $this->belongsTo('Modules\Configure\Entities\Category');
    }

    public function retail_outlet_photo()
    {
        return $this->hasMany('RetailOutlet');
    }

    public function sales_order()
    {
        return $this->hasMany('SalesOrder');
    }

    public function route()
    {
        return $this->belongsToMany('Modules\Sales\Entities\Route', 'route_retail_outlet');
    }

    public function town()
    {
        return $this->belongsTo('Modules\Configure\Entities\Town');
    }
}
