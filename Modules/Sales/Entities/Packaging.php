<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:36 PM.
 */

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class Packaging extends BaseModel implements  PackagingInterface
{
    /***
 *  Database Structure of the model
 /*
                 *id_packing
                sku_id
                unit_id
                quantity
                created on
                updated on
                created by
                updated by
 */

    protected $fillable = [];

    /**
     *  table of the model.
     */
    protected $table = 'packaging';

    /**
     *  primary key of the table;.
     */
    protected $primaryKey = 'id_packing';
}
