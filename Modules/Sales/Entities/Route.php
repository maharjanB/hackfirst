<?php

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class Route extends BaseModel implements RouteInterface
{
    /**
     * Database Structure of Route
     * /*
     * id_route    int(10)
     * title    varchar(255)
     * route_visit_category_id    int(10)
     * route_visit_type_id    int(10)
     * route_delivery_type_id    int(10)
     * route_visit_frequency_id    int(10)
     * status    tinyint(4)
     * created_by    int(10)
     * updated_by    int(10)
     * created_at    timestamp
     * updated_at    timestamp.
     */
    protected $guarded = [];
    /**
     * @database table name
     */
    protected $table = 'route';

    /**
     * @primary key of table
     */
    protected $primaryKey = 'id_route';

    /**
     *  route_visit_category Belongs To Route.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route_visit_type()
    {
        return $this->belongsTo('\Modules\Configure\Entities\RouteVisitType','route_visit_type_id');
    }

    /**
     * Route_visit_category Belongs To Route.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route_visit_category()
    {
        return $this->belongsTo('\Modules\Configure\Entities\RouteVisitCategory','route_visit_category_id');
    }

    /**
     *  route_delivery_type Belongs to Route.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route_delivery_type()
    {
        return $this->belongsTo('\Modules\Configure\Entities\RouteDeliveryType','route_delivery_type_id');
    }

    /**
     *route_visit_frequency belongs to Route.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function route_visit_frequency()
    {
        return $this->belongsTo('\Modules\Configure\Entities\RouteVisitFrequency','route_visit_frequency_id');
    }

    /**
     *  Route belongs to many retail_outlet.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function retail_outlet()
    {
        return $this->belongsToMany('Modules\Sales\Entities\RetailOutlet', 'route_retail_outlet');
    }
}
