<?php

namespace Modules\Sales\Entities;

use Modules\BaseModel;

class OrderStatus extends BaseModel implements OrderStatusInterface
{
    /***
     *  Database Structure of the model
     *
     /*
      id	int(10)
      title	varchar(255)
      status	tinyint(4)
      created_at	timestamp
      updated_at	timestamp
     */

    /**
     *  table used by the model.
     */
    protected $table = 'order_status';

    protected $primaryKey = 'id_order_status';

    protected $fillable = ['title','status'];

    /**
     * Sales_order has many order_status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salesOrder()
    {
        return $this->hasMany('SalesOrder');
    }
}
