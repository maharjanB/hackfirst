<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateSalesOrderTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('sales_order', function (CustomBluePrint $table) {
            $table->increments('id_sales_order');
            $table->integer('retail_outlet_id')->unsigned();
            $table->string('GID');
            $table->integer('sku_id')->unsigned();
            $table->integer('quantity');
            $table->float('price');
            $table->float('promotion_id');
            $table->integer('order_status_id')->unsigned();
            $table->string('geolocation_latitude');
            $table->string('geolocation_longitude');
            $table->authors();
            $table->timestamps();

            $table->foreign('retail_outlet_id')->references('id_retail_outlet')->on('retail_outlet');
            $table->foreign('sku_id')->references('id_sku')->on('sku');
            $table->foreign('order_status_id')->references('id_order_status')->on('order_status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('sales_order');
    }
}
