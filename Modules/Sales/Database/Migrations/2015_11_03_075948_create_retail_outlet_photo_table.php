<?php

use Modules\CustomMigration;
use Modules\CustomBluePrint;

class CreateRetailOutletPhotoTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('retail_outlet_photo', function (CustomBluePrint $table) {
            $table->increments('id_retail_outlet_photo');
            $table->integer('retail_outlet_id')->unsigned();
            $table->string('name');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();
            $table->foreign('retail_outlet_id')->references('id_retail_outlet')->on('retail_outlet');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('retail_outlet_photo');
    }
}
