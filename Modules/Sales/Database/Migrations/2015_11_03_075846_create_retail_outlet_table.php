<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateRetailOutletTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('retail_outlet', function (CustomBluePrint $table) {
            $table->increments('id_retail_outlet');
            $table->string('title');
            $table->string('contact_number1');
            $table->string('contact_number2');
            $table->string('contact_name');
            $table->string('geolocation_longitude');
            $table->string('geolocation_latitude');
            $table->integer('channel_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->integer('town_id')->unsigned();
            $table->string('PAN_number');
            $table->tinyInteger('status');
            $table->authors();
            $table->timestamps();

            $table->foreign('channel_id')->references('id_channel')->on('channel');
            $table->foreign('category_id')->references('id_category')->on('category');
            $table->foreign('town_id')->references('id_town')->on('town');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('retail_outlet');
    }
}
