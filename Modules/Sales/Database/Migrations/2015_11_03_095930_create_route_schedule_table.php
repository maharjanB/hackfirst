<?php

use Modules\CustomBluePrint;
use Modules\CustomMigration;

class CreateRouteScheduleTable extends CustomMigration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        $this->schema->create('route_schedule', function (CustomBluePrint $table) {
            $table->increments('id_route_schedule');
            $table->integer('route_id')->unsigned();
            $table->integer('retail_outlet_id')->unsigned();
            $table->date('schedule');
            $table->authors();
            $table->timestamps();

            $table->foreign('route_id')->references('id_route')->on('route');
            $table->foreign('retail_outlet_id')->references('id_retail_outlet')->on('retail_outlet');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('route_schedule');
    }
}
