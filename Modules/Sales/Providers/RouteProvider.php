<?php

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\Route;
use Modules\Sales\Entities\RouteInterface;

class RouteProvider extends  ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RouteInterface::class, function () {
            return new Route();
        });
    }
}
