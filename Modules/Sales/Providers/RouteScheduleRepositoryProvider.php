<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 12:40 PM.
 */

namespace Modules\Sales\Providers;

use Modules\Sales\Repositories\RouteScheduleRepositoryInterface;
use Modules\Sales\Entities\RouteScheduleInterface;
use Modules\Sales\Repositories\RouteScheduleRepository;
use Illuminate\Support\ServiceProvider;

class RouteScheduleRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register RetailOutletRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(RouteScheduleRepositoryInterface::class, function () {
            return new RouteScheduleRepository($this->app[RouteScheduleInterface::class]);
        });
    }
}
