<?php

namespace Modules\Sales\Providers;

use Modules\Sales\Entities\SalesOrderInterface;
use Modules\Sales\Repositories\SalesOrderRepository;
use Modules\Sales\Repositories\SalesOrderRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class SalesOrderRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(SalesOrderRepositoryInterface::class, function () {
            return new SalesOrderRepository($this->app[SalesOrderInterface::class]);
        });
    }
}
