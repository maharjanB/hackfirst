<?php

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\PackagingInterface;
use Modules\Sales\Entities\Packaging;

class PackagingProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PackagingInterface::class, function () {
            return new Packaging();
        });
    }
}
