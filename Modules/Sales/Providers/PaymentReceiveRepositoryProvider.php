<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/31/2016
 * Time: 12:20 PM
 */

namespace Modules\Sales\Providers;


use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\PaymentReceiveInterface;
use Modules\Sales\Repositories\PaymentReceiveRepositoryInterface;
use Modules\Sales\Repositories\PaymentReceiveRepository;


class PaymentReceiveRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register PaymentReceiveRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(PaymentReceiveRepositoryInterface::class, function () {
            return new PaymentReceiveRepository($this->app[PaymentReceiveInterface::class]);
        });
    }
}
