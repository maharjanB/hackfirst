<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/31/2016
 * Time: 12:20 PM
 */

namespace Modules\Sales\Providers;


use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\PaymentReceiveInterface;
use Modules\Sales\Entities\PaymentReceive;

class PaymentReceiveProvider extends ServiceProvider
{
    /**
     * Register   PaymentReceive
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PaymentReceiveInterface::class, function () {
            return new PaymentReceive();
        });
    }

}
