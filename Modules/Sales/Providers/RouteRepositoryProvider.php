<?php

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\RouteInterface;
use Modules\Sales\Repositories\RouteRepository;
use Modules\Sales\Repositories\RouteRepositoryInterface;

class RouteRepositoryProvider extends ServiceProvider
{
    /**
     * Registers a Service into the Container.
     */
    public function register()
    {
        // Register RouteRepositoryInterface
        $this->container = $this->app;

        $this->container->bind(RouteRepositoryInterface::class, function () {
            return new RouteRepository($this->app[RouteInterface::class]);
        });
    }
}
