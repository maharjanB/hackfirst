<?php

namespace Modules\Sales\Providers;

use Modules\Sales\Entities\SalesOrder;
use Modules\Sales\Entities\SalesOrderInterface;
use Illuminate\Contracts\Container\Container;
use Illuminate\Support\ServiceProvider;

class SalesOrderModelProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(SalesOrderInterface::class, function () {
            return new SalesOrder();
        });
    }
}
