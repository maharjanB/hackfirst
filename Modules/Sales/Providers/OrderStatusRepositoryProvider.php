<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 2:58 PM
 */

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Repositories\OrderStatusRepositoryInterface;
use Modules\Sales\Entities\OrderStatusInterface;
use Modules\Sales\Repositories\OrderStatusRepository;


class OrderStatusRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register OrderStatusRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(OrderStatusRepositoryInterface::class, function () {
            return new OrderStatusRepository($this->app[OrderStatusInterface::class]);
        });
    }
}
