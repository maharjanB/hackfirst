<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 12:40 PM.
 */

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\RouteScheduleInterface;
use Modules\Sales\Entities\RouteSchedule;

class RouteScheduleProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RouteScheduleInterface::class, function () {
            return new RouteSchedule();
        });
    }
}
