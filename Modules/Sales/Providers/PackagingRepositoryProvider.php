<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:47 PM.
 */

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Repositories\PackagingRepositoryInterface;
use Modules\Sales\Repositories\PackagingRepository;
use Modules\Sales\Entities\PackagingInterface;

class PackagingRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        // Register RetailOutletRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(PackagingRepositoryInterface::class, function () {
            return new PackagingRepository($this->app[PackagingInterface::class]);
        });
    }
}
