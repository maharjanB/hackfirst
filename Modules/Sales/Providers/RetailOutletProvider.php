<?php
/**
 * Created by Suresh
 * Date: 101/13/2016
 * Time: 10:19 PM.
 */

namespace Modules\Sales\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\RetailOutlet;
use Modules\Sales\Entities\RetailOutletInterface;

class RetailOutletProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return RetailOutlet
     */
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(RetailOutletInterface::class, function () {
            return new RetailOutlet();
        });
    }
}
