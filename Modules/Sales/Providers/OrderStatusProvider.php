<?php
/**
 * Created by PhpStorm.
 * User: binita
 * Date: 2/18/16
 * Time: 12:19 PM
 */

namespace Modules\Sales\Providers;


use Illuminate\Support\ServiceProvider;
use Modules\Sales\Entities\OrderStatus;
use Modules\Sales\Entities\OrderStatusInterface;

class OrderStatusProvider  extends ServiceProvider
{
    public function register()
{
    $this->container = $this->app;
    $this->container->bind(OrderStatusInterface::class, function () {
        return new OrderStatus();
    });
}
}