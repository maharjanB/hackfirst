<?php
/**
 * Created by Suresh
 * Date: 01/13/2016
 * Time: 10:22 PM.
 */

namespace Modules\Sales\Providers;

use Modules\Sales\Entities\RetailOutletInterface;
use Modules\Sales\Repositories\RetailOutletRepository;
use Modules\Sales\Repositories\RetailOutletRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RetailOutletRepositoryProvider extends ServiceProvider
{
    /**
     * Register Service into the Container.
     *
     * @return RetailOutletRepository
     */
    public function register()
    {
        // Register RetailOutletRepositoryInterface
        $this->container = $this->app;
        $this->container->bind(RetailOutletRepositoryInterface::class, function () {
            return new RetailOutletRepository($this->app[RetailOutletInterface::class]);
        });
    }
}
