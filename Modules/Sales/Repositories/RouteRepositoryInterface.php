<?php

namespace Modules\Sales\Repositories;

use Modules\BaseRepositoryInterface;

interface RouteRepositoryInterface extends BaseRepositoryInterface
{
    public function assignRetailOutlet($id, array $data = []);

    public function transferRetailOutlet($id, array $data = []);
}
