<?php

namespace Modules\Sales\Repositories;

use Modules\BaseRepositoryInterface;

interface SalesOrderRepositoryInterface extends BaseRepositoryInterface
{
}
