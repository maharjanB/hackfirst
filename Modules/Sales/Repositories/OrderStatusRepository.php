<?php
/**
 * Created by PhpStorm.
 * User: binita
 * Date: 2/18/16
 * Time: 12:12 PM
 */

namespace Modules\Sales\Repositories;

use Modules\Sales\Entities\OrderStatusInterface;
use Modules\AbstractRepository;

class OrderStatusRepository extends AbstractRepository implements OrderStatusRepositoryInterface
{
    protected $model;

    public function __construct(OrderStatusInterface $model)
    {
        $this->model = $model;
    }
}