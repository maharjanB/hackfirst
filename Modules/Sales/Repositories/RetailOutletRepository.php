<?php

namespace Modules\Sales\Repositories;

use Modules\AbstractRepository;
use Modules\Sales\Entities\RetailOutletInterface;
use Modules\Sales\Entities\RetailOutlet;

class RetailOutletRepository extends AbstractRepository implements RetailOutletRepositoryInterface
{
    /**
     * The Model instance.
     *
     * @var RetailOutletInterface
     */
    protected $model;

    public function __construct(RetailOutlet $model)
    {
        $this->model = $model;
    }


}
