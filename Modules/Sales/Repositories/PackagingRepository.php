<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:48 PM.
 */

namespace Modules\Sales\Repositories;

use Modules\AbstractRepository;
use Modules\Sales\Entities\PackagingInterface;

class PackagingRepository extends AbstractRepository implements PackagingRepositoryInterface
{
    protected $model;

    public function __construct(PackagingInterface $model)
    {
        $this->model = $model;
    }
}
