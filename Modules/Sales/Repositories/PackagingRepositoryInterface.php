<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/30/2016
 * Time: 11:48 PM.
 */

namespace Modules\Sales\Repositories;

use Modules\BaseRepositoryInterface;

interface PackagingRepositoryInterface extends BaseRepositoryInterface
{
}
