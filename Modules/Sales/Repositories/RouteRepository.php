<?php

namespace Modules\Sales\Repositories;

use Modules\Exception\ExceptionLogger;
use Modules\Sales\Entities\RouteInterface;
use Modules\AbstractRepository;

class RouteRepository extends AbstractRepository implements RouteRepositoryInterface
{
    /**
     * @var RouteInterface
     */
    protected $model;

    /**
     * RouteRepository constructor.
     *
     * @param  $model
     */
    public function __construct(RouteInterface $model)
    {
        $this->model = $model;
    }


    /**
     * Remove specific data.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function removeAssignedOutlets($id, array $data = [])
    {
        $result = '';
        $id = $data['route_id'] = $data['id_route'];
        $model = $this->model->findById($id);
        try {
            if (isset($data['id_retail_outlet'])) {
                $pivot_id['retail_outlet_id'] = $data['id_retail_outlet'];
                $output = $model->find($id)->retail_outlet()->detach($pivot_id);
                $result = $data;
                //$result['message']['route_id'][$id]['retail_outlet_id'][$data['id_retail_outlet']] = $output;
            } else {
                $result['message'] = 'Retail Outlet is not entered';
            }
            return $result;
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' removeAssignedOutlets Error: ' . $e->getMessage(), 'DBError');
            return false;
        }
    }

    /**
     * assign specific data.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function assignRetailOutlet($id, array $data = [])
    {
        $result = '';//detail[id_retail_outlet]
        $data['retail_outlet_id'] = $data['id_retail_outlet'];
        $data['route_id'] = $id;
        //$new_array = array($data['retail_outlet_id'] => $data['id_retail_outlet'], $data['route_id'] => $id);
        //array_merge($data, $new_array);
        // $data['retail_outlet_id'] = $data;
        $model = $this->model->findById($id);
        try {
            //$output = $model->find($id)->retail_outlet()->detach();
            //  if (isset($data['retail_outlet_id']) || is_array($data['retail_outlet_id'])) {
            if (isset($data['retail_outlet_id'])) {
                // foreach ($data['retail_outlet_id'] as $value) {

                $pivot_id['retail_outlet_id'] = $data['retail_outlet_id'];
                $output = $model->retail_outlet()->attach($pivot_id);
                $result = $data;
                // $result['message']['route_id'][$id]['retail_outlet_id'][$data['retail_outlet_id']] = $output;
                // }
            } else {
                $result['message'] = 'Retail Outlet is not entered';
            }
            return $result;
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' assignRetailOutlet Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }

    /**
     * Transfer specific data.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function transferRetailOutlet($id, array $data = [])
    {
        $result = '';

        $model = $this->model->findById($id);
        try {
            if (isset($data['retail_outlet_id']) && is_array($data['retail_outlet_id'])) {
                foreach ($data['retail_outlet_id'] as $value) {
                    $pivot_id['retail_outlet_id'] = $value;

                    $field['status'] = 0;
                    $output = $model->find($id)->retail_outlet()
                        ->updateExistingPivot($pivot_id['retail_outlet_id'], $field);

                    $result['message']['route_id'][$id]['retail_outlet_id'][$value]['status'] = $output;
                }
            } else {
                $result['message'] = 'Retail Outlet is not entered';
            }

            return $result;
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' transferRetailOutlet Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }
}
