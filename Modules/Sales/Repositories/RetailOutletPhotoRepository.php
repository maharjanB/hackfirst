<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 2:25 PM
 */

namespace Modules\Sales\Repositories;

use Modules\AbstractRepository;
use Modules\Sales\Entities\RetailOutletPhotoInterface;
use Modules\Sales\Entities\RetailOutletPhoto;


class RetailOutletPhotoRepository extends AbstractRepository implements RetailOutletPhotoRepositoryInterface
{
    protected $model;

    public function __construct(RetailOutletPhoto $model)
    {
        $this->model = $model;
    }

}
