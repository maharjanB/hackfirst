<?php


namespace Modules\Sales\Repositories;

use Modules\AbstractRepository;
use Modules\Sales\Entities\PaymentReceiveInterface;

class PaymentReceiveRepository extends AbstractRepository implements PaymentReceiveRepositoryInterface
{

    protected $model;

    public function __construct(PaymentReceiveInterface $model)
    {
        $this->model = $model;
    }
}
