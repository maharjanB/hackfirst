<?php

namespace Modules\Sales\Repositories;

use Modules\BaseRepositoryInterface;

interface RetailOutletRepositoryInterface extends BaseRepositoryInterface
{
}
