<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 11:34 AM.
 */

namespace Modules\Sales\Repositories;

use Modules\AbstractRepository;
use Modules\Sales\Entities\RouteScheduleInterface;

class RouteScheduleRepository extends AbstractRepository implements RouteScheduleRepositoryInterface
{
    protected $model;

    public function __construct(RouteScheduleInterface $model)
    {
        $this->model = $model;
    }
}
