<?php

namespace Modules\Sales\Repositories;

use Modules\Sales\Entities\SalesOrder;
use Modules\AbstractRepository;

class SalesOrderRepository extends AbstractRepository implements SalesOrderRepositoryInterface
{
    protected $model;

    public function __construct(SalesOrder $salesOrder)
    {
        $this->model = $salesOrder;
    }
}
