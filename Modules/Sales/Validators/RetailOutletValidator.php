<?php

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RetailOutletValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required',
        'contact_number1' => 'required',
        'contact_number2' => 'required',
        // 'contact_name' => 'required',
        //'geolocation_longitude' => 'required',
        // 'geolocation_latitude' => 'required',
        'channel_id' => 'required',
        'category_id' => 'required',
        'town_id' => 'required',
        'PAN_number' => 'required',
        'status' => 'required',
    );
}
