<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/31/2016
 * Time: 12:36 PM
 */

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class PaymentReceiveValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating new SalesOrder.
     */
    protected $rules = array(
        'sales_order_id' => 'required',
        'payment_date' => 'required',
        'payment_amount' => 'required',
        'payment_mode_id' => 'required',
        'acceptance' => 'required',
    );
}

    {

    }
