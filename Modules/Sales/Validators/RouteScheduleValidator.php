<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 11:38 AM.
 */

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RouteScheduleValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'route_id' => 'required|integer',
        'retail_outlet_id' => 'integer|integer',
        'schedule' => 'required', );
}
