<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:27 PM.
 */

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class OrderStatusValidator extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'title' => 'required',
        'status' => 'required',
    );
}
