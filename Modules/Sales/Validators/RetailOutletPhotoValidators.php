<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/29/2016
 * Time: 3:28 PM.
 */

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RetailOutletPhotoValidators extends LaravelValidator implements ValidationInterface
{
    protected $rules = array(
        'retail_outlet_id' => 'required|integer',
        'name' => 'required',
        'status' => 'required|integer',
    );
}
