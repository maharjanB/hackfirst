<?php

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class RouteValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * @var Validation of different Field
     */
    protected $rules = array(
        'title' => 'required',
        'route_visit_category_id' => 'required|integer',
        'route_visit_type_id' => 'required|integer',
        'route_delivery_type_id' => 'required|integer',
        'status' => 'required',
    );
}
