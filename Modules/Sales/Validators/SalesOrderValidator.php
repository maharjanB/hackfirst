<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/22/2016
 * Time: 1:43 PM.
 */

namespace Modules\Sales\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class SalesOrderValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating new SalesOrder.
     */
    protected $rules = array(
        //'retail_outlet_id' => 'required',
        //'sku_id' => 'required',
        'quantity' => 'required',
        'price' => 'required',
       //'order_status_id' => 'required',
       // 'geolocation_latitude' => 'required',
       // 'geolocation_longitude' => 'required',
    );
}
