<?php

namespace Modules\Sales\Services\WeightAndVolume;

use Modules\AbstractService;
use Modules\Sales\Validators\PackagingValidator;
use Modules\Sales\Repositories\PackagingRepositoryInterface;

class PackagingService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(PackagingRepositoryInterface $repositoryInterface)
    {
        $this->repository = $repositoryInterface;
        $this->validator = new PackagingValidator();
    }
}
