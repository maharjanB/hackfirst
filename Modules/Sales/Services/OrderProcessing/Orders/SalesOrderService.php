<?php

namespace Modules\Sales\Services\OrderProcessing\Orders;

use Modules\Sales\Repositories\SalesOrderRepositoryInterface;
use Modules\AbstractService;
use Modules\Sales\Validators\SalesOrderValidator;

class SalesOrderService extends AbstractService
{
    public function __construct(SalesOrderRepositoryInterface $salesInterface)
    {
        $this->repository = $salesInterface;
        $this->validator = new SalesOrderValidator();
    }
}
