<?php
namespace Modules\Sales\Services\OrderProcessing\Orders;

use Modules\AbstractService;
use Modules\Sales\Repositories\OrderStatusRepositoryInterface;
use Modules\Sales\Validators\OrderStatusValidator;

class OrderStatusService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(OrderStatusRepositoryInterface $repositoryInterface)
    {
        $this->repository = $repositoryInterface;
        $this->validator = new OrderStatusValidator();
    }
}
