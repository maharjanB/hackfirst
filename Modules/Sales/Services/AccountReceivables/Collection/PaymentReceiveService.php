<?php
namespace Modules\Sales\Services\AccountReceivables\Collection;


use Modules\AbstractService;
use Modules\Sales\Repositories\PaymentReceiveRepositoryInterface;
use Modules\Sales\Validators\PaymentReceiveValidator;

class PaymentReceiveService extends AbstractService
{
    protected $validator;
    protected $repository;

    public function __construct(PaymentReceiveRepositoryInterface $repositoryInterface)
    {
        $this->repository = $repositoryInterface;
        $this->validator = new PaymentReceiveValidator();
    }

}
