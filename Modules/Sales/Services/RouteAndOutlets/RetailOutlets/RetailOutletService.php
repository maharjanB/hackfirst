<?php

namespace Modules\Sales\Services\RouteAndOutlets\RetailOutlets;

use Modules\Sales\Repositories\RetailOutletRepositoryInterface;
use Modules\AbstractService;
use Modules\Sales\Validators\RetailOutletValidator;

class RetailOutletService extends AbstractService
{
    protected $repository;
    protected $validator;

    /**
     * Create a new RetailOutletService instance.
     *
     * @param RetailOutletRepositoryInterface $repository
     */
    public function __construct(RetailOutletRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new RetailOutletValidator();
    }
}
