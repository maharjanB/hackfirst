<?php

namespace Modules\Sales\Services\RouteAndOutlets\Routes;

use Modules\AbstractService;
use Modules\Sales\Repositories\RouteRepositoryInterface;
use Modules\Sales\Validators\RouteValidator;

class RouteService extends AbstractService
{
    /**
     * @var RouteRepositoryInterface
     */
    protected $repository;
    protected $validator;

    /**
     * @param RouteRepositoryInterface $repository
     */
    public function __construct(RouteRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new RouteValidator();
    }

    public function assignRetailOutlet($id, $input)
    {
        return $this->repository->assignRetailOutlet($id, $input);
    }

    public function transferRetailOutlet($id, $input)
    {
        return $this->repository->transferRetailOutlet($id, $input);
    }

    public function removeAssignedOutlets($id, $input)
    {
        return $this->repository->removeAssignedOutlets($id, $input);
    }
}
