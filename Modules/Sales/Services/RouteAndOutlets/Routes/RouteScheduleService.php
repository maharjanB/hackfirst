<?php

namespace Modules\Sales\Services\RouteAndOutlets\Routes;

use Modules\AbstractService;
use Modules\Sales\Validators\RouteScheduleValidator;
use Modules\Sales\Repositories\RouteScheduleRepositoryInterface;

class RouteScheduleService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(RouteScheduleRepositoryInterface $repositoryInterface)
    {
        $this->repository = $repositoryInterface;
        $this->validator = new RouteScheduleValidator();

    }
}
