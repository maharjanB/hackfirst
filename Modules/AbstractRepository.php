<?php
/**
 * Created by PhpStorm.
 * User: udnbikesh
 * Date: 1/21/16
 * Time: 4:32 PM.
 */

namespace Modules;

use Modules\Exception\ExceptionLogger;

class AbstractRepository
{
    /**
     * Get all.
     */
    public function get()
    {
        return $this->model->all();
    }

    /**
     * Get specific data by Id.
     *
     * @param $id
     *
     * @return mixed
     */
    public function getById($id)
    {
        return $this->model->findById($id);
    }

    /**
     * Create specific data.
     *
     * @param array $data
     *
     * @return mixed
     */
    public function create(array $data = [])
    {
        try {
            return $this->model->create($data);
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' create Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }

    /**
     * Update specific data.
     *
     * @param int $id
     * @param array $data
     *
     * @return mixed
     */
    public function update($id, array $data = [])
    {
        $model = $this->getById($id);
        try {
            return $model->update($data);
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' update Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }

    /**
     * Delete specific data.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        $model = $this->getById($id);
        try {
            return $model->delete();
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' delete Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }

    /**
     * Deactivate specific data.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function deActivate($id)
    {
        $model = $this->getById($id);
        $model->status = 0;

        if (!$model->save()) {
            return false;
        }
        try {
            return true;
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' deactivate Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }

    /**
     * Deactivate specific data.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function activate($id)
    {
        $model = $this->getById($id);
        $model->status = 1;

        if (!$model->save()) {
            return false;
        }
        try {
            return true;
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' activate Error: ' . $e->getMessage(), 'DBError');

            return false;
        }

    }

    /**
     * @param $field
     * @param $value
     *
     * @return mixed
     *
     * @throws DbException
     */
    public function select($field, $value)
    {
        try {
            return $this->model->where($field, '=', $value)->get();
        } catch (\Exception $e) {
            ExceptionLogger::write(get_called_class() . ' Select ' . 'Error: ' . $e->getMessage(), 'DBError');

            return false;
        }
    }
}
