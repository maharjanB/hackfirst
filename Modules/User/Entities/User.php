<?php

namespace Modules\User\Entities;

use Modules\BaseModel;

class User extends BaseModel implements UserInterface
{
    /* Model database structure
      id_user                        int(10)
      user_group_id             int(10)
      email                     varchar(255)
      password                  varchar(60)
      remember_token            varchar(100)
      first_name                varchar(255)
      last_name                 varchar(255)
      IMEI_number               varchar(255)
      mobile_number             varchar(255)
      MAC_id                    varchar(255)
      auth_type                 varchar(255)
      password_reset_hash	varchar(255)
      password_reset_time	timestamp
      status                    tinyint(4)
      created_by                int(10)
      updated_by                int(10)
      created_at                timestamp
      updated_at                timestamp
     */

    protected $table = 'user';

    protected $guarded = [];
    protected $primaryKey = 'id_user';

/*    protected $fillable = ['id_user','user_group_id', 'email', 'password', 'remember_token', 'first_name', 'last_name',
        'IMEI_number', 'mobile_number', 'MAC_id', 'auth_type', 'status', ];*/

    public function user_group()
    {
        return $this->belongsTo('UserGroup');
    }

    public function user_session()
    {
        return $this->hasOne('UserSession');
    }

    /*
     * user is associated to geographic_location.
     */

    public function geographic_location()
    {
        $this->belongsToMany('Modules\Configure\Entities\Geographic_Location');
    }

    /*
     * A route is assigned to a user(DSEs)
     */

    public function route()
    {
        $this->belongsToMany('Modules\Configure\Entities\Route');
    }

    /*
     * A distributors has many users( Distributor user)
     */

    public function distributor()
    {
        $this->belongsToMany('Modules\Configure\Entities\Distributor');
    }
}
