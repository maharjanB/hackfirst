<?php

namespace Modules\User\Entities;

use Modules\BaseModel;

class UserSession extends BaseModel implements UserSessionInterface
{
    /*
      id                int(10)
      user_id           int(10)
      token             varchar(90)
      latitude          varchar(255)
      longitude         varchar(255)
      expired_on	timestamp
      created_by	int(10)
      updated_by	int(10)
      created_at	timestamp
      updated_at	timestamp
     */

    protected $table = 'user_session';

    protected $primaryKey = 'id_user_session';

    protected $fillable = ['user_id','token','latitude','longitude','created_on','expired_on','status'];

    public function user()
    {
        return $this->belongsTo('User');
    }
}
