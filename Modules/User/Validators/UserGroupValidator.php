<?php

namespace Modules\User\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class UserGroupValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new UserGroup.
     *
     * @var array
     */
    protected $rules = array(
        'group_name' => 'required|min:6',
        'parent_group_id' => 'required|integer',
        'status' => 'required',
    );
}
