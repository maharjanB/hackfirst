<?php

namespace Modules\User\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class UserGroupPermissionValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new UserGroupPermission.
     *
     * @var array
     */
    protected $rules = array(
        'user_group_id' => 'required|integer',
        'permission_id' => 'required|integer',
        'module_id' => 'required|integer',
        'status' => 'required',
    );
}
