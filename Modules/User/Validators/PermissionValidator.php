<?php

namespace Modules\User\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class PermissionValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new Permission.
     *
     * @var array
     */
    protected $rules = array(
        'description' => 'required|min:6',
        'role_name' => 'required',
        'status' => 'required',
    );
}
