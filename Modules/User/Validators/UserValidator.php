<?php

namespace Modules\User\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class UserValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new User.
     *
     * @var array
     */
    protected $rules = array(
        'email' => 'required',
        'password' => 'required|min:6',
        'first_name' => 'required',
        'last_name' => 'required',
        'IMEI_number' => 'required',
        'mobile_number' => 'required',
        'MAC_id' => 'required',
        'auth_type' => 'required',
       // 'password_reset_hash' => 'required',
       // 'password_reset_time' => 'required',
        'status' => 'required',
    );
}
