<?php

namespace Modules\User\Validators;

use Modules\LaravelValidator;
use Modules\ValidationInterface;

class UserSessionValidator extends LaravelValidator implements ValidationInterface
{
    /**
     * Validation for creating a new UserSession.
     *
     * @var array
     */
    protected $rules = array(
        'token' => 'required|min:6',
        'user_id' => 'required|integer',
        'status' => 'required',
    );
}
