<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 9:01 AM.
 */

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\User\Entities\UserGroupInterface;
use Modules\User\Repositories\UserGroupRepositoryInterface;
use Modules\User\Repositories\UserGroupRepository;

class UserGroupRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserGroupRepositoryInterface::class, function () {
            return new UserGroupRepository($this->container[UserGroupInterface::class]);
        });
    }
}
