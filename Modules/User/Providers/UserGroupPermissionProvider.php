<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 10:09 AM.
 */

namespace Modules\User\Providers;

use Modules\User\Entities\UserGroupPermissionInterface;
use Modules\User\Entities\UserGroupPermission;
use Illuminate\Support\ServiceProvider;

class UserGroupPermissionProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserGroupPermissionInterface::class, function () {
            return new UserGroupPermission();
        });
    }
}
