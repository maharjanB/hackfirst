<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/13/2016
 * Time: 10:09 AM.
 */

namespace Modules\User\Providers;

use Modules\User\Entities\UserInterface;
use Modules\User\Entities\User;
use Illuminate\Support\ServiceProvider;

class UserProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserInterface::class, function () {
            return new User();
        });
    }
}
