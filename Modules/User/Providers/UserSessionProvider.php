<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 10:09 AM.
 */

namespace Modules\User\Providers;

use Modules\User\Entities\UserSessionInterface;
use Modules\User\Entities\UserSession;
use Illuminate\Support\ServiceProvider;

class UserSessionProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserSessionInterface::class, function () {
            return new UserSession();
        });
    }
}
