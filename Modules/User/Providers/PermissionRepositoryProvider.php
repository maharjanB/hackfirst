<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 9:01 AM.
 */

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\User\Entities\PermissionInterface;
use Modules\User\Repositories\PermissionRepositoryInterface;
use Modules\User\Repositories\PermissionRepository;

class PermissionRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PermissionRepositoryInterface::class, function () {
            return new PermissionRepository($this->container[PermissionInterface::class]);
        });
    }
}
