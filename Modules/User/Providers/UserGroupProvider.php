<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 10:09 AM.
 */

namespace Modules\User\Providers;

use Modules\User\Entities\UserGroupInterface;
use Modules\User\Entities\UserGroup;
use Illuminate\Support\ServiceProvider;

class UserGroupProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserGroupInterface::class, function () {
            return new UserGroup();
        });
    }
}
