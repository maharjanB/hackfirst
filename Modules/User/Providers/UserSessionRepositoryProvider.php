<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 9:01 AM.
 */

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\User\Entities\UserSessionInterface;
use Modules\User\Repositories\UserSessionRepositoryInterface;
use Modules\User\Repositories\UserSessionRepository;

class UserSessionRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserSessionRepositoryInterface::class, function () {
            return new UserSessionRepository($this->container[UserSessionInterface::class]);
        });
    }
}
