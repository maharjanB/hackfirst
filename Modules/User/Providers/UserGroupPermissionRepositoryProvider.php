<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 9:01 AM.
 */

namespace Modules\User\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\User\Entities\UserGroupPermissionInterface;
use Modules\User\Repositories\UserGroupPermissionRepositoryInterface;
use Modules\User\Repositories\UserGroupPermissionRepository;

class UserGroupPermissionRepositoryProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(UserGroupPermissionRepositoryInterface::class, function () {
            return new UserGroupPermissionRepository($this->container[UserGroupPermissionInterface::class]);
        });
    }
}
