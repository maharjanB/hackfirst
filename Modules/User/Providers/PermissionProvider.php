<?php
/**
 * Created by PhpStorm.
 * User: Suraj
 * Date: 1/13/2016
 * Time: 10:09 AM.
 */

namespace Modules\User\Providers;

use Modules\User\Entities\PermissionInterface;
use Modules\User\Entities\Permission;
use Illuminate\Support\ServiceProvider;

class PermissionProvider extends ServiceProvider
{
    public function register()
    {
        $this->container = $this->app;
        $this->container->bind(PermissionInterface::class, function () {
            return new Permission();
        });
    }
}
