<?php

namespace Modules\User\Services;

use Modules\User\Repositories\UserGroupPermissionRepositoryInterface;
use Modules\AbstractService;
use Modules\User\Validators\UserGroupPermissionValidator;

class UserGroupPermissionService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(UserGroupPermissionRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new UserGroupPermissionValidator();
    }
}
