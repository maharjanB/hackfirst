<?php

namespace Modules\User\Services;

use Modules\User\Repositories\PermissionRepositoryInterface;
use Modules\AbstractService;
use Modules\User\Validators\PermissionValidator;

class PermissionService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(PermissionRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new PermissionValidator();
    }
}
