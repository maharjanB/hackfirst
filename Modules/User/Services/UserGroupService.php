<?php

namespace Modules\User\Services;

use Modules\User\Repositories\UserGroupRepositoryInterface;
use Modules\AbstractService;
use Modules\User\Validators\UserGroupValidator;

class UserGroupService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(UserGroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new UserGroupValidator();
    }
}
