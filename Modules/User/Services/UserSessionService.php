<?php

namespace Modules\User\Services;

use Modules\User\Repositories\UserSessionRepositoryInterface;
use Modules\AbstractService;
use Modules\User\Validators\UserSessionValidator;

class UserSessionService extends AbstractService
{
    protected $repository;
    protected $validator;

    public function __construct(UserSessionRepositoryInterface $repository)
    {
        $this->repository = $repository;
        $this->validator = new UserSessionValidator();
    }
}
