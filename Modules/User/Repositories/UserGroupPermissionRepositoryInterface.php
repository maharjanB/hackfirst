<?php

namespace Modules\User\Repositories;

use Modules\BaseRepositoryInterface;

interface UserGroupPermissionRepositoryInterface extends BaseRepositoryInterface
{
}
