<?php

namespace Modules\User\Repositories;

use Modules\BaseRepositoryInterface;

interface UserSessionRepositoryInterface extends BaseRepositoryInterface
{
}
