<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/13/2016
 * Time: 10:28 AM.
 */

namespace Modules\User\Repositories;

use Modules\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
}
