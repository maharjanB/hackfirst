<?php


namespace Modules\User\Repositories;

use Modules\User\Entities\UserGroupPermissionInterface;
use Modules\AbstractRepository;

class UserGroupPermissionRepository extends AbstractRepository implements UserGroupPermissionRepositoryInterface
{
    protected $model;

    public function __construct(UserGroupPermissionInterface $model)
    {
        $this->model = $model;
    }
}
