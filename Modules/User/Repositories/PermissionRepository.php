<?php


namespace Modules\User\Repositories;

use Modules\User\Entities\PermissionInterface;
use Modules\AbstractRepository;

class PermissionRepository extends AbstractRepository implements PermissionRepositoryInterface
{
    protected $model;

    public function __construct(PermissionInterface $model)
    {
        $this->model = $model;
    }
}
