<?php


namespace Modules\User\Repositories;

use Modules\User\Entities\UserGroupInterface;
use Modules\AbstractRepository;

class UserGroupRepository extends AbstractRepository implements UserGroupRepositoryInterface
{
    protected $model;

    public function __construct(UserGroupInterface $model)
    {
        $this->model = $model;
    }
}
