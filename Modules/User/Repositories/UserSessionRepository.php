<?php


namespace Modules\User\Repositories;

use Modules\User\Entities\UserSessionInterface;
use Modules\AbstractRepository;

class UserSessionRepository extends AbstractRepository implements UserSessionRepositoryInterface
{
    protected $model;

    public function __construct(UserSessionInterface $model)
    {
        $this->model = $model;
    }
}
