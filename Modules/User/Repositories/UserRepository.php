<?php
/**
 * Created by PhpStorm.
 * User: BNTA
 * Date: 1/13/2016
 * Time: 10:28 AM.
 */

namespace Modules\User\Repositories;

use Modules\User\Entities\UserInterface;
use Modules\AbstractRepository;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(UserInterface $model)
    {
        $this->model = $model;
    }
}
