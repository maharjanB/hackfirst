<?php

namespace Modules\User\Repositories;

use Modules\BaseRepositoryInterface;

interface PermissionRepositoryInterface extends BaseRepositoryInterface
{
}
