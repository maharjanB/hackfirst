<?php


Route::get('/', function () {
    //return ("base routing ");
    return view('index');
});

//Route::get('retail-outlet/nullOutlets',
//    'API\Sales\RouteAndOutlet\RetailOutlets\RetailOutletController@getUnassignedOutlets');
//Route::post('retail-outlet/removeOutlets', 'API\Sales\RouteAndOutlet\Route\RouteController@removeAssignedOutlets');
//Route::get('test', function () {
//    //$retailOutlet = new \Modules\Sales\Entities\RetailOutlet();
//    $route = new \Modules\Sales\Entities\Route();
//    return $route->retail_outlet()->wherePivot('id_route', '')->get();
//});


//Route::get('test', function () {
//    return \Modules\Sales\Entities\RetailOutlet::find(1)->route();
//});


Route::group(['prefix' => 'api/v2'], function () {

//Route::post('register','AccountController@register');
	Route::resource('login', 'AccountController');
	Route::get('logout', 'AccountController@logout');
    Route::resource('principal', 'API\Configure\Universe\Organization\PrincipalController');
    Route::resource('project', 'API\Configure\Universe\Organization\ProjectController');
    Route::resource('resource', 'API\Configure\Universe\Organization\ResourceController');
    Route::resource('bussiness-unit', 'API\Configure\Universe\Organization\PrincipalController');
    Route::resource('user', 'API\Configure\Universe\Organization\UserController');


//    Route::group(['middleware' => 'oauth'], function () {
//        	Route::resource('principal', 'API\Configure\Universe\Organization\PrincipalController');
//
//    });
    /*Route::group(['middleware' => 'oauth'], function () {
		Route::resource('principal', 'API\Configure\Universe\Organization\PrincipalController');

	});*/
});


