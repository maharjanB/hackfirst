<?php

namespace App\Http\Controllers;

use App\APIHelpers\Transformers\AuthTransformer;
use App\APIHelpers\Transformers\UserTransformer;
use App\Http\Controllers\API\ApiController;
use app\Http\Controllers\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\User\Entities\User;
use Modules\User\Services\OauthService;
use Modules\User\Services\UserService;
use Modules\User\Services\UserSessionService;
use Symfony\Component\Console\Input\Input;

class AccountController extends ApiController
{

    protected $userService;
    protected $authTransformer;

    protected $userTransformer;

    /**
     * UserController constructor.
     * @param UserService $service
     * @param AuthTransformer $authTransformer
     * @param UserTransformer $userTransformer
     * @param OauthService $oauthService
     * @param UserSessionService $userSessionService
     */
    public function __construct(
        UserService $service,
        AuthTransformer $authTransformer,
        UserTransformer $userTransformer,
        OauthService $oauthService,
        UserSessionService $userSessionService
    ) {
        //   $this->middleware('oauth');
        $this->oauthService = $oauthService;
        $this->userService = $service;
        $this->userSessionService = $userSessionService;
        $this->userTransformer = $userTransformer;
        $this->authTransformer =  $authTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //TODO

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->input();
        if (isset($data['username']) && isset($data['password'])) {
            $result = $this->oauthService->setOauthProcess($data);
            $result['access_token'] = 'f05953e4d72988d6b211d4b756cbc510cd37039f';
            $result['refresh_token'] = 'da19696f6cadd23292e896cdbbf08f09f6d8f708';
            $result['token'] = 'f05953e4d72988d6b211d4b756cbc510cd37039f';
            $result['token_type'] = 'Bearer';
            if (array_key_exists('access_token', $result)) {

                $user = $this->userService->select('email', $data['username']);
                $userInfo = $this->userTransformer->transform($user[0]);
                $mergeResult = array_merge($result,array('userInfo'=>$userInfo));


                return $this->respond([
                    'data' => $this->authTransformer->transform($mergeResult)
                ]);
               // return $this->respond(['status'=>'success', 'data' => $mergeResult]);
            }
            return $this->respond(['data' => $result]);
        }

        return $this->respondWithError(['login credential not provided']);
    }


    /**
     * logout from session
     *
     * @param user id
     *
     * @return bool
     */
    public function logout(Request $request)
    {
        $data = $request->input();
        if (isset($data['user_id'])) {

            $result = $this->userSessionService->delete($data['user_id']);

            if ($result) {

                echo 'Logout Successfull';
            }


        }

    }

    public function register(Request $request)
    {
        $data = $request->input();


    }

}
