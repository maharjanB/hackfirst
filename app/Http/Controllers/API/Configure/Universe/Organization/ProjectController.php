<?php

namespace app\Http\Controllers\API\Configure\Universe\Organization;

use App\APIHelpers\Transformers\PrincipalTransformer;
use App\APIHelpers\Transformers\ProjectTransformer;
use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Modules\Configure\Services\Universe\Organization\PrincipalService;
use Modules\Configure\Services\Universe\Project\ProjectService;

class ProjectController extends ApiController
{
    protected $service;

    protected $dataTransformer;

    /**
     * PrincipalController constructor.
     *
     * @param PrincipalService $service
     * @param PrincipalTransformer $dataTransformer
     */
    public function __construct(ProjectService $service, ProjectTransformer $dataTransformer)
    {
        $this->service = $service;
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project = $this->service->getAll();

        return $this->respond([
            'data' => $this->dataTransformer->transformCollection($project),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //TODO
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createData = $this->service->create($request->input('detail'));

        if (!($createData instanceof ProjectController)) {
            return $this->responseValidationError($createData);
        }

        return $this->respond([
            'data' => $this->dataTransformer->transform($createData),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->service->getById($id);

        if (!$project) {
            return $this->responseNotFound('Project not exists');
        }

        return $this->respond([
            'data' => $this->dataTransformer->transform($project),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //TODO
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = $this->service->getById($id);
        if (!$project) {
            return $this->responseNotFound('Project not exists');
        }
        $updateData = $this->service->edit($project->id_project, $request->input('detail'));
        if (!($updateData)) {
            return $this->responseValidationError($updateData);
        }
        if ($updateData instanceof MessageBag) {
            return $this->responseValidationError($updateData);
        }

        return $this->respond(['data' => $updateData]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = $this->service->getById($id);
        if (!$project) {
            return $this->responseNotFound('Project not exists');
        }
        $delete = $this->service->delete($project->id_principal);

        return $this->respond(['data' => $delete,
        ]);
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param array $input
     *
     * @return \Illuminate\Http\Response
     */


    public function deactivate($id)
    {
        $project = $this->service->getById($id);
        if (!$project) {
            return $this->responseNotFound('Project not exists');
        }
        $deactivate = $this->service->deactivate($project->id_project);
        return $this->respond([
            'data' => $deactivate,
        ]);
    }

    public function activate($id)
    {
        $project = $this->service->getById($id);
        if (!$project) {
            return $this->responseNotFound('Principal not exists');
        }
        $deactivate = $this->service->activate($project->id_project);
        return $this->respond([
            'data' => $deactivate,
        ]);
    }

//    public function deactivate(Request $request)
//    {
//        $result = '';
//        $input = $request->input();
//        if (isset($input['id_principal']) && is_array($input['id_principal'])) {
//            foreach ($input['id_principal'] as $id) {
//                $principal = $this->service->getById($id);
//                if (!$principal) {
//                    $result['fail']['Principal not exists']['id_principal'][] = $id;
//                } else {
//                    $deactivate = $this->service->deactivate($principal->id_principal);
//                    $result['message']['id'][$id] = $deactivate;
//                }
//            }
//        } else {
//            $result['fail'] = 'Principal is not entered';
//        }
//
//        return $this->respond(['data' => $result]);
//    }
//
//    public function activate(Request $request)
//    {
//        $result = '';
//        $input = $request->input();
//        if (isset($input['id_principal']) && is_array($input['id_principal'])) {
//            foreach ($input['id_principal'] as $id) {
//                $principal = $this->service->getById($id);
//                if (!$principal) {
//                    $result['fail']['Principal not exists']['id_principal'][] = $id;
//                } else {
//                    $activate = $this->service->activate($principal->id_principal);
//                    $result['message']['id'][$id] = $activate;
//                }
//            }
//        } else {
//            $result['fail'] = 'Principal is not entered';
//        }
//
//        return $this->respond(['data' => $result]);
//    }
}
