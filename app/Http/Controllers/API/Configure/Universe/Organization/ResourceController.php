<?php

namespace app\Http\Controllers\API\Configure\Universe\Organization;

use App\APIHelpers\Transformers\PrincipalTransformer;
use App\APIHelpers\Transformers\ResourceTransformer;
use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Modules\Configure\Entities\Principal;
use Modules\Configure\Entities\Resource;
use Modules\Configure\Services\Universe\Organization\PrincipalService;
use Modules\Configure\Services\Universe\Resource\ResourceService;

class ResourceController extends ApiController
{
    protected $service;

    protected $dataTransformer;

    /**
     * PrincipalController constructor.
     *
     * @param PrincipalService $service
     * @param PrincipalTransformer $dataTransformer
     */
    public function __construct(ResourceService $service, ResourceTransformer $dataTransformer)
    {
        $this->service = $service;
        $this->dataTransformer = $dataTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $principal = $this->service->getAll();

        return $this->respond([
            'data' => $this->dataTransformer->transformCollection($principal),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //TODO
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $createData = $this->service->create($request->input('detail'));

        if (!($createData instanceof Resource)) {
            return $this->responseValidationError($createData);
        }

        return $this->respond([
            'data' => $this->dataTransformer->transform($createData),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $principal = $this->service->getById($id);

        if (!$principal) {
            return $this->responseNotFound('Resource not exists');
        }

        return $this->respond([
            'data' => $this->dataTransformer->transform($principal),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //TODO
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $principal = $this->service->getById($id);
        if (!$principal) {
            return $this->responseNotFound('Resource not exists');
        }
        $updateData = $this->service->edit($principal->id_resource, $request->input('detail'));
        if (!($updateData)) {
            return $this->responseValidationError($updateData);
        }
        if ($updateData instanceof MessageBag) {
            return $this->responseValidationError($updateData);
        }

        return $this->respond(['data' => $updateData]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $principal = $this->service->getById($id);
        if (!$principal) {
            return $this->responseNotFound('Resource not exists');
        }
        $delete = $this->service->delete($principal->id_resource);

        return $this->respond(['data' => $delete,
        ]);
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param array $input
     *
     * @return \Illuminate\Http\Response
     */


    public function deactivate($id)
    {
        $principal = $this->service->getById($id);
        if (!$principal) {
            return $this->responseNotFound('Principal not exists');
        }
        $deactivate = $this->service->deactivate($principal->id_resource);
        return $this->respond([
            'data' => $deactivate,
        ]);
    }

    public function activate($id)
    {
        $principal = $this->service->getById($id);
        if (!$principal) {
            return $this->responseNotFound('Principal not exists');
        }
        $deactivate = $this->service->activate($principal->id_resource);
        return $this->respond([
            'data' => $deactivate,
        ]);
    }

}
