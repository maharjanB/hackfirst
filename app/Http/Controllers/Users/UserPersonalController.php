<?php

namespace App\Http\Controllers\Users;

use App\APIHelpers\Transformers\UserPersonalTransformer;
use App\Modules\User\Entities\UserImage;
use App\Modules\User\Services\UserPersonalService;
use App\Modules\User\Services\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Modules\Foundation\Controllers\AbstractController;
use App\Modules\Configurations\Services\AddressService;


class UserPersonalController extends AbstractController
{
    protected $service;
    protected $addressService;
    protected $dataTransformer;
    protected $userService;
    protected $primary_key = 'id_user_personal_dtl';


    public function __construct(
        UserPersonalService $service,
        UserPersonalTransformer $userPersonalTransformer,
        AddressService $addressService,
        UserService $userService
    )
    {
        $this->service = $service;
        $this->addressService = $addressService;
        $this->userService = $userService;
        $this->dataTransformer = $userPersonalTransformer;
    }

    public function store(Request $request)
    {

        $location = [];
        $restrictions = [];
        $errorMsg = null;
        $input = $request->input();
        $data = isset($input['detail']) ? $input['detail'] : '';

        $location['country'] = $data['country_id'];
        $location['state'] = $data['state_id'];
        $location['district'] = $data['district_id'];
        $location['city'] = $data['city_id'];
        $location['locality'] = $data['locality_id'];
        $location['address'] = $data['address'];
        if (!empty($location)) {
            $addressInfo = $this->addressService->locationCreate($location);
            $data['country_id'] = isset($addressInfo['country_id']) ? $addressInfo['country_id'] : null;
            $data['state_id'] = isset($addressInfo['state_id']) ? $addressInfo['state_id'] : null;
            $data['district_id'] = isset($addressInfo['district_id']) ? $addressInfo['district_id'] : null;
            $data['locality_id'] = isset($addressInfo['locality_id']) ? $addressInfo['locality_id'] : null;
            $data['city_id'] = isset($addressInfo['city_id']) ? $addressInfo['city_id'] : null;
        }
        $page['pagelimit'] = $input['pagelimit'];
        $page['page'] = $input['page'];
        $page['field'] = 'created_at';
        $page['order'] = 'desc';
        $page['total'] = $input['total'];
        $data['status'] = 1;
        $createData = $this->service->create($data);
        /**
         * Validation Error Section.
         */
        if ($createData instanceof MessageBag) {
            return $this->responseValidationError($createData);
        }

        if (!$createData) {
            return $this->respondWithError('Fail to Create');
        }
        if ($createData['message']) {
            $dbError = explode(':', $createData['message']);
            if ($dbError[2]) {
                $errorMsg = explode('(', $dbError[2]);
                return $this->respondWithError(substr($errorMsg[0], 5));
            }
            return $this->respondWithError(substr($errorMsg[0], 53));
        }
        $permission = [];

        $entityCollection = $this->service->getPaginateList($page);
        if (is_array($entityCollection)) {
            return $this->respond([
                'total'       => $entityCollection ['total'],
                'data'        => $this->dataTransformer->transformCollection($entityCollection['data'], $restrictions),
                'permissions' => $restrictions
            ]);
        }


        return $this->respond([
            'total'       => $entityCollection->total(),
            'data'        => $this->dataTransformer->transformCollection($entityCollection, $restrictions),
            'permissions' => $restrictions

        ]);

    }

    public function update(Request $request,$id)
    {
        $location = [];
        $restrictions = [];
        $errorMsg = null;
        $input = $request->input();
        $data = isset($input['detail']) ? $input['detail'] : '';
        $id = $data['id_user_personal_dtl'];
        $location['country'] = $data['country_id'];
        $location['state'] = $data['state_id'];
        $location['district'] = $data['district_id'];
        $location['city'] = $data['city_id'];
        $location['locality'] = $data['locality_id'];
        $location['address'] = $data['address'];
        if (!empty($location)) {
            $addressInfo = $this->addressService->locationCreate($location);
            $data['country_id'] = isset($addressInfo['country_id']) ? $addressInfo['country_id'] : null;
            $data['state_id'] = isset($addressInfo['state_id']) ? $addressInfo['state_id'] : null;
            $data['district_id'] = isset($addressInfo['district_id']) ? $addressInfo['district_id'] : null;
            $data['locality_id'] = isset($addressInfo['locality_id']) ? $addressInfo['locality_id'] : null;
            $data['city_id'] = isset($addressInfo['city_id']) ? $addressInfo['city_id'] : null;
        }
        $data['status'] = 1;
        $modelId = $this->service->update($id,$data);

        $modelId = $this->service->getById($id);

        if (!$modelId) {
            return $this->responseNotFound('Not found Exception.');
        }
        $updateData = $this->service->update($modelId[ $this->primary_key ], $data);

        if (!$updateData) {
            return $this->responseValidationError($updateData);
        }

        if ($updateData instanceof MessageBag) {
            return $this->responseValidationError($updateData);
        }

        if ($updateData['message']) {
            $errorMsg = explode('(', $updateData['message']);
            return $this->respondWithError(substr($errorMsg[0],53));
        }
        return $this->respond([
            'data' => $updateData,
        ]);



    }


    public function uploadUsedImage(Request $request, $id)
    {

        /**
         * Initially check User exists or Not
         */
//        $userModelId = $this->userService->getById($id);
//
//        if (!$userModelId) {
//            return $this->responseNotFound('User does not Exists');
//        }

        /***
         * Check in the User Images if particular User exists or not
         * if yes delete first then insert
         */
        $userImageId = UserImage::where('user_id',$id)->first();
        if ($userImageId) {
            $file= $userImageId['images'];
            $filename = base_path().'public/images/avatars/'.$file;
            if(file_exists($filename)){
                @unlink($filename);
            }
            UserImage::where('user_id',$id)->delete();
        }
        $errorMsg = null;
        $inputs = $request->all();
        if (isset($inputs['file'])) {
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $destinationPath = "public/images/avatars";
            $fileName = $timestamp . '_' . $inputs['file']->getClientOriginalName();
            $move_photo = $inputs['file']->move(base_path() . '/' . $destinationPath, $fileName);
            $allPath = $move_photo;
            $type = pathinfo($allPath, PATHINFO_EXTENSION);
            $data = file_get_contents($allPath);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
            $inputs['photo'] = $base64;
        }
        $create['images'] = $fileName;
        $create['user_id'] = $id;
        $createImages = UserImage::create($create);
        if ($createImages instanceof MessageBag) {
            return $this->responseValidationError($createImages);
        }

        if (!$createImages) {
            return $this->respondWithError('Fail to Create');
        }
        if ($createImages['message']) {
            $dbError = explode(':', $createImages['message']);
            if ($dbError[2]) {
                $errorMsg = explode('(', $dbError[2]);
                return $this->respondWithError(substr($errorMsg[0], 5));
            }
            return $this->respondWithError(substr($errorMsg[0], 53));
        }
        return $this->respond([
            'data' => true,
        ]);

    }


}
