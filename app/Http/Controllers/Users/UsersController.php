<?php

namespace App\Http\Controllers\Users;

use App\APIHelpers\Transformers\UserHierarchyFromLowTransformer;
use App\APIHelpers\Transformers\UserHierarchyTransformer;
use App\APIHelpers\Transformers\UserTransformer;
use App\Modules\Configurations\Entities\Project;
use App\Modules\Configurations\Services\AddressService;
use App\Modules\User\Entities\User;
use App\Modules\User\Services\ClientDetailService;
use App\Modules\User\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Hash;
use App\Modules\PerformanceEvaluation\Services\EvaluationReviewerService;


class UsersController extends AbstractController
{
    protected $service;
    protected $addressService;
    protected $clientDetailService;
    protected $dataTransformer;
    protected $primary_key = 'id_user';
    protected $userHierarchyTransformer;
    protected $evaluationReviewerService;

    public function __construct(
        UserService $service,
        AddressService $addressService,
        ClientDetailService $clientDetailService,
        UserTransformer $userTransformer,
        UserHierarchyTransformer $userHierarchyTransformer,
        UserHierarchyFromLowTransformer $hierarchyFromLowTransformer,
        EvaluationReviewerService $evaluationReviewerService
    )
    {
        $this->service = $service;
        $this->addressService = $addressService;
        $this->clientDetailService = $clientDetailService;
        $this->dataTransformer = $userTransformer;
        $this->userHierarchyTransformer = $userHierarchyTransformer;
        $this->hierarchyFromLowTransformer = $hierarchyFromLowTransformer;
        $this->evaluationReviewerService = $evaluationReviewerService;
    }

    public function detail(Request $request, $id)
    {
        $data = $this->service->get_user_detail($id);

        return $this->respond(['data' => $this->dataTransformer->userGenerateTransform($data)]);
    }

    public function getPaginateList($query = null)
    {
        $userSkill = $this->model->filtered()->with('skill')->orderBy($query['field'], $query['order']);
        if (isset($query['user_id']))
            $userSkill->where('user_id', $query['user_id']);

        $res = $userSkill->paginate($query['pagelimit'])->toArray();
        return $res;
    }

    public function store(Request $request)
    {

        $location = [];
        $client_detail = [];
        $restrictions = [];
        $errorMsg = null;
        $input = $request->input();
        $data = isset($input['detail']) ? $input['detail'] : '';
        if ($data['country_id'] != null)
            $location['country'] = $data['country_id'];
        if ($data['state_id'] != null)
            $location['state'] = $data['state_id'];
        if ($data['district_id'] != null)
            $location['district'] = $data['district_id'];
        if ($data['city_id'] != null)
            $location['city'] = $data['city_id'];
        if ($data['locality_id'] != null)
            $location['locality'] = $data['locality_id'];
        if ($data['address'] != null)
            $client_detail['address'] = $data['address'];
        if ($data['longitude'] != null)
            $client_detail['longitude'] = $data['longitude'];
        if ($data['latitude'] != null)
            $client_detail['latitude'] = $data['latitude'];
        if ($data['phone_no'] != null)
            $client_detail['phone_no'] = $data['phone_no'];
        if ($data['primary_email'] != null)
            $client_detail['primary_email'] = $data['primary_email'];
        if ($data['primary_person'] != null)
            $client_detail['primary_person'] = $data['primary_person'];
        if ($data['secondary_email'] != null)
            $client_detail['secondary_email'] = $data['secondary_email'];
        if ($data['secondary_person'] != null)
            $client_detail['secondary_person'] = $data['secondary_person'];

        $register['password'] = isset($data['password']) ? bcrypt($data['password']) : null;
        $register['email'] = $data['email'];
        $register['personal_email'] = $data['personal_email'];
        $register['id_card'] = $data['id_card'];
        $register['first_name'] = $data['first_name'];
        $register['middle_name'] = $data['middle_name'];
        $register['last_name'] = $data['last_name'];
        $register['username'] = $data['username'];
        $register['joined_date'] = $data['joinedDate'];
        $register['user_group_id'] = $data['user_group_id'];
        $register['role_id'] = $data['role_id'];
        $register['post_id'] = $data['post_id'];
        $page['pagelimit'] = $input['pagelimit'];
        $page['page'] = $input['page'];
        $page['field'] = 'created_at';
        $page['order'] = 'desc';
        $page['total'] = $input['total'];
        $register['status'] = 1;
        if (!empty($register)) {
            $createData = $this->service->create($register);
            if ($createData instanceof MessageBag) {
                return $this->responseValidationError($createData);
            }

            if (!$createData) {
                return $this->respondWithError('Fail to Create');
            }
            if ($createData['message']) {
                $dbError = explode(':', $createData['message']);
                if ($dbError[2]) {
                    $errorMsg = explode('(', $dbError[2]);
                    return $this->respondWithError(substr($errorMsg[0], 5));
                }
                return $this->respondWithError(substr($errorMsg[0], 53));
            }

            if (!empty($location)) {
                $addressInfo = $this->addressService->locationCreate($location);
                $client_detail['country_id'] = isset($addressInfo['country_id']) ? $addressInfo['country_id'] : null;
                $client_detail['state_id'] = isset($addressInfo['state_id']) ? $addressInfo['state_id'] : null;
                $client_detail['district_id'] = isset($addressInfo['district_id']) ? $addressInfo['district_id'] : null;
                $client_detail['locality_id'] = isset($addressInfo['locality_id']) ? $addressInfo['locality_id'] : null;
                $client_detail['city_id'] = isset($addressInfo['city_id']) ? $addressInfo['city_id'] : null;
                $client_detail['status'] = 1;
                if ($addressInfo instanceof MessageBag) {
                    return $this->responseValidationError($addressInfo);
                }

                if (!$addressInfo) {
                    return $this->respondWithError('Fail to Create');
                }
                if (isset($addressInfo['message'])) {
                    $dbError = explode(':', $addressInfo['message']);
                    if ($dbError[2]) {
                        $errorMsg = explode('(', $dbError[2]);
                        return $this->respondWithError(substr($errorMsg[0], 5));
                    }
                    return $this->respondWithError(substr($errorMsg[0], 53));
                }
            }
            if (!empty($client_detail)) {
                $client_detail['client_id'] = $createData['id_user'];

                $client_detail = $this->clientDetailService->create($client_detail);
                if ($client_detail instanceof MessageBag) {
                    return $this->responseValidationError($client_detail);
                }

                if (!$client_detail) {
                    return $this->respondWithError('Fail to Create');
                }
                if ($client_detail['message']) {
                    $dbError = explode(':', $client_detail['message']);
                    if ($dbError[2]) {
                        $errorMsg = explode('(', $dbError[2]);
                        return $this->respondWithError(substr($errorMsg[0], 5));
                    }
                    return $this->respondWithError(substr($errorMsg[0], 53));
                }
            }


            $permission = [];

            $entityCollection = $this->service->getPaginateList($page);
            if (is_array($entityCollection)) {
                return $this->respond([
                    'total' => $entityCollection ['total'],
                    'data' => $this->dataTransformer->transformCollection($entityCollection['data'], $restrictions),
                    'permissions' => $restrictions
                ]);
            }


            return $this->respond([
                'total' => $entityCollection->total(),
                'data' => $this->dataTransformer->transformCollection($entityCollection, $restrictions),
                'permissions' => $restrictions

            ]);

        }
    }



//    public function getUserInfo(Request $request){
//
//        $permission = [];
//        $id = $request->input('user_id');
//        $user = $this->service->getById($id);
//        if (!$user) {
//            return $this->responseNotFound('Not found Exception');
//        }
//        return $this->respond([
//            'data' => $this->dataTransformer->transformCollection($user, $permission),
//        ]);
//
//    }


    /***
     * Fetch All Child user Id
     * @param Request $request
     * @return mixed
     */
    public function getChildUser(Request $request)
    {
        $parent_user = $request->input('id_user');
        $project = $request->input('project_id');

        $UserId = $this->service->getbyId($parent_user);

        if (empty($UserId)) {
            return $this->responseNotFound('User is not exists');
        }

        $result = $this->service->getChildUser($parent_user, $project);

        return $this->respond(['data' => $result]);

    }


    public function getParentUser(Request $request)
    {
        $parent_user = $request->input('id_user');
        $project = $request->input('project_id');

        $UserId = $this->service->getbyId($parent_user);

        if (empty($UserId)) {
            return $this->responseNotFound('User is not exists');
        }

        $result = $this->service->getParentUser($parent_user, $project);

        return $this->respond(['data' => $result]);

    }


    public function getParentUserAsManager(Request $request)
    {
        $parent_user = $request->input('id_user');
        $project = $request->input('project_id');

        $UserId = $this->service->getbyId($parent_user);

        if (empty($UserId)) {
            return $this->responseNotFound('User is not exists');
        }

        $result = $this->service->getParentUser($parent_user, $project);

        return $this->respond(['data' => $result]);

    }

    /**
     * Assign user as the supervisor
     *
     * @param Request $request
     */
    public function assignParentUserGroup(Request $request)
    {
        $userUpdate = $this->service->updateUser($request->input('user'), $request->input('parent'));
        return $this->respond(['data' => $userUpdate]);
    }

    public function userHierarchy()
    {
        $entityCollection = $this->service->userHierarchy()->toArray();
        $permissionFilter = [];
        $mail = [];

        foreach($entityCollection as $key=>$val){
  $mail[] =$val['parent_user']['email'];
            $mail[] = $val['parent_user']['parent_user']['email'];

        //    for($i=)

        }

        dump($mail);

//        return $this->respond([
//            'data' => $this->hierarchyFromLowTransformer->transformCollection($entityCollection, $permissionFilter),
//        ]);

    }


    /**
     * Employee List Only Exclude
     * @return mixed
     */

    public function getActiveEmployeeList()
    {
        $client = Config::get('constants.userRole.Client');
        $permission = [];
        $resultSet = User::where('status', '1')->where('role_id', '<>', $client)->get()->toArray();
        return $this->respond([
            'data' => $this->dataTransformer->transformCollection($resultSet, $permission),
        ]);

    }

    /***
     * Get the list of  Client only for  creation of project
     */

    public function getSelectedUser(Request $request)
    {
        $clientUser = $request->input('client');
        $client = Config::get('constants.userRole.MANAGER');
        $resultSet = $this->service->getSelectedUser($request->input('lead'), $client);
        return $this->respond(['data' => $resultSet]);

    }

    /***
     * List all the active client for the Project Creation
     * @param Request $request
     * @return mixed
     */
    public function getClientUser(Request $request)
    {
        $client = Config::get('constants.userRole.Client');
        $permission = [];
        $resultSet = $this->service->select('role_id', $client)->where('status', '1')->toArray();
        return $this->respond([
            'data' => $this->dataTransformer->transformCollection($resultSet, $permission),
        ]);

    }

    /***
     * Attach Employee and Project
     */

    public function assignEmpProject(Request $request)
    {
        $data['user_id'] = $request->input('detail');

        if (!isset($data['user_id']) || !is_numeric($data['user_id'])) {
            return $this->responseNotFound('User is not entered');
        }
        $id = $data['user_id'];
        $UserId = $this->service->getById($id);
        if (!$UserId) {
            return $this->responseNotFound('User is not exists');
        }

//        $deactivate = $this->service->disablePreviousProject($UserId->id_user);

        /***
         * Assign New Project
         */
        $assign = $this->service->assignEmpProject($UserId->id_user, $request->input('project'));
        return $this->respond(['data' => $assign]);
    }


    /***Get all the employee list in the Hierarchy Form
     * @return mixed
     */
    public function organisationHierarchy()
    {
        $resultSet = $this->service->organisationHierarchy();
        $permission = [];
        return $this->respond([
            'data' => $this->dataTransformer->hierarchyTransform($resultSet, $permission),
        ]);
    }


    /***
     * Change Password
     * @param Request $request
     * @return mixed
     */
    public function changePassword(Request $request)
    {
        $data = $request->input();
        $id_user = $data['id_user'];
        $old_password = $data['oPassword'];
        $new_password = $data['nPassword'];
        $confirm_password = $data['cPassword'];
        $modelId = $this->service->getById($id_user)->toArray();
        if (!$modelId) {
            return $this->responseNotFound('Not found Exception.');
        }
        if (!$modelId) {
            return $this->responseValidationError($modelId);
        }

        if ($modelId instanceof MessageBag) {
            return $this->responseValidationError($modelId);
        }
        if ($new_password != $confirm_password) {
            return $this->respondWithError('Password Mismatch');
        }
        if (!Hash::check($old_password, $modelId['password'])) {
            return $this->respondWithError('Password Mismatch');
        }

        $new_encrypt_password = bcrypt($new_password);
        // dd($modelId);
        $users = User::find($id_user);

        $users->password = $new_encrypt_password;

        if ($users->save()) {
            return $this->respond([
                'data' => true,
            ]);
        }
        return $this->respond([
            'data' => false,
        ]);
    }


    public function detachProjectEmployee(Request $request)
    {
        $modelId = $this->service->getById($request->input('user'));
        if (!$modelId) {
            return $this->responseNotFound('User Not found.');
        }

        $projectID = Project::find($request->input('project'));
        if (!$projectID) {
            return $this->responseNotFound('Project Not found.');
        }
        $result = $this->service->detachProjectEmployee($request->input('project'), $request->input('user'));
        return $this->respond(['data' => $result]);

    }

    public function getReviewerList(Request $request)
    {
        $modelId = $this->service->getById($request->input('user'));
        if (!$modelId) {
            return $this->responseNotFound('User Not found.');
        }

        /***
         * Get the list of reviwer List of this appraisal Cycle of this Year
         */



      $list =   $this->evaluationReviewerService->getReviewerListofPresentCycle($modelId->id_user);

        $resultset = $this->service->getReviewerList($modelId->id_user);
        return $this->respond(['data' => $resultset,
                                'reviewList'=> $list]);
    }


}
