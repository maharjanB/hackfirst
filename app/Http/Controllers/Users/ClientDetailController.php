<?php

namespace App\Http\Controllers\Users;

use App\APIHelpers\Transformers\UserClientUpdateTransformer;
use App\Modules\User\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\APIHelpers\Transformers\UserClientTransformer;
use App\Modules\Configurations\Services\AddressService;

use App\Modules\User\Services\ClientDetailService;

use App\Modules\Foundation\Controllers\AbstractController;


class ClientDetailController extends AbstractController
{
    protected $service;
    protected $addressService;
    protected $dataTransformer;
    protected $clientUpdateTransformer;
    protected $userService;
    protected $primary_key = 'id_client_detail';

    public function __construct(
        ClientDetailService $service,
        UserClientTransformer $dataTransformer,
        AddressService $addressService,
        UserService $userService,
        UserClientUpdateTransformer $clientUpdateTransformer


    )
    {
        $this->service = $service;
        $this->userService = $userService;
        $this->addressService = $addressService;
        $this->dataTransformer = $dataTransformer;
        $this->clientUpdateTransformer = $clientUpdateTransformer;

    }

    public function update(Request $request, $id)
    {
        $location = [];
        $restrictions = [];
        $errorMsg = null;
        $input = $request->input();

        $data = isset($input['detail']) ? $input['detail'] : '';
        $user_id = $data['id_user'];
        $register = $this->userService->getById($user_id)->toArray();
        $register['email'] = $data['email'];
        $register['first_name'] = $data['first_name'];
        $register['role_id'] = $data['role_id'];
        $register['post_id'] = $data['post_id'];
        $register['user_group_id'] = $data['role_id'];
        $modelId = $this->userService->update($user_id, $register);
        $modelId = $this->userService->getById($user_id);
        if (!$modelId) {
            return $this->responseNotFound('Not found Exception.');
        }
        if (!$modelId) {
            return $this->responseValidationError($modelId);
        }

        if ($modelId instanceof MessageBag) {
            return $this->responseValidationError($modelId);
        }

        if ($modelId['message']) {
            $errorMsg = explode('(', $modelId['message']);
            return $this->respondWithError(substr($errorMsg[0],53));
        }




        $client_detail_id = $data['id_client_detail'];
        $client_detail = $this->service->getById($client_detail_id)->toArray();
        $client_detail['phone_no'] = $data['phone_no'];
        $client_detail['primary_email'] = $data['primary_email'];
        $client_detail['primary_person'] = $data['primary_person'];
        $client_detail['secondary_email'] = $data['secondary_email'];
        $client_detail['secondary_person'] = $data['secondary_person'];
        $client_detail['status'] = 1;
        $client_detail['longitude'] = $data['longitude'];
        $client_detail['latitude'] = $data['latitude'];
        $client_detail['client_id'] = $user_id;

        $location['country'] = $data['country_id'];
        $location['state'] = $data['state_id'];
        $location['district'] = $data['district_id'];
        $location['city'] = $data['city_id'];
        $location['locality'] = $data['locality_id'];
        $location['address'] = $data['address'];
        if (!empty($location)) {
            $addressInfo = $this->addressService->locationCreate($location);
            $client_detail['country_id'] = isset($addressInfo['country_id']) ? $addressInfo['country_id'] : null;
            $client_detail['state_id'] = isset($addressInfo['state_id']) ? $addressInfo['state_id'] : null;
            $client_detail['district_id'] = isset($addressInfo['district_id']) ? $addressInfo['district_id'] : null;
            $client_detail['locality_id'] = isset($addressInfo['locality_id']) ? $addressInfo['locality_id'] : null;
            $client_detail['city_id'] = isset($addressInfo['city_id']) ? $addressInfo['city_id'] : null;
        }
        $data['status'] = 1;
        $clientId = $this->service->update($client_detail_id,$client_detail);
        $clientId = $this->service->getById($id);
        $clientId['country'] = $data['country_id'];
        $clientId['state'] = $data['state_id'];
        $clientId['district'] = $data['district_id'];
        $clientId['city'] = $data['city_id'];
        $clientId['locality'] = $data['locality_id'];
        $clientId['address'] = $data['address'];


        if (!$clientId) {
            return $this->responseNotFound('Not found Exception.');
        }

        if (!$clientId) {
            return $this->responseValidationError($clientId);
        }

        if ($clientId instanceof MessageBag) {
            return $this->responseValidationError($clientId);
        }

        if ($clientId['message']) {
            $errorMsg = explode('(', $clientId['message']);
            return $this->respondWithError(substr($errorMsg[0],53));
        }
        $data = false;
        if($modelId && $clientId){
            $data = true;
        }
        return $this->respond([
            'data' => true,
        ]);



    }


}
