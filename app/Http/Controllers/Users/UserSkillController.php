<?php

namespace App\Http\Controllers\Users;

use App\APIHelpers\Transformers\userskillTransformer;
use App\Modules\Configurations\Entities\Skill;
use App\Modules\User\Entities\UserSkill;
use App\Modules\User\Services\userskillService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use App\Modules\Foundation\Controllers\AbstractController;


class UserSkillController extends AbstractController
{
    protected $service;
    protected $dataTransformer;
    protected $primary_key = 'id_user_skill';

    public function __construct(
        UserSkillService $service,
        UserSkillTransformer $userskillTransformer
    ) {
        $this->service = $service;
        $this->dataTransformer = $userskillTransformer;
    }

    public function getReservedSkill(Request $request, $userId){
        $skill = DB::select('select * from tbl_skill where id_skill not in (select skill_id from tbl_user_skill where user_id = ?) 
                  and status = 1',[$userId]);
        return $skill;
    }

}