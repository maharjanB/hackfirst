<?php

namespace app\Http\Middleware;

use Closure;
//use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\App;
use Modules\User\Repositories\OAuthRepository;
use Modules\User\Services\OauthService;
use OAuth2\HttpFoundationBridge\Request as OAuthRequest;
use OAuth2\HttpFoundationBridge\Response;

use App\APIHelpers\Transformers\UserSessionTransformer;
use App\Http\Controllers\API\ApiController;


use Modules\User\Services\UserSessionService;


class RequestDataTransformerMiddleware
{

	public function handle($request, Closure $next)
	{
		$data = $request->input();

		$decodedRequest =json_decode(stripslashes($data['data']), 1);
/*
		var_dump($decodedRequest['token']);
		exit;*/

		if(isset($decodedRequest['token']))
		{
			$request->headers->set('AUTHORIZATION','Bearer '.$decodedRequest['token']);

		}

		$request->merge($decodedRequest);

		return $next($request);
	}
}
