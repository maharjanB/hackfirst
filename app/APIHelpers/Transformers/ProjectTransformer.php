<?php

namespace App\APIHelpers\Transformers;

class ProjectTransformer extends Transformer
{
    public function transform($project)
    {
        return [
            'id_project' => $project['id_project'],
            'title' => $project['title'],
            'resource_id' => $project['resource_id'],
            'product_id' => $project['product_id'],
            'description' => $project['description'],
            'created_by' => $project['created_by'],
            'updated_by' => $project['updated_by'],
            'created_at' => $project['created_at'],
            'updated_at' => $project['updated_at'],
        ];
    }
}


