<?php

namespace App\APIHelpers\Transformers;


class UserTransformer extends Transformer
{

    public function transform($user)
    {
        return [
            'id_user' => $user['id_user'],
            'user_group_id' => $user['user_group_id'],
            'email' => $user['email'],
            'password' => $user['password'],
            'remember_token' => $user['remember_token'],
            'address' => isset($user['address'])?$user['address']:null,
            'occupation' => isset($user['occupation'])?$user['occupation']:null,
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'id_resource' => $user['id_resource'],
            'mobile_number' => $user['mobile_number'],
            'auth_type' => $user['auth_type'],
            'status' => (boolean)$user['status'],
            'updated_by' => $user['updated_by'],
            'updated_at' => $user['updated_at'],
            'created_by' => $user['created_by'],
            'created_at' => $user['created_at']
        ];
    }
}
