<?php

namespace App\APIHelpers\Transformers;

class ResourceTransformer extends Transformer
{
    public function transform($principal)
    {
        return [
            'id_resource' => $principal['id_resource'],
            'title' => $principal['title'],
            'status' => (boolean)$principal['status'],
            'created_by' => $principal['created_by'],
            'updated_by' => $principal['updated_by'],
            'created_at' => $principal['created_at'],
            'updated_at' => $principal['updated_at'],
        ];
    }
}
