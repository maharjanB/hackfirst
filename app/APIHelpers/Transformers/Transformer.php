<?php

namespace App\APIHelpers\Transformers;

abstract class Transformer
{
    /**
     * @param $items ( Collection  ORM Object.)
     *
     * @return array
     *               *
     */
    public function transformCollection($items)
    {
        //return array_map([$this, 'transform'], $items);


        if (!$items) {
            return $items;

        }
        $collectionArray = [];
        foreach ($items as $item) {
            $collectionArray[] = $this->transform($item);
        }

        return $collectionArray;
    }

    /**
     * @param $item
     *
     * @return mixed
     */
    abstract public function transform($item);
}
