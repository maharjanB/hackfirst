<?php
Route::post('inventory/list', function () {

return json_encode([
'status' => 'success',
'data'   => [
'inventory_list' => [
0   => [
'id'    => 3649,
'title' => 'Ambip Airfr Spray B&B Reg 275G',
],
1   => [
'id'    => 3650,
'title' => 'Ambip Airfr Spray Lv&C Reg 275G',
],
2   => [
'id'    => 3651,
'title' => 'Ambip Airfr Spray S&R 275G',
],
3   => [
'id'    => 3652,
'title' => 'Ambip Airfr Spray Sc&Z Reg 275G',
],
4   => [
'id'    => 3653,
'title' => 'Ambip Airfr Spray Sc&Z Reg 275G (6Pcs)',
],
5   => [
'id'    => 3654,
'title' => 'Ambip AER B.O 300Ml - 6 Pcs',
],
6   => [
'id'    => 3655,
'title' => 'Ambip AER F&F 300Ml',
],
7   => [
'id'    => 3656,
'title' => 'Ambip AER F&F 300Mlx12',
],
8   => [
'id'    => 3657,
'title' => 'Ambip AER F&L 300Ml',
],
9   => [
'id'    => 3658,
'title' => 'Ambip AER F&L 300Mlx12',
],
10  => [
'id'    => 3659,
'title' => 'Ambip AER L.B. 300Ml -6Pcs)',
],
11  => [
'id'    => 3660,
'title' => 'Pampers CMFT. Xl - 10S',
],
12  => [
'id'    => 3661,
'title' => 'Pampers CMFT. Xlg 18S',
],
13  => [
'id'    => 3662,
'title' => 'Pampers DPR Lg - 5S',
],
14  => [
'id'    => 3663,
'title' => 'Pampers DPR Md - 5S',
],
15  => [
'id'    => 3664,
'title' => 'Pampers DPR Sm - 5 S',
],
16  => [
'id'    => 3665,
'title' => 'Pampers DPR Xlg - 54s',
],
17  => [
'id'    => 3666,
'title' => 'Pampers DPR Lg - 60s',
],
18  => [
'id'    => 4684,
'title' => 'Pampers Pants - 60s (M) ',
],
19  => [
'id'    => 3667,
'title' => 'Pampers DPR Xlg - 8s',
],
20  => [
'id'    => 4683,
'title' => 'Pampers Pants - 8s (L) ',
],
21  => [
'id'    => 3668,
'title' => 'Pampers DPR Lg - 9S',
],
22  => [
'id'    => 4674,
'title' => 'Pampers Pants - 9s (M) ',
],
23  => [
'id'    => 4681,
'title' => 'Pampers Pants - 9s (S) ',
],
24  => [
'id'    => 3669,
'title' => 'Pampers DPR Md - 10S',
],
25  => [
'id'    => 3670,
'title' => 'Pampers DPR Md - 11s',
],
26  => [
'id'    => 3671,
'title' => 'Pampers DPR Sm - 11s',
],
27  => [
'id'    => 3672,
'title' => 'Pampers DPR Lg - 2s',
],
28  => [
'id'    => 3673,
'title' => 'Pampers DPR Lg 2s (96)',
],
29  => [
'id'    => 3674,
'title' => 'Pampers DPR Md - 2s',
],
30  => [
'id'    => 3675,
'title' => 'Pampers DPR Md 2s (96)',
],
31  => [
'id'    => 3676,
'title' => 'Pampers DPR Sm - 2 S',
],
32  => [
'id'    => 3677,
'title' => 'Pampers DPR Sm 2s (96)',
],
33  => [
'id'    => 4672,
'title' => 'Pampers Pants - 2s (XL) ',
],
34  => [
'id'    => 4673,
'title' => 'Pampers Pants - 2s (S) (96) ',
],
35  => [
'id'    => 4679,
'title' => 'Pampers Pants - 2s (M) ',
],
36  => [
'id'    => 4685,
'title' => 'Pampers Pants - 2s (L) ',
],
37  => [
'id'    => 3678,
'title' => 'Pampers DPR Xlg - 28s',
],
38  => [
'id'    => 3679,
'title' => 'Pampers DPR Lg - 32s',
],
39  => [
'id'    => 4689,
'title' => 'Pampers Pants - 32s(XL) ',
],
40  => [
'id'    => 3680,
'title' => 'Pampers DPR Md - 34s',
],
41  => [
'id'    => 3681,
'title' => 'Pampers DPR Sm - 38s',
],
42  => [
'id'    => 3682,
'title' => 'Pampers DPR Md - 66s',
],
43  => [
'id'    => 4675,
'title' => 'Pampers Pants - 20s(M) 8 ',
],
44  => [
'id'    => 4676,
'title' => 'Pampers Pants - 18s(L) 8 ',
],
45  => [
'id'    => 4677,
'title' => 'Pampers Pants - 7s(XL) ',
],
46  => [
'id'    => 4678,
'title' => 'Pampers Pants - 36s(L) ',
],
47  => [
'id'    => 4680,
'title' => 'Pampers Pants - 46s(S) ',
],
48  => [
'id'    => 4682,
'title' => 'Pampers Pants - 42s(M) ',
],
49  => [
'id'    => 4686,
'title' => 'Pampers Pants - 22s(S) ',
],
50  => [
'id'    => 4687,
'title' => 'Pampers Pants - 52s(L) ',
],
51  => [
'id'    => 4688,
'title' => 'Pampers Pants - 16s(XL) 8 ',
],
52  => [
'id'    => 4691,
'title' => 'Pampers Pants - 48s(XL) ',
],
53  => [
'id'    => 3683,
'title' => '"Duracell ""9V"" Battery"',
],
54  => [
'id'    => 3684,
'title' => '"Duracell ""C"" Size Battery"',
],
55  => [
'id'    => 3685,
'title' => '"Duracell ""D"" Size Battery"',
],
56  => [
'id'    => 3686,
'title' => '223 Battery Lithium',
],
57  => [
'id'    => 3687,
'title' => 'Duracell AA1',
],
58  => [
'id'    => 3688,
'title' => 'Duracell AA2',
],
59  => [
'id'    => 3689,
'title' => 'Duracell AA4',
],
60  => [
'id'    => 3690,
'title' => 'Duracell AAA1',
],
61  => [
'id'    => 3691,
'title' => 'Duracell AAA2',
],
62  => [
'id'    => 3692,
'title' => 'Duracell AAA4',
],
63  => [
'id'    => 3693,
'title' => '7`O Clock PII - 5s',
],
64  => [
'id'    => 3694,
'title' => '7`O Clock PII Ms Razor (R)',
],
65  => [
'id'    => 3695,
'title' => '7`O Clock PII Razor',
],
66  => [
'id'    => 3696,
'title' => 'Fusion Cart - 2s',
],
67  => [
'id'    => 3697,
'title' => 'Fusion Cart - 4s',
],
68  => [
'id'    => 3698,
'title' => 'Fusion Cart - 6s',
],
69  => [
'id'    => 3699,
'title' => 'Fusion Cart - 8s',
],
70  => [
'id'    => 3700,
'title' => 'Fusion Power Cart - 2s',
],
71  => [
'id'    => 3701,
'title' => 'Fusion Power Cart - 4s',
],
72  => [
'id'    => 3702,
'title' => 'Fusion Power Cart - 6s',
],
73  => [
'id'    => 3703,
'title' => 'Fusion Power Razor',
],
74  => [
'id'    => 3704,
'title' => 'Fusion Razor ( R )',
],
75  => [
'id'    => 3705,
'title' => 'G7. DE Blade - 3S',
],
76  => [
'id'    => 3706,
'title' => 'G7. Razor (R)',
],
77  => [
'id'    => 3707,
'title' => 'G7. Sterling Razor (R)',
],
78  => [
'id'    => 3708,
'title' => 'G7. Super Platinum - 5',
],
79  => [
'id'    => 3709,
'title' => 'Gillette 2 Disposable',
],
80  => [
'id'    => 3710,
'title' => 'Gillette Guard 3Ct',
],
81  => [
'id'    => 3711,
'title' => 'Gillette Guard Crt 1',
],
82  => [
'id'    => 3712,
'title' => 'Gillette Guard Razor',
],
83  => [
'id'    => 3713,
'title' => 'Gillette Mach III Cart - 2s',
],
84  => [
'id'    => 3714,
'title' => 'Gillette Mach III Cart - 4s',
],
85  => [
'id'    => 3715,
'title' => 'Gillette Mach III Cart - 8s',
],
86  => [
'id'    => 3716,
'title' => 'Gillette Mach III Power Cart - 4s',
],
87  => [
'id'    => 3717,
'title' => 'Gillette Mach III Turbo Cart - 2s',
],
88  => [
'id'    => 3718,
'title' => 'Gillette Mach III Turbo Cart - 4s',
],
89  => [
'id'    => 3719,
'title' => 'Gillette Mach III Turbo Cart - 8s',
],
90  => [
'id'    => 4248,
'title' => 'Gillette Mach III Turbo sensitive 2CT',
],
91  => [
'id'    => 4249,
'title' => 'Gillette Mach III Turbo sensitive 4CT',
],
92  => [
'id'    => 3720,
'title' => 'Gillette Mach III Hercules (R)',
],
93  => [
'id'    => 3721,
'title' => 'Gillette Mach III Power Razor',
],
94  => [
'id'    => 3722,
'title' => 'Gillette Mach III Turbo Razor (R)',
],
95  => [
'id'    => 3723,
'title' => 'Gillette Mach III T. Razor (R) Trmph',
],
96  => [
'id'    => 4250,
'title' => 'Gillette Mach III Turbo Sensitive Razor 1 Up',
],
97  => [
'id'    => 3724,
'title' => 'Gillette Presto - 5s',
],
98  => [
'id'    => 3725,
'title' => 'Gillette Presto Int`L',
],
99  => [
'id'    => 3726,
'title' => 'Sensor Excel - 10s',
],
100 => [
'id'    => 3727,
'title' => 'Sensor Excel - 3s',
],
101 => [
'id'    => 3728,
'title' => 'Sensor Excel - 5s',
],
102 => [
'id'    => 3729,
'title' => 'Sensor Excel Fw - 3s',
],
103 => [
'id'    => 3730,
'title' => 'Gillette Super Thin Blade (5)',
],
104 => [
'id'    => 3731,
'title' => 'Vector Plus - 1s',
],
105 => [
'id'    => 3732,
'title' => 'Vector Plus - 2s',
],
106 => [
'id'    => 3733,
'title' => 'Vector Plus - 2s (600)',
],
107 => [
'id'    => 3734,
'title' => 'Vector Plus 2s (India)',
],
108 => [
'id'    => 3735,
'title' => 'Vector Plus - 4s',
],
109 => [
'id'    => 3736,
'title' => 'Vector Plus - 4s (600)',
],
110 => [
'id'    => 3737,
'title' => 'Vector Plus 4s (India)',
],
111 => [
'id'    => 3738,
'title' => 'Vector Plus Razor (R)',
],
112 => [
'id'    => 3739,
'title' => 'Vector Razor 139 INR',
],
113 => [
'id'    => 3740,
'title' => 'Wilkinson D/E',
],
114 => [
'id'    => 3741,
'title' => 'Wilkinson Sword - 5S',
],
115 => [
'id'    => 3742,
'title' => 'Ambip Car Refill AQ 7Ml',
],
116 => [
'id'    => 3743,
'title' => 'Ambip Car Refill F&L 7Ml',
],
117 => [
'id'    => 3744,
'title' => 'Ambip Car Refill L.S 7Ml',
],
118 => [
'id'    => 3745,
'title' => 'Ambip Car Refill P.A 7Ml',
],
119 => [
'id'    => 3746,
'title' => 'Ambip Car Refill V.B 7Ml',
],
120 => [
'id'    => 3747,
'title' => 'Ambip Car Ambip Car Starter & Refill Aqua St 7Ml',
],
121 => [
'id'    => 3748,
'title' => 'Ambip Car Starter & Refill Aq 7Ml',
],
122 => [
'id'    => 3749,
'title' => 'Ambip Car Starter & Refill Aq Rf 7Ml',
],
123 => [
'id'    => 3750,
'title' => 'Ambip Car Starter & Refill F&L 7Ml',
],
124 => [
'id'    => 3751,
'title' => 'Ambip Car Starter & Refill F&L 7Ml Frs&Lght St 7Ml',
],
125 => [
'id'    => 3752,
'title' => 'Ambip Car Starter & Refill F&L Rf 7Ml',
],
126 => [
'id'    => 3753,
'title' => 'Ambip Car Starter & Refill L.S 7Ml',
],
127 => [
'id'    => 3754,
'title' => 'Ambip Car Starter & Refill L.S Rf 7Ml',
],
128 => [
'id'    => 3755,
'title' => 'Ambip Carambip Car Starter & Refill L.S 7Ml Spa St 7Ml',
],
129 => [
'id'    => 3756,
'title' => 'Ambip Car Starter & Refill Pac Air St 7Mlx4',
],
130 => [
'id'    => 3757,
'title' => 'Ambip Car Starter & Refill P.A 7Ml',
],
131 => [
'id'    => 3758,
'title' => 'Ambip Car Starter & Refill P.A Rf 7Ml',
],
132 => [
'id'    => 3759,
'title' => 'Ambip Car Starter & Refill V.B 7Ml',
],
133 => [
'id'    => 3760,
'title' => 'Ambip Car Starter & Refill V.Bouq Rf 7Mlx6',
],
134 => [
'id'    => 3761,
'title' => 'Ambip Car Starter & Refill V.Bouq St 7Ml',
],
135 => [
'id'    => 3762,
'title' => 'Ariel Detergent - 1 Kg',
],
136 => [
'id'    => 3763,
'title' => 'Ariel Detergent - 2 Kg',
],
137 => [
'id'    => 3764,
'title' => 'Ariel Detergent C&S- 2 Kg',
],
138 => [
'id'    => 3765,
'title' => 'Ariel Detergent - 200 Gm',
],
139 => [
'id'    => 3766,
'title' => 'Ariel Detergent - 500 Gm',
],
140 => [
'id'    => 3767,
'title' => 'Ariel Detergent 24 Hrs Fresh - 1 Kg',
],
141 => [
'id'    => 3768,
'title' => 'Ariel Detergent 24 Hrs Fresh - 2 Kg',
],
142 => [
'id'    => 3769,
'title' => 'Ariel Detergent 24 Hrs Fresh - 500 Gm',
],
143 => [
'id'    => 3770,
'title' => 'Ariel Detergent 3Kg',
],
144 => [
'id'    => 3771,
'title' => 'Ariel Detergent Matic 500G',
],
145 => [
'id'    => 3772,
'title' => 'Ariel Detergent C&S 500Gm',
],
146 => [
'id'    => 3773,
'title' => 'Ariel Detergent C&S - 1 Kg',
],
147 => [
'id'    => 3774,
'title' => 'Ariel Detergent - 12 Gm',
],
148 => [
'id'    => 3775,
'title' => 'Ariel Detergent - 13 Gm',
],
149 => [
'id'    => 3776,
'title' => 'Ariel Detergent - 14 Gm',
],
150 => [
'id'    => 3777,
'title' => 'Ariel Liq Reg 1Kg',
],
151 => [
'id'    => 3778,
'title' => 'Ariel Liq Reg 3Kg',
],
152 => [
'id'    => 3779,
'title' => 'Tide Detergent - 1 Kg (22)',
],
153 => [
'id'    => 3780,
'title' => 'Tide Detergent - 500 Gm',
],
154 => [
'id'    => 3781,
'title' => 'Tide Detergent - 800 Gm',
],
155 => [
'id'    => 3782,
'title' => 'Tide Detergent - 13 Gm',
],
156 => [
'id'    => 3783,
'title' => 'Tide LAU PWD Reg - 1.5 Kg',
],
157 => [
'id'    => 3784,
'title' => 'Tide LAU PWD Reg - 3 Kg',
],
158 => [
'id'    => 3785,
'title' => 'Tide LAU PWD Reg - 4.5 Kg',
],
159 => [
'id'    => 3786,
'title' => 'Tide LAU PWD Reg - 400 Gm',
],
160 => [
'id'    => 3787,
'title' => 'Tide LAU PWD Reg - 6 Kg',
],
161 => [
'id'    => 3788,
'title' => 'Tide LAU PWD Reg - 800 Gm',
],
162 => [
'id'    => 3789,
'title' => 'Tide LAU PWD Reg - 9 Kg',
],
163 => [
'id'    => 3790,
'title' => 'Whisper Choice Non Wing - 8s',
],
164 => [
'id'    => 3791,
'title' => 'Whisper Choice Wing - 8s',
],
165 => [
'id'    => 3792,
'title' => 'Whisper Ln Ultra Choice - 6s',
],
166 => [
'id'    => 3793,
'title' => 'Whisper Ultra Choice - 6s',
],
167 => [
'id'    => 3794,
'title' => 'Whisper Ultra Choice - 8s',
],
168 => [
'id'    => 3795,
'title' => 'Whisper Ultra Wings 8S',
],
169 => [
'id'    => 3796,
'title' => 'Whisper Heavy Flow - 10s',
],
170 => [
'id'    => 3797,
'title' => 'Whisper Heavy Flow - 20s',
],
171 => [
'id'    => 3798,
'title' => 'Whisper Maxi - 15s',
],
172 => [
'id'    => 3799,
'title' => 'Whisper Maxi - 7S Overnight',
],
173 => [
'id'    => 3800,
'title' => 'Whisper Maxi - 8s',
],
174 => [
'id'    => 3801,
'title' => 'Whisper Maxi- 15 S Overnight',
],
175 => [
'id'    => 3802,
'title' => 'Whisper Xln Maxi 7S',
],
176 => [
'id'    => 3803,
'title' => 'Whisper Maxi Ln-Xln 10S',
],
177 => [
'id'    => 3804,
'title' => 'Whisper Maxi Ln-Xln 15S',
],
178 => [
'id'    => 3805,
'title' => 'Whisper Maxi Ln-Xln 8S',
],
179 => [
'id'    => 3806,
'title' => 'Whisper Maxi Reg 15S',
],
180 => [
'id'    => 3807,
'title' => 'Whisper Maxi Reg 8S',
],
181 => [
'id'    => 3808,
'title' => 'Whisper Pad Reg 8Sx60 Wg Choice Mddp',
],
182 => [
'id'    => 3809,
'title' => 'Whisper Reg Non Wing 8S',
],
183 => [
'id'    => 3810,
'title' => 'Whisper Ultra - 7s',
],
184 => [
'id'    => 3811,
'title' => 'Whisper Ultra - 8s',
],
185 => [
'id'    => 3812,
'title' => 'Whisper Ultra Wing - 15s',
],
186 => [
'id'    => 3813,
'title' => 'Whisper Ultra Wing - 15s (24)',
],
187 => [
'id'    => 3814,
'title' => 'Whisper Ultra Wing 7S',
],
188 => [
'id'    => 3815,
'title' => 'Herbal ESS. Conditioner B.O - 160 Ml',
],
189 => [
'id'    => 3816,
'title' => 'Herbal ESS. Conditioner C.S - 160 Ml',
],
190 => [
'id'    => 3817,
'title' => 'Herbal ESS. Conditioner D.S - 160 Ml',
],
191 => [
'id'    => 3818,
'title' => 'Herbal ESS. Conditioner B.O - 300 Ml',
],
192 => [
'id'    => 3819,
'title' => 'Herbal ESS. Conditioner C.S. - 300 Ml',
],
193 => [
'id'    => 3820,
'title' => 'Herbal ESS. Conditioner D.C - 300 Ml',
],
194 => [
'id'    => 3821,
'title' => 'Herbal ESS. Conditioner D.S - 300Ml',
],
195 => [
'id'    => 3822,
'title' => 'Herbal ESS. Conditioner H.H - 300 Ml',
],
196 => [
'id'    => 3823,
'title' => 'Herbal ESS. Conditioner B.E - 355 Ml',
],
197 => [
'id'    => 3824,
'title' => 'Herbal ESS. Conditioner D.C - 355 Ml',
],
198 => [
'id'    => 3825,
'title' => 'Herbal ESS. Conditioner D.S - 355 Ml',
],
199 => [
'id'    => 3826,
'title' => 'Herbal ESS. Conditioner H.H - 355 Ml',
],
200 => [
'id'    => 3827,
'title' => 'Herbal ESS. Shampoo B.O. - 160 Ml',
],
201 => [
'id'    => 3828,
'title' => 'Herbal ESS. Shampoo B.O 300Ml',
],
202 => [
'id'    => 3829,
'title' => 'Herbal ESS. Shampoo C.S - 300 Ml',
],
203 => [
'id'    => 3830,
'title' => 'Herbal ESS. Shampoo D.S - 300 Ml',
],
204 => [
'id'    => 3831,
'title' => 'Herbal ESS. Shampoo H. H. - 300 Ml',
],
205 => [
'id'    => 3832,
'title' => 'Gillette Shampoo 2 In 1 12.2 Oz',
],
206 => [
'id'    => 3833,
'title' => 'Gillette Shampoo C .R. 12.2 Oz',
],
207 => [
'id'    => 3834,
'title' => 'Gillette Shampoo C .Thick 12.2 Oz',
],
208 => [
'id'    => 3835,
'title' => 'Gillette Shampoo Cn Hydrating 11.5 Oz',
],
209 => [
'id'    => 3836,
'title' => 'H&S Purely Gentle 23.7Oz',
],
210 => [
'id'    => 3837,
'title' => 'H&S Shampoo Purely Gentle 6 14.2Oz',
],
211 => [
'id'    => 3838,
'title' => 'H&S Shampoo - 180 Ml A.H.F',
],
212 => [
'id'    => 3839,
'title' => 'H&S Shampoo - 180 Ml C.B',
],
213 => [
'id'    => 3840,
'title' => 'H&S Shampoo - 180 Ml D.S.C.A.',
],
214 => [
'id'    => 3841,
'title' => 'H&S Shampoo - 180 Ml F.M.H.R',
],
215 => [
'id'    => 3842,
'title' => 'H&S Shampoo - 180 Ml I.S.C.',
],
216 => [
'id'    => 3843,
'title' => 'H&S Shampoo - 180 Ml R.M.',
],
217 => [
'id'    => 3844,
'title' => 'H&S Shampoo - 180 Ml S.S',
],
218 => [
'id'    => 3845,
'title' => 'H&S Shampoo - 315Ml Hair Retain',
],
219 => [
'id'    => 3846,
'title' => 'H&S Shampoo - 350 Ml A.H.F',
],
220 => [
'id'    => 3847,
'title' => 'H&S Shampoo - 350 Ml A.V',
],
221 => [
'id'    => 3848,
'title' => 'H&S Shampoo - 350 Ml C.B',
],
222 => [
'id'    => 3849,
'title' => 'H&S Shampoo - 350 Ml D.S.C.A.',
],
223 => [
'id'    => 3850,
'title' => 'H&S Shampoo - 350 Ml F.M.H.R.',
],
224 => [
'id'    => 3851,
'title' => 'H&S Shampoo - 350 Ml I.S.C.',
],
225 => [
'id'    => 3852,
'title' => 'H&S Shampoo - 350 Ml R.M.',
],
226 => [
'id'    => 3853,
'title' => 'H&S Shampoo - 350 Ml S.S',
],
227 => [
'id'    => 3854,
'title' => 'H&S Shampoo - 350 Ml T.L.',
],
228 => [
'id'    => 3855,
'title' => 'H&S Shampoo - 610 Ml F.M.H.R',
],
229 => [
'id'    => 3856,
'title' => 'H&S Shampoo - 675 Ml A.H.F',
],
230 => [
'id'    => 3857,
'title' => 'H&S Shampoo - 675 Ml C.B',
],
231 => [
'id'    => 3858,
'title' => 'H&S Shampoo - 675 Ml F.M.H.R.',
],
232 => [
'id'    => 3859,
'title' => 'H&S Shampoo - 675 Ml R.M.',
],
233 => [
'id'    => 3860,
'title' => 'H&S Shampoo - 675 Ml S.S.',
],
234 => [
'id'    => 3861,
'title' => 'H&S Shampoo - 70 Ml AHF.',
],
235 => [
'id'    => 3862,
'title' => 'H&S Shampoo - 70 Ml C.B.',
],
236 => [
'id'    => 3863,
'title' => 'H&S Shampoo - 70 Ml C.M.',
],
237 => [
'id'    => 3864,
'title' => 'H&S Shampoo - 70 Ml D.S.C.A.',
],
238 => [
'id'    => 3865,
'title' => 'H&S Shampoo - 70 Ml I.S.C.',
],
239 => [
'id'    => 3866,
'title' => 'H&S Shampoo - 70Ml A.H.F.',
],
240 => [
'id'    => 3867,
'title' => 'H&S Shampoo - 70Ml C&B',
],
241 => [
'id'    => 3868,
'title' => 'H&S Shampoo - 70Ml D.S.C.',
],
242 => [
'id'    => 3869,
'title' => 'H&S Shampoo - 70Ml F.M.H.R',
],
243 => [
'id'    => 3870,
'title' => 'H&S Shampoo - 70Ml I.S.C.',
],
244 => [
'id'    => 3871,
'title' => 'H&S Shampoo - 70Ml R.M.',
],
245 => [
'id'    => 3872,
'title' => 'H&S Shampoo - 70Ml S&S',
],
246 => [
'id'    => 3873,
'title' => 'H&S Shampoo - 75 Ml A.H.F',
],
247 => [
'id'    => 3874,
'title' => 'H&S Shampoo - 75 Ml C.B',
],
248 => [
'id'    => 3875,
'title' => 'H&S Shampoo - 75 Ml D.S.C.A.',
],
249 => [
'id'    => 3876,
'title' => 'H&S Shampoo - 75 Ml F.M.H.R',
],
250 => [
'id'    => 3877,
'title' => 'H&S Shampoo - 75 Ml I.S.C.',
],
251 => [
'id'    => 3878,
'title' => 'H&S Shampoo - 75 Ml R.M.',
],
252 => [
'id'    => 3879,
'title' => 'H&S Shampoo - 75 Ml S.S',
],
253 => [
'id'    => 3880,
'title' => 'H&S Shampoo - 80 Ml A.H.F',
],
254 => [
'id'    => 3881,
'title' => 'H&S Shampoo - 80 Ml C.B',
],
255 => [
'id'    => 3882,
'title' => 'H&S Shampoo - 80 Ml F.M.H.R',
],
256 => [
'id'    => 3883,
'title' => 'H&S Shampoo - 80 Ml R. M.',
],
257 => [
'id'    => 3884,
'title' => 'H&S Shampoo - 80 Ml S.S',
],
258 => [
'id'    => 3885,
'title' => 'H&S Shampoo Extra Strength Men 6 14.2Oz',
],
259 => [
'id'    => 3886,
'title' => 'H&S Shampoo 165 Ml F.M.H.R',
],
260 => [
'id'    => 3887,
'title' => 'H&S Shampoo 180Ml C.M',
],
261 => [
'id'    => 3888,
'title' => 'H&S Shampoo 2 N 1 - 14.2 Oz Clcln',
],
262 => [
'id'    => 3889,
'title' => 'H&S Shampoo 2 N 1 - 14.2 Oz Refsh',
],
263 => [
'id'    => 3890,
'title' => 'H&S Shampoo 2 N 1 - 14.2 Oz Smslky',
],
264 => [
'id'    => 3891,
'title' => 'H&S Shampoo 2 In 1 - 200 Ml S.S',
],
265 => [
'id'    => 3892,
'title' => 'H&S Shampoo 5Ml C.M',
],
266 => [
'id'    => 3893,
'title' => 'H&S Shampoo Clinical Strength 14.2Oz',
],
267 => [
'id'    => 3894,
'title' => 'H&S Shampoo - 5 Ml A.H.F',
],
268 => [
'id'    => 3895,
'title' => 'H&S Shampoo - 5 Ml C.B',
],
269 => [
'id'    => 3896,
'title' => 'H&S Shampoo - 5 Ml F.M.H.R',
],
270 => [
'id'    => 3897,
'title' => 'H&S Shampoo - 5 Ml R.M.',
],
271 => [
'id'    => 3898,
'title' => 'H&S Shampoo - 5 Ml S.S',
],
272 => [
'id'    => 3899,
'title' => 'H&S Shampoo - 165 Ml F.M.H.R',
],
273 => [
'id'    => 3900,
'title' => 'Pantene Hair Treatment-300Ml-E.M.',
],
274 => [
'id'    => 3901,
'title' => 'Pantene Conditioner- 165Ml E.M.',
],
275 => [
'id'    => 3902,
'title' => 'Pantene Conditioner- 165Ml H.F.C.',
],
276 => [
'id'    => 3903,
'title' => 'Pantene Conditioner 165Ml N. C.',
],
277 => [
'id'    => 3904,
'title' => 'Pantene Conditioner- 165Ml N. C.',
],
278 => [
'id'    => 3905,
'title' => 'Pantene Conditioner- 165Ml N. S.',
],
279 => [
'id'    => 3906,
'title' => 'Pantene Conditioner- 165Ml S. S.',
],
280 => [
'id'    => 3907,
'title' => 'Pantene Conditioner- 165Ml T. C.',
],
281 => [
'id'    => 3908,
'title' => 'Pantene Conditioner- 170Ml H.F.C.',
],
282 => [
'id'    => 3909,
'title' => 'Pantene Conditioner- 170Ml N. S.',
],
283 => [
'id'    => 3910,
'title' => 'Pantene Conditioner -180 Ml E.M.',
],
284 => [
'id'    => 3911,
'title' => 'Pantene Conditioner -180 Ml N.S.',
],
285 => [
'id'    => 3912,
'title' => 'Pantene Conditioner -180 Ml S.S.',
],
286 => [
'id'    => 3913,
'title' => 'Pantene Conditioner -180 Ml T.C.',
],
287 => [
'id'    => 3914,
'title' => 'Pantene Conditioner- 335Ml T.C.',
],
288 => [
'id'    => 3915,
'title' => 'Pantene Conditioner - 335Ml N.C',
],
289 => [
'id'    => 3916,
'title' => 'Pantene Conditioner Nc F&L 335Ml',
],
290 => [
'id'    => 3917,
'title' => 'Pantene Conditioner- 335Ml E.M.',
],
291 => [
'id'    => 3918,
'title' => 'Pantene Conditioner- 335Ml H.F.C.',
],
292 => [
'id'    => 3919,
'title' => 'Pantene Conditioner- 335Ml N. S.',
],
293 => [
'id'    => 3920,
'title' => 'Pantene Conditioner- 335Ml S. S.',
],
294 => [
'id'    => 3921,
'title' => 'Pantene Conditioner 2 In 1 A.D - 400 Ml',
],
295 => [
'id'    => 3922,
'title' => 'Pantene 2N1 Aqua Light 12.6Oz',
],
296 => [
'id'    => 3923,
'title' => 'Pantene Shampoo 2N1 Curl Perfection 12.6Oz',
],
297 => [
'id'    => 3924,
'title' => 'Pantene 2N1 Ice Shine 12.6Oz',
],
298 => [
'id'    => 3925,
'title' => 'Pantene Shampoo -170Ml A.D.',
],
299 => [
'id'    => 3926,
'title' => 'Pantene Shampoo -170Ml H.F.C.',
],
300 => [
'id'    => 3927,
'title' => 'Pantene Shampoo -170Ml L.B.',
],
301 => [
'id'    => 3928,
'title' => 'Pantene Shampoo -170Ml L.C.',
],
302 => [
'id'    => 3929,
'title' => 'Pantene Shampoo 170Ml N.C.',
],
303 => [
'id'    => 3930,
'title' => 'Pantene Shampoo -170Ml N.C.',
],
304 => [
'id'    => 3931,
'title' => 'Pantene Shampoo -170Ml N.S.',
],
305 => [
'id'    => 3932,
'title' => 'Pantene Shampoo -170Ml S.S.',
],
306 => [
'id'    => 3933,
'title' => 'Pantene Shampoo -170Ml T.C.',
],
307 => [
'id'    => 3934,
'title' => 'Pantene Shampoo -180Ml A.D.',
],
308 => [
'id'    => 3935,
'title' => 'Pantene Shampoo -180Ml L.C.',
],
309 => [
'id'    => 3936,
'title' => 'Pantene Shampoo -180Ml N.S.',
],
310 => [
'id'    => 3937,
'title' => 'Pantene Shampoo -180Ml S.S.',
],
311 => [
'id'    => 3938,
'title' => 'Pantene Shampoo 200 Ml A.D',
],
312 => [
'id'    => 3939,
'title' => 'Pantene Shampoo -340Ml - A.D.',
],
313 => [
'id'    => 3940,
'title' => 'Pantene Shampoo -340Ml - E.M.',
],
314 => [
'id'    => 3941,
'title' => 'Pantene Shampoo -340Ml - H.F.C.',
],
315 => [
'id'    => 3942,
'title' => 'Pantene Shampoo -340Ml - L.B.',
],
316 => [
'id'    => 3943,
'title' => 'Pantene Shampoo -340Ml - N.C.',
],
317 => [
'id'    => 3944,
'title' => 'Pantene Shampoo 340Ml N.C.',
],
318 => [
'id'    => 3945,
'title' => 'Pantene Shampoo -340Ml - N.S.',
],
319 => [
'id'    => 3946,
'title' => 'Pantene Shampoo -340Ml - S.S.',
],
320 => [
'id'    => 3947,
'title' => 'Pantene Shampoo -340Ml - T.C.',
],
321 => [
'id'    => 3948,
'title' => 'Pantene Shampoo -670Ml A.D.',
],
322 => [
'id'    => 3949,
'title' => 'Pantene Shampoo -670Ml H.F.C.',
],
323 => [
'id'    => 3950,
'title' => 'Pantene Shampoo -670Ml N.S.',
],
324 => [
'id'    => 3951,
'title' => 'Pantene Shampoo -670Ml S.S.',
],
325 => [
'id'    => 3952,
'title' => 'Pantene Shampoo -70Ml A.D.',
],
326 => [
'id'    => 3953,
'title' => 'Pantene Shampoo -70Ml H.F.C.',
],
327 => [
'id'    => 3954,
'title' => 'Pantene Shampoo -70Ml L.B',
],
328 => [
'id'    => 3955,
'title' => 'Pantene Shampoo 70Ml N.S.',
],
329 => [
'id'    => 3956,
'title' => 'Pantene Shampoo N.S. 70Ml',
],
330 => [
'id'    => 3957,
'title' => 'Pantene Shampoo 70Ml S.S.C',
],
331 => [
'id'    => 3958,
'title' => 'Pantene Shampoo 70Ml TDC',
],
332 => [
'id'    => 3959,
'title' => 'Pantene Shampoo -700Ml T.C.',
],
333 => [
'id'    => 3960,
'title' => 'Pantene Shampoo -750Ml T.C',
],
334 => [
'id'    => 4693,
'title' => 'Pantene Shampoo - 750Ml (A.D.)',
],
335 => [
'id'    => 3961,
'title' => 'Pantene Shampoo -80Ml A.D',
],
336 => [
'id'    => 3962,
'title' => 'Pantene Shampoo -80Ml H.F.C',
],
337 => [
'id'    => 3963,
'title' => 'Pantene Shampoo 80Ml L.B',
],
338 => [
'id'    => 3964,
'title' => 'Pantene Shampoo -90Ml H.F.C',
],
339 => [
'id'    => 3965,
'title' => 'Pantene Shampoo 90Ml S.S',
],
340 => [
'id'    => 3966,
'title' => 'Pantene Shampoo -90Ml T.C.',
],
341 => [
'id'    => 3967,
'title' => 'Pantene Shampoo 2N1 Smooth & Sleek 12.6Z',
],
342 => [
'id'    => 3968,
'title' => 'Pantene Shampoo 2N1 Clncare 12.6Oz',
],
343 => [
'id'    => 3969,
'title' => 'Pantene Shampoo 2 N 1 - 12.6 Oz Curly Hair',
],
344 => [
'id'    => 3970,
'title' => 'Pantene Shampoo 2 N 1 - 12.6 Oz Fine Hair',
],
345 => [
'id'    => 3971,
'title' => 'Pantene Shampoo 2 N 1 - 12.6 Oz Md Th. Hair',
],
346 => [
'id'    => 3972,
'title' => 'Pantene Shampoo Prov Repair & Protect 12.6Oz',
],
347 => [
'id'    => 3973,
'title' => 'Pantene Shampoo Volume 2N1 12.6 Oz',
],
348 => [
'id'    => 3974,
'title' => 'Pantene Shampoo - 5 Ml A.D (960)',
],
349 => [
'id'    => 3975,
'title' => 'Pantene Shampoo - 5 Ml T.C',
],
350 => [
'id'    => 3976,
'title' => 'Pantene Shampoo 5Ml A.D',
],
351 => [
'id'    => 3977,
'title' => 'Pantene Shampoo 5Ml H.F.C',
],
352 => [
'id'    => 3978,
'title' => 'Pantene Shampoo -5Ml H.F.C',
],
353 => [
'id'    => 3979,
'title' => 'Pantene Shampoo -5Ml L.B',
],
354 => [
'id'    => 3980,
'title' => 'Pantene Shampoo -5Ml N.S',
],
355 => [
'id'    => 3981,
'title' => 'Pantene Shampoo -5Ml S.S',
],
356 => [
'id'    => 3982,
'title' => 'Pantene Shampoo 5Ml T.C',
],
357 => [
'id'    => 3983,
'title' => 'Rejoice Conditioner - (Rem) - 100 Ml',
],
358 => [
'id'    => 3984,
'title' => 'Rejoice Conditioner - (Rem) - 200 Ml',
],
359 => [
'id'    => 3985,
'title' => 'Rejoice Shampoo - 170 Ml A.D',
],
360 => [
'id'    => 3986,
'title' => 'Rejoice Shampoo - 170 Ml A.H.F.',
],
361 => [
'id'    => 3987,
'title' => 'Rejoice Shampoo - 170 Ml Anti-Frizz',
],
362 => [
'id'    => 3988,
'title' => 'Rejoice Shampoo - 170 Ml Fruity',
],
363 => [
'id'    => 3989,
'title' => 'Rejoice Shampoo - 170 Ml Hair Retain',
],
364 => [
'id'    => 3990,
'title' => 'Rejoice Shampoo - 170 Ml Rich',
],
365 => [
'id'    => 3991,
'title' => 'Rejoice Shampoo 170Ml S.S',
],
366 => [
'id'    => 3992,
'title' => 'Rejoice Shampoo - 180 Ml Fruity',
],
367 => [
'id'    => 3993,
'title' => 'Rejoice Shampoo - 320 Ml A.H.F.',
],
368 => [
'id'    => 3994,
'title' => 'Rejoice Shampoo - 320 Ml Anti-Frizz',
],
369 => [
'id'    => 3995,
'title' => 'Rejoice Shampoo - 320 Ml Rich',
],
370 => [
'id'    => 3996,
'title' => 'Rejoice Shampoo - 320 Ml A.D.',
],
371 => [
'id'    => 3997,
'title' => 'Rejoice Shampoo - 340 Ml A.H.F.',
],
372 => [
'id'    => 3998,
'title' => 'Rejoice Shampoo - 340 Ml Fruity',
],
373 => [
'id'    => 3999,
'title' => 'Rejoice Shampoo - 340 Ml Rich',
],
374 => [
'id'    => 4000,
'title' => 'Rejoice Shampoo - 360 Ml Rich',
],
375 => [
'id'    => 4001,
'title' => 'Rejoice Shampoo - 600 Ml A.D.',
],
376 => [
'id'    => 4002,
'title' => 'Rejoice Shampoo - 600 Ml A.H.F.',
],
377 => [
'id'    => 4003,
'title' => 'Rejoice Shampoo - 600 Ml Anti-Frizz',
],
378 => [
'id'    => 4004,
'title' => 'Rejoice Shampoo - 600 Ml Fruity',
],
379 => [
'id'    => 4005,
'title' => 'Rejoice Shampoo - 600 Ml Rich',
],
380 => [
'id'    => 4006,
'title' => 'Rejoice Shampoo - 70 Ml A. D.',
],
381 => [
'id'    => 4007,
'title' => 'Rejoice Shampoo - 70 Ml A.H.F.',
],
382 => [
'id'    => 4008,
'title' => 'Rejoice Shampoo - 70 Ml Anti-Friz',
],
383 => [
'id'    => 4009,
'title' => 'Rejoice Shampoo - 70 Ml Fruity',
],
384 => [
'id'    => 4010,
'title' => 'Rejoice Shampoo - 70 Ml Rich',
],
385 => [
'id'    => 4011,
'title' => 'Rejoice Shampoo 80 Ml A.D',
],
386 => [
'id'    => 4012,
'title' => 'Rejoice Shampoo - 90 Ml Fruity',
],
387 => [
'id'    => 4013,
'title' => 'Rejoice Shampoo - (Rem) - 100 Ml',
],
388 => [
'id'    => 4014,
'title' => 'Rejoice Shampoo - (Rem) - 200 Ml',
],
389 => [
'id'    => 4015,
'title' => 'Rejoice Shampoo - (Rem) - 400 Ml',
],
390 => [
'id'    => 4016,
'title' => 'Rejoice Shampoo - (Rem) - 750 Ml',
],
391 => [
'id'    => 4017,
'title' => 'Rejoice Shampoo 320Ml S.S Hero',
],
392 => [
'id'    => 4018,
'title' => 'Rejoice Shampoo 340X12 SS Exp Eol',
],
393 => [
'id'    => 4019,
'title' => 'Rejoice Shampoo 600 Ml SS Hero',
],
394 => [
'id'    => 4020,
'title' => 'Rejoice Shampoo 4 Ml',
],
395 => [
'id'    => 4021,
'title' => 'Rejoice Shampoo - 5 Ml Fruity',
],
396 => [
'id'    => 4022,
'title' => 'Rejoice Shampoo - 5 Ml Rich',
],
397 => [
'id'    => 4023,
'title' => 'Kolestone 2000 - 302/0',
],
398 => [
'id'    => 4024,
'title' => 'Kolestone 2000 - 303/4',
],
399 => [
'id'    => 4025,
'title' => 'Kolestone 2000 - 303/6',
],
400 => [
'id'    => 4026,
'title' => 'Kolestone 2000 - 304/0',
],
401 => [
'id'    => 4027,
'title' => 'Kolestone 2000 - 304/5',
],
402 => [
'id'    => 4028,
'title' => 'Kolestone 2000 - 304/6',
],
403 => [
'id'    => 4029,
'title' => 'Kolestone 2000 - 305/0',
],
404 => [
'id'    => 4030,
'title' => 'Kolestone 2000 - 305/4',
],
405 => [
'id'    => 4031,
'title' => 'Kolestone 2000 - 305/66',
],
406 => [
'id'    => 4032,
'title' => 'Kolestone 2000 - 306/0',
],
407 => [
'id'    => 4033,
'title' => 'Kolestone 2000 - 306/45',
],
408 => [
'id'    => 4034,
'title' => 'Kolestone 2000 - 307/0',
],
409 => [
'id'    => 4035,
'title' => 'Kolestone 2000 - 307/1',
],
410 => [
'id'    => 4036,
'title' => 'Kolestone 2000 - 307/3',
],
411 => [
'id'    => 4037,
'title' => 'Kolestone 2000 - 307/34',
],
412 => [
'id'    => 4038,
'title' => 'Kolestone 2000 - 307/7',
],
413 => [
'id'    => 4039,
'title' => 'Kolestone 2000 - 308/0',
],
414 => [
'id'    => 4040,
'title' => 'Kolestone 2000 - 308/3',
],
415 => [
'id'    => 4041,
'title' => 'Kolestone 2000 - 309/3',
],
416 => [
'id'    => 4042,
'title' => 'Kolestone 2000 - 310/0',
],
417 => [
'id'    => 4043,
'title' => 'New Wave Gel - 150 Ml',
],
418 => [
'id'    => 4044,
'title' => 'Vicks Cough Drops - 120 10s',
],
419 => [
'id'    => 4045,
'title' => 'Vicks Cough Drops - 120s',
],
420 => [
'id'    => 4046,
'title' => 'Vicks Inhaler - 0.5 Ml',
],
421 => [
'id'    => 4047,
'title' => 'Vicks Inhaler - 0.5 Ml (30X35)',
],
422 => [
'id'    => 4048,
'title' => 'Vicks Vaporub - 10 Gm',
],
423 => [
'id'    => 4049,
'title' => 'Vicks Vaporub - 25 Gm',
],
424 => [
'id'    => 4050,
'title' => 'Vicks Vaporub - 5 Gm',
],
425 => [
'id'    => 4051,
'title' => 'Vicks Vaporub - 50 Gm',
],
426 => [
'id'    => 4052,
'title' => 'Oral B All Rounder 40XSFT Bcd 32',
],
427 => [
'id'    => 4053,
'title' => 'Oral B All Rounder 123 Clean',
],
428 => [
'id'    => 4054,
'title' => 'Oral B All Rounder 123 Medium',
],
429 => [
'id'    => 4055,
'title' => 'Oral B All Rounder 123 Soft',
],
430 => [
'id'    => 4056,
'title' => 'Oral B Contura Ripple',
],
431 => [
'id'    => 4057,
'title' => 'Oral B Cross Action - Medium',
],
432 => [
'id'    => 4058,
'title' => 'Oral B Cross Action - Soft',
],
433 => [
'id'    => 4059,
'title' => 'Oral B Cross Action - Toothbrush',
],
434 => [
'id'    => 4060,
'title' => 'Oral B All Rounder Dc. White Soft',
],
435 => [
'id'    => 4061,
'title' => 'Oral B All Rounder Gum Pt. 40 Soft',
],
436 => [
'id'    => 4062,
'title' => 'Oral B Shiny Clean Medium',
],
437 => [
'id'    => 4063,
'title' => 'Oral B Shiny Clean Medium -12X12 Pcs',
],
438 => [
'id'    => 4064,
'title' => 'Oral B Shiny Clean Soft',
],
439 => [
'id'    => 4065,
'title' => 'Oral B Shiny Clean Soft -12X12 Pcs',
],
440 => [
'id'    => 4066,
'title' => 'Oral B Classic Toothbrush',
],
441 => [
'id'    => 4067,
'title' => '7`O Clock Shave Brush',
],
442 => [
'id'    => 4068,
'title' => 'Sr. Deo Spray C.W - 150 Ml',
],
443 => [
'id'    => 4069,
'title' => 'Sr. Deo Spray I.C - 150 Ml',
],
444 => [
'id'    => 4070,
'title' => 'Sr. Deo Spray P.R - 150 Ml',
],
445 => [
'id'    => 4071,
'title' => 'Gillette Foamy Senst 98G',
],
446 => [
'id'    => 4072,
'title' => 'Gillette Foamy - 11 Oz B. Frsh',
],
447 => [
'id'    => 4073,
'title' => 'Gillette Foamy - 11 Oz L.L.',
],
448 => [
'id'    => 4074,
'title' => 'Gillette Foamy - 11 Oz Reg.',
],
449 => [
'id'    => 4075,
'title' => 'Gillette Foamy - 11 Oz S.S.',
],
450 => [
'id'    => 4076,
'title' => 'Gillette Foamy Lmnl. - 196G',
],
451 => [
'id'    => 4077,
'title' => 'Gillette Foamy Mnth. - 196G',
],
452 => [
'id'    => 4078,
'title' => 'Gil Fm Shvp 196 G Reg 150Inr 1Csx24It',
],
453 => [
'id'    => 4079,
'title' => 'Gillette Foamy Reg. - 196 Gm',
],
454 => [
'id'    => 4080,
'title' => 'Gillette Foamy Reg. - 418 Gm',
],
455 => [
'id'    => 4081,
'title' => 'Gillette Foamy Senst. - 418 Gm',
],
456 => [
'id'    => 4082,
'title' => 'Gillette Foamy Reg 98Gm',
],
457 => [
'id'    => 4083,
'title' => 'Sr. A/Shave Splash C.W - 100 Ml',
],
458 => [
'id'    => 4084,
'title' => 'Sr. A/Shave Splash I.C - 100 Ml',
],
459 => [
'id'    => 4085,
'title' => 'Sr. A/Shave Splash P.R - 100 Ml',
],
460 => [
'id'    => 4086,
'title' => 'Sr. A/Shave Splash S.F - 100 Ml',
],
461 => [
'id'    => 4087,
'title' => 'Sr. Shaving Foam Prot - 250 Ml',
],
462 => [
'id'    => 4088,
'title' => 'Sr. Shaving Foam S.Ski. - 250 Ml',
],
463 => [
'id'    => 4089,
'title' => 'TGS Fm Shvp 245 Gm Prot 1Csx6It',
],
464 => [
'id'    => 4090,
'title' => 'Sr. Shaving Gel C. C. - 200 Ml',
],
465 => [
'id'    => 4091,
'title' => 'Sr. Shaving Gel Mois. - 200 Ml',
],
466 => [
'id'    => 4092,
'title' => 'Sr. Tube Shave Gel - 25 Gm',
],
467 => [
'id'    => 4093,
'title' => 'Sr. Tube Shave Gel - 60 Gm',
],
468 => [
'id'    => 4094,
'title' => 'Tube Shave Gel Shvp 245 Gm Senst',
],
469 => [
'id'    => 4095,
'title' => 'Old Spice Asl Classic 4.25 Oz',
],
470 => [
'id'    => 4096,
'title' => 'Old Spice Asl Classic 6.375 Oz',
],
471 => [
'id'    => 4097,
'title' => 'Old Spice Asl Fresh 4.25 Oz',
],
472 => [
'id'    => 4098,
'title' => 'Old Spice Asl Pure Sport 6.375 Oz',
],
473 => [
'id'    => 4099,
'title' => 'Old Spice Asl Atomizer Lime 150Ml',
],
474 => [
'id'    => 4100,
'title' => 'Old Spice Asl Atomizer Musk 150Ml',
],
475 => [
'id'    => 4101,
'title' => 'Old Spice Asl Atomizer Original 150Ml',
],
476 => [
'id'    => 4102,
'title' => 'Old Spice Asl Lime 100Ml',
],
477 => [
'id'    => 4103,
'title' => 'Old Spice Asl Lime 150Ml',
],
478 => [
'id'    => 4104,
'title' => 'Old Spice Asl Lime 50Ml',
],
479 => [
'id'    => 4105,
'title' => 'Old Spice Asl Musk 100Ml',
],
480 => [
'id'    => 4106,
'title' => 'Old Spice Asl Musk 50Ml',
],
481 => [
'id'    => 4107,
'title' => 'Old Spice Asl Splash Musk 150Ml',
],
482 => [
'id'    => 4108,
'title' => 'Old Spice Asl Original 100Ml',
],
483 => [
'id'    => 4109,
'title' => 'Old Spice Asl Original 50Ml',
],
484 => [
'id'    => 4110,
'title' => 'Old Spice Asl Splash Lime 150Ml',
],
485 => [
'id'    => 4111,
'title' => 'Old Spice Asl Splash Original 150Ml',
],
486 => [
'id'    => 4112,
'title' => 'Old Spice Swagr Bs 4 Oz',
],
487 => [
'id'    => 4113,
'title' => 'Old Spice Deo Spray Lime 150Ml',
],
488 => [
'id'    => 4114,
'title' => 'Old Spice Deo Spray Ori 150Ml',
],
489 => [
'id'    => 4115,
'title' => 'Old Spice Deo Spray White Water 150Ml',
],
490 => [
'id'    => 4116,
'title' => 'Old Spice Deo Spray Musk 150Ml',
],
491 => [
'id'    => 4117,
'title' => 'Old Spice Shave Cream Lime 30G',
],
492 => [
'id'    => 4118,
'title' => 'Old Spice Shave Cream Lime 70G',
],
493 => [
'id'    => 4119,
'title' => 'Old Spice Shave Cream Musk 30G',
],
494 => [
'id'    => 4120,
'title' => 'Old Spice Shave Cream Musk 70G',
],
495 => [
'id'    => 4121,
'title' => 'Old Spice Shave Cream Original 30G',
],
496 => [
'id'    => 4122,
'title' => 'Old Spice Shave Cream Original 70G',
],
497 => [
'id'    => 4123,
'title' => 'Camay Soap Black - 125 Gm',
],
498 => [
'id'    => 4124,
'title' => 'Camay Soap Pink - 125 Gm',
],
499 => [
'id'    => 4125,
'title' => 'Camay Soap White - 125 Gm',
],
500 => [
'id'    => 4126,
'title' => 'Camay Soap Black - 90 Gm',
],
501 => [
'id'    => 4127,
'title' => 'Camay Soap Pink - 90 Gm',
],
502 => [
'id'    => 4128,
'title' => 'Camay Soap White - 90 Gm',
],
503 => [
'id'    => 4129,
'title' => 'Olay Body Wash Ad - 12 Oz',
],
504 => [
'id'    => 4130,
'title' => 'Olay Body Wash Qnch - 12 Oz',
],
505 => [
'id'    => 4131,
'title' => 'Olay Body Wash Dry Skin - 355 Ml',
],
506 => [
'id'    => 4132,
'title' => 'Olay Body Wash Um 13.5Oz',
],
507 => [
'id'    => 4133,
'title' => 'Olay Soap Ad - 4 Oz',
],
508 => [
'id'    => 4134,
'title' => 'Olay Soap Fr - 4.25 Oz',
],
509 => [
'id'    => 4135,
'title' => 'Olay Soap Qu - 4 Oz',
],
510 => [
'id'    => 4136,
'title' => 'Olay Soap Um - 4 Oz',
],
511 => [
'id'    => 4137,
'title' => '"Olay Soap ""Dry Skin"" - 140 Gm"',
],
512 => [
'id'    => 4138,
'title' => 'Safeguard Soap 90Gm - Green',
],
513 => [
'id'    => 4139,
'title' => 'Safeguard Soap 90Gm - Menthol',
],
514 => [
'id'    => 4140,
'title' => 'Safeguard Soap 90Gm - Pink',
],
515 => [
'id'    => 4141,
'title' => 'Safeguard Soap 90Gm - White',
],
516 => [
'id'    => 4142,
'title' => 'Olay DUVP Normal Cream - 56 ML',
],
517 => [
'id'    => 4217,
'title' => 'Olay Duvp FF Cream - 56 Ml',
],
518 => [
'id'    => 4218,
'title' => 'Olay Duvp Lotion - 118 Ml',
],
519 => [
'id'    => 4219,
'title' => 'Olay Duvp Lotion - 177 Ml',
],
520 => [
'id'    => 4220,
'title' => 'Olay Duvp Lotion 4 Oz',
],
521 => [
'id'    => 4221,
'title' => 'Olay Cad UVFF Cream 2Oz',
],
522 => [
'id'    => 4222,
'title' => 'Olay Regenerist Cream Cleanser 5Oz',
],
523 => [
'id'    => 4223,
'title' => 'Olay Te WetCloth Scntd 30Ct',
],
524 => [
'id'    => 4224,
'title' => 'Olay Wet Towelettes Normal 30Ct/12',
],
525 => [
'id'    => 4225,
'title' => 'Olay Wet Towelettes Sens 30Ct/12',
],
526 => [
'id'    => 4226,
'title' => 'Olay Ads Classic Eye Gel 12/0.5Oz',
],
527 => [
'id'    => 4227,
'title' => 'Olay Eye Gel - 15 Ml',
],
528 => [
'id'    => 4228,
'title' => 'Olay Te Mature Skin Therapy Cream 12/1.7Oz',
],
529 => [
'id'    => 4229,
'title' => 'Olay Cream Noo 12/2Oz',
],
530 => [
'id'    => 4230,
'title' => 'Olay Night Of Olay Cream - 56 Ml',
],
531 => [
'id'    => 4231,
'title' => 'Olay Cream Reg 12/2Oz',
],
532 => [
'id'    => 4232,
'title' => 'Olay Regular Cream - 56 Ml',
],
533 => [
'id'    => 4233,
'title' => 'Olay Regular Lotion - 118 Ml',
],
534 => [
'id'    => 4234,
'title' => 'Olay Regular Lotion 118 Ml',
],
535 => [
'id'    => 4235,
'title' => 'Olay Regular Lotion - 177 Ml',
],
536 => [
'id'    => 4236,
'title' => 'Olay Regular Lotion - 4 Oz',
],
537 => [
'id'    => 4237,
'title' => 'Olay UV Lotion 177 Ml',
],
538 => [
'id'    => 4238,
'title' => 'Olay Renewal Cleanser - 200 Ml',
],
539 => [
'id'    => 4239,
'title' => 'Olay Age Defy Classic SPF 15 12/4Oz',
],
540 => [
'id'    => 4241,
'title' => 'Olay Renewal Cream - 56 Ml',
],
541 => [
'id'    => 4240,
'title' => 'Olay Ads Classic Ngt Crm 12/2 Oz',
],
542 => [
'id'    => 4242,
'title' => 'Olay TE Cream - 50 Ml',
],
543 => [
'id'    => 4243,
'title' => 'Olay TE Eye Cream 12/0.5Oz',
],
544 => [
'id'    => 4244,
'title' => 'Olay Cream Te FF 121.7Z',
],
545 => [
'id'    => 4245,
'title' => 'Olay Teaaff Moisturiser 12/1.7Oz',
],
546 => [
'id'    => 4246,
'title' => 'Olay TE Night Cream - 50 Ml',
],
547 => [
'id'    => 4247,
'title' => 'Olay Cad UV Cream 12/2Z',
],
548 => [
'id'    => 4143,
'title' => 'Olay Nw Cream 20 Gm',
],
549 => [
'id'    => 4144,
'title' => 'Olay Nw Cream 40 Gm',
],
550 => [
'id'    => 4145,
'title' => 'Olay N. W. Day Cream - 50',
],
551 => [
'id'    => 4146,
'title' => 'Olay N. W. Light Day Cream - 20 Gm',
],
552 => [
'id'    => 4147,
'title' => 'Olay N. W. Light Day Cream - 40 Gm',
],
553 => [
'id'    => 4148,
'title' => 'Olay N. W. Day Lotion - 30 Ml',
],
554 => [
'id'    => 4149,
'title' => 'Olay N. W. Foaming Cleanser - 50 Gm',
],
555 => [
'id'    => 4150,
'title' => 'Olay N. W. Night Cream - 50 Gm',
],
556 => [
'id'    => 4151,
'title' => 'Olay Cleanser Clarity 100G',
],
557 => [
'id'    => 4152,
'title' => 'Olay Cleanser Clarity 18G',
],
558 => [
'id'    => 4153,
'title' => 'Olay Cleanser Clarity',
],
559 => [
'id'    => 4154,
'title' => 'Olay Cleanser Clarity 50G',
],
560 => [
'id'    => 4155,
'title' => 'Olay Cleanser Gentle 100G',
],
561 => [
'id'    => 4156,
'title' => 'Olay Cleanser Gentle 18G',
],
562 => [
'id'    => 4157,
'title' => 'Olay Cleanser Gentle 50G',
],
563 => [
'id'    => 4158,
'title' => 'Olay Cleanser Moisture Balance 100G',
],
564 => [
'id'    => 4159,
'title' => 'Olay Cleanser Moisture Balance 50G',
],
565 => [
'id'    => 4160,
'title' => 'Olay Cream- 100 Gm',
],
566 => [
'id'    => 4161,
'title' => 'Olay Cream -50 Gm',
],
567 => [
'id'    => 4162,
'title' => 'Olay Cream TotlFx FragFree 1.7Oz',
],
568 => [
'id'    => 4163,
'title' => 'Olay Oil Fr Lotion - 118 Ml',
],
569 => [
'id'    => 4164,
'title' => 'Olay Pink Cream-50Gm Thai',
],
570 => [
'id'    => 4165,
'title' => 'Olay Pink Lotion-150Ml Thai',
],
571 => [
'id'    => 4166,
'title' => 'Olay Pink Lotion-75Ml Thai',
],
572 => [
'id'    => 4167,
'title' => 'Olay Regen Dp Hydra Cream',
],
573 => [
'id'    => 4168,
'title' => 'Olay Regenerist Cleanser 100Gm',
],
574 => [
'id'    => 4169,
'title' => 'Olay Regen Cream 12/1.7 Oz',
],
575 => [
'id'    => 4170,
'title' => 'Olay Regenerist Day Cream 14G',
],
576 => [
'id'    => 4171,
'title' => 'Olay Regenerist Day Cream',
],
577 => [
'id'    => 4172,
'title' => 'Olay Regenerist Day Cream 50G',
],
578 => [
'id'    => 4173,
'title' => 'Olay Regenerist Eye Roller 12/0.2 Fl Oz',
],
579 => [
'id'    => 4174,
'title' => 'Olay Regenerist Eye Serum 12/0.5 Oz',
],
580 => [
'id'    => 4175,
'title' => 'Olay Regenerist Eye Serum 15Ml',
],
581 => [
'id'    => 4176,
'title' => 'Olay Reg Msc Bngk Blk 12/1.7 Oz',
],
582 => [
'id'    => 4177,
'title' => 'Olay Regenerist Msc Cream 50G',
],
583 => [
'id'    => 4178,
'title' => 'Olay Regenerist Msc Wrinkle Relaxing Cream',
],
584 => [
'id'    => 4179,
'title' => 'Olay Regenerist Msc Cream FF 48G',
],
585 => [
'id'    => 4180,
'title' => 'Olay Regenerist Msc Night Essence 50Ml',
],
586 => [
'id'    => 4181,
'title' => 'Olay Regenerist Msc Serum 50Ml',
],
587 => [
'id'    => 4182,
'title' => 'Olay Regenerist Msc SPF 30 50Ml',
],
588 => [
'id'    => 4183,
'title' => 'Olay Regenerist Msc Wr Cream 50Ml',
],
589 => [
'id'    => 4184,
'title' => 'Olay Regen Night 12/1.7Oz',
],
590 => [
'id'    => 4185,
'title' => 'Olay Regenerist Night Cream',
],
591 => [
'id'    => 4186,
'title' => 'Olay Regenerist Night Recovery Cream',
],
592 => [
'id'    => 4187,
'title' => 'Olay Regen Serum 1.7Oz',
],
593 => [
'id'    => 4188,
'title' => 'Olay Regen Serum FragFree 12/1.7Oz',
],
594 => [
'id'    => 4189,
'title' => 'Olay Regenerist Serum 50Ml',
],
595 => [
'id'    => 4190,
'title' => 'Olay Regenerist Serum 50Ml SPF 30',
],
596 => [
'id'    => 4191,
'title' => 'Olay TE Cleaner-100Gm Thai',
],
597 => [
'id'    => 4192,
'title' => 'Olay TE Cleaner-50Gm Thai',
],
598 => [
'id'    => 4193,
'title' => 'Olay TE Cleanser-50Gm Thai',
],
599 => [
'id'    => 4194,
'title' => 'Olay TE Gentle Cream-50Gm Thai',
],
600 => [
'id'    => 4195,
'title' => 'Olay TE Gentle UV Cream-50Gm Thai',
],
601 => [
'id'    => 4196,
'title' => 'Olay TE Cream Normal-20Gm Thai',
],
602 => [
'id'    => 4197,
'title' => 'Olay TE Normal Cream-50Gm Thai',
],
603 => [
'id'    => 4198,
'title' => 'Olay TE Oil Cream 50G Thai',
],
604 => [
'id'    => 4199,
'title' => 'Olay TE Oil Cream 100Gm',
],
605 => [
'id'    => 4200,
'title' => 'Olay Cream TE FF 50Gm',
],
606 => [
'id'    => 4201,
'title' => 'Olay TE Normal UV Cream-20Gm Thai',
],
607 => [
'id'    => 4202,
'title' => 'Olay Face Cream TE UVff 50Gm',
],
608 => [
'id'    => 4203,
'title' => 'Olay TE Normal UV Cream-50Gm Thai',
],
609 => [
'id'    => 4204,
'title' => 'Olay TE Touch Of Found. Cream-50Gm Thai',
],
610 => [
'id'    => 4205,
'title' => 'Olay White Lotion - 75 Ml Thai',
],
611 => [
'id'    => 4206,
'title' => 'Olay White Night Cream-50Gm Thai',
],
612 => [
'id'    => 4207,
'title' => 'Olay White Radiance Cleanser 100G',
],
613 => [
'id'    => 4208,
'title' => 'Olay White Radiance Cream 50G',
],
614 => [
'id'    => 4209,
'title' => 'Olay White Radiance 50G',
],
615 => [
'id'    => 4210,
'title' => 'Olay White Radiance Day Cream 15G',
],
616 => [
'id'    => 4211,
'title' => 'Olay White Radiance Dropper Essence 40Ml',
],
617 => [
'id'    => 4212,
'title' => 'Olay White Radiance Eye Serum 15Ml',
],
618 => [
'id'    => 4213,
'title' => 'Olay White Radiance Lotion 75Ml',
],
619 => [
'id'    => 4214,
'title' => 'Olay White Radiance Lotion SPF 30',
],
620 => [
'id'    => 4215,
'title' => 'Olay White Radiance Night 50G',
],
621 => [
'id'    => 4216,
'title' => 'Olay White Radiance Serum 50Ml',
],
622 => [
'id'    => 4690,
'title' => ' H&S Shampoo - 720 Ml (A.H.F)',
],
],
],
]);
});

Route::post('retail/add', function () {
return json_encode([
'status'      => 'success',
'identifiers' => [
0 => [
'id'         => '1001',
'identifier' => 962,
],
],
'message'     => 'Retail Shops created',
]);
});

Route::post('retail/update', function () {
return json_encode([
'status'      => 'success',
'identifiers' => [
0 => [
'id'         => '1001',
'identifier' => 959,
],
],
'message'     => 'Retail Shops Updated',
]);
});


Route::post('retail/order', function () {
return json_encode([
'status'      => 'success',
'identifiers' => [
0 => [
'order_id'   => 12,
'identifier' => 11,
],
1 => [
'order_id'   => 13,
'identifier' => 12,
],
2 => [
'order_id'   => 3,
'identifier' => 13,
],
],
'message'     => 'Orders taken.',
]);
});


Route::post('categoryandinventory/list', function () {
return json_encode([
'status' => 'success',
'data'   => [
'inventory_list' => [
0  => [
'category' => [
'info'      => [
'id'    => 1556,
'title' => 'Air Freshener',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1557,
'title' => 'Ambipur',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3649,
'title'      => 'Ambip Airfr Spray B&B Reg 275G',
'price'      => '355.360',
'updated_on' => '2014-11-28 15:59:20',
],
],
1  => [
'sku_package' => [
'id'         => 3650,
'title'      => 'Ambip Airfr Spray Lv&C Reg 275G',
'price'      => '355.360',
'updated_on' => '2014-11-28 15:59:20',
],
],
2  => [
'sku_package' => [
'id'         => 3651,
'title'      => 'Ambip Airfr Spray S&R 275G',
'price'      => '355.360',
'updated_on' => '2014-11-28 15:59:21',
],
],
3  => [
'sku_package' => [
'id'         => 3652,
'title'      => 'Ambip Airfr Spray Sc&Z Reg 275G',
'price'      => '355.360',
'updated_on' => '2014-11-28 15:59:21',
],
],
4  => [
'sku_package' => [
'id'         => 3653,
'title'      => 'Ambip Airfr Spray Sc&Z Reg 275G (6Pcs)',
'price'      => '355.360',
'updated_on' => '2014-11-28 15:59:21',
],
],
5  => [
'sku_package' => [
'id'         => 3654,
'title'      => 'Ambip AER B.O 300Ml - 6 Pcs',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:21',
],
],
6  => [
'sku_package' => [
'id'         => 3655,
'title'      => 'Ambip AER F&F 300Ml',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:21',
],
],
7  => [
'sku_package' => [
'id'         => 3656,
'title'      => 'Ambip AER F&F 300Mlx12',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:21',
],
],
8  => [
'sku_package' => [
'id'         => 3657,
'title'      => 'Ambip AER F&L 300Ml',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:21',
],
],
9  => [
'sku_package' => [
'id'         => 3658,
'title'      => 'Ambip AER F&L 300Mlx12',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:21',
],
],
10 => [
'sku_package' => [
'id'         => 3659,
'title'      => 'Ambip AER L.B. 300Ml -6Pcs)',
'price'      => '176.790',
'updated_on' => '2014-11-28 15:59:20',
],
],
],
],
],
],
],
1  => [
'category' => [
'info'      => [
'id'    => 1563,
'title' => 'Baby Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1564,
'title' => 'Pampers',
],
'inventory' => [
0  => [
'sku_package' => [
'id'    => 4684,
'title' => 'Pampers Pants - 60s(M) ',
],
],
1  => [
'sku_package' => [
'id'    => 4683,
'title' => 'Pampers Pants - 8s(L) ',
],
],
2  => [
'sku_package' => [
'id'         => 4674,
'title'      => 'Pampers Pants - 9s(M) ',
'price'      => '205.360',
'updated_on' => '2015-12-15 11:41:49',
],
],
3  => [
'sku_package' => [
'id'    => 4681,
'title' => 'Pampers Pants - 9s(S) ',
],
],
4  => [
'sku_package' => [
'id'    => 4672,
'title' => 'Pampers Pants - 2s(XL) ',
],
],
5  => [
'sku_package' => [
'id'    => 4673,
'title' => 'Pampers Pants - 2s(S) (96) ',
],
],
6  => [
'sku_package' => [
'id'    => 4679,
'title' => 'Pampers Pants - 2s(M) ',
],
],
7  => [
'sku_package' => [
'id'    => 4685,
'title' => 'Pampers Pants - 2s(L) ',
],
],
8  => [
'sku_package' => [
'id'         => 4689,
'title'      => 'Pampers Pants - 32s(XL) ',
'price'      => '1008.930',
'updated_on' => '2015-12-15 11:41:49',
],
],
9  => [
'sku_package' => [
'id'         => 4675,
'title'      => 'Pampers Pants - 20s(M) 8 ',
'price'      => '391.070',
'updated_on' => '2015-12-15 11:41:49',
],
],
10 => [
'sku_package' => [
'id'         => 4676,
'title'      => 'Pampers Pants - 18s(L) 8 ',
'price'      => '391.070',
'updated_on' => '2015-12-15 11:41:49',
],
],
11 => [
'sku_package' => [
'id'    => 4677,
'title' => 'Pampers Pants - 7s(XL) ',
],
],
12 => [
'sku_package' => [
'id'    => 4678,
'title' => 'Pampers Pants - 36s(L) ',
],
],
13 => [
'sku_package' => [
'id'    => 4680,
'title' => 'Pampers Pants - 46s(S) ',
],
],
14 => [
'sku_package' => [
'id'         => 4682,
'title'      => 'Pampers Pants - 42s(M) ',
'price'      => '779.460',
'updated_on' => '2015-12-15 11:41:49',
],
],
15 => [
'sku_package' => [
'id'    => 4686,
'title' => 'Pampers Pants - 22s(S) ',
],
],
16 => [
'sku_package' => [
'id'    => 4687,
'title' => 'Pampers Pants - 52s(L) ',
],
],
17 => [
'sku_package' => [
'id'    => 4688,
'title' => 'Pampers Pants - 16s(XL) 8 ',
],
],
18 => [
'sku_package' => [
'id'         => 4691,
'title'      => 'Pampers Pants - 48s(XL) ',
'price'      => '1248.210',
'updated_on' => '2015-12-15 11:41:49',
],
],
],
],
],
],
],
2  => [
'category' => [
'info'      => [
'id'    => 1582,
'title' => 'Battery',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1583,
'title' => 'Duracell',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 3683,
'title'      => '"Duracell ""9V"" Battery"',
'price'      => '238.460',
'updated_on' => '2015-03-10 12:22:54',
],
],
1 => [
'sku_package' => [
'id'         => 3687,
'title'      => 'Duracell AA1',
'price'      => '42.310',
'updated_on' => '2015-03-10 12:22:54',
],
],
2 => [
'sku_package' => [
'id'         => 3688,
'title'      => 'Duracell AA2',
'price'      => '76.920',
'updated_on' => '2015-03-10 12:22:17',
],
],
3 => [
'sku_package' => [
'id'         => 3689,
'title'      => 'Duracell AA4',
'price'      => '138.460',
'updated_on' => '2015-03-10 12:22:17',
],
],
4 => [
'sku_package' => [
'id'         => 3690,
'title'      => 'Duracell AAA1',
'price'      => '42.310',
'updated_on' => '2015-03-10 12:22:54',
],
],
5 => [
'sku_package' => [
'id'         => 3691,
'title'      => 'Duracell AAA2',
'price'      => '76.920',
'updated_on' => '2015-03-10 12:22:17',
],
],
6 => [
'sku_package' => [
'id'         => 3692,
'title'      => 'Duracell AAA4',
'price'      => '138.460',
'updated_on' => '2015-03-10 12:22:17',
],
],
],
],
],
],
],
3  => [
'category' => [
'info'      => [
'id'    => 1601,
'title' => 'Blades & Razors',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1602,
'title' => 'Gillette',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3693,
'title'      => '7`O Clock PII - 5`S',
'price'      => '196.360',
'updated_on' => '2015-03-10 12:10:21',
],
],
1  => [
'sku_package' => [
'id'         => 3694,
'title'      => '7`O Clock PII Ms Razor (R)',
'price'      => '123.640',
'updated_on' => '2014-11-30 15:55:54',
],
],
2  => [
'sku_package' => [
'id'         => 3695,
'title'      => '7`O Clock PII Razor',
'price'      => '123.640',
'updated_on' => '2015-03-10 12:14:24',
],
],
3  => [
'sku_package' => [
'id'         => 3696,
'title'      => 'Fusion Cart - 2`S',
'price'      => '518.180',
'updated_on' => '2015-03-10 12:13:15',
],
],
4  => [
'sku_package' => [
'id'         => 3697,
'title'      => 'Fusion Cart - 4`S',
'price'      => '1000.000',
'updated_on' => '2015-03-10 12:13:16',
],
],
5  => [
'sku_package' => [
'id'         => 3698,
'title'      => 'Fusion Cart - 6`S',
'price'      => '1390.910',
'updated_on' => '2015-03-10 12:13:16',
],
],
6  => [
'sku_package' => [
'id'         => 3699,
'title'      => 'Fusion Cart - 8`S',
'price'      => '1727.270',
'updated_on' => '2015-03-10 12:13:16',
],
],
7  => [
'sku_package' => [
'id'         => 3700,
'title'      => 'Fusion Power Cart - 2`S',
'price'      => '595.450',
'updated_on' => '2015-03-10 12:13:16',
],
],
8  => [
'sku_package' => [
'id'         => 3701,
'title'      => 'Fusion Power Cart - 4`S',
'price'      => '1077.270',
'updated_on' => '2015-03-10 12:13:16',
],
],
9  => [
'sku_package' => [
'id'         => 3702,
'title'      => 'Fusion Power Cart - 6`S',
'price'      => '1431.820',
'updated_on' => '2015-03-10 12:13:16',
],
],
10 => [
'sku_package' => [
'id'         => 3703,
'title'      => 'Fusion Power Razor',
'price'      => '545.450',
'updated_on' => '2014-11-28 15:54:40',
],
],
11 => [
'sku_package' => [
'id'         => 3704,
'title'      => 'Fusion Razor ( R )',
'price'      => '545.450',
'updated_on' => '2014-11-28 15:54:39',
],
],
12 => [
'sku_package' => [
'id'         => 3709,
'title'      => 'Gillette 2 Disposable',
'price'      => '14.550',
'updated_on' => '2015-03-10 12:05:51',
],
],
13 => [
'sku_package' => [
'id'         => 3710,
'title'      => 'Gillette Guard 3Ct',
'price'      => '28.180',
'updated_on' => '2014-11-28 15:42:32',
],
],
14 => [
'sku_package' => [
'id'         => 3711,
'title'      => 'Gillette Guard Crt 1',
'price'      => '10.910',
'updated_on' => '2014-11-28 15:42:10',
],
],
15 => [
'sku_package' => [
'id'         => 3712,
'title'      => 'Gillette Guard Razor',
'price'      => '28.180',
'updated_on' => '2014-11-28 15:54:39',
],
],
16 => [
'sku_package' => [
'id'         => 3713,
'title'      => 'Gillette Mach III Cart - 2`S',
'price'      => '340.910',
'updated_on' => '2015-03-10 12:08:55',
],
],
17 => [
'sku_package' => [
'id'         => 3714,
'title'      => 'Gillette Mach III Cart - 4`S',
'price'      => '659.090',
'updated_on' => '2015-03-10 12:08:55',
],
],
18 => [
'sku_package' => [
'id'         => 3715,
'title'      => 'Gillette Mach III Cart - 8`S',
'price'      => '1059.090',
'updated_on' => '2015-03-10 12:08:55',
],
],
19 => [
'sku_package' => [
'id'         => 3716,
'title'      => 'Gillette Mach III Power Cart - 4`S',
'price'      => '659.090',
'updated_on' => '2014-11-28 15:54:39',
],
],
20 => [
'sku_package' => [
'id'         => 3717,
'title'      => 'Gillette Mach III Turbo Cart - 2`S',
'price'      => '422.730',
'updated_on' => '2015-03-10 12:09:36',
],
],
21 => [
'sku_package' => [
'id'         => 3718,
'title'      => 'Gillette Mach III Turbo Cart - 4`S',
'price'      => '818.180',
'updated_on' => '2015-03-10 12:09:36',
],
],
22 => [
'sku_package' => [
'id'         => 3719,
'title'      => 'Gillette Mach III Turbo Cart - 8`S',
'price'      => '1472.730',
'updated_on' => '2015-03-10 12:09:36',
],
],
23 => [
'sku_package' => [
'id'         => 4248,
'title'      => 'Gillette Mach III Turbo sensitive 2CT',
'price'      => '422.730',
'updated_on' => '2015-03-10 12:10:05',
],
],
24 => [
'sku_package' => [
'id'         => 4249,
'title'      => 'Gillette Mach III Turbo sensitive 4CT',
'price'      => '818.180',
'updated_on' => '2015-03-10 12:10:05',
],
],
25 => [
'sku_package' => [
'id'         => 3720,
'title'      => 'Gillette Mach III Hercules (R)',
'price'      => '200.000',
'updated_on' => '2014-11-28 15:54:39',
],
],
26 => [
'sku_package' => [
'id'         => 3721,
'title'      => 'Gillette Mach III Power Razor',
'price'      => '735.450',
'updated_on' => '2015-03-10 12:16:03',
],
],
27 => [
'sku_package' => [
'id'         => 3722,
'title'      => 'Gillette Mach III Turbo Razor (R)',
'price'      => '522.730',
'updated_on' => '2015-03-10 12:15:12',
],
],
28 => [
'sku_package' => [
'id'         => 4250,
'title'      => 'Gillette Mach III Turbo Sensitive Razor 1 Up',
'price'      => '244.550',
'updated_on' => '2015-03-10 12:16:53',
],
],
29 => [
'sku_package' => [
'id'         => 3724,
'title'      => 'Gillette Presto - 5`S',
'price'      => '128.180',
'updated_on' => '2015-03-10 12:05:23',
],
],
30 => [
'sku_package' => [
'id'         => 3725,
'title'      => 'Gillette Presto Int`L',
'price'      => '26.360',
'updated_on' => '2014-11-28 15:54:39',
],
],
31 => [
'sku_package' => [
'id'         => 3726,
'title'      => 'Sensor Excel - 10`S',
'price'      => '568.180',
'updated_on' => '2015-03-10 12:07:47',
],
],
32 => [
'sku_package' => [
'id'         => 3727,
'title'      => 'Sensor Excel - 3`S',
'price'      => '192.730',
'updated_on' => '2015-03-10 12:07:47',
],
],
33 => [
'sku_package' => [
'id'         => 3728,
'title'      => 'Sensor Excel - 5`S',
'price'      => '303.640',
'updated_on' => '2015-03-10 12:07:47',
],
],
34 => [
'sku_package' => [
'id'         => 3730,
'title'      => 'Gillette Super Thin Blade (5)',
'price'      => '20.000',
'updated_on' => '2014-11-28 15:54:39',
],
],
35 => [
'sku_package' => [
'id'    => 3731,
'title' => 'Vector Plus - 1`S',
],
],
36 => [
'sku_package' => [
'id'         => 3732,
'title'      => 'Vector Plus - 2`S',
'price'      => '59.640',
'updated_on' => '2015-03-10 12:06:44',
],
],
37 => [
'sku_package' => [
'id'         => 3733,
'title'      => 'Vector Plus - 2`S (600)',
'price'      => '53.640',
'updated_on' => '2014-11-28 15:54:39',
],
],
38 => [
'sku_package' => [
'id'         => 3734,
'title'      => 'Vector Plus 2`S (India)',
'price'      => '53.640',
'updated_on' => '2014-11-28 15:54:39',
],
],
39 => [
'sku_package' => [
'id'         => 3735,
'title'      => 'Vector Plus - 4`S',
'price'      => '104.730',
'updated_on' => '2015-03-10 12:07:05',
],
],
40 => [
'sku_package' => [
'id'         => 3736,
'title'      => 'Vector Plus - 4`S (600)',
'price'      => '94.550',
'updated_on' => '2014-11-28 15:54:39',
],
],
41 => [
'sku_package' => [
'id'         => 3737,
'title'      => 'Vector Plus 4`S (India)',
'price'      => '94.550',
'updated_on' => '2014-11-28 15:54:39',
],
],
42 => [
'sku_package' => [
'id'         => 3738,
'title'      => 'Vector Plus Razor (R)',
'price'      => '64.000',
'updated_on' => '2014-11-28 15:54:39',
],
],
43 => [
'sku_package' => [
'id'    => 3739,
'title' => 'Vector Razor 139 INR',
],
],
],
],
],
],
],
4  => [
'category' => [
'info'      => [
'id'    => 1676,
'title' => 'Car Freshener',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1677,
'title' => 'Ambipur',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3742,
'title'      => 'Ambip Car Refill AQ 7Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:59:20',
],
],
1  => [
'sku_package' => [
'id'         => 3743,
'title'      => 'Ambip Car Refill F&L 7Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:59:20',
],
],
2  => [
'sku_package' => [
'id'         => 3744,
'title'      => 'Ambip Car Refill L.S 7Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:59:20',
],
],
3  => [
'sku_package' => [
'id'         => 3745,
'title'      => 'Ambip Car Refill P.A 7Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:59:20',
],
],
4  => [
'sku_package' => [
'id'         => 3746,
'title'      => 'Ambip Car Refill V.B 7Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:59:20',
],
],
5  => [
'sku_package' => [
'id'         => 3747,
'title'      => 'Ambip Car Ambip Car Starter & Refill Aqua St 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
6  => [
'sku_package' => [
'id'         => 3748,
'title'      => 'Ambip Car Starter & Refill Aq 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
7  => [
'sku_package' => [
'id'         => 3749,
'title'      => 'Ambip Car Starter & Refill Aq Rf 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
8  => [
'sku_package' => [
'id'         => 3750,
'title'      => 'Ambip Car Starter & Refill F&L 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
9  => [
'sku_package' => [
'id'         => 3751,
'title'      => 'Ambip Car Starter & Refill F&L 7Ml Frs&Lght St 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
10 => [
'sku_package' => [
'id'         => 3752,
'title'      => 'Ambip Car Starter & Refill F&L Rf 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
11 => [
'sku_package' => [
'id'         => 3753,
'title'      => 'Ambip Car Starter & Refill L.S 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
12 => [
'sku_package' => [
'id'         => 3754,
'title'      => 'Ambip Car Starter & Refill L.S Rf 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
13 => [
'sku_package' => [
'id'         => 3755,
'title'      => 'Ambip Carambip Car Starter & Refill L.S 7Ml Spa St 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
14 => [
'sku_package' => [
'id'         => 3756,
'title'      => 'Ambip Car Starter & Refill Pac Air St 7Mlx4',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
15 => [
'sku_package' => [
'id'         => 3757,
'title'      => 'Ambip Car Starter & Refill P.A 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
16 => [
'sku_package' => [
'id'         => 3758,
'title'      => 'Ambip Car Starter & Refill P.A Rf 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
17 => [
'sku_package' => [
'id'         => 3759,
'title'      => 'Ambip Car Starter & Refill V.B 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
18 => [
'sku_package' => [
'id'         => 3760,
'title'      => 'Ambip Car Starter & Refill V.Bouq Rf 7Mlx6',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:21',
],
],
19 => [
'sku_package' => [
'id'         => 3761,
'title'      => 'Ambip Car Starter & Refill V.Bouq St 7Ml',
'price'      => '321.430',
'updated_on' => '2014-11-28 15:59:20',
],
],
],
],
],
],
],
5  => [
'category' => [
'info'      => [
'id'    => 1683,
'title' => 'Detergent',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1684,
'title' => 'Ariel',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3762,
'title'      => 'Ariel Detergent - 1 Kg',
'price'      => '274.070',
'updated_on' => '2015-10-06 12:17:27',
],
],
1  => [
'sku_package' => [
'id'         => 3763,
'title'      => 'Ariel Detergent - 2 Kg',
'price'      => '531.850',
'updated_on' => '2015-10-06 12:17:27',
],
],
2  => [
'sku_package' => [
'id'         => 3764,
'title'      => 'Ariel Detergent C&S- 2 Kg',
'price'      => '531.850',
'updated_on' => '2015-10-06 12:17:27',
],
],
3  => [
'sku_package' => [
'id'         => 3765,
'title'      => 'Ariel Detergent - 200 Gm',
'price'      => '57.780',
'updated_on' => '2015-10-06 12:17:27',
],
],
4  => [
'sku_package' => [
'id'         => 3766,
'title'      => 'Ariel Detergent - 500 Gm',
'price'      => '139.260',
'updated_on' => '2015-10-06 12:17:27',
],
],
5  => [
'sku_package' => [
'id'         => 3767,
'title'      => 'Ariel Detergent 24 Hrs Fresh - 1 Kg',
'price'      => '303.700',
'updated_on' => '2015-10-06 12:17:27',
],
],
6  => [
'sku_package' => [
'id'         => 3768,
'title'      => 'Ariel Detergent 24 Hrs Fresh - 2 Kg',
'price'      => '595.110',
'updated_on' => '2015-10-06 12:17:27',
],
],
7  => [
'sku_package' => [
'id'         => 3769,
'title'      => 'Ariel Detergent 24 Hrs Fresh - 500 Gm',
'price'      => '151.110',
'updated_on' => '2015-10-06 12:17:27',
],
],
8  => [
'sku_package' => [
'id'         => 3770,
'title'      => 'Ariel Detergent 3Kg',
'price'      => '739.260',
'updated_on' => '2015-10-06 12:17:27',
],
],
9  => [
'sku_package' => [
'id'         => 3771,
'title'      => 'Ariel Detergent Matic 500G',
'price'      => '173.330',
'updated_on' => '2015-10-06 12:17:27',
],
],
10 => [
'sku_package' => [
'id'         => 3772,
'title'      => 'Ariel Detergent C&S 500Gm',
'price'      => '139.260',
'updated_on' => '2015-10-06 12:17:27',
],
],
11 => [
'sku_package' => [
'id'         => 3773,
'title'      => 'Ariel Detergent C&S - 1 Kg',
'price'      => '274.070',
'updated_on' => '2015-10-06 12:17:27',
],
],
12 => [
'sku_package' => [
'id'         => 3774,
'title'      => 'Ariel Detergent - 12 Gm',
'price'      => '3.700',
'updated_on' => '2014-11-14 09:43:18',
],
],
13 => [
'sku_package' => [
'id'         => 3777,
'title'      => 'Ariel Liq Reg 1Kg',
'price'      => '368.520',
'updated_on' => '2015-03-13 13:22:25',
],
],
14 => [
'sku_package' => [
'id'         => 3778,
'title'      => 'Ariel Liq Reg 3Kg',
'price'      => '971.290',
'updated_on' => '2015-03-13 13:22:25',
],
],
],
],
1 => [
'info'      => [
'id'    => 1703,
'title' => 'Tide',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 3781,
'title'      => 'Tide Detergent - 800 Gm',
'price'      => '185.190',
'updated_on' => '2014-11-12 11:12:36',
],
],
1 => [
'sku_package' => [
'id'         => 3783,
'title'      => 'Tide LAU PWD Reg - 1.5 Kg',
'price'      => '334.260',
'updated_on' => '2015-03-13 13:24:18',
],
],
2 => [
'sku_package' => [
'id'         => 3784,
'title'      => 'Tide LAU PWD Reg - 3 Kg',
'price'      => '680.560',
'updated_on' => '2015-03-13 13:24:18',
],
],
3 => [
'sku_package' => [
'id'         => 3786,
'title'      => 'Tide LAU PWD Reg - 400 Gm',
'price'      => '92.590',
'updated_on' => '2015-03-13 13:24:18',
],
],
],
],
],
],
],
6  => [
'category' => [
'info'      => [
'id'    => 1720,
'title' => 'Feminine Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1721,
'title' => 'Whisper',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3790,
'title'      => 'Whisper Choice Non Wing - 8`S',
'price'      => '41.430',
'updated_on' => '2014-11-28 15:15:33',
],
],
1  => [
'sku_package' => [
'id'         => 3791,
'title'      => 'Whisper Choice Wing - 8`S',
'price'      => '45.710',
'updated_on' => '2014-11-28 15:15:33',
],
],
2  => [
'sku_package' => [
'id'         => 3793,
'title'      => 'Whisper Ultra Choice - 6`S',
'price'      => '57.140',
'updated_on' => '2014-11-28 15:15:33',
],
],
3  => [
'sku_package' => [
'id'         => 3795,
'title'      => 'Whisper Ultra Wings 8S',
'price'      => '111.430',
'updated_on' => '2014-11-28 15:15:33',
],
],
4  => [
'sku_package' => [
'id'         => 3798,
'title'      => 'Whisper Maxi - 15`S',
'price'      => '250.000',
'updated_on' => '2014-11-28 15:15:33',
],
],
5  => [
'sku_package' => [
'id'         => 3799,
'title'      => 'Whisper Maxi - 7S Overnight',
'price'      => '121.430',
'updated_on' => '2014-11-28 15:15:33',
],
],
6  => [
'sku_package' => [
'id'         => 3800,
'title'      => 'Whisper Maxi - 8`S',
'price'      => '100.000',
'updated_on' => '2014-11-28 15:15:33',
],
],
7  => [
'sku_package' => [
'id'         => 3801,
'title'      => 'Whisper Maxi- 15 S Overnight',
'price'      => '250.000',
'updated_on' => '2014-11-28 15:15:33',
],
],
8  => [
'sku_package' => [
'id'         => 3810,
'title'      => 'Whisper Ultra - 7`S',
'price'      => '111.430',
'updated_on' => '2014-11-28 15:15:33',
],
],
9  => [
'sku_package' => [
'id'         => 3811,
'title'      => 'Whisper Ultra - 8`S',
'price'      => '111.430',
'updated_on' => '2014-11-28 15:15:33',
],
],
10 => [
'sku_package' => [
'id'         => 3812,
'title'      => 'Whisper Ultra Wing - 15`S',
'price'      => '200.000',
'updated_on' => '2014-11-28 15:15:33',
],
],
],
],
],
],
],
7  => [
'category' => [
'info'      => [
'id'    => 1750,
'title' => 'Hair Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1751,
'title' => 'Clairol',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 3831,
'title'      => 'Herbal ESS. Shampoo H. H. - 300 Ml',
'price'      => '236.610',
'updated_on' => '2014-11-28 15:25:13',
],
],
],
],
1 => [
'info'      => [
'id'    => 1760,
'title' => 'Gillette',
],
'inventory' => [
0 => [
'sku_package' => [
'id'    => 3832,
'title' => 'Gillette Shampoo 2 In 1 12.2 Oz',
],
],
],
],
2 => [
'info'      => [
'id'    => 1767,
'title' => 'Head & Shoulders',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3836,
'title'      => 'H&S Purely Gentle 23.7Oz',
'price'      => '538.260',
'updated_on' => '2014-07-29 17:20:24',
],
],
1  => [
'sku_package' => [
'id'         => 3837,
'title'      => 'H&S Shampoo Purely Gentle 6 14.2Oz',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:07',
],
],
2  => [
'sku_package' => [
'id'         => 3838,
'title'      => 'H&S Shampoo - 180 Ml A.H.F',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
3  => [
'sku_package' => [
'id'         => 3839,
'title'      => 'H&S Shampoo - 180 Ml C.B',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
4  => [
'sku_package' => [
'id'         => 3840,
'title'      => 'H&S Shampoo - 180 Ml D.S.C.A.',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
5  => [
'sku_package' => [
'id'         => 3841,
'title'      => 'H&S Shampoo - 180 Ml F.M.H.R',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
6  => [
'sku_package' => [
'id'         => 3842,
'title'      => 'H&S Shampoo - 180 Ml I.S.C.',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
7  => [
'sku_package' => [
'id'         => 3843,
'title'      => 'H&S Shampoo - 180 Ml R.M.',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:07',
],
],
8  => [
'sku_package' => [
'id'         => 3844,
'title'      => 'H&S Shampoo - 180 Ml S.S',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
9  => [
'sku_package' => [
'id'    => 3845,
'title' => 'H&S Shampoo - 315Ml Hair Retain',
],
],
10 => [
'sku_package' => [
'id'         => 3846,
'title'      => 'H&S Shampoo - 350 Ml A.H.F',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
11 => [
'sku_package' => [
'id'         => 3847,
'title'      => 'H&S Shampoo - 350 Ml A.V',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
12 => [
'sku_package' => [
'id'         => 3848,
'title'      => 'H&S Shampoo - 350 Ml C.B',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
13 => [
'sku_package' => [
'id'         => 3849,
'title'      => 'H&S Shampoo - 350 Ml D.S.C.A.',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
14 => [
'sku_package' => [
'id'         => 3850,
'title'      => 'H&S Shampoo - 350 Ml F.M.H.R.',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
15 => [
'sku_package' => [
'id'         => 3851,
'title'      => 'H&S Shampoo - 350 Ml I.S.C.',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
16 => [
'sku_package' => [
'id'         => 3852,
'title'      => 'H&S Shampoo - 350 Ml R.M.',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
17 => [
'sku_package' => [
'id'         => 3853,
'title'      => 'H&S Shampoo - 350 Ml S.S',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
18 => [
'sku_package' => [
'id'         => 3854,
'title'      => 'H&S Shampoo - 350 Ml T.L.',
'price'      => '302.680',
'updated_on' => '2014-11-28 15:12:07',
],
],
19 => [
'sku_package' => [
'id'         => 3855,
'title'      => 'H&S Shampoo - 610 Ml F.M.H.R',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
20 => [
'sku_package' => [
'id'         => 3856,
'title'      => 'H&S Shampoo - 675 Ml A.H.F',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
21 => [
'sku_package' => [
'id'         => 3857,
'title'      => 'H&S Shampoo - 675 Ml C.B',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
22 => [
'sku_package' => [
'id'         => 3858,
'title'      => 'H&S Shampoo - 675 Ml F.M.H.R.',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
23 => [
'sku_package' => [
'id'         => 3859,
'title'      => 'H&S Shampoo - 675 Ml R.M.',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
24 => [
'sku_package' => [
'id'         => 3860,
'title'      => 'H&S Shampoo - 675 Ml S.S.',
'price'      => '549.110',
'updated_on' => '2014-11-28 15:12:06',
],
],
25 => [
'sku_package' => [
'id'         => 3861,
'title'      => 'H&S Shampoo - 70 Ml AHF.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
26 => [
'sku_package' => [
'id'         => 3862,
'title'      => 'H&S Shampoo - 70 Ml C.B.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
27 => [
'sku_package' => [
'id'         => 3863,
'title'      => 'H&S Shampoo - 70 Ml C.M.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
28 => [
'sku_package' => [
'id'         => 3864,
'title'      => 'H&S Shampoo - 70 Ml D.S.C.A.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
29 => [
'sku_package' => [
'id'         => 3865,
'title'      => 'H&S Shampoo - 70 Ml I.S.C.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
30 => [
'sku_package' => [
'id'         => 3866,
'title'      => 'H&S Shampoo - 70Ml A.H.F.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
31 => [
'sku_package' => [
'id'         => 3867,
'title'      => 'H&S Shampoo - 70Ml C&B',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
32 => [
'sku_package' => [
'id'         => 3868,
'title'      => 'H&S Shampoo - 70Ml D.S.C.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
33 => [
'sku_package' => [
'id'         => 3869,
'title'      => 'H&S Shampoo - 70Ml F.M.H.R',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
34 => [
'sku_package' => [
'id'         => 3870,
'title'      => 'H&S Shampoo - 70Ml I.S.C.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
35 => [
'sku_package' => [
'id'         => 3871,
'title'      => 'H&S Shampoo - 70Ml R.M.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
36 => [
'sku_package' => [
'id'         => 3872,
'title'      => 'H&S Shampoo - 70Ml S&S',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
37 => [
'sku_package' => [
'id'         => 3873,
'title'      => 'H&S Shampoo - 75 Ml A.H.F',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
38 => [
'sku_package' => [
'id'         => 3874,
'title'      => 'H&S Shampoo - 75 Ml C.B',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
39 => [
'sku_package' => [
'id'         => 3875,
'title'      => 'H&S Shampoo - 75 Ml D.S.C.A.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
40 => [
'sku_package' => [
'id'         => 3876,
'title'      => 'H&S Shampoo - 75 Ml F.M.H.R',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
41 => [
'sku_package' => [
'id'         => 3877,
'title'      => 'H&S Shampoo - 75 Ml I.S.C.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
42 => [
'sku_package' => [
'id'         => 3878,
'title'      => 'H&S Shampoo - 75 Ml R.M.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:07',
],
],
43 => [
'sku_package' => [
'id'         => 3879,
'title'      => 'H&S Shampoo - 75 Ml S.S',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
44 => [
'sku_package' => [
'id'         => 3880,
'title'      => 'H&S Shampoo - 80 Ml A.H.F',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:06',
],
],
45 => [
'sku_package' => [
'id'         => 3881,
'title'      => 'H&S Shampoo - 80 Ml C.B',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:07',
],
],
46 => [
'sku_package' => [
'id'         => 3882,
'title'      => 'H&S Shampoo - 80 Ml F.M.H.R',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:08',
],
],
47 => [
'sku_package' => [
'id'         => 3883,
'title'      => 'H&S Shampoo - 80 Ml R. M.',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:08',
],
],
48 => [
'sku_package' => [
'id'         => 3884,
'title'      => 'H&S Shampoo - 80 Ml S.S',
'price'      => '66.960',
'updated_on' => '2014-11-28 15:12:08',
],
],
49 => [
'sku_package' => [
'id'         => 3885,
'title'      => 'H&S Shampoo Extra Strength Men 6 14.2Oz',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:08',
],
],
50 => [
'sku_package' => [
'id'         => 3886,
'title'      => 'H&S Shampoo 165 Ml F.M.H.R',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:08',
],
],
51 => [
'sku_package' => [
'id'         => 3887,
'title'      => 'H&S Shampoo 180Ml C.M',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:08',
],
],
52 => [
'sku_package' => [
'id'         => 3888,
'title'      => 'H&S Shampoo 2 N 1 - 14.2 Oz Clcln',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:08',
],
],
53 => [
'sku_package' => [
'id'         => 3889,
'title'      => 'H&S Shampoo 2 N 1 - 14.2 Oz Refsh',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:08',
],
],
54 => [
'sku_package' => [
'id'         => 3890,
'title'      => 'H&S Shampoo 2 N 1 - 14.2 Oz Smslky',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:07',
],
],
55 => [
'sku_package' => [
'id'    => 3891,
'title' => 'H&S Shampoo 2 In 1 - 200 Ml S.S',
],
],
56 => [
'sku_package' => [
'id'         => 3892,
'title'      => 'H&S Shampoo 5Ml C.M',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
57 => [
'sku_package' => [
'id'         => 3893,
'title'      => 'H&S Shampoo Clinical Strength 14.2Oz',
'price'      => '400.890',
'updated_on' => '2014-11-28 15:12:08',
],
],
58 => [
'sku_package' => [
'id'         => 3894,
'title'      => 'H&S Shampoo - 5 Ml A.H.F',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
59 => [
'sku_package' => [
'id'         => 3895,
'title'      => 'H&S Shampoo - 5 Ml C.B',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
60 => [
'sku_package' => [
'id'         => 3896,
'title'      => 'H&S Shampoo - 5 Ml F.M.H.R',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
61 => [
'sku_package' => [
'id'         => 3897,
'title'      => 'H&S Shampoo - 5 Ml R.M.',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
62 => [
'sku_package' => [
'id'         => 3898,
'title'      => 'H&S Shampoo - 5 Ml S.S',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:12:08',
],
],
],
],
3 => [
'info'      => [
'id'    => 1790,
'title' => 'Herbal',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 3899,
'title'      => 'H&S Shampoo - 165 Ml F.M.H.R',
'price'      => '174.110',
'updated_on' => '2014-11-28 15:12:08',
],
],
],
],
4 => [
'info'      => [
'id'    => 1794,
'title' => 'Pantene',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 3900,
'title'      => 'Pantene Hair Treatment-300Ml-E.M.',
'price'      => '260.870',
'updated_on' => '2014-07-29 17:33:37',
],
],
1  => [
'sku_package' => [
'id'         => 3902,
'title'      => 'Pantene Conditioner- 165Ml H.F.C.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
2  => [
'sku_package' => [
'id'         => 3904,
'title'      => 'Pantene Conditioner- 165Ml N. C.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
3  => [
'sku_package' => [
'id'         => 3905,
'title'      => 'Pantene Conditioner- 165Ml N. S.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
4  => [
'sku_package' => [
'id'         => 3906,
'title'      => 'Pantene Conditioner- 165Ml S. S.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:24',
],
],
5  => [
'sku_package' => [
'id'         => 3907,
'title'      => 'Pantene Conditioner- 165Ml T. C.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:24',
],
],
6  => [
'sku_package' => [
'id'         => 3908,
'title'      => 'Pantene Conditioner- 170Ml H.F.C.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:24',
],
],
7  => [
'sku_package' => [
'id'         => 3909,
'title'      => 'Pantene Conditioner- 170Ml N. S.',
'price'      => '151.790',
'updated_on' => '2014-11-28 15:21:24',
],
],
8  => [
'sku_package' => [
'id'         => 3914,
'title'      => 'Pantene Conditioner- 335Ml T.C.',
'price'      => '276.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
9  => [
'sku_package' => [
'id'         => 3918,
'title'      => 'Pantene Conditioner- 335Ml H.F.C.',
'price'      => '276.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
10 => [
'sku_package' => [
'id'         => 3919,
'title'      => 'Pantene Conditioner- 335Ml N. S.',
'price'      => '276.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
11 => [
'sku_package' => [
'id'         => 3920,
'title'      => 'Pantene Conditioner- 335Ml S. S.',
'price'      => '276.790',
'updated_on' => '2014-11-28 15:21:25',
],
],
12 => [
'sku_package' => [
'id'         => 3925,
'title'      => 'Pantene Shampoo -170Ml A.D.',
'price'      => '125.890',
'updated_on' => '2014-11-28 15:21:25',
],
],
13 => [
'sku_package' => [
'id'         => 3930,
'title'      => 'Pantene Shampoo -170Ml N.C.',
'price'      => '125.890',
'updated_on' => '2014-11-28 15:21:25',
],
],
14 => [
'sku_package' => [
'id'         => 3932,
'title'      => 'Pantene Shampoo -170Ml S.S.',
'price'      => '125.890',
'updated_on' => '2014-11-28 15:21:25',
],
],
15 => [
'sku_package' => [
'id'         => 3933,
'title'      => 'Pantene Shampoo -170Ml T.C.',
'price'      => '125.890',
'updated_on' => '2014-11-28 15:21:25',
],
],
16 => [
'sku_package' => [
'id'         => 3939,
'title'      => 'Pantene Shampoo -340Ml - A.D.',
'price'      => '223.210',
'updated_on' => '2014-11-28 15:21:26',
],
],
17 => [
'sku_package' => [
'id'         => 3940,
'title'      => 'Pantene Shampoo -340Ml - E.M.',
'price'      => '223.210',
'updated_on' => '2014-11-28 15:21:26',
],
],
18 => [
'sku_package' => [
'id'         => 3941,
'title'      => 'Pantene Shampoo -340Ml - H.F.C.',
'price'      => '223.210',
'updated_on' => '2014-11-28 15:21:26',
],
],
19 => [
'sku_package' => [
'id'         => 3942,
'title'      => 'Pantene Shampoo -340Ml - L.B.',
'price'      => '223.310',
'updated_on' => '2014-11-28 15:21:26',
],
],
20 => [
'sku_package' => [
'id'         => 3944,
'title'      => 'Pantene Shampoo 340Ml N.C.',
'price'      => '223.310',
'updated_on' => '2014-11-28 15:21:26',
],
],
21 => [
'sku_package' => [
'id'         => 3945,
'title'      => 'Pantene Shampoo -340Ml - N.S.',
'price'      => '223.310',
'updated_on' => '2014-11-28 15:21:26',
],
],
22 => [
'sku_package' => [
'id'         => 3946,
'title'      => 'Pantene Shampoo -340Ml - S.S.',
'price'      => '223.210',
'updated_on' => '2014-11-28 15:21:25',
],
],
23 => [
'sku_package' => [
'id'         => 3947,
'title'      => 'Pantene Shampoo -340Ml - T.C.',
'price'      => '223.210',
'updated_on' => '2014-11-28 15:21:25',
],
],
24 => [
'sku_package' => [
'id'         => 3948,
'title'      => 'Pantene Shampoo -670Ml A.D.',
'price'      => '437.500',
'updated_on' => '2014-11-28 15:21:25',
],
],
25 => [
'sku_package' => [
'id'         => 3949,
'title'      => 'Pantene Shampoo -670Ml H.F.C.',
'price'      => '437.500',
'updated_on' => '2014-11-28 15:21:25',
],
],
26 => [
'sku_package' => [
'id'         => 3950,
'title'      => 'Pantene Shampoo -670Ml N.S.',
'price'      => '437.500',
'updated_on' => '2014-11-28 15:21:25',
],
],
27 => [
'sku_package' => [
'id'         => 3951,
'title'      => 'Pantene Shampoo -670Ml S.S.',
'price'      => '437.500',
'updated_on' => '2014-11-28 15:21:25',
],
],
28 => [
'sku_package' => [
'id'         => 3952,
'title'      => 'Pantene Shampoo -70Ml A.D.',
'price'      => '55.360',
'updated_on' => '2014-11-28 15:21:25',
],
],
29 => [
'sku_package' => [
'id'         => 3953,
'title'      => 'Pantene Shampoo -70Ml H.F.C.',
'price'      => '55.360',
'updated_on' => '2014-11-28 15:21:25',
],
],
30 => [
'sku_package' => [
'id'         => 3954,
'title'      => 'Pantene Shampoo -70Ml L.B',
'price'      => '55.360',
'updated_on' => '2014-11-28 15:21:25',
],
],
31 => [
'sku_package' => [
'id'         => 3955,
'title'      => 'Pantene Shampoo 70Ml N.S.',
'price'      => '55.360',
'updated_on' => '2014-11-28 15:21:25',
],
],
32 => [
'sku_package' => [
'id'         => 4693,
'title'      => 'Pantene Shampoo - 750Ml (A.D.)',
'price'      => '491.070',
'updated_on' => '2015-12-01 16:17:13',
],
],
33 => [
'sku_package' => [
'id'         => 3977,
'title'      => 'Pantene Shampoo 5Ml H.F.C',
'price'      => '2.230',
'updated_on' => '2014-11-28 15:21:26',
],
],
],
],
5 => [
'info'      => [
'id'    => 1823,
'title' => 'Rejoice',
],
'inventory' => [
0 => [
'sku_package' => [
'id'    => 4022,
'title' => 'Rejoice Shampoo - 5 Ml Rich',
],
],
],
],
6 => [
'info'      => [
'id'    => 1848,
'title' => 'Wella',
],
'inventory' => [
0 => [
'sku_package' => [
'id'    => 4043,
'title' => 'New Wave Gel - 150 Ml',
],
],
],
],
],
],
],
8  => [
'category' => [
'info'      => [
'id'    => 1853,
'title' => 'Health Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1854,
'title' => 'Vicks',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 4046,
'title'      => 'Vicks Inhaler - 0.5 Ml',
'price'      => '51.030',
'updated_on' => '2015-03-17 10:22:02',
],
],
1 => [
'sku_package' => [
'id'         => 4048,
'title'      => 'Vicks Vaporub - 10 Gm',
'price'      => '37.240',
'updated_on' => '2015-03-17 10:22:02',
],
],
2 => [
'sku_package' => [
'id'         => 4049,
'title'      => 'Vicks Vaporub - 25 Gm',
'price'      => '73.100',
'updated_on' => '2015-03-17 10:22:02',
],
],
3 => [
'sku_package' => [
'id'         => 4050,
'title'      => 'Vicks Vaporub - 5 Gm',
'price'      => '20.690',
'updated_on' => '2015-03-13 13:40:10',
],
],
4 => [
'sku_package' => [
'id'         => 4051,
'title'      => 'Vicks Vaporub - 50 Gm',
'price'      => '131.030',
'updated_on' => '2015-03-17 10:22:02',
],
],
],
],
],
],
],
9  => [
'category' => [
'info'      => [
'id'    => 1865,
'title' => 'Oral Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1866,
'title' => 'Oral B',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 4054,
'title'      => 'Oral B All Rounder 123 Medium',
'price'      => '36.040',
'updated_on' => '2015-10-06 12:17:27',
],
],
1 => [
'sku_package' => [
'id'         => 4055,
'title'      => 'Oral B All Rounder 123 Soft',
'price'      => '36.040',
'updated_on' => '2015-10-06 12:17:27',
],
],
2 => [
'sku_package' => [
'id'         => 4057,
'title'      => 'Oral B Cross Action - Medium',
'price'      => '99.460',
'updated_on' => '2015-10-06 12:17:27',
],
],
3 => [
'sku_package' => [
'id'         => 4058,
'title'      => 'Oral B Cross Action - Soft',
'price'      => '99.460',
'updated_on' => '2015-10-06 12:17:27',
],
],
4 => [
'sku_package' => [
'id'         => 4060,
'title'      => 'Oral B All Rounder Dc. White Soft',
'price'      => '36.040',
'updated_on' => '2015-03-10 12:03:25',
],
],
5 => [
'sku_package' => [
'id'         => 4061,
'title'      => 'Oral B All Rounder Gum Pt. 40 Soft',
'price'      => '43.240',
'updated_on' => '2014-09-09 17:21:01',
],
],
6 => [
'sku_package' => [
'id'         => 4062,
'title'      => 'Oral B Shiny Clean Medium',
'price'      => '23.060',
'updated_on' => '2015-10-06 12:17:27',
],
],
7 => [
'sku_package' => [
'id'         => 4064,
'title'      => 'Oral B Shiny Clean Soft',
'price'      => '23.060',
'updated_on' => '2015-10-06 12:17:27',
],
],
],
],
],
],
],
10 => [
'category' => [
'info'      => [
'id'    => 1889,
'title' => 'Personal Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1890,
'title' => 'Gillette ',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 4068,
'title'      => 'Sr. Deo Spray C.W - 150 Ml',
'price'      => '250.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
1  => [
'sku_package' => [
'id'         => 4069,
'title'      => 'Sr. Deo Spray I.C - 150 Ml',
'price'      => '250.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
2  => [
'sku_package' => [
'id'         => 4070,
'title'      => 'Sr. Deo Spray P.R - 150 Ml',
'price'      => '250.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
3  => [
'sku_package' => [
'id'         => 4076,
'title'      => 'Gillette Foamy Lmnl. - 196G',
'price'      => '240.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
4  => [
'sku_package' => [
'id'         => 4077,
'title'      => 'Gillette Foamy Mnth. - 196G',
'price'      => '240.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
5  => [
'sku_package' => [
'id'         => 4079,
'title'      => 'Gillette Foamy Reg. - 196 Gm',
'price'      => '240.000',
'updated_on' => '2015-10-06 12:17:27',
],
],
6  => [
'sku_package' => [
'id'         => 4080,
'title'      => 'Gillette Foamy Reg. - 418 Gm',
'price'      => '318.550',
'updated_on' => '2015-10-06 12:17:27',
],
],
7  => [
'sku_package' => [
'id'         => 4081,
'title'      => 'Gillette Foamy Senst. - 418 Gm',
'price'      => '318.550',
'updated_on' => '2015-10-06 12:17:27',
],
],
8  => [
'sku_package' => [
'id'         => 4083,
'title'      => 'Sr. A/Shave Splash C.W - 100 Ml',
'price'      => '340.190',
'updated_on' => '2015-10-06 12:17:27',
],
],
9  => [
'sku_package' => [
'id'         => 4084,
'title'      => 'Sr. A/Shave Splash I.C - 100 Ml',
'price'      => '340.190',
'updated_on' => '2015-10-06 12:17:27',
],
],
10 => [
'sku_package' => [
'id'         => 4085,
'title'      => 'Sr. A/Shave Splash P.R - 100 Ml',
'price'      => '340.190',
'updated_on' => '2015-10-06 12:17:27',
],
],
11 => [
'sku_package' => [
'id'         => 4086,
'title'      => 'Sr. A/Shave Splash S.F - 100 Ml',
'price'      => '340.190',
'updated_on' => '2015-10-06 12:17:27',
],
],
12 => [
'sku_package' => [
'id'         => 4087,
'title'      => 'Sr. Shaving Foam Prot - 250 Ml',
'price'      => '235.450',
'updated_on' => '2015-10-06 12:17:27',
],
],
13 => [
'sku_package' => [
'id'         => 4088,
'title'      => 'Sr. Shaving Foam S.Ski. - 250 Ml',
'price'      => '235.450',
'updated_on' => '2015-10-06 12:17:27',
],
],
14 => [
'sku_package' => [
'id'         => 4090,
'title'      => 'Sr. Shaving Gel C. C. - 200 Ml',
'price'      => '299.090',
'updated_on' => '2015-10-06 12:17:27',
],
],
15 => [
'sku_package' => [
'id'         => 4091,
'title'      => 'Sr. Shaving Gel Mois. - 200 Ml',
'price'      => '299.090',
'updated_on' => '2015-10-06 12:17:27',
],
],
16 => [
'sku_package' => [
'id'         => 4092,
'title'      => 'Sr. Tube Shave Gel - 25 Gm',
'price'      => '53.820',
'updated_on' => '2015-10-06 12:17:27',
],
],
],
],
1 => [
'info'      => [
'id'    => 1917,
'title' => 'Old Spice',
],
'inventory' => [
0  => [
'sku_package' => [
'id'         => 4099,
'title'      => 'Old Spice Asl Atomizer Lime 150Ml',
'price'      => '276.870',
'updated_on' => '2014-11-28 15:34:11',
],
],
1  => [
'sku_package' => [
'id'         => 4100,
'title'      => 'Old Spice Asl Atomizer Musk 150Ml',
'price'      => '276.870',
'updated_on' => '2014-11-28 15:34:11',
],
],
2  => [
'sku_package' => [
'id'         => 4101,
'title'      => 'Old Spice Asl Atomizer Original 150Ml',
'price'      => '276.870',
'updated_on' => '2014-11-28 15:34:11',
],
],
3  => [
'sku_package' => [
'id'         => 4102,
'title'      => 'Old Spice Asl Lime 100Ml',
'price'      => '222.610',
'updated_on' => '2014-11-28 15:34:11',
],
],
4  => [
'sku_package' => [
'id'         => 4103,
'title'      => 'Old Spice Asl Lime 150Ml',
'price'      => '222.610',
'updated_on' => '2014-11-28 15:34:11',
],
],
5  => [
'sku_package' => [
'id'         => 4104,
'title'      => 'Old Spice Asl Lime 50Ml',
'price'      => '132.170',
'updated_on' => '2014-11-28 15:34:11',
],
],
6  => [
'sku_package' => [
'id'         => 4105,
'title'      => 'Old Spice Asl Musk 100Ml',
'price'      => '222.610',
'updated_on' => '2014-11-28 15:34:11',
],
],
7  => [
'sku_package' => [
'id'         => 4106,
'title'      => 'Old Spice Asl Musk 50Ml',
'price'      => '132.170',
'updated_on' => '2014-11-28 15:34:11',
],
],
8  => [
'sku_package' => [
'id'         => 4107,
'title'      => 'Old Spice Asl Splash Musk 150Ml',
'price'      => '243.480',
'updated_on' => '2014-11-28 15:34:10',
],
],
9  => [
'sku_package' => [
'id'         => 4108,
'title'      => 'Old Spice Asl Original 100Ml',
'price'      => '222.610',
'updated_on' => '2014-11-28 15:34:10',
],
],
10 => [
'sku_package' => [
'id'         => 4109,
'title'      => 'Old Spice Asl Original 50Ml',
'price'      => '132.170',
'updated_on' => '2014-11-28 15:34:10',
],
],
11 => [
'sku_package' => [
'id'         => 4110,
'title'      => 'Old Spice Asl Splash Lime 150Ml',
'price'      => '243.480',
'updated_on' => '2014-11-28 15:34:10',
],
],
12 => [
'sku_package' => [
'id'         => 4111,
'title'      => 'Old Spice Asl Splash Original 150Ml',
'price'      => '243.480',
'updated_on' => '2014-11-28 15:34:10',
],
],
13 => [
'sku_package' => [
'id'         => 4113,
'title'      => 'Old Spice Deo Spray Lime 150Ml',
'price'      => '208.700',
'updated_on' => '2014-11-28 15:34:10',
],
],
14 => [
'sku_package' => [
'id'         => 4114,
'title'      => 'Old Spice Deo Spray Ori 150Ml',
'price'      => '208.700',
'updated_on' => '2014-11-28 15:34:10',
],
],
15 => [
'sku_package' => [
'id'         => 4115,
'title'      => 'Old Spice Deo Spray White Water 150Ml',
'price'      => '208.700',
'updated_on' => '2014-11-28 15:34:10',
],
],
16 => [
'sku_package' => [
'id'         => 4116,
'title'      => 'Old Spice Deo Spray Musk 150Ml',
'price'      => '208.700',
'updated_on' => '2014-11-28 15:34:10',
],
],
17 => [
'sku_package' => [
'id'         => 4117,
'title'      => 'Old Spice Shave Cream Lime 30G',
'price'      => '41.740',
'updated_on' => '2014-11-28 15:34:10',
],
],
18 => [
'sku_package' => [
'id'         => 4118,
'title'      => 'Old Spice Shave Cream Lime 70G',
'price'      => '79.300',
'updated_on' => '2014-11-28 15:34:10',
],
],
19 => [
'sku_package' => [
'id'         => 4119,
'title'      => 'Old Spice Shave Cream Musk 30G',
'price'      => '41.740',
'updated_on' => '2014-11-28 15:34:11',
],
],
20 => [
'sku_package' => [
'id'         => 4120,
'title'      => 'Old Spice Shave Cream Musk 70G',
'price'      => '79.300',
'updated_on' => '2014-11-28 15:34:11',
],
],
21 => [
'sku_package' => [
'id'         => 4121,
'title'      => 'Old Spice Shave Cream Original 30G',
'price'      => '41.740',
'updated_on' => '2014-11-28 15:34:11',
],
],
22 => [
'sku_package' => [
'id'         => 4122,
'title'      => 'Old Spice Shave Cream Original 70G',
'price'      => '79.300',
'updated_on' => '2014-11-28 15:34:11',
],
],
],
],
],
],
],
11 => [
'category' => [
'info'      => [
'id'    => 1942,
'title' => 'Personal Cleansing',
],
'brandList' => [
0 => [
'info' => [
'id'    => 1943,
'title' => 'Camay',
],
],
1 => [
'info' => [
'id'    => 1948,
'title' => 'Olay',
],
],
2 => [
'info' => [
'id'    => 1957,
'title' => 'Safeguard',
],
],
],
],
],
12 => [
'category' => [
'info'      => [
'id'    => 1961,
'title' => 'Skin Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 1962,
'title' => 'Olay',
],
'inventory' => [
0   => [
'sku_package' => [
'id'         => 4142,
'title'      => 'Olay DUVP Normal Cream - 56 ML',
'price'      => '325.220',
'updated_on' => '2014-12-04 12:04:57',
],
],
1   => [
'sku_package' => [
'id'         => 4217,
'title'      => 'Olay Duvp FF Cream - 56 Ml',
'price'      => '325.220',
'updated_on' => '2014-12-04 12:04:57',
],
],
2   => [
'sku_package' => [
'id'         => 4218,
'title'      => 'Olay Duvp Lotion - 118 Ml',
'price'      => '381.740',
'updated_on' => '2014-12-04 12:04:57',
],
],
3   => [
'sku_package' => [
'id'         => 4219,
'title'      => 'Olay Duvp Lotion - 177 Ml',
'price'      => '573.040',
'updated_on' => '2014-12-04 12:04:57',
],
],
4   => [
'sku_package' => [
'id'         => 4220,
'title'      => 'Olay Duvp Lotion 4 Oz',
'price'      => '381.740',
'updated_on' => '2014-12-04 12:04:57',
],
],
5   => [
'sku_package' => [
'id'         => 4221,
'title'      => 'Olay Cad UVFF Cream 2Oz',
'price'      => '433.910',
'updated_on' => '2014-11-28 15:31:13',
],
],
6   => [
'sku_package' => [
'id'    => 4222,
'title' => 'Olay Regenerist Cream Cleanser 5Oz',
],
],
7   => [
'sku_package' => [
'id'         => 4223,
'title'      => 'Olay Te WetCloth Scntd 30Ct',
'price'      => '447.830',
'updated_on' => '2014-11-28 15:31:12',
],
],
8   => [
'sku_package' => [
'id'         => 4224,
'title'      => 'Olay Wet Towelettes Normal 30Ct/12',
'price'      => '447.830',
'updated_on' => '2014-11-28 15:31:12',
],
],
9   => [
'sku_package' => [
'id'         => 4225,
'title'      => 'Olay Wet Towelettes Sens 30Ct/12',
'price'      => '447.830',
'updated_on' => '2014-11-28 15:31:12',
],
],
10  => [
'sku_package' => [
'id'         => 4226,
'title'      => 'Olay Ads Classic Eye Gel 12/0.5Oz',
'price'      => '346.960',
'updated_on' => '2014-11-28 15:31:12',
],
],
11  => [
'sku_package' => [
'id'         => 4227,
'title'      => 'Olay Eye Gel - 15 Ml',
'price'      => '325.220',
'updated_on' => '2014-12-04 12:04:57',
],
],
12  => [
'sku_package' => [
'id'         => 4228,
'title'      => 'Olay Te Mature Skin Therapy Cream 12/1.7Oz',
'price'      => '894.780',
'updated_on' => '2014-11-28 15:31:12',
],
],
13  => [
'sku_package' => [
'id'    => 4229,
'title' => 'Olay Cream Noo 12/2Oz',
],
],
14  => [
'sku_package' => [
'id'         => 4230,
'title'      => 'Olay Night Of Olay Cream - 56 Ml',
'price'      => '433.910',
'updated_on' => '2014-12-04 12:04:57',
],
],
15  => [
'sku_package' => [
'id'    => 4231,
'title' => 'Olay Cream Reg 12/2Oz',
],
],
16  => [
'sku_package' => [
'id'         => 4232,
'title'      => 'Olay Regular Cream - 56 Ml',
'price'      => '433.910',
'updated_on' => '2014-12-04 12:04:57',
],
],
17  => [
'sku_package' => [
'id'         => 4233,
'title'      => 'Olay Regular Lotion - 118 Ml',
'price'      => '381.740',
'updated_on' => '2014-12-04 12:04:57',
],
],
18  => [
'sku_package' => [
'id'         => 4234,
'title'      => 'Olay Regular Lotion 118 Ml',
'price'      => '381.740',
'updated_on' => '2014-12-04 12:04:57',
],
],
19  => [
'sku_package' => [
'id'         => 4235,
'title'      => 'Olay Regular Lotion - 177 Ml',
'price'      => '573.040',
'updated_on' => '2014-12-04 12:04:57',
],
],
20  => [
'sku_package' => [
'id'         => 4236,
'title'      => 'Olay Regular Lotion - 4 Oz',
'price'      => '381.740',
'updated_on' => '2014-12-04 12:04:57',
],
],
21  => [
'sku_package' => [
'id'    => 4237,
'title' => 'Olay UV Lotion 177 Ml',
],
],
22  => [
'sku_package' => [
'id'    => 4238,
'title' => 'Olay Renewal Cleanser - 200 Ml',
],
],
23  => [
'sku_package' => [
'id'    => 4239,
'title' => 'Olay Age Defy Classic SPF 15 12/4Oz',
],
],
24  => [
'sku_package' => [
'id'    => 4241,
'title' => 'Olay Renewal Cream - 56 Ml',
],
],
25  => [
'sku_package' => [
'id'    => 4240,
'title' => 'Olay Ads Classic Ngt Crm 12/2 Oz',
],
],
26  => [
'sku_package' => [
'id'         => 4242,
'title'      => 'Olay TE Cream - 50 Ml',
'price'      => '868.700',
'updated_on' => '2014-09-09 14:03:03',
],
],
27  => [
'sku_package' => [
'id'         => 4243,
'title'      => 'Olay TE Eye Cream 12/0.5Oz',
'price'      => '807.830',
'updated_on' => '2014-11-28 15:31:12',
],
],
28  => [
'sku_package' => [
'id'         => 4244,
'title'      => 'Olay Cream Te FF 121.7Z',
'price'      => '894.780',
'updated_on' => '2014-11-28 15:31:12',
],
],
29  => [
'sku_package' => [
'id'         => 4245,
'title'      => 'Olay Teaaff Moisturiser 12/1.7Oz',
'price'      => '894.780',
'updated_on' => '2014-11-28 15:31:12',
],
],
30  => [
'sku_package' => [
'id'         => 4246,
'title'      => 'Olay TE Night Cream - 50 Ml',
'price'      => '868.700',
'updated_on' => '2014-09-09 14:03:03',
],
],
31  => [
'sku_package' => [
'id'         => 4247,
'title'      => 'Olay Cad UV Cream 12/2Z',
'price'      => '433.910',
'updated_on' => '2014-11-28 15:31:12',
],
],
32  => [
'sku_package' => [
'id'         => 4143,
'title'      => 'Olay Nw Cream 20 Gm',
'price'      => '68.700',
'updated_on' => '2014-11-28 15:31:12',
],
],
33  => [
'sku_package' => [
'id'         => 4144,
'title'      => 'Olay Nw Cream 40 Gm',
'price'      => '123.480',
'updated_on' => '2014-11-28 15:31:12',
],
],
34  => [
'sku_package' => [
'id'         => 4145,
'title'      => 'Olay N. W. Day Cream - 50',
'price'      => '269.570',
'updated_on' => '2014-11-28 15:31:12',
],
],
35  => [
'sku_package' => [
'id'         => 4146,
'title'      => 'Olay N. W. Light Day Cream - 20 Gm',
'price'      => '68.700',
'updated_on' => '2014-11-28 15:31:12',
],
],
36  => [
'sku_package' => [
'id'         => 4147,
'title'      => 'Olay N. W. Light Day Cream - 40 Gm',
'price'      => '123.480',
'updated_on' => '2014-11-28 15:31:12',
],
],
37  => [
'sku_package' => [
'id'         => 4148,
'title'      => 'Olay N. W. Day Lotion - 30 Ml',
'price'      => '120.870',
'updated_on' => '2014-08-01 13:53:17',
],
],
38  => [
'sku_package' => [
'id'         => 4149,
'title'      => 'Olay N. W. Foaming Cleanser - 50 Gm',
'price'      => '120.870',
'updated_on' => '2014-11-28 15:31:12',
],
],
39  => [
'sku_package' => [
'id'         => 4150,
'title'      => 'Olay N. W. Night Cream - 50 Gm',
'price'      => '269.570',
'updated_on' => '2014-11-28 15:31:12',
],
],
40  => [
'sku_package' => [
'id'         => 4151,
'title'      => 'Olay Cleanser Clarity 100G',
'price'      => '204.350',
'updated_on' => '2014-11-28 15:31:12',
],
],
41  => [
'sku_package' => [
'id'    => 4152,
'title' => 'Olay Cleanser Clarity 18G',
],
],
42  => [
'sku_package' => [
'id'         => 4154,
'title'      => 'Olay Cleanser Clarity 50G',
'price'      => '113.040',
'updated_on' => '2014-11-28 15:31:12',
],
],
43  => [
'sku_package' => [
'id'         => 4155,
'title'      => 'Olay Cleanser Gentle 100G',
'price'      => '204.350',
'updated_on' => '2014-11-28 15:31:12',
],
],
44  => [
'sku_package' => [
'id'    => 4156,
'title' => 'Olay Cleanser Gentle 18G',
],
],
45  => [
'sku_package' => [
'id'         => 4157,
'title'      => 'Olay Cleanser Gentle 50G',
'price'      => '113.040',
'updated_on' => '2014-11-28 15:31:12',
],
],
46  => [
'sku_package' => [
'id'         => 4158,
'title'      => 'Olay Cleanser Moisture Balance 100G',
'price'      => '204.350',
'updated_on' => '2014-11-28 15:31:12',
],
],
47  => [
'sku_package' => [
'id'         => 4159,
'title'      => 'Olay Cleanser Moisture Balance 50G',
'price'      => '113.040',
'updated_on' => '2014-11-28 15:31:12',
],
],
48  => [
'sku_package' => [
'id'    => 4160,
'title' => 'Olay Cream- 100 Gm',
],
],
49  => [
'sku_package' => [
'id'    => 4161,
'title' => 'Olay Cream -50 Gm',
],
],
50  => [
'sku_package' => [
'id'    => 4162,
'title' => 'Olay Cream TotlFx FragFree 1.7Oz',
],
],
51  => [
'sku_package' => [
'id'         => 4163,
'title'      => 'Olay Oil Fr Lotion - 118 Ml',
'price'      => '381.740',
'updated_on' => '2014-08-01 11:06:14',
],
],
52  => [
'sku_package' => [
'id'         => 4164,
'title'      => 'Olay Pink Cream-50Gm Thai',
'price'      => '278.260',
'updated_on' => '2014-08-01 11:06:15',
],
],
53  => [
'sku_package' => [
'id'         => 4165,
'title'      => 'Olay Pink Lotion-150Ml Thai',
'price'      => '539.130',
'updated_on' => '2014-08-01 11:06:15',
],
],
54  => [
'sku_package' => [
'id'         => 4166,
'title'      => 'Olay Pink Lotion-75Ml Thai',
'price'      => '295.650',
'updated_on' => '2014-08-01 11:06:15',
],
],
55  => [
'sku_package' => [
'id'         => 4167,
'title'      => 'Olay Regen Dp Hydra Cream',
'price'      => '1346.960',
'updated_on' => '2014-11-28 15:31:12',
],
],
56  => [
'sku_package' => [
'id'         => 4168,
'title'      => 'Olay Regenerist Cleanser 100Gm',
'price'      => '460.000',
'updated_on' => '2014-11-28 15:31:12',
],
],
57  => [
'sku_package' => [
'id'         => 4169,
'title'      => 'Olay Regen Cream 12/1.7 Oz',
'price'      => '860.000',
'updated_on' => '2014-08-01 11:06:15',
],
],
58  => [
'sku_package' => [
'id'         => 4170,
'title'      => 'Olay Regenerist Day Cream 14G',
'price'      => '373.040',
'updated_on' => '2014-11-28 15:31:12',
],
],
59  => [
'sku_package' => [
'id'         => 4171,
'title'      => 'Olay Regenerist Day Cream',
'price'      => '1303.480',
'updated_on' => '2014-08-01 11:06:15',
],
],
60  => [
'sku_package' => [
'id'         => 4172,
'title'      => 'Olay Regenerist Day Cream 50G',
'price'      => '1303.480',
'updated_on' => '2014-11-28 15:31:12',
],
],
61  => [
'sku_package' => [
'id'         => 4173,
'title'      => 'Olay Regenerist Eye Roller 12/0.2 Fl Oz',
'price'      => '1294.780',
'updated_on' => '2014-11-28 15:31:12',
],
],
62  => [
'sku_package' => [
'id'    => 4174,
'title' => 'Olay Regenerist Eye Serum 12/0.5 Oz',
],
],
63  => [
'sku_package' => [
'id'         => 4175,
'title'      => 'Olay Regenerist Eye Serum 15Ml',
'price'      => '1294.780',
'updated_on' => '2014-11-28 15:31:12',
],
],
64  => [
'sku_package' => [
'id'    => 4176,
'title' => 'Olay Reg Msc Bngk Blk 12/1.7 Oz',
],
],
65  => [
'sku_package' => [
'id'         => 4177,
'title'      => 'Olay Regenerist Msc Cream 50G',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
66  => [
'sku_package' => [
'id'         => 4178,
'title'      => 'Olay Regenerist Msc Wrinkle Relaxing Cream',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
67  => [
'sku_package' => [
'id'         => 4179,
'title'      => 'Olay Regenerist Msc Cream FF 48G',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
68  => [
'sku_package' => [
'id'         => 4180,
'title'      => 'Olay Regenerist Msc Night Essence 50Ml',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
69  => [
'sku_package' => [
'id'         => 4181,
'title'      => 'Olay Regenerist Msc Serum 50Ml',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
70  => [
'sku_package' => [
'id'         => 4182,
'title'      => 'Olay Regenerist Msc SPF 30 50Ml',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
71  => [
'sku_package' => [
'id'         => 4183,
'title'      => 'Olay Regenerist Msc Wr Cream 50Ml',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:13',
],
],
72  => [
'sku_package' => [
'id'         => 4184,
'title'      => 'Olay Regen Night 12/1.7Oz',
'price'      => '1303.480',
'updated_on' => '2014-11-28 15:31:13',
],
],
73  => [
'sku_package' => [
'id'    => 4185,
'title' => 'Olay Regenerist Night Cream',
],
],
74  => [
'sku_package' => [
'id'         => 4186,
'title'      => 'Olay Regenerist Night Recovery Cream',
'price'      => '1790.430',
'updated_on' => '2014-11-28 15:31:13',
],
],
75  => [
'sku_package' => [
'id'         => 4187,
'title'      => 'Olay Regen Serum 1.7Oz',
'price'      => '1346.960',
'updated_on' => '2014-11-28 15:31:13',
],
],
76  => [
'sku_package' => [
'id'         => 4188,
'title'      => 'Olay Regen Serum FragFree 12/1.7Oz',
'price'      => '1346.960',
'updated_on' => '2014-11-28 15:31:13',
],
],
77  => [
'sku_package' => [
'id'         => 4189,
'title'      => 'Olay Regenerist Serum 50Ml',
'price'      => '1346.960',
'updated_on' => '2014-11-28 15:31:13',
],
],
78  => [
'sku_package' => [
'id'         => 4190,
'title'      => 'Olay Regenerist Serum 50Ml SPF 30',
'price'      => '1346.960',
'updated_on' => '2014-11-28 15:31:13',
],
],
79  => [
'sku_package' => [
'id'         => 4191,
'title'      => 'Olay TE Cleaner-100Gm Thai',
'price'      => '317.390',
'updated_on' => '2014-11-28 15:31:13',
],
],
80  => [
'sku_package' => [
'id'         => 4192,
'title'      => 'Olay TE Cleaner-50Gm Thai',
'price'      => '169.570',
'updated_on' => '2014-11-28 15:31:13',
],
],
81  => [
'sku_package' => [
'id'         => 4193,
'title'      => 'Olay TE Cleanser-50Gm Thai',
'price'      => '169.570',
'updated_on' => '2014-11-28 15:31:13',
],
],
82  => [
'sku_package' => [
'id'         => 4194,
'title'      => 'Olay TE Gentle Cream-50Gm Thai',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:11',
],
],
83  => [
'sku_package' => [
'id'         => 4195,
'title'      => 'Olay TE Gentle UV Cream-50Gm Thai',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:11',
],
],
84  => [
'sku_package' => [
'id'         => 4196,
'title'      => 'Olay TE Cream Normal-20Gm Thai',
'price'      => '390.430',
'updated_on' => '2014-11-28 15:31:11',
],
],
85  => [
'sku_package' => [
'id'         => 4197,
'title'      => 'Olay TE Normal Cream-50Gm Thai',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:11',
],
],
86  => [
'sku_package' => [
'id'    => 4198,
'title' => 'Olay TE Oil Cream 50G Thai',
],
],
87  => [
'sku_package' => [
'id'    => 4199,
'title' => 'Olay TE Oil Cream 100Gm',
],
],
88  => [
'sku_package' => [
'id'         => 4200,
'title'      => 'Olay Cream TE FF 50Gm',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:11',
],
],
89  => [
'sku_package' => [
'id'         => 4201,
'title'      => 'Olay TE Normal UV Cream-20Gm Thai',
'price'      => '390.430',
'updated_on' => '2014-11-28 15:31:11',
],
],
90  => [
'sku_package' => [
'id'         => 4202,
'title'      => 'Olay Face Cream TE UVff 50Gm',
'price'      => '868.700',
'updated_on' => '2014-08-01 11:06:15',
],
],
91  => [
'sku_package' => [
'id'         => 4203,
'title'      => 'Olay TE Normal UV Cream-50Gm Thai',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:11',
],
],
92  => [
'sku_package' => [
'id'         => 4204,
'title'      => 'Olay TE Touch Of Found. Cream-50Gm Thai',
'price'      => '868.700',
'updated_on' => '2014-08-01 11:06:15',
],
],
93  => [
'sku_package' => [
'id'         => 4205,
'title'      => 'Olay White Lotion - 75 Ml Thai',
'price'      => '621.740',
'updated_on' => '2014-08-01 11:06:15',
],
],
94  => [
'sku_package' => [
'id'         => 4206,
'title'      => 'Olay White Night Cream-50Gm Thai',
'price'      => '621.740',
'updated_on' => '2014-08-01 11:06:15',
],
],
95  => [
'sku_package' => [
'id'         => 4207,
'title'      => 'Olay White Radiance Cleanser 100G',
'price'      => '460.000',
'updated_on' => '2014-11-28 15:31:11',
],
],
96  => [
'sku_package' => [
'id'         => 4208,
'title'      => 'Olay White Radiance Cream 50G',
'price'      => '1303.480',
'updated_on' => '2014-11-28 15:31:11',
],
],
97  => [
'sku_package' => [
'id'         => 4209,
'title'      => 'Olay White Radiance 50G',
'price'      => '1303.480',
'updated_on' => '2014-11-28 15:31:12',
],
],
98  => [
'sku_package' => [
'id'         => 4210,
'title'      => 'Olay White Radiance Day Cream 15G',
'price'      => '373.040',
'updated_on' => '2014-11-28 15:31:12',
],
],
99  => [
'sku_package' => [
'id'         => 4212,
'title'      => 'Olay White Radiance Eye Serum 15Ml',
'price'      => '868.700',
'updated_on' => '2014-11-28 15:31:12',
],
],
100 => [
'sku_package' => [
'id'         => 4213,
'title'      => 'Olay White Radiance Lotion 75Ml',
'price'      => '1999.130',
'updated_on' => '2014-11-28 15:31:12',
],
],
101 => [
'sku_package' => [
'id'         => 4214,
'title'      => 'Olay White Radiance Lotion SPF 30',
'price'      => '1738.260',
'updated_on' => '2014-11-28 15:31:12',
],
],
102 => [
'sku_package' => [
'id'         => 4215,
'title'      => 'Olay White Radiance Night 50G',
'price'      => '1303.480',
'updated_on' => '2014-11-28 15:31:12',
],
],
103 => [
'sku_package' => [
'id'    => 4216,
'title' => 'Olay White Radiance Serum 50Ml',
],
],
],
],
],
],
],
13 => [
'category' => [
'info'      => [
'id'    => 2484,
'title' => ' Hair Care',
],
'brandList' => [
0 => [
'info'      => [
'id'    => 2485,
'title' => ' Head & Shoulders',
],
'inventory' => [
0 => [
'sku_package' => [
'id'         => 4690,
'title'      => ' H&S Shampoo - 720 Ml (A.H.F)',
'price'      => '647.320',
'updated_on' => '2015-12-01 15:50:36',
],
],
],
],
],
],
],
],
],
]);
});


Route::post('retail/orderHistory', function () {
return json_encode([
'status' => 'success',
'data'   => [
0 => [
'date'   => '2014-04-01',
'total'  => '1',
'orders' => [
0 => [
'order_id'       => 19,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
],
],
1 => [
'date'   => '2014-04-03',
'total'  => '2',
'orders' => [
0 => [
'order_id'       => 28,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
1 => [
'order_id'       => 31,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
],
],
2 => [
'date'   => '2014-04-04',
'total'  => '4',
'orders' => [
0 => [
'order_id'       => 34,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
1 => [
'order_id'       => 37,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
2 => [
'order_id'       => 40,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 959,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
3 => [
'order_id'       => 43,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 10,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
],
],
3 => [
'date'   => '2014-04-08',
'total'  => '1',
'orders' => [
0 => [
'order_id'       => 46,
'inventory_id'   => 1,
'inventory'      => 'AMBIP AER B.O 300ML - 6 PCS',
'shoproute'      => 'GUDRI',
'quantity'       => 10,
'packaging_unit' => 'cases',
'sales_status'   => 'Received',
],
],
],
],
]);
});


Route::post('retail/noorders', function () {
return json_encode([
'status'      => 'success',
'identifiers' => [
0 => [
'id'         => 12,
'identifier' => 7221,
],
1 => [
'id'         => 13,
'identifier' => 7222,
],
2 => [
'id'         => 3,
'identifier' => 7223,
],
],
'message'     => 'Orders taken.',
]);
});


Route::post('retail/category/list', function () {
return json_encode([
'status' => 'success',
'data'   => [
'business_units' => [
0 => [
'id'       => 1,
'name'     => 'P&G',
'channels' => [
0 => [
'id'         => 1,
'name'       => 'MAS',
'categories' => [
0 => [
'id'   => 7,
'name' => 'Novelty',
],
1 => [
'id'   => 28,
'name' => 'categoty one',
],
],
],
1 => [
'id'         => 2,
'name'       => 'OT',
'categories' => [
0 => [
'id'   => 29,
'name' => 'tesin',
],
1 => [
'id'   => 30,
'name' => 'one',
],
2 => [
'id'   => 31,
'name' => 'two',
],
],
],
2 => [
'id'   => 3,
'name' => 'W.S',
],
3 => [
'id'         => 10,
'name'       => 'Pharma',
'categories' => [
0 => [
'id'   => 8,
'name' => 'Conf',
],
],
],
4 => [
'id'   => 15,
'name' => 'this is channels',
],
5 => [
'id'   => 18,
'name' => 'channel One',
],
6 => [
'id'   => 19,
'name' => 'Channel Two',
],
],
],
],
]
]);
});


Route::post('targets', function () {
return json_encode([
'status' => 'success',
'data'   => [
'targets' => [
0 => [
'id'                    => 6,
'year'                  => 2014,
'month'                 => 8,
'target'                => '1000000.00',
'achievement'           => '69413.70',
'invoiced_value'        => '69413.70',
'todays_invoiced_value' => '6943.70',
],
],
],
]);
});

Route::post('app/version', function () {
json_encode([
'status' => 'success',
'data'   => [
'file'          => 'Rosia_4_v1_1_0_2.apk',
'modified_date' => '2014-10-07 16:26',
],
]);
});